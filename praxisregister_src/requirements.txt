# requirements last updated 16.11.23

# Django related
Django==4.2.7
django-bootstrap-v5==1.0.11
django-crispy-forms==2.1
django-filter==23.3
django-property-filter==1.1.2
django-guardian==2.4.0 # object level permissions
django-import-export==3.3.3
django-login-required-middleware==0.9.0
django-tables2==2.6.0
django-polymorphic==3.1.0
django-taggit==5.0.1 # tags
taggit-selectize==2.11.0 # tag autocomplete
djangorestframework==3.14.0
djangorestframework-datatables==0.7.0
django-ckeditor-5==0.2.11 # embedded text editor
django-dirtyfields==1.9.2 # track changes to models
django-debug-toolbar==4.2.0;
gunicorn==21.2.0 # web server

beautifulsoup4==4.12.2 # html formating
soupsieve==2.5 # beautifulsoup4 support
crispy-bootstrap5==2023.10 # crispy forms
coverage==7.3.2 # test coverage
Faker==20.0.3 # Fake data generator
fontawesomefree==6.4.2 # icons
python-dateutil==2.8.2 # date parsing
tablib==3.5.0
pytz==2023.3.post1 # timezone support
icalendar==5.0.11
levenshtein==0.26.1
setuptools>=68.2.2
