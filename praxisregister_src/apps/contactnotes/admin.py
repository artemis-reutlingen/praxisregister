from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from guardian.admin import GuardedModelAdmin

from contactnotes.models import ContactNote

class ContactNoteAdmin(GuardedModelAdmin):
    model = ContactNote
    list_display = ('id', 'author', 'contact_person', 'practice', 'tags') 
    list_per_page = 20
    list_select_related = ['author', 'practice']
    list_filter = ['created_at', 'practice__assigned_institute']
    ordering = ('-created_at',)
    search_fields = ['practice__name__icontains', 'author__first_name__startswith', 'author__last_name__startswith']

admin.site.register(ContactNote, ContactNoteAdmin)
