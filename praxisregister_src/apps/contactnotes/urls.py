from django.urls import path
from contactnotes import views

app_name = 'contactnotes'

urlpatterns = [
    path('<int:pk>/attachments/<int:attachment_id>/download', views.download_contact_note_attachment_file,
         name='attachment-download'),
    path('<int:pk>/edit', views.ContactNoteEdit.as_view(),
         name='edit'),
    path('<int:pk>/delete', views.ContactNoteDelete.as_view(),
         name='contact-note-delete'),
]
