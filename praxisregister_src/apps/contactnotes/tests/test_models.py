from django.test import TestCase

from contactnotes.models import ContactNote
from contactnotes.types import ContactMedium, ContactDirection
from utils.utils_test import setup_test_basics


class ContactNoteModelTest(TestCase):
    def setUp(self) -> None:
        self.institute, self.group, self.user, [self.practice_1, self.practice_2] = setup_test_basics()

    def test_contact_note_has_field_direction(self):
        ContactNote.objects.create(
            contact_medium=ContactMedium.EMAIL,
            contact_person="Dr. Smith",
            practice=self.practice_1,
            direction=ContactDirection.INCOMING,
            contact_date='2007-04-05',
            contact_time='12:30',
            author=self.user
        )