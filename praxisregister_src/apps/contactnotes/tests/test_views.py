from django.test import TestCase, Client
from django.urls import reverse

from contactnotes.models import ContactNote
from contactnotes.types import ContactMedium, ContactDirection
from utils.utils_test import setup_test_basics


class ContactNoteViewTests(TestCase):
    def setUp(self) -> None:
        self.institute, self.group, self.user, [self.practice_1, self.practice_2] = setup_test_basics()
        self.client = Client()
        login = self.client.login(username="tester", password="password")
        self.note_data = {
            'contact_medium': ContactMedium.EMAIL,
            "contact_date": '2007-04-05',
            "contact_time": '12:30',
            'contact_person': "Mr. Mayer",
            'direction': ContactDirection.INCOMING,
            'note': "Something important",
        }

    def test_create_contact_note_view(self):
        response = self.client.post(reverse('practices:contactnote-create', kwargs={"pk": self.practice_1.id}),
                                    data=self.note_data)
        self.assertRedirects(response,
                             expected_url=reverse('practices:contactnote-list', kwargs={"pk": self.practice_1.id}))
        self.assertEqual(ContactNote.objects.count(), 1)
        self.assertEqual(ContactNote.objects.first().practice.id, self.practice_1.id)
        self.assertEqual(ContactNote.objects.first().note, "Something important")

    def test_create_contact_note_view_has_created_note_object(self):
        self.client.post(reverse('practices:contactnote-create', kwargs={"pk": self.practice_1.id}),
                         data=self.note_data)
        self.assertEqual(self.practice_1.contact_notes[0], ContactNote.objects.first())

    def test_delete_contact_note(self):
        self.client.post(reverse('practices:contactnote-create', kwargs={"pk": self.practice_1.id}),
                                    data=self.note_data)
        self.assertEqual(ContactNote.objects.count(), 1)
        contact_note_id = ContactNote.objects.first().pk
        response = self.client.post(reverse('contactnotes:contact-note-delete', kwargs={"pk": contact_note_id}))
        self.assertRedirects(response,
                             expected_url=reverse('practices:contactnote-list', kwargs={"pk": self.practice_1.id}))
        self.assertEqual(ContactNote.objects.count(), 0)