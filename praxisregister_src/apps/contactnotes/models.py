from django.db import models
from django.utils.translation import gettext_lazy as _
from taggit_selectize.managers import TaggableManager

from config.settings import UPLOAD_DIRECTORY
from contactnotes.types import ContactMedium, ContactDirection
from practices.models import Practice
from users.models import User


def generate_attachment_upload_path(instance, filename):
    """
    Generates upload path, where user-uplaoded attachments for contact notes are saved.
    """
    path = f"{UPLOAD_DIRECTORY}/contact_note_attachments/{filename}"
    return path


class ContactNote(models.Model):
    """
    ContactNote model representing documented contacts between institutes and practices.
    """

    class Meta:
        verbose_name = _("Contact note")
        verbose_name_plural = _("Contact notes")
        
    created_at = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, verbose_name=_('Author'), on_delete=models.SET_NULL, null=True)

    contact_person = models.CharField(verbose_name=_('Contact person'), max_length=255, null=True, blank=True,
                                      default=None)
    note = models.TextField(verbose_name=_('Note'), blank=False, default="")
    contact_medium = models.CharField(
        verbose_name=_("Medium"),
        max_length=128,
        choices=ContactMedium.choices,
        default=ContactMedium.PHONE,
        blank=False,
    )
    practice = models.ForeignKey(Practice, on_delete=models.CASCADE, null=True)  # null setting for now
    direction = models.CharField(
        verbose_name=_("Direction"),
        max_length=128,
        choices=ContactDirection.choices,
        default=ContactDirection.OUTGOING,
        blank=False,
    )
    contact_time = models.TimeField(verbose_name=_("Contact time"), null=True, blank=True)
    contact_date = models.DateField(verbose_name=_("Contact date"), null=True, blank=True)

    tags = TaggableManager(blank=True)

    attachment_1 = models.FileField(
        verbose_name=_('File 1'),
        upload_to=generate_attachment_upload_path,
        blank=True,
        null=True
    )
    attachment_2 = models.FileField(
        verbose_name=_('File 2'),
        upload_to=generate_attachment_upload_path,
        blank=True,
        null=True
    )
    attachment_3 = models.FileField(
        verbose_name=_('File 3'),
        upload_to=generate_attachment_upload_path,
        blank=True,
        null=True
    )
    attachment_4 = models.FileField(
        verbose_name=_('File 4'),
        upload_to=generate_attachment_upload_path,
        blank=True,
        null=True,
    )

    @property
    def attachments(self):
        indexed_file_fields = [
            (1, self.attachment_1),
            (2, self.attachment_2),
            (3, self.attachment_3),
            (4, self.attachment_4),
        ]

        return [(i, file_field) for (i, file_field) in indexed_file_fields if file_field]

    @property
    def attachments_with_id_and_short_name(self):
        return_list = []
        for i, attachment in self.attachments:
            extension = attachment.name.split(".")[-1]
            path_part = attachment.name.split(".")[-2]
            filename = path_part.split("/")[-1]
            short_name = f"{filename}.{extension}"
            return_list.append((i, attachment, short_name))

        return return_list

    def __str__(self) -> str:
        note=_('Contact note')
        date=_('Contact note created on')
        return f"{date} {self.created_at.ctime()}" if self.created_at else f"{note}"