from django.forms import ModelForm, DateInput, TimeInput, FileField, FileInput, CharField

from contactnotes.models import ContactNote
from utils.constants import DATALIST_INPUT_HELP_TEXT
from utils.fields import ListTextWidget
from django.utils.translation import gettext_lazy as _


class ContactNoteForm(ModelForm):
    attachment_1 = FileField(widget=FileInput)
    attachment_2 = FileField(widget=FileInput)
    attachment_3 = FileField(widget=FileInput)
    attachment_4 = FileField(widget=FileInput)

    contact_person = CharField(
        required=False,
        help_text=DATALIST_INPUT_HELP_TEXT
    )

    def __init__(self, *args, **kwargs):
        _type_choices = kwargs.pop('person_data_list', None)

        super().__init__(*args, **kwargs)
        self.fields['contact_person'].widget = ListTextWidget(
            data_list=_type_choices,
            name='person-choices',
        )
        self.fields['contact_person'].label = _('Contact person')

        self.fields['attachment_1'].required = False
        self.fields['attachment_1'].label = _('File 1')
        self.fields['attachment_2'].required = False
        self.fields['attachment_2'].label = _('File 2')
        self.fields['attachment_3'].required = False
        self.fields['attachment_3'].label = _('File 3')
        self.fields['attachment_4'].required = False
        self.fields['attachment_4'].label = _('File 4')

    class Meta:
        model = ContactNote
        fields = [
            'contact_medium', 'contact_date', 'contact_time', 'contact_person', 'direction', 'note', 'tags',
            'attachment_1', 'attachment_2', 'attachment_3', 'attachment_4',
        ]
        widgets = {
            'contact_date': DateInput(
                format='%Y-%m-%d',
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            ),
            'contact_time': TimeInput(
                format='%H:%M',
                attrs={
                    'type': 'time',
                    'class': 'form-control',
                }
            ),
        }
