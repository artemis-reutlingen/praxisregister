from django.apps import AppConfig


class ContactnotesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'contactnotes'