from django.urls import path

from studies.views import *

app_name = "studies"

urlpatterns = [
    path('', details.StudyList.as_view(), name='list'),
    path('new/', details.StudyCreate.as_view(), name='create'),
    path('<int:pk>/', details.StudyDetail.as_view(), name='detail'),
    path('<int:pk>/edit/', details.StudyEdit.as_view(), name='edit'),
    path('<int:pk>/delete/', details.StudyDelete.as_view(), name='delete'),
    path('<int:pk>/participations/', participating_practices.StudyParticipationList.as_view(), name='participation-list'),

    path('<int:pk>/participations/add/', participating_practices.bulk_create_participations, name='bulk-add-practices'),

    path('participations/<int:pk>/edit/', participating_practices.StudyParticipationEdit.as_view(), name='participation-edit'),
    path('participations/<int:pk>/delete/', participating_practices.RemovePracticeParticipation.as_view(), name='participation-delete'),
    path('<int:pk>/people/', participating_people.ParticipatingPeopleList.as_view(), name='people-participation-list'),
    path('<int:pk>/people/add/', participating_people.bulk_create_participations, name='bulk-add-people'),

    path('participations/<int:study_participation_id>/persons/add', participating_people.AddParticipatingPerson.as_view(), name='participation-add-person'),
    path('participations/<int:study_participation_id>/persons/<int:pk>/edit', participating_people.EditParticipatingPerson.as_view(), name='participation-edit-person'),
    path('participations/<int:study_participation_id>/persons/<int:pk>/delete', participating_people.RemoveParticipatingPerson.as_view(), name='participation-delete-person'),
]
