from django.dispatch import Signal
studies_log = Signal()
study_participation_log = Signal()
study_bulk_practice_log = Signal()
study_bulk_people_log = Signal()

__all__ = [
    "details",
    "participating_people",
    "participating_practices",
    "practice_study_overview",
]