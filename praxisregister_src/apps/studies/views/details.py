"""
This file contains the views for the study list and study details page that can 
be accessed directly via the study tab in the top bar.
"""
from ..models import Study
from ..tables import StudiesTable
from ..forms import StudyForm

from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from django_tables2 import SingleTableMixin
from guardian.mixins import PermissionRequiredMixin

from . import studies_log

class StudyList(SingleTableMixin, ListView):
    model = Study
    paginate_by = 15
    template_name = 'studies/study_list.html'
    table_class = StudiesTable

    def get_queryset(self):
        return Study.objects.get_studies_for_user(self.request.user).distinct().order_by('-start_date')


class StudyDetail(DetailView):
    model = Study
    template_name = 'studies/study_detail.html'
    context_object_name = 'study'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()

        actions = []
        if self.request.user.has_perm('studies.change_study', self.get_object()):
            actions.append("EDIT_STUDY")
        if self.request.user.has_perm('studies.change_study', self.get_object()):
            actions.append("DELETE_STUDY")

        context['actions'] = actions
        context['active_study_details'] = "active"

        return context

class StudyCreate(CreateView):
    model = Study
    form_class = StudyForm
    template_name = 'studies/forms/study_create.html'

    def form_valid(self, form):
        form.instance.assigned_institute = self.request.user.assigned_institute
        return super().form_valid(form)

    def get_success_url(self):
        self.object.assign_editor_perms_to_institute(self.request.user.assigned_institute)
        studies_log.send(sender=self.__class__, username=self.request.user.username, study=self.object)
        return reverse_lazy('studies:detail', kwargs={'pk': self.object.id})


class StudyEdit(PermissionRequiredMixin, UpdateView):
    model = Study
    form_class = StudyForm
    template_name = 'global/forms/generic.html'
    permission_required = 'studies.change_study'

    def get_success_url(self):
        studies_log.send(sender=self.__class__, username=self.request.user.username, study=self.object)
        return reverse_lazy('studies:detail', kwargs={'pk': self.kwargs['pk']})


class StudyDelete(PermissionRequiredMixin, DeleteView):
    model = Study
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = 'studies.delete_study'

    def get_success_url(self):
        studies_log.send(sender=self.__class__, username=self.request.user.username, study=self.object)
        return reverse_lazy('studies:list')
