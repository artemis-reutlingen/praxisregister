"""
Contains the views for the list of practices participating in a study 
and adding, editing or deleting participations.
"""
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.db.models import QuerySet
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, UpdateView, DeleteView

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Submit
from django_tables2.config import RequestConfig

from practices.filters import PracticeFilter
from practices.models import Practice
from practices.tables import SelectPracticesTable
from utils.view_mixins import ManagePracticePermissionRequired

from ..models import Study, StudyParticipation
from ..filters import StudyParticipationFilter
from ..tables import StudyParticipationsForStudyTable
from . import study_participation_log, study_bulk_practice_log

class StudyParticipationList(DetailView):
    class _FormHelper(FormHelper):
        form_method = 'GET'
        layout = Layout(
            Row(
                Column('practice'),
                Column(Submit('submit', value=_('Apply filter')))
            )
        )

    model = Study
    template_name = 'studies/study_participations_list.html'
    context_object_name = 'study'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        study_participations_filter = StudyParticipationFilter(
            self.request.GET,
            #For now the user should only be able to see their own practices
            queryset=self.object.participations.filter(practice_id__in=Practice.objects.get_practice_objects_for_user(self.request.user).values('id'))
        )
        study_participations_filter.form.helper = StudyParticipationList._FormHelper()

        table = StudyParticipationsForStudyTable(study_participations_filter.qs, order_by='-status')

        RequestConfig(self.request).configure(table)

        context['filter'] = study_participations_filter
        context['table'] = table
        context['active_participating_practices'] = "active"

        return context

class StudyParticipationEdit(ManagePracticePermissionRequired, UpdateView):
    model = StudyParticipation
    template_name = 'global/forms/generic.html'
    fields = [
        'status',
    ]

    def get_permission_object(self):
        return self.get_object().practice

    def get_success_url(self):
        study_participation_log.send(sender=self.__class__, username=self.request.user.username, study_id=self.object.study.id, practice_id=self.object.practice.id)
        return reverse_lazy('practices:study-participation-list', kwargs={'pk': self.object.practice_id})

def add_practice_to_study(study: Study, practice: Practice) -> StudyParticipation:
    participation, _ = StudyParticipation.objects.update_or_create(study = study, practice = practice)
    return participation

def add_practices_to_study(study: Study, practices: QuerySet[Practice]):
    for practice in practices:
        add_practice_to_study(study, practice)

def bulk_create_participations(request, pk):
    study = get_object_or_404(Study, pk = pk)
    if not request.user.has_perm('studies.view_study', study):
        raise PermissionDenied
    
    participating_practice_ids = study.participations.exclude(practice__isnull = True).values_list('practice__id', flat = True)
    available_practices = Practice.objects.get_practice_objects_for_user(request.user) \
                                          .exclude(id__in = participating_practice_ids)
    practice_filter = PracticeFilter(request, queryset = available_practices)
    table = SelectPracticesTable(practice_filter.qs)
    RequestConfig(request, paginate={"per_page": 12}).configure(table)

    if request.method == "POST":
        with transaction.atomic():
            selected_practices = Practice.objects.filter(id__in=request.POST.getlist("selection"))
            add_practices_to_study(study, selected_practices)

            # NOTE: THREE LINES OF CODE for a log entry. WHY
            class BulkCreateStudyParticipationsForPractices: pass
            participations_text = ",".join([str(practice.id) for practice in selected_practices])
            study_bulk_practice_log.send(sender = BulkCreateStudyParticipationsForPractices, username = request.user.username, study_id = study.id, participations = participations_text)

            messages.success(request, _("Successfully added practices to the study"))
            return redirect("studies:participation-list", pk = pk)
    
    context = {
        "study": study,
        "table": table,
        "filter": practice_filter,
        "active_add_practices": "active"
    }
    return render(request, "studies/select_practices.html", context)

class RemovePracticeParticipation(ManagePracticePermissionRequired, DeleteView):
    model = StudyParticipation
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return self.get_object().practice

    def get_success_url(self):
        study_participation_log.send(sender=self.__class__, username=self.request.user.username, study_id=self.object.study.id, practice_id=self.object.practice.id, participation_id=self.object.id)
        return reverse_lazy('practices:study-participation-list', kwargs={'pk': self.object.practice_id})
    
