"""
Contains the views for viewing the list of studies a practice participates in.
Additionally, it has views for adding, editing and deleting participations.
Also it contains views for managing the people of the practice participating in a study.
"""
from django.views.generic import ListView, CreateView
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django import forms

from utils.view_mixins import ManagePracticePermissionRequired

from ..models import StudyParticipation, Study
from ..forms import StudyParticipationForm
from practices.models import Practice

from . import study_participation_log

class PracticeStudyParticipations(ManagePracticePermissionRequired, ListView):
    model = StudyParticipation
    template_name = "studies/study_list_for_practice.html"
    paginate_by = 10

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['practice'] = self.get_practice_object()
        return context

    def get_queryset(self):
        queryset = super().get_queryset().filter(
            practice=self.get_practice_object()
        ).order_by('-created_at')
        return queryset



class StudyParticipationCreate(ManagePracticePermissionRequired, CreateView):
    model = StudyParticipation
    template_name = 'global/forms/generic.html'
    form_class = StudyParticipationForm

    def get_permission_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def form_valid(self, form):
        form.instance.practice = Practice.objects.get(pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_form_class(self):
        practice = Practice.objects.get(pk=self.kwargs['pk'])

        practice_studies = StudyParticipation.objects.filter(practice=practice)
        existing_participation_study_ids = practice_studies.values('study_id')

        available_choices = Study.objects.get_studies_for_user(self.request.user).distinct().exclude(pk__in=existing_participation_study_ids)

        form_class = StudyParticipationForm
        form_class.base_fields['study'] = forms.ModelChoiceField(
            label=_('Study'),
            queryset=available_choices,
            required=True,
        )
        return form_class
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_title"] = _("Add practice to a study")
        return context

    def get_success_url(self):
        study_participation_log.send(sender=self.__class__, username=self.request.user.username, participation_id=self.object.id, study_id=self.object.study.id, practice_id=self.object.practice.id)
        return reverse_lazy('practices:study-participation-list', kwargs={'pk': self.object.practice_id})
