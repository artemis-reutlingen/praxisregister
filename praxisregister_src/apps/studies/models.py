from django.db import models
from django.db.models import F, Q
from django.utils.translation import gettext_lazy as _
from guardian.shortcuts import assign_perm, get_objects_for_user
from guardian.models import UserObjectPermissionBase, GroupObjectPermissionBase

from .types import StudyType, InterventionType, LongCrossType, HasSignedUpStatus
from importer.models import ImportableMixin
from track_changes.models import TrackChangesMixin

class StudyManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()
    
    @staticmethod
    def get_studies_for_user(user):
        return get_objects_for_user(user, 'studies.view_study')

class Study(TrackChangesMixin, ImportableMixin, models.Model):
    objects = StudyManager()

    title = models.CharField(max_length=512, blank=False, verbose_name=_('Title'))
    type = models.CharField(
        verbose_name=_("Study type"),
        max_length=128,
        choices=StudyType.choices,
        default=StudyType.RCT,
        blank=True,
    )
    intervention = models.CharField(
        verbose_name=_("Intervention type"),
        max_length=128,
        choices=InterventionType.choices,
        default=InterventionType.OBS,
        blank=True,
    )
    long_cross = models.CharField(
        verbose_name=_("Longitudinal/Cross-sectional"),
        max_length=128,
        choices=LongCrossType.choices,
        default=LongCrossType.CROSS,
        blank=True,
    )
    description = models.TextField(verbose_name=_('Description'))
    url = models.URLField(verbose_name=_('URL'), blank=True)

    assigned_institute = models.ForeignKey('institutes.Institute', verbose_name=_('Institute'),
                                           on_delete=models.SET_NULL,
                                           null=True)

    start_date = models.DateField(verbose_name = _("Start date"))
    end_date = models.DateField(verbose_name = _("End date"))

    def __str__(self):
        return _('Study ') + str(self.id) + ": " + self.title

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=Q(start_date__lte=F('end_date')),
                name='Start and end date integrity'
            ),
        ]

    @property
    def participations(self):
        return StudyParticipation.objects.filter(study=self.id)

    def assign_editor_perms_to_institute(self, institute):
        assign_perm('studies.view_study', institute.group, self)
        assign_perm('studies.change_study', institute.group, self)
        assign_perm('studies.delete_study', institute.group, self)

# Explicit definition of Study object permissions. This ensures permission objects are also deleted when a study is deleted instead of being orphaned.
class StudyUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(Study, on_delete = models.CASCADE)

class StudyGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(Study, on_delete = models.CASCADE)

class StudyParticipation(TrackChangesMixin, ImportableMixin, models.Model):

    study = models.ForeignKey(Study, on_delete=models.CASCADE, null=False, related_name = "participations")
    practice = models.ForeignKey('practices.Practice', on_delete=models.CASCADE, null = True, blank = True, related_name = "study_participations")

    class Meta:
        verbose_name = _("Study participation")
        verbose_name_plural = _("Study participations")
        unique_together = ['study', 'practice']

    def __str__(self) -> str:
        return f"{_('Study participation')} {self.study.title}: {self.practice.name if self.practice else _('No practice')}"
    

class PersonStudyParticipation(TrackChangesMixin, ImportableMixin, models.Model):
    person = models.ForeignKey('persons.Person', on_delete = models.CASCADE, related_name = "study_participations")
    study_participation = models.ForeignKey(StudyParticipation, on_delete = models.CASCADE, related_name = "person_participations")

    invitation_sent = models.BooleanField(verbose_name=_('Invitation sent'), default=False)
    invitation_sent_at = models.DateField(verbose_name=_("Invitation sent at"), null=True, blank=True)

    has_signed_up = models.CharField(verbose_name=_('Registration status'), max_length=128, default=HasSignedUpStatus.UNKNOWN, choices=HasSignedUpStatus.choices)
    responded_to_invitation_at = models.DateField(verbose_name=_("Responded to invitation at"), null=True, blank=True)
    has_participated = models.BooleanField(verbose_name=_('Has participated'), default=False)

    points = models.IntegerField(verbose_name = _("Points"), null = True, blank = True)
    comment = models.TextField(verbose_name = _("Comment"), null = True, blank = True)

    class Meta:
        verbose_name = _("Person study participation")
        verbose_name_plural = _("Person study participations")
        unique_together = ['person', 'study_participation']

    def __str__(self) -> str:
        return _("Study participation") + f"{self.study_participation.study.title}: {self.person.full_name_with_title}"
