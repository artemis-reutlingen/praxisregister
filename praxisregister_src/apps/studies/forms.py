from django import forms

from .models import Study, PersonStudyParticipation, StudyParticipation
from persons.models import Person

class StudyForm(forms.ModelForm):
    class Meta:
        model = Study
        fields = [
            'title',
            'start_date',
            'end_date',
            'type',
            'intervention',
            'long_cross',
            'description',
            'url'
        ]

        widgets = {
            'start_date': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            ),
            'end_date': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            ),
        }


class ParticipatingPersonEditForm(forms.ModelForm):
    class Meta:
        model = PersonStudyParticipation
        fields = ['invitation_sent', 'invitation_sent_at', 'has_signed_up', 'responded_to_invitation_at', 'has_participated', 'points', 'comment']
        widgets = {
            'invitation_sent_at': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            ),
            'responded_to_invitation_at': forms.DateTimeInput(
                format='%Y-%m-%d',
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            ),
        }


class PersonStudyParticipationForm(forms.ModelForm):

    person = forms.ModelChoiceField(queryset = Person.objects.none(), required = True)

    def __init__(self, study_participation: StudyParticipation, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.study_participation = study_participation
        already_participating_persons_ids = self.study_participation.person_participations.values('person_id')
        available_choices = self.study_participation.practice.person_set.exclude(pk__in=already_participating_persons_ids)
        self.fields["person"].queryset = available_choices

    class Meta:
        model = PersonStudyParticipation
        fields = ['person']


class StudyParticipationForm(forms.ModelForm):
    class Meta:
        model = StudyParticipation
        fields = [
            'study',
        ]