from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django_tables2 import Table, Column

from .models import StudyParticipation, Study, PersonStudyParticipation
from .types import HasSignedUpStatus

class StudiesTable(Table):
    def render_title(self, value, record):
        study_url = reverse('studies:detail', kwargs={"pk": record.id})
        study_link = mark_safe(f'<a href="{study_url}">{value}</a>')
        return study_link

    class Meta:
        model = Study
        template_name = "django_tables2/bootstrap5.html"
        fields = ("id", "title", "assigned_institute", "start_date", "end_date",)

class StudyParticipationsForStudyTable(Table):  # used in study context
    def render_practice(self, value, record):
        html = "-"
        if record.practice:
            practice_url = reverse("practices:detail", kwargs={"pk": record.practice.id})
            html = mark_safe(f'<a href="{practice_url}" id="id_practice_{record.practice.id}_link">{record.practice}</a>') 
        return html

    class Meta:
        model = StudyParticipation
        template_name = "django_tables2/bootstrap5.html"
        fields = ("practice", "practice__assigned_institute",)


class StudyParticipatingPeopleTable(Table):
    person = Column(verbose_name=_("Person"))
    study_participation__practice__name = Column(verbose_name=_('Practice'))
    invitation_sent = Column(verbose_name=_("Invitation"), attrs = {"td": {"class": "text-nowrap"}})
    has_signed_up = Column(verbose_name=_("Registration"), attrs = {"td": {"class": "text-nowrap"}})
    has_participated = Column(verbose_name=_("Participated"), attrs = {"td": {"class": "text-nowrap"}})
    edit_participation = Column(verbose_name=_('Edit'), orderable=False, empty_values=(), )
    
    def render_person(self, value, record):
        person_url = reverse("persons:detail", kwargs={"pk": value.pk})
        return mark_safe(f'<a href="{person_url}" style="text-decoration:none;">{value}</a></br>')

    def render_study_participation__practice__name(self, value, record):
        html = "-"
        if record.study_participation and record.study_participation.practice:
            practice = record.study_participation.practice
            practice_url = reverse("practices:study-participation-list", kwargs={"pk": practice.id})
            html = mark_safe(f'<a href="{practice_url}" id="id_practice_{practice.id}_link" style="text-decoration:none;">{practice}</a>')
        return html
    
    def render_invitation_sent(self, value, record):
        if record.invitation_sent == True:
            html = '<i class="fa fa-check-circle text-success me-2"></i>' + _("Sent")
            # html += f'<p>{record.invitation_sent_at.strftime("%d.%m.%Y")}</p>' if record.invitation_sent_at else ""
        else:
            html = '<i class="fa fa-times-circle text-danger"></i>'
        return mark_safe(html)
    
    def render_has_signed_up(self, value, record):
        html = ""
        if record.has_signed_up == HasSignedUpStatus.CONFIRMATION:
            html = '<i class="fa fa-check-circle text-success me-2"></i>' + _("Confirmation")
            # html += f'<p>{record.responded_to_invitation_at.strftime("%d.%m.%Y")}</p>' if record.responded_to_invitation_at else ""
        elif record.has_signed_up == HasSignedUpStatus.CANCELLATION:
            html = '<i class="fa fa-times-circle text-danger me-2"></i> ' + _("Cancellation")
            # html += f'<p>{record.responded_to_invitation_at.strftime("%d.%m.%Y")}</p>' if record.responded_to_invitation_at else ""
        else:
            html = '<i class="fa-solid fa-circle-question text-secondary"></i>'
        return mark_safe(html)
    
    def render_has_participated(self, value, record):
        html = '<i class="fa fa-check-circle text-success"></i>' if record.has_participated == True else '<i class="fa fa-times-circle text-danger"></i>'
        return mark_safe(html)

    def render_edit_participation(self, value, record):
        context = {"participation": record}
        edit_button = render_to_string("studies/edit_person_participation_button.html", context = context, request = self.request)
        delete_button = render_to_string("studies/delete_person_participation_button.html", context = context, request = self.request)
        return mark_safe(edit_button + delete_button)

    def render_comment(self, value, record):
        text_limit = 100
        if len(value) > text_limit:
            return value[:text_limit-3] + "..."
        return value

    class Meta:
        model = PersonStudyParticipation
        template_name = "django_tables2/bootstrap5.html"
        fields = ("person",)
        # fields = ("person", "study_participation__practice__name", "invitation_sent", "has_signed_up", "has_participated", "points", "comment", "edit_participation")

class StudyFullPeopleTable(StudyParticipatingPeopleTable):
    class Meta(StudyParticipatingPeopleTable.Meta):
        fields = ("person", "study_participation__practice__name", "invitation_sent", "has_signed_up", "has_participated", "points", "comment", "edit_participation")

class StudyInvitedPeopleTable(StudyParticipatingPeopleTable):
    has_participated = Column(visible = False)
    class Meta(StudyParticipatingPeopleTable.Meta):
        fields = ("person", "study_participation__practice__name", "invitation_sent", "has_signed_up", "edit_participation")

class StudyConfirmedPeopleTable(StudyParticipatingPeopleTable):
    invitation_sent = Column(visible = False)
    class Meta(StudyParticipatingPeopleTable.Meta):
        fields = ("person", "study_participation__practice__name", "has_signed_up", "has_participated", "points", "comment", "edit_participation")