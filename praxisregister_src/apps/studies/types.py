from django.db import models
from django.utils.translation import gettext_lazy as _


class StudyType(models.TextChoices):
    RCT = 'RCT', _('Randomized Controlled Trial')
    COS = 'CoS', _('Cohort Study')
    CCS = 'CCS', _('Case Control Study')
    MEA = 'MEA', _('Meta Analysis')


class InterventionType(models.TextChoices):
    INT = 'INT', _('Interventional Study')
    OBS = 'OBS', _('Observational Study')


class LongCrossType(models.TextChoices):
    LONG = 'LONG', _('Longitudinal Study')
    CROSS = 'CROSS', _('Cross-sectional Study')

class HasSignedUpStatus(models.TextChoices):
    CONFIRMATION = "Confirmation", _("Confirmation")
    CANCELLATION = "Cancellation", _("Cancellation")
    UNKNOWN = "UNKNOWN", _("Unknown")

class ParticipatingPersonInvitationChoices(models.TextChoices):
    INVITATION_PENDING = 'INVITATION_PENDING', _('Invitation pending')
    INVITATION_SENT = 'INVITATION_SENT', _('Invitation sent')
    SIGNED_UP =  'SIGNED_UP', _('Signed up')
    CANCELLED = 'CANCELLED', _('Cancelled')
    NO_RESPONSE = 'NO_RESPONSE', _('No response')
    PARTICIPATED = 'PARTICIPATED', _('Participated')