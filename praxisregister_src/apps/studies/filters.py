from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from django.forms.widgets import TextInput, Select

from django_filters import ChoiceFilter, CharFilter
from django_property_filter import PropertyFilterSet, PropertyCharFilter
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Row, Column as FormCol, Submit, Layout

from .models import StudyParticipation, PersonStudyParticipation
from .types import HasSignedUpStatus, ParticipatingPersonInvitationChoices

class StudyParticipationFilter(PropertyFilterSet):
    practice = PropertyCharFilter(
        lookup_expr='icontains',
        field_name="practice__name",
        label=_("Practice name")
    )

    class Meta:
        model = StudyParticipation
        fields = ('practice',)

class StudyPeopleParticipationFilter(PropertyFilterSet):
    class Meta:
        model = PersonStudyParticipation
        fields = ('person__full_name', 'study_participation__practice__name', 'participation_status')
    
    practice = PropertyCharFilter(
        lookup_expr="icontains",
        field_name="person__practice__name",
        label=_("Practice name")
    )

    person__full_name = CharFilter(
        method='filter_persons_full_name',
        label=_('Person'),
        widget=TextInput()
    )

    study_participation__practice__name = CharFilter(
        lookup_expr='icontains',
        label=_('Practice name'),
        widget=TextInput()
    )

    participation_status = ChoiceFilter(
        method='filter_participation_status',
        label=_('Participation status'),
        choices=ParticipatingPersonInvitationChoices.choices,
        widget=Select()
    )

    @staticmethod
    def filter_persons_full_name(queryset, name, value):
        return queryset.filter(Q(person__first_name__icontains=value) | Q(person__last_name__icontains=value))

    @staticmethod
    def filter_participation_status(queryset, name, value):
        qs = None
        match value:
            case ParticipatingPersonInvitationChoices.INVITATION_PENDING:
                qs = queryset.filter(has_participated = False, invitation_sent = False)
            case ParticipatingPersonInvitationChoices.INVITATION_SENT:
                qs = queryset.filter(has_participated = False, invitation_sent = True)
            case ParticipatingPersonInvitationChoices.NO_RESPONSE:
                qs = queryset.filter(has_participated = False, invitation_sent = True, has_signed_up = HasSignedUpStatus.UNKNOWN)
            case ParticipatingPersonInvitationChoices.SIGNED_UP:
                qs = queryset.filter(has_participated = False, has_signed_up = HasSignedUpStatus.CONFIRMATION)
            case ParticipatingPersonInvitationChoices.CANCELLED:
                qs = queryset.filter(has_participated = False, has_signed_up = HasSignedUpStatus.CANCELLATION)
            case ParticipatingPersonInvitationChoices.PARTICIPATED:
                qs = queryset.filter(has_participated = True)
        return qs.distinct()


class StudyParticipationFormHelper(FormHelper):
    form_method = 'GET'
    layout = Layout(
        Row(
            FormCol('person__full_name'),
            FormCol('study_participation__practice__name'),
            FormCol('participation_status'),
            FormCol(Submit('submit', value=_('Apply Filter')),
                    css_class="mb-3", style="align-self:flex-end")
        )
    )