from django.contrib import admin
from studies.models import Study, StudyParticipation, PersonStudyParticipation
from guardian.admin import GuardedModelAdmin

class StudyAdmin(GuardedModelAdmin):
    pass

admin.site.register(Study, GuardedModelAdmin)
admin.site.register(StudyParticipation)
admin.site.register(PersonStudyParticipation)
