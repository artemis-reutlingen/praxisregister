from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column as FormCol, Submit
from django.utils.translation import gettext_lazy as _
from django_filters import CharFilter, ChoiceFilter
from django_property_filter import PropertyCharFilter, PropertyFilterSet
from django.db.models import TextChoices

from processes.models import ProcessDefinition
from utils.filters import AbstractPracticeFilter
from datetime import date

class ProcessStatus(TextChoices):
    ALL = "all", _("All"),
    DRAFT = "draft", _("Draft"),
    FINALIZED = "final", _("Process"),
    OVERDUE = "due", _("Overdue process"),

class ProcessDefinitionFilter(PropertyFilterSet):
    name = CharFilter(
        lookup_expr='icontains',
        label=_("Name"),
    )
    author = PropertyCharFilter(
        lookup_expr='icontains',
        label=_("Author"),
        field_name="author__full_name_with_title",
    )
    status = ChoiceFilter(
        choices = ProcessStatus.choices,
        label = _("Status"),
        method = "filter_by_status",
        empty_label = None
    )

    def filter_by_status(self, queryset, name, value):
        match value:
            case ProcessStatus.ALL:
                return queryset
            case ProcessStatus.DRAFT:
                return queryset.filter(is_draft = True)
            case ProcessStatus.FINALIZED:
                return queryset.filter(is_draft = False)
            case ProcessStatus.OVERDUE:
                return queryset.filter(process__deadline__lt = date.today()).distinct()
        return queryset

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form.helper = ProcessDefinitionFilterFormHelper()

    class Meta:
        model = ProcessDefinition
        fields = ('name', 'author', 'status')




class ProcessDefinitionFilterFormHelper(FormHelper):
    form_method = 'GET'
    layout = Layout(
        Row(
            FormCol('name'),
            FormCol('author'),
            FormCol('status'),
            FormCol(
                Submit('submit', value=_('Apply filter')),
                css_class="d-flex align-items-end mb-3"
            ),
            css_class="mb-3"
        )
    )


class ProcessFilter(AbstractPracticeFilter):
    class Meta:
        model = ProcessDefinition
        fields = ('practice',)

