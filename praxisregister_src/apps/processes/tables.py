from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django_tables2 import Table, Column
from django.db.models import Count, QuerySet, Sum, Q

from utils.templatetags.done_or_not_checkmark import doneornot
from processes.models import ProcessDefinition, Process, StepDefinition
from utils.templatetags.done_or_not_checkmark import doneornot

class ProcessDefinitionTable(Table):
    number_of_steps = Column(verbose_name=_('Number of steps'), orderable=False)

    def render_name(self, value, record):
        process_url = reverse("processes:definition-detail", kwargs={"pk": record.id})

        # Adds (Draft) to the start if the process definition is a draft.
        if record.is_draft:
            value = "(" + _("Draft") + ") " + value

        content = f'<a href="{process_url}">{value}</a>'

        # Adds an exclamation to the end if one or more instances of this process are overdue.
        if record.is_overdue:
            content += '<i class="fa-solid fa-circle-exclamation text-danger ms-2"></i>'
        return mark_safe(content)

    class Meta:
        model = ProcessDefinition
        template_name = "django_tables2/bootstrap5.html"
        fields = (
            'id',
            'name',
            'number_of_steps',
            'author',
            'created_at',
        )


class ProcessStepColumn(Column):
    def __init__(self, step: StepDefinition, *args, **kwargs):
        self.step = step
        super().__init__(accessor = "practice",*args, **kwargs)

    def render(self, record):
        if record:
            step = record.steps.get(step_definition__position_index = self.step.position_index)
            return doneornot(step.is_done)

class ProcessPracticeProgress(Table):
    practice = Column(verbose_name = _("Practice"))
    completion = Column(verbose_name = _("Completed"), accessor = "completed_in_percent")
    deadline = Column(verbose_name = _("Deadline"))

    def __init__(self, process_definition: ProcessDefinition, *args, **kwargs):
        step_columns = []
        for step_definition in process_definition.step_definitions:
            column = ProcessStepColumn(step_definition, verbose_name = step_definition.name, orderable = False)
            step_columns.append((step_definition.name, column))

        processes = process_definition.processes.filter(practice__isnull = False)
        super().__init__(processes, extra_columns=step_columns, *args, **kwargs)

    def render_practice(self, record):
        if record.practice:
            practice_processes_url = reverse('practices:process-list', kwargs={"pk": record.practice.pk})
            practice_link = mark_safe(f'<a href="{practice_processes_url}">{record.practice.name}</a>')
            return practice_link

    def render_completion(self, record):
        return f"{record.completed_in_percent}%"
    
    def order_completion(self, queryset: QuerySet[Process], is_descending: bool):
        queryset = queryset.annotate(num_completed_steps = Count('step', Q(step__is_done=True)))
        if not is_descending:
            queryset = queryset.order_by("-num_completed_steps")
        else:
            queryset = queryset.order_by("num_completed_steps")
        return queryset, True
    
    def render_deadline(self, record):
        date_text = record.deadline.strftime("%d.%m.%Y")
        if record.is_due:
            return mark_safe(f'{date_text} <i class="fa-solid fa-circle-exclamation text-danger"></i>')
        else:
            return date_text
    
    class Meta:
        model = Process
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("practice", "completion", "deadline")
        sequence = ("practice", "completion", "deadline", "...")

