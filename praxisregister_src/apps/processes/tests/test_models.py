from django.test import TestCase

from processes.exceptions import StepDefinitionPositionError
from processes.models import ProcessDefinition, Process
from utils.utils_test import (create_test_users_with_institute,
                              setup_test_basics)

class ProcessModelTest(TestCase):
    def setUp(self) -> None:
        institute, group, self.user, [self.practice_1, self.practice_2] = setup_test_basics()

    def test_process_def_and_instance_models_work_properly(self):
        # process / step definitions
        process_def = ProcessDefinition.objects.create(name="ProcessDef Foo")

        for step_number in range(0, 10):
            process_def.add_step_definition(
                step_name=f"FooBarStep#{step_number}",
                step_description="Lorem Ipsum",
            )
        process_def.finalize(self.user)

        process = Process.objects.create(process_definition=process_def)
        self.assertEqual(process.process_definition.step_definitions.count(), process.steps.count())

    def test_step_definition_ordering(self):
        # process / step definitions
        process_def = ProcessDefinition.objects.create(name="Foo")

        # inserting in reverse stp numbers
        for step_number in range(0, 10):
            process_def.add_step_definition(
                step_name=f"Bar#{step_number}",
                step_description="FooBar",
            )
        process_def.finalize(self.user)

        # check stepdefs are in the correct order when accessed via property of processdef
        for step_def, expected_position_index in zip(process_def.step_definitions, range(1, 11)):
            self.assertEqual(step_def.position_index, expected_position_index)

    def test_step_instance_ordering(self):
        # process / step definitions
        process_def = ProcessDefinition.objects.create(name="Fooo")

        for step_number in range(0, 10):
            process_def.add_step_definition(
                step_name=f"Bar#{step_number}",
                step_description="FooBar",
            )
        process_def.finalize(self.user)

        # check stepdefs are in the correct order when accessed via property of processdef
        for step_def, expected_position_index in zip(process_def.step_definitions, range(1, 11)):
            self.assertEqual(step_def.position_index, expected_position_index)

        process = Process.objects.create(process_definition=process_def)
        for step, expected_position_index in zip(process.steps, range(1, 11)):
            self.assertEqual(step.step_definition.position_index, expected_position_index)

    def test_step_definition_move_position(self):
        # process / step definitions
        process_def = ProcessDefinition.objects.create(name="Foooo")

        for step_number in range(1, 4):
            process_def.add_step_definition(
                step_name=f"#{step_number}",
                step_description="FooBarrr",
            )
        process_def.finalize(self.user)

        step_defs = process_def.step_definitions
        self.assertRaises(StepDefinitionPositionError, step_defs[0].move_down)
        self.assertRaises(StepDefinitionPositionError, step_defs[2].move_up)

        self.assertEqual(step_defs[0].position_index, 1)
        self.assertEqual(step_defs[1].position_index, 2)

        step_defs[0].move_up()
        step_defs = process_def.step_definitions  # refresh

        self.assertEqual(step_defs[0].position_index, 1)
        self.assertEqual(step_defs[0].name, "#2")

        self.assertEqual(step_defs[1].position_index, 2)
        self.assertEqual(step_defs[1].name, "#1")

        step_defs[1].move_down()  # undo previous movement
        step_defs = process_def.step_definitions  # refresh

        self.assertEqual(step_defs[0].position_index, 1)
        self.assertEqual(step_defs[0].name, "#1")

        self.assertEqual(step_defs[1].position_index, 2)
        self.assertEqual(step_defs[1].name, "#2")

    def test_update_all_step_def_indexes_for_process_def(self):
        process_def = ProcessDefinition.objects.create(name="Foooo")

        for step_number in range(1, 11):
            process_def.add_step_definition(
                step_name=f"#{step_number}",
                step_description="FOOOBAAAR",
            )
        process_def.finalize(self.user)

        step_defs = process_def.step_definitions
        original_length = len(step_defs)

        step_defs[0].delete()  # remove first, indexes should be updated via signal

        step_defs = process_def.step_definitions  # refresh

        for sd, i in zip(step_defs, range(1, len(step_defs) + 1)):  # start at idx 1 after update
            self.assertEqual(sd.position_index, i)

    def test_create_copy_of_process_definition(self):
        users, institute = create_test_users_with_institute(n_users=2, institute_name="X")

        process_def = ProcessDefinition(name="ProcessDef Foo", author=users[0])
        process_def.save()

        for step_number in range(0, 3):
            process_def.add_step_definition(step_name=f"FooStepBar#{step_number}", step_description="Loremipsum")

        process_def.finalize(self.user)
        process_def_copy = process_def.create_copy(author=users[1])

        self.assertNotEqual(process_def.id, process_def_copy.id)
        self.assertEqual(process_def.name, process_def_copy.name)
        self.assertEqual(process_def.description, process_def_copy.description)
        self.assertEqual(process_def_copy.is_draft, True)
        self.assertNotEqual(process_def.author, process_def_copy.author)
        self.assertEqual(process_def_copy.author, users[1])
        self.assertEqual(process_def.number_of_steps, process_def_copy.number_of_steps)

        # go through step definitions attributes
        for s_original, s_copy in zip(process_def.step_definitions, process_def_copy.step_definitions):
            self.assertEqual(s_original.name, s_copy.name)
            self.assertEqual(s_original.description, s_copy.description)
            self.assertNotEqual(s_original.id, s_copy.id)

    def test_mark_step_as_done_and_unmark(self):
        process_def = ProcessDefinition(name="ProcessDef Foo")
        process_def.save()
        process_def.add_step_definition("Foo", "Bar")
        process_def.finalize(self.user)

        process = Process.objects.create(process_definition=process_def)

        step = process.steps[0]

        assert not step.is_done

        step.mark_as_done()

        assert step.is_done and step.done_at

        step.reset()

        assert not step.is_done
        assert not step.done_at
