from django.db import models
from django.utils.translation import gettext_lazy as _

from processes.exceptions import StepDefinitionPositionError


class StepDefinition(models.Model):
    """
    Blueprint for single steps.
    Do not directly create instances via StepDefinition.objects.create().
    Use add_step_definition method of parent ProcessDefinition.
    """
    name = models.CharField(max_length=256, verbose_name=_("Step name"), null=False, blank=False)
    description = models.TextField(verbose_name=_("Step description"), null=False, blank=False)
    process_definition = models.ForeignKey("processes.ProcessDefinition", null=False, on_delete=models.CASCADE)
    position_index = models.PositiveIntegerField(verbose_name=_("Step number"), blank=False, null=True)

    def switch_position_indexes_with_other_instance(self, other) -> None:
        """
        Switching position indexes with another StepDefinition object.
        """
        old_index = self.position_index

        self.position_index = other.position_index
        other.position_index = old_index

        self.save()
        other.save()

    def move_up(self):
        """Move StepDefinition position index up (increase index value)."""
        if self.position_index == self.process_definition.last_step_definition_index:
            raise StepDefinitionPositionError("StepDefinition is already at last position.")

        successor = StepDefinition.objects.get(
            process_definition=self.process_definition,
            position_index=self.position_index + 1
        )

        # Switch indexes with successor
        self.switch_position_indexes_with_other_instance(successor)

    def move_down(self):
        """Move StepDefinition position index down (decrease index value)."""
        if self.position_index == 1:  # 1 is minimum, 0 can not be assigned to any step def
            raise StepDefinitionPositionError("StepDefinition is already at first position.")

        predecessor = StepDefinition.objects.get(
            process_definition=self.process_definition,
            position_index=self.position_index - 1
        )

        # Switch indexes with predecessor
        self.switch_position_indexes_with_other_instance(predecessor)

    def __str__(self) -> str:
        return f"{self.name}"