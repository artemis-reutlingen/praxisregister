from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.conf import settings


class Step(models.Model):
    """
    Instance of a StepDefinition. Keeps relation to respective practice model via Process instance.
    """
    process = models.ForeignKey("processes.Process", null=False, on_delete=models.CASCADE)
    step_definition = models.ForeignKey("processes.StepDefinition", null=False, on_delete=models.CASCADE)
    is_done = models.BooleanField(verbose_name=_("Step done"), default=False, blank=False, null=False)
    done_at = models.DateField(verbose_name=_("Done at"), null=True, blank=True)
    done_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)

    def mark_as_done(self, user = None):
        self.is_done = True
        self.done_at = timezone.now()
        self.done_by = user
        self.save()

    def reset(self):
        self.is_done = False
        self.done_at = None
        self.done_by = None
        self.save()

    def __str__(self):
        return str(self.step_definition)
