from django.db import models
from django.utils.translation import gettext_lazy as _
from guardian.shortcuts import get_objects_for_user, assign_perm
from guardian.models import UserObjectPermissionBase, GroupObjectPermissionBase

from processes.exceptions import DraftStatusError
from processes.models.process import Process
from processes.models.step import Step
from processes.models.step_definition import StepDefinition
from users.models import User


class ProcessDefinitionManager(models.Manager):
    def get_process_definitions_for_user(self, user) -> models.QuerySet["ProcessDefinition"]:
        """
        Returns all ProcessDefinition objects a certain user has access to.
        """
        return get_objects_for_user(user, 'processes.view_processdefinition').order_by('-created_at')

    def create_process_definition_for_user(self, user, title):
        process_definition = self.create(name=title, author=user)
        process_definition.assign_perms_for_author_and_institute(author=user)
        return process_definition


class ProcessDefinition(models.Model):
    """
    Represents a blueprint for a multi-stepped process.
    """
    objects: ProcessDefinitionManager = ProcessDefinitionManager()
    name = models.CharField(max_length=256, verbose_name=_("Process name"), null=False, blank=False, default="")
    description = models.TextField(verbose_name=_("Process description"), null=False, blank=False, default="")
    created_at = models.DateTimeField(verbose_name=_("Created at"), auto_now_add=True)
    is_draft = models.BooleanField(verbose_name=_("Is draft"), default=True, blank=False, null=False)
    author = models.ForeignKey(User, verbose_name=_('Author'), on_delete=models.SET_NULL, null=True)

    # StepDefinitions will have continuous position_indexes like  1,2,3, ...
    last_step_definition_index = models.PositiveIntegerField(blank=False, default=0)

    class Meta:
        verbose_name = _("Process definition")
        verbose_name_plural = _("Process definitions")

    def assign_perms_for_author_and_institute(self, author):
        """
        Grant edit permissions for all members of author's institute / group.
        Institute is derived from author/user object.
        """
        institute_group_of_user = author.group
        assign_perm('processes.view_processdefinition', institute_group_of_user, self)
        assign_perm('processes.change_processdefinition', institute_group_of_user, self)
        assign_perm('processes.delete_processdefinition', institute_group_of_user, self)

    def finalize(self, user):
        if not self.is_draft:
            raise DraftStatusError("Tried to finalize already finalized ProcessDefinition.")
        self.is_draft = False
        Process.objects.get_or_create(process_definition=self, created_by=user, general=True)
        self.save()

    @property
    def number_of_steps(self):
        return StepDefinition.objects.filter(process_definition=self).count()

    @property
    def processes(self):
        return Process.objects.filter(process_definition_id=self.id)
    
    @property
    def general_process(self):
        return self.processes.filter(general=True).first()

    @property
    def step_definitions(self):
        return StepDefinition.objects.filter(process_definition=self).order_by('position_index')
    

    def add_step_definition(self, step_name, step_description):
        """
        Add a StepDefinition to a ProcessDefinition.
        Create and add StepDefinition models for ProcessDefinitions with this method.
        """

        step_definition = StepDefinition(
            name=step_name,
            description=step_description,
            process_definition=self,
            position_index=self.last_step_definition_index + 1,
        )
        step_definition.save()

        # Update/increment last index on self
        self.last_step_definition_index += 1
        self.save()

        # Check for active instances and create step instances accordingly
        if self.processes:
            for process in self.processes:
                Step.objects.create(process=process, step_definition=step_definition)

    def update_all_position_indexes(self):
        """
        Updating all indexes for StepDefinition child objects.
        Ensures position_index attributes are 1,2,3, ...
        Also called via post_delete signal for StepDefinition model.
        """
        step_defs = self.step_definitions
        max_index = len(step_defs)
        for step_def, i in zip(step_defs, range(1, max_index + 1)):
            step_def.position_index = i
            step_def.save()
        self.last_step_definition_index = max_index
        self.save()

    def create_copy(self, author):
        """
        Method for creating new versions of existing ProcessDefinition models.
        Only ProcessDefinition/StepDefinitions are copied, not the actual instances.
        The requesting user is set as author of the copy.
        """
        new_process_definition = ProcessDefinition.objects.create(
            name=self.name,
            author=author,
            description=self.description
        )
        new_process_definition.is_draft = True
        new_process_definition.save()
        new_process_definition.assign_perms_for_author_and_institute(author=author)

        # create "deep" copy for step definitions
        for stp_def in self.step_definitions:
            new_process_definition.add_step_definition(stp_def.name, stp_def.description)

        return new_process_definition

    def __str__(self):
        add_process = _('Add process')
        process = _('Process')
        return f"{process} {self.id} : {self.name}" if self.id and self.name else f"{add_process}"

# Explicit definition of ProcessDefinition object permissions. This ensures permission objects are also deleted when a process definition is deleted instead of being orphaned.
class ProcessDefinitionUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(ProcessDefinition, on_delete = models.CASCADE)

class ProcessDefinitionGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(ProcessDefinition, on_delete = models.CASCADE)
