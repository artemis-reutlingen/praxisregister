from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from processes.models import ProcessDefinition, Process, StepDefinition, Step
from guardian.admin import GuardedModelAdmin
from guardian.shortcuts import get_objects_for_group

from institutes.models import Institute


class InstituteFilter(admin.SimpleListFilter):
    title = _('Institute')
    parameter_name = 'institute'

    def lookups(self, request, model_admin):
        institutes = []
        for institute in Institute.objects.all():
            institutes.append((institute.id, institute.full_name))
        return institutes

    def queryset(self, request, queryset):
            if request.GET.get(self.parameter_name):
                institute = Institute.objects.get(pk=request.GET.get(self.parameter_name))
                queryset = get_objects_for_group(institute.group, 'processes.change_processdefinition')
            return queryset

class ProcessDefinitionAdmin(GuardedModelAdmin):
    model = ProcessDefinition
    list_display = ('id', 'name', 'author', 'is_draft') 
    list_per_page = 20
    list_select_related = ['author']
    list_filter = ['created_at', 'is_draft', InstituteFilter]
    ordering = ('-created_at',)
    search_fields = ['name__icontains', 'author__first_name__startswith', 'author__last_name__startswith']

class ProcessAdmin(GuardedModelAdmin):
    model = Process
    list_display = ('id', 'process_definition', 'practice') 
    list_per_page = 20
    list_select_related = ['practice', 'process_definition']
    list_filter = ['practice__assigned_institute']
    search_fields = ['process_definition__name', 'process_definition__author__first_name__startswith', 'process_definition__author__last_name__startswith']

class StepDefinitionAdmin(GuardedModelAdmin):
    model = StepDefinition
    list_display = ('id', 'name', 'process_definition') 
    list_per_page = 20
    list_select_related = ['process_definition']
    search_fields = ['name__icontains']


class StepAdmin(GuardedModelAdmin):
    model = Step
    list_display = ('id', 'process', 'step_definition', 'is_done', 'done_at') 
    list_per_page = 20
    list_select_related = ['process', 'step_definition']
    list_filter = ['is_done', 'process__practice__assigned_institute']
    ordering = ('-done_at',)
    search_fields = ['step_definition__name__icontains']

ProcessDefinitionAdmin.model._meta.verbose_name_plural = "1. " + _("Process definitions")
ProcessAdmin.model._meta.verbose_name_plural = "2. " + _("Processes")
StepDefinitionAdmin.model._meta.verbose_name_plural = "3. " + _("Step definitions")
StepAdmin.model._meta.verbose_name_plural = "4. " + _("Steps")

admin.site.register(ProcessDefinition, ProcessDefinitionAdmin)
admin.site.register(StepDefinition, StepDefinitionAdmin)
admin.site.register(Process, ProcessAdmin)
admin.site.register(Step, StepAdmin)