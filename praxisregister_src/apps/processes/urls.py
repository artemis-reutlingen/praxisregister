from django.urls import path
from processes import views

app_name = 'processes'
urlpatterns = [
    
     # Process definitions
     path('definitions/', views.ProcessDefinitions.as_view(), name='definition-list'),
     path('definitions/<int:pk>/copy', views.CreateProcessDefinitionCopy.as_view(), name='definition-copy'),
     path('definitions/new', views.ProcessDefinitionCreate.as_view(), name='definition-create'),
     path('definitions/<int:pk>/', views.ProcessDefinitionDetail.as_view(), name='definition-detail'),  
     path('definitions/<int:pk>/update', views.ProcessDefinitionUpdate.as_view(), name='definition-update'),
     path('definitions/<int:pk>/delete', views.ProcessDefinitionDelete.as_view(), name='definition-delete'),
     path('definitions/<int:pk>/instantiate', views.bulk_create_processes, name='definition-instantiate'),
     path('definitions/<int:pk>/finalize', views.ProcessDefinitionFinalize.as_view(), name='definition-finalize'),

     # Process instances
     path('definitions/<int:pk>/instances', views.ProcessInstances.as_view(), name='process-instances'),
     path('instances/<int:pk>/delete', views.ProcessDelete.as_view(), name='process-delete'),
     path('instances/<int:pk>/edit-deadline', views.ProcessDeadlineEdit.as_view(), name='process-edit-deadline'),

     # Step definitions
     path('definitions/<int:pk>/steps/new', views.StepDefinitionCreate.as_view(), name='step-definition-add'),
     path('definitions/<int:pk>/steps/detail', views.StepDefinitionDetail.as_view(), name='step-definition-detail'),
     path('steps/definitions/<int:pk>/update', views.StepDefinitionUpdate.as_view(), name='step-definition-update'),
     path('steps/definitions/<int:pk>/delete', views.StepDefinitionDelete.as_view(), name='step-definition-delete'),
     path('steps/definitions/<int:pk>/move_up', views.StepDefinitionMoveUp.as_view(), name='step-definition-move-up'),
     path('steps/definitions/<int:pk>/move_down', views.StepDefinitionMoveDown.as_view(), name='step-definition-move-down'),

     # Step instances
     path('steps/instances/<int:pk>/mark_as_done', views.StepMarkAsDone.as_view(), name='step-mark-as-done'),
     path('steps/instances/<int:pk>/reset', views.StepReset.as_view(), name='step-reset'),
     path('steps/instances/<int:pk>/edit-date', views.StepDateEdit.as_view(), name='step-edit-date'),
]
