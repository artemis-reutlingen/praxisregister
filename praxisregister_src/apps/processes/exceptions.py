class DraftStatusError(Exception):
    pass


class StepDefinitionPositionError(Exception):
    pass
