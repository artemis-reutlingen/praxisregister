from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from guardian.admin import GuardedModelAdmin

from documents.models import Document
# Register your models here.

class DocumentAdmin(GuardedModelAdmin):
    model = Document
    list_display = ('id', 'title', 'description', 'file', 'practice', 'uploaded_by') 
    list_per_page = 20
    list_select_related = ['uploaded_by', 'practice']
    list_filter = ['created_at', 'practice__assigned_institute']
    ordering = ('-created_at',)
    search_fields = ['title__startswith', 'practice__name__icontains', 'uploaded_by__first_name__startswith', 'uploaded_by__last_name__startswith']

admin.site.register(Document, DocumentAdmin)