from django.forms import ModelForm, FileField, FileInput

from documents.models import Document
from django.utils.translation import gettext_lazy as _


class DocumentUploadForm(ModelForm):
    file = FileField(
        label=_('File'),
        widget=FileInput
        )

    class Meta:
        model = Document
        fields = [
            'title', 'description', 'file',
        ]
