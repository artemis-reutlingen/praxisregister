from django.db import models
from django.utils.translation import gettext_lazy as _

from config.settings import UPLOAD_DIRECTORY
from practices.models import Practice
from users.models import User


def generate_upload_path(instance, filename):
    """
    Generates upload path for files in the Document model.
    """
    path = f"{UPLOAD_DIRECTORY}/documents/{filename}"
    return path


class Document(models.Model):
    """
    A Document model for content uploaded by users. Essentially just a FileField, enriched with a name and
    description which is provided by the user. A Document is associated with a specific practice.
    """

    class Meta:
        verbose_name = _("Document")
        verbose_name_plural = _("Documents")
        
    created_at = models.DateTimeField(auto_now_add=True)
    uploaded_by = models.ForeignKey(User, verbose_name=_('Author'), on_delete=models.SET_NULL, null=True)
    title = models.CharField(verbose_name=_('Title'), max_length=512, default="", blank=False)
    description = models.TextField(verbose_name=_('Note'), blank=True, default="")
    file = models.FileField(
        verbose_name=_('File'),
        upload_to=generate_upload_path,
        blank=False,
        null=True
    )
    practice = models.ForeignKey(
        'practices.Practice',
        on_delete=models.CASCADE,
        null=False
    )

    def __str__(self) -> str:
        edit = _("Edit document")
        new = _("New document")
        return f"{edit if self.pk else new}"

