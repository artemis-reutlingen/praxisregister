from django.urls import path
from documents import views

app_name = 'documents'

urlpatterns = [
    path('<int:pk>/download', views.download_document_file,
         name='download'),
    path('<int:pk>/edit', views.DocumentEdit.as_view(),
         name='edit'),
    path('<int:pk>/delete', views.DocumentDelete.as_view(),
         name='delete'),
]
