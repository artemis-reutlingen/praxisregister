from django.urls import path

from . import views

app_name = 'persons'
urlpatterns = [
    path('<int:pk>/', views.person_show_detail,
         name='detail'),
    path('<int:pk>/edit', views.PersonEdit.as_view(),
         name='edit'),
    path('<int:pk>/delete', views.PersonDelete.as_view(),
         name='delete'),
    path('<int:pk>/contact-times/edit', views.PersonContactTimesEdit.as_view(),
         name='edit-contact-times'),


    path('<int:pk>/make-owner', views.person_make_owner,
         name='make-owner'),
    path('<int:pk>/remove-owner', views.person_remove_owner,
         name='remove-owner'),
    path('<int:pk>/make-doctor', views.person_make_doctor,
         name='make-doctor'),
    path('<int:pk>/remove-doctor', views.person_remove_doctor,
         name='remove-doctor'),
    path('<int:pk>/make-assistant', views.person_make_assistant,
         name='make-assistant'),
    path('<int:pk>/remove-assistant', views.person_remove_assistant,
         name='remove-assistant'),

    path('<int:pk>/make-tandem-doctor', views.person_make_tandem_doctor,
         name='make-tandem-doctor'),
    path('<int:pk>/remove-tandem-doctor', views.person_remove_tandem_doctor,
         name='remove-tandem-doctor'),
    path('<int:pk>/make-tandem-assistant', views.person_make_tandem_assistant,
         name='make-tandem-assistant'),
    path('<int:pk>/remove-tandem-assistant', views.person_remove_tandem_assistant,
         name='remove-tandem-assistant'),

    path('<int:pk>/research-experiences/add', views.ResearchExperienceAdd.as_view(),
         name='research-experiences-add'),
    path('research-experiences/<int:pk>/delete', views.ResearchExperienceDelete.as_view(),
         name='research-experiences-delete'),
    path('research-experiences/<int:pk>/edit', views.ResearchExperienceEdit.as_view(),
         name='research-experiences-edit'),

    path('<int:pk>/research-trainings/add', views.ResearchTrainingAdd.as_view(),
         name='research-trainings-add'),
    path('research-trainings/<int:pk>/delete', views.ResearchTrainingDelete.as_view(),
         name='research-trainings-delete'),
    path('research-trainings/<int:pk>/edit', views.ResearchTrainingEdit.as_view(),
         name='research-trainings-edit'),
    
    path('<int:pk>/fopranet-trainings/add', views.FoPraNetTrainingAdd.as_view(),
         name='fopranet-trainings-add'),
    path('fopranet-trainings/<int:pk>/delete', views.FoPraNetTrainingDelete.as_view(),
         name='fopranet-trainings-delete'),

    path('<int:pk>/study-specific-trainings/add', views.StudySpecificTrainingAdd.as_view(),
         name='study-specific-trainings-add'),
    path('study-specific-trainings/<int:pk>/delete', views.StudySpecificTrainingDelete.as_view(),
         name='study-specific-trainings-delete'),


    path('doctors/<int:pk>/general-info/edit', views.DoctorGeneralInfoEdit.as_view(),
         name='doctor-general-info-edit'),
    path('doctors/<int:pk>/additional-trainings/add', views.DoctorAdditionalTrainingAdd.as_view(),
         name='doctor-additional-trainings-add'),
    path('doctors/additional-trainings/<int:pk>/delete', views.DoctorAdditionalTrainingDelete.as_view(),
         name='doctor-additional-trainings-delete'),
    path('doctors/additional-trainings/<int:pk>/edit', views.DoctorAdditionalTrainingEdit.as_view(),
         name='doctor-additional-trainings-edit'),

    path('doctors/<int:pk>/specialties/add', views.DoctorSpecialtyAdd.as_view(),
         name='doctor-specialties-add'),
    path('doctors/specialties/<int:pk>/delete', views.DoctorSpecialtyDelete.as_view(),
         name='doctor-specialties-delete'),
    path('doctors/specialties/<int:pk>/edit', views.DoctorSpecialtyEdit.as_view(),
         name='doctor-specialties-edit'),

    path('doctors/<int:pk>/latest-research-experience/edit', views.DoctorResearchExperienceLastYearEdit.as_view(),
         name='doctor-research-experience-last-year-edit'),


    path('assistants/<int:pk>/vocational-training/edit', views.AssistantVocationalTrainingEdit.as_view(),
         name='assistant-vocational-training-edit'),
    path('assistants/<int:pk>/qualification/add', views.AssistantQualificationAdd.as_view(),
         name='assistant-qualification-add'),
    path('assistants/qualifications/<int:pk>/delete', views.AssistantQualificationDelete.as_view(),
         name='assistant-qualification-delete'),
    path('assistants/qualifications/<int:pk>/edit', views.AssistantQualificationEdit.as_view(),
         name='assistant-qualification-edit'),

    path('assistants/<int:pk>/latest-research-experience/edit', views.AssistantResearchExperienceLastYearEdit.as_view(),
         name='assistant-research-experience-last-year-edit'),


    path('search/', views.PeopleSearch.as_view(), name="search"),
    path('htmx/person-table/', views.person_table, name='person_table'),
    path('new/', views.PersonNew.as_view(), name="new"),


]
