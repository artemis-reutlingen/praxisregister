from django.db import models
from django.utils.translation import gettext_lazy as _


class DoctorSpecialtyStatusType(models.TextChoices):
    GENERAL_CARE = 'GENERAL_CARE', _('General Care')
    INTERNAL_MEDICINE = 'INTERNAL_MEDICINE', _('Internal Medicine')
    DOCTOR_IN_TRAINING = 'DOCTOR_IN_TRAINING', _('Doctor in training')
    GENERAL_PRACTICIONER = 'GENERAL_PRACTICIONER', _('General Practicioner')
    OTHER = 'OTHER', _('Other')


class WorkFocusDesignationType(models.TextChoices):
    IN_TRAINING = 'IN_TRAINING', _('in training')
    FINISHED = 'TRAINING_FINISHED', _('training finished')
    WORK_FOCUS = 'WORK_FOCUS', _('focus of work')
    NONE = 'NONE', _('-')


class GenderType(models.TextChoices):
    UNKNOWN = 'UNKNOWN', _('unknown')
    MALE = 'MALE', _('male')
    FEMALE = 'FEMALE', _('female')
    DIVERSE = 'DIVERSE', _('diverse')

class WorkingHoursType(models.TextChoices):
    UNDER_20 = 'UNDER_20', _('Under 20')
    BETWEEN_20_AND_29 = 'BETWEEN_20_AND_29', _('Between 20 and 29')
    BETWEEN_30_AND_39 = 'BETWEEN_30_AND_39', _('Between 30 and 39')
    BETWEEN_40_AND_49 = 'BETWEEN_40_AND_49', _('Between 40 and 49')
    BETWEEN_50_AND_59 = 'BETWEEN_50_AND_59', _('Between 50 and 59')
    MORE_THAN_60 = 'MORE_THAN_60', _('More than 60')

class YesNoUnknowType(models.TextChoices):
    YES = "YES", _("Yes")
    NO = "NO", _("No")
    UNKNOWN = "UNKNOWN", _("Unknown")

class AssistantQualificationStatusType(models.TextChoices):
    IN_TRAINING = 'IN_TRAINING', _('in training')
    TRAINING_FINISHED = 'TRAINING_FINISHED', _('training finished')
    WORK_FOCUS = 'WORK_FOCUS', _('focus of work')