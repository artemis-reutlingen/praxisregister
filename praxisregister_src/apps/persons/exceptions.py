class RoleAlreadyAssigned(Exception):
    pass


class RoleNotAssigned(Exception):
    pass


class MutualExclusiveRolesException(Exception):
    pass

class BadRole(Exception):
    pass