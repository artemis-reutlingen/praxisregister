from django.db import models
from django.utils.translation import gettext_lazy as _
from persons.types import AssistantQualificationStatusType
from importer.models import ImportableMixin

class ResearchExperience(ImportableMixin):
    """
    Represents a ResearchExperience, associated currently with a person.
    Type field is CharField - neither ForeignKey nor limited to text choices in order to allow users to enter free text.
    Associated forms will still give the user pre-defined options based on ResearchExperienceOption model.
    """
    type = models.CharField(verbose_name="Type", default="", blank=True, max_length=512, )
    person = models.ForeignKey(
        'persons.Person',
        verbose_name=_('Person'),
        related_name="research_experiences",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self) -> str:
        return f"{_('Research Experiences')}"
    
    class Meta:
        verbose_name = _("Research Experience")
        verbose_name_plural = _("Research Experiences")


class ResearchTraining(ImportableMixin):
    """
    Represents a ResearchTraining, associated with a single Person. A Person can have multiple ResearchTrainings.
    Type field is CharField - neither ForeignKey nor limited to text choices in order to allow users to enter free text.
    Associated forms will still give the user pre-defined options based on ResearchExperienceOption model.
    """
    type = models.CharField(verbose_name="Type", default="", blank=True, max_length=512, )
    person = models.ForeignKey(
        'persons.Person',
        verbose_name=_('Person'),
        related_name="research_trainings",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self) -> str:
        return f"{_('Certified Research Trainings')}"
    
    class Meta:
        verbose_name = _("Certified Research Training")
        verbose_name_plural = _("Certified Research Trainings")

class FoPraNetTraining(models.Model):
    """
    Represents a FoPraNetTraining, associated with a single Person. A Person can have multiple FoPraNetTraining.
    """
    type = models.ForeignKey(
        'persons.FoPraNetTrainingOption',
        verbose_name="Type",
        on_delete=models.CASCADE,
    )

    person = models.ForeignKey(
        'persons.Person',
        verbose_name=_('Person'),
        related_name="fopranet_trainings",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = _("FoPraNet-BW Training")
        verbose_name_plural = _("FoPraNet-BW Trainings")
        unique_together = ('type', 'person')

    def __str__(self) -> str:
        return f"{_('FoPraNet-BW Trainings')}"


class StudySpecificTraining(models.Model):
    """
    Represents a StudySpecificTraining, associated with a single Person.
    A Person can have multiple StudySpecificTrainings.
    """
    type = models.ForeignKey(
        'persons.StudySpecificTrainingOption',
        verbose_name="Type",
        on_delete=models.CASCADE,
    )

    person = models.ForeignKey(
        'persons.Person',
        verbose_name=_('Person'),
        related_name="study_specific_trainings",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = _("Study-specific Training")
        verbose_name_plural = _("Study-specific Trainings")
        unique_together = ('type', 'person')

    def __str__(self) -> str:
        return f"{_('Study-specific Trainings')}"


class DoctorAdditionalTraining(models.Model):
    """
    Represents additional trainings for DOCTORs. A DOCTOR can have multiple additional trainings.
    """

    class StatusType(models.TextChoices):
        IN_TRAINING = 'IN_TRAINING', _('in training')
        TRAINING_FINISHED = 'TRAINING_FINISHED', _('training finished')
        WORK_FOCUS = 'WORK_FOCUS', _('focus of work')

    type = models.CharField(verbose_name="Type", default="", blank=True, max_length=512, )

    status = models.CharField(
        verbose_name=_('Status'),
        max_length=128,
        choices=StatusType.choices,
        default=StatusType.TRAINING_FINISHED,
        blank=False)

    doctor_role = models.ForeignKey(
        'persons.DoctorRole',
        verbose_name=_('Doctor Role'),
        related_name="additional_trainings",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self):
        return f"{_('Additional Training')}"


class DoctorSpecialty(ImportableMixin, models.Model):
    """
    Represents additional specialties (Fachrichtung) for DOCTORs. A DOCTOR can have multiple specialties.
    """

    class StatusType(models.TextChoices):
        IN_TRAINING = 'IN_TRAINING', _('in training')
        TRAINING_FINISHED = 'TRAINING_FINISHED', _('training finished')

    type = models.CharField(verbose_name="Type", default="", blank=True, max_length=512, )

    status = models.CharField(
        verbose_name=_('Status'),
        max_length=128,
        choices=StatusType.choices,
        default=StatusType.TRAINING_FINISHED,
        blank=False)

    doctor_role = models.ForeignKey(
        'persons.DoctorRole',
        verbose_name=_('Doctor Role'),
        related_name="specialties",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self):
        return f"{_('Specialties')}"


class AssistantAdditionalQualification(ImportableMixin, models.Model):
    """
    Represents a single additional qualification for persons with ASSISTANT role.
    """

    assistant_role = models.ForeignKey(
        'persons.AssistantRole',
        verbose_name=_('Assistant Role'),
        related_name="additional_qualifications",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    type = models.CharField(verbose_name="Type", default="", blank=True, max_length=512, )

    status = models.CharField(
        verbose_name=_('Status'),
        max_length=128,
        choices=AssistantQualificationStatusType.choices,
        default=AssistantQualificationStatusType.TRAINING_FINISHED,
        blank=False
    )

    def __str__(self) -> str:
        return f"{_('Additional Qualifications')}"
