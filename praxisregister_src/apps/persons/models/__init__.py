from .person import Person
from .roles import OwnerRole, DoctorRole, AssistantRole
from .additionals import ResearchExperience, FoPraNetTraining, ResearchTraining, DoctorAdditionalTraining, \
    AssistantAdditionalQualification
from .options import AssistantVocationalTrainingOption, AssistantAdditionalQualificationOption, \
    FoPraNetTrainingOption, ResearchTrainingOption, ResearchExperienceOption, DoctorAdditionalTrainingOption, StudySpecificTrainingOption
