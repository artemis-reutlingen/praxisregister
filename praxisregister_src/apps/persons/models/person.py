from django.contrib.contenttypes.fields import GenericRelation
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _, pgettext_lazy
from guardian.shortcuts import (assign_perm, get_objects_for_user, get_objects_for_group)
from guardian.models import UserObjectPermissionBase, GroupObjectPermissionBase
from persons.constants import RoleActions, Roles, WorkRelationshipType
from persons.exceptions import (BadRole, MutualExclusiveRolesException,
                                RoleAlreadyAssigned, RoleNotAssigned)
from persons.models.roles import AssistantRole, DoctorRole, OwnerRole
from persons.types import GenderType, WorkingHoursType, YesNoUnknowType
from practices.models import Practice
from track_changes.models import TrackChangesMixin
from importer.models import ImportableMixin

class PersonManager(models.Manager):

    def get_person_ids_for_user(self, user):
        return self.get_person_objects_for_user(user).values_list('id', flat=True)

    def get_person_objects_for_user(self, user):
        """
        Fetches person objects based on users's permissions / group membership.
        """
        # Get practices for user
        practice_ids = Practice.objects.get_practice_ids_for_user(user)
        persons_practice = self.filter(practice_id__in=practice_ids).values_list("id", flat=True)
        # Add persons without a practice
        persons_no_practice = get_objects_for_user(user, 'persons.manage_person').filter(practice__isnull=True).values_list("id", flat=True)
        #not using union on a queryset here, because of further query restrictions
        return self.filter(id__in=list(set(persons_practice) | set(persons_no_practice)))

    def get_person_objects_for_group(self, group):
        return get_objects_for_group(group, 'persons.manage_person')

    def create_person_for_practice(self, first_name, last_name, practice):
        person = self.create(first_name=first_name, last_name=last_name, practice=practice)
        person.assign_perms(institute=practice.assigned_institute)
        return person

class Person(TrackChangesMixin, ImportableMixin, models.Model):
    """
    Main person model. Has associations (OneToOneFields) with AssistantRole, DoctorRole and OwnerRole.
    """

    class Meta:
        verbose_name = _("Person")
        verbose_name_plural = _("Persons")
        permissions = (
            ('manage_person', 'Manage person'),
        )

    objects = PersonManager()
    practice = models.ForeignKey('practices.Practice', verbose_name=_("Practice"), on_delete=models.SET_NULL, null=True)

    gender = models.CharField(
        verbose_name=_("Gender"),
        max_length=128,
        choices=GenderType.choices,
        default=GenderType.UNKNOWN,
        blank=False,
    )

    year_of_birth = models.CharField(verbose_name=_('Year of birth'), max_length=128, blank=True, null=True, default="")

    title = models.CharField(verbose_name=_('Title'), max_length=256, blank=True, null=True, default="")
    first_name = models.CharField(verbose_name=_("First name"), max_length=256, blank=True, null=True, default="")
    last_name = models.CharField(verbose_name=_("Last name"), max_length=256, blank=True, null=True, default="")
    former_name = models.CharField(verbose_name=_("Former name"), max_length=256, blank=True, null=True, default="")

    contact_times = GenericRelation(
        "contacttimes.DayContactTimeSlot",
        verbose_name = _("Contact times"),
        related_query_name = "person"
    )

    email = models.EmailField(verbose_name=_('E-Mail'), max_length=255, null=True, blank=False)
    phone = models.CharField(verbose_name=_('Phone'), max_length=255, null=True, blank=False)

    estimated_weekly_working_hours = models.CharField(verbose_name=_("Estimated weekly working hours"), max_length=128, default="", blank=True, choices=WorkingHoursType.choices)
    weekly_working_hours = models.DecimalField(verbose_name=_('Contractual weekly working hours'),
                                               default=0.0, max_digits=5, decimal_places=2, blank=True)
    starting_year_in_gp_care = models.IntegerField(verbose_name=_('Starting year in GP care'), blank=True, null=True, validators=[MinValueValidator(1900), MaxValueValidator(2200)])

    work_relationship_type = models.CharField(
        verbose_name=_("Work relationship"),
        max_length=256,
        default="",
        blank=False,
    )

    questionnaire_date = models.DateField(verbose_name=_('Questionnaire date'), blank=True, null=True)
    involved_in_research_projects = models.CharField(verbose_name=_("Involved in research projects"), default=YesNoUnknowType.UNKNOWN, max_length=32, choices=YesNoUnknowType.choices,)

    def assign_perms(self, institute):
        """
        Granting permissions for assigned_institute. Called when creating/initializing a person.
        """
        assign_perm('persons.manage_person', institute.group, self)

    @property
    def contact_times_ordered(self):
        """
        Helper to have contact times in the right order in template.
        Using ID to order works because of initial create order of contact time objects.
        """
        return self.contact_times.all().order_by('id')

    def make_owner(self):
        """ Create OwnerRole and update work_relationship_type """
        if self.is_owner:
            raise RoleAlreadyAssigned(
                f"Tried to create {OwnerRole.__name__} for person model but already has this role set.")
        if self.practice:
            OwnerRole.objects.create(person=self)
            self.work_relationship_type = WorkRelationshipType.OWNER.label
            self.save()

    def remove_owner(self):
        if not self.role_owner:
            raise RoleNotAssigned(f"Tried to remove unassigned role for person model")
        if self.practice:
            self.role_owner.delete()
            self.role_owner = None
            self.work_relationship_type = ""
            self.save()

    def make_doctor(self):
        if self.is_doctor:
            raise RoleAlreadyAssigned(
                f"Tried to create {DoctorRole.__name__} for person model but already has this role set.")
        if self.is_assistant:
            raise MutualExclusiveRolesException(f"Tried to create role doctor for person but already is assistant.")
        DoctorRole.objects.create(person=self)

    def remove_doctor(self):
        if not self.role_doctor:
            raise RoleNotAssigned(f"Tried to remove unassigned role for person model")
        self.role_doctor.delete()
        self.role_doctor = None
        self.save()

    def make_assistant(self):
        if self.is_assistant:
            raise RoleAlreadyAssigned(
                f"Tried to create {AssistantRole.__name__} for person model but already has this role set.")
        if self.is_doctor:
            raise MutualExclusiveRolesException(f"Tried to create role assistant for person but already is doctor.")
        AssistantRole.objects.create(person=self)

    def remove_assistant(self):
        if not self.role_assistant:
            raise RoleNotAssigned(f"Tried to remove unassigned role for person model")
        self.role_assistant.delete()
        self.role_assistant = None
        self.save()

    def make_tandem_doctor(self):
        if self.practice:
            if self.practice.research_doctor:
                raise RoleAlreadyAssigned("Practice model already has research doctor set.")
            if not self.role_doctor:
                raise BadRole("Person must have role doctor to be tandem doctor")
            self.practice.research_doctor = self
            self.practice.save()

    def remove_tandem_doctor(self):
        if self.practice:
            if not self == self.practice.research_doctor:
                raise RoleNotAssigned(f"Tried to remove role tandem doctor, but person is not tandem doctor")
            self.practice.research_doctor = None
            self.practice.save()

    def make_tandem_assistant(self):
        if self.practice:
            if self.practice.research_assistant:
                raise RoleAlreadyAssigned("Practice model already has research assistant set.")
            if not self.role_assistant:
                raise BadRole("Person must have role assistant to be tandem assistant")
            self.practice.research_assistant = self
            self.practice.save()

    def remove_tandem_assistant(self):
        if self.practice:
            if not self == self.practice.research_assistant:
                raise RoleNotAssigned(f"Tried to remove role tandem assistant, but person is not tandem assistant")
            self.practice.research_assistant = None
            self.practice.save()

    @property
    def is_tandem_doctor(self):
        return self.practice and self == self.practice.research_doctor

    @property
    def is_tandem_assistant(self):
        return self.practice and self == self.practice.research_assistant

    @property
    def is_owner(self):
        return hasattr(self, "role_owner")

    @property
    def is_doctor(self):
        return hasattr(self, "role_doctor")

    @property
    def is_assistant(self):
        return hasattr(self, "role_assistant")

    @property
    def allowed_role_actions(self):
        actions = []
        if not self.is_owner:
            # maybe also add condition !role_assistant here... assumptions
            actions.append(RoleActions.MAKE_OWNER.name)
        else:
            actions.append(RoleActions.REMOVE_OWNER.name)

        if not self.is_doctor:
            if not self.is_assistant:
                actions.append(RoleActions.MAKE_DOCTOR.name)
        else:
            if not self.is_tandem_doctor:
                actions.append(RoleActions.REMOVE_DOCTOR.name)
                if self.practice and not self.practice.research_doctor:
                    actions.append(RoleActions.MAKE_TANDEM_DOCTOR.name)
            else:
                actions.append(RoleActions.REMOVE_TANDEM_DOCTOR.name)

        if not self.is_assistant:
            if not self.is_doctor:
                actions.append(RoleActions.MAKE_ASSISTANT.name)
        else:
            if not self.is_tandem_assistant:
                actions.append(RoleActions.REMOVE_ASSISTANT.name)
                if self.practice and not self.practice.research_assistant:
                    actions.append(RoleActions.MAKE_TANDEM_ASSISTANT.name)
            else:
                actions.append(RoleActions.REMOVE_TANDEM_ASSISTANT.name)

        return actions

    @property
    def roles(self):
        roles = []
        if self.is_owner:
            roles.append(Roles.OWNER)
        if self.is_doctor:
            roles.append(Roles.DOCTOR)
        if self.is_assistant:
            roles.append(Roles.ASSISTANT)
        if self.practice:
            if self.practice.research_doctor == self:
                roles.append(Roles.TANDEM_DOCTOR)
            if self.practice.research_assistant == self:
                roles.append(Roles.TANDEM_ASSISTANT)
        return roles

    @property
    def work_address(self):
        return self.practice.main_address if self.practice else "-"

    @property
    def full_name(self):
        full_name = f'{self.first_name if self.first_name else ""} {self.last_name if self.last_name else ""}'.strip()
        return full_name if (full_name and full_name != "") else _("Unnamed Person")

    @property
    def full_name_with_title(self):
        title = self.title if self.title else ""
        return f"{title} {self.full_name}".strip()
    
    @property
    def form_of_address_gender(self):
        male, female = _("Mr."), _("Ms.")
        if self.gender == GenderType.MALE:
            return f"{male}"
        elif self.gender == GenderType.FEMALE:
            return f"{female}"
        elif self.gender == GenderType.DIVERSE:
            return ""
        
    @property
    def form_of_address_formal(self):
        male, female, diverse = pgettext_lazy("male", "Dear"), pgettext_lazy("female", "Dear"), pgettext_lazy("diverse", "Dear")
        if self.gender == GenderType.MALE:
            return f"{male}"
        elif self.gender == GenderType.FEMALE:
            return f"{female}"
        elif self.gender == GenderType.DIVERSE:
            return f"{diverse}"

    @property
    def form_of_address(self):
        return f"{self.form_of_address_formal}" if self.form_of_address_gender == "" else f"{self.form_of_address_formal} {self.form_of_address_gender}"

    # Generalization of title (e.g. Prof. Dr. med -> Prof.)
    @property
    def addressing_title(self):
        if self.title:
            if "Prof." in self.title:
                return "Prof."
            if "Dr." in self.title:
                return "Dr."
        return ""
    
    def __str__(self) -> str:
        return self.full_name_with_title

# Explicit definition of Person object permissions. This ensures permission objects are also deleted when a person is deleted instead of being orphaned.
class PersonUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(Person, on_delete = models.CASCADE)

class PersonGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(Person, on_delete = models.CASCADE)