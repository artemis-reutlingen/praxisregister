from django.db import models


class AbstractChoiceOption(models.Model):
    """
    Abstract Option model. Just encapsulating a simple name attribute, but allowing DB driven management
    of data-list / choices style behavior.
    """
    name = models.CharField(max_length=256, blank=False, default="", unique=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class ResearchExperienceOption(AbstractChoiceOption):
    """
    Represents a single option as used for type field in AbstractResearchExperience.
    """


class DoctorAdditionalTrainingOption(AbstractChoiceOption):
    """
    Represents a single option as used in the DoctorAdditionalTraining model.
    """


class DoctorSpecialtyOption(AbstractChoiceOption):
    """
    Represents a single option as used in the DoctorSpecialty model / view / form.
    """


class AssistantVocationalTrainingOption(AbstractChoiceOption):
    """
    Represents a single option for vocational_training_type field on AssistantRole model.
    """


class AssistantAdditionalQualificationOption(AbstractChoiceOption):
    """
    Represents options for the ForeignKey field "type" on the AssistantAdditionalQualification model.
    """


class ResearchTrainingOption(AbstractChoiceOption):
    """
    Represents options for pre-defined research trainings that user can choose for persons.
    """


class FoPraNetTrainingOption(AbstractChoiceOption):
    """
    Represents options for pre-defined trainings specifically for FoPraNet-BW that users can choose for persons.
    """


class StudySpecificTrainingOption(AbstractChoiceOption):
    """
    Represents options for pre-defined study-specific trainings specifically for
    FoPraNet-BW that users can choose for persons.
    """
