from django import forms
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import UpdateView

from persons.models import DoctorAdditionalTraining, DoctorAdditionalTrainingOption, AssistantAdditionalQualification, \
    AssistantAdditionalQualificationOption, ResearchExperienceOption, ResearchExperience, ResearchTraining, \
    ResearchTrainingOption
from persons.models.additionals import DoctorSpecialty
from persons.models.options import DoctorSpecialtyOption
from persons.models.person import Person
from utils.constants import DATALIST_INPUT_HELP_TEXT
from utils.fields import ListTextWidget
from utils.view_mixins import ManagePracticePermissionRequired
from persons.types import YesNoUnknowType


class RoleUpdateMixin(ManagePracticePermissionRequired, UpdateView):
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        return self.get_object().person.practice

    def get_success_url(self):
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().person.id})


class UseDoctorAdditionalTrainingFormMixin:
    """
    Mixin for DoctorAdditionalTrainingForm injecting into Create-/UpdateView for DoctorAdditionalTraining models.
    """

    class DoctorAdditionalTrainingForm(forms.ModelForm):
        type = forms.CharField(
            required=True,
            help_text=DATALIST_INPUT_HELP_TEXT
        )

        def __init__(self, *args, **kwargs):
            _type_choices = kwargs.pop('data_list', None)
            super().__init__(*args, **kwargs)
            self.fields['type'].widget = ListTextWidget(
                data_list=_type_choices,
                name='training-type-choices',
            )
            self.fields['type'].label = _('Training type')

        class Meta:
            model = DoctorAdditionalTraining
            fields = ['type', 'status']

    form_class = DoctorAdditionalTrainingForm

    def get_form_kwargs(self):
        training_type_choices = [
            option.name for option in DoctorAdditionalTrainingOption.objects.all().order_by('name')
        ]
        kwargs = super().get_form_kwargs()
        kwargs['data_list'] = training_type_choices
        return kwargs


class UseDoctorSpecialtyFormMixin:
    """
    Mixin for DoctorSpecialtyForm injecting into Create-/UpdateView for DoctorSpecialty models.
    """

    class DoctorSpecialtyForm(forms.ModelForm):
        type = forms.CharField(
            required=True,
            help_text=DATALIST_INPUT_HELP_TEXT
        )

        def __init__(self, *args, **kwargs):
            _type_choices = kwargs.pop('data_list', None)
            super().__init__(*args, **kwargs)
            self.fields['type'].widget = ListTextWidget(
                data_list=_type_choices,
                name='specialty-type-choices',
            )
            self.fields['type'].label = _('Specialty type')

        class Meta:
            model = DoctorSpecialty
            fields = ['type', 'status']

    form_class = DoctorSpecialtyForm

    def get_form_kwargs(self):
        specialty_type_choices = [
            option.name for option in DoctorSpecialtyOption.objects.all().order_by('name')
        ]
        kwargs = super().get_form_kwargs()
        kwargs['data_list'] = specialty_type_choices
        return kwargs


class UseAssistantQualificationFormMixin:
    """
    Mixin for AssistantQualificationForm injecting into Create-/UpdateView for AssistantAdditionalQualification models.
    """

    class AssistantQualificationForm(forms.ModelForm):
        type = forms.CharField(
            required=True,
            help_text=DATALIST_INPUT_HELP_TEXT
        )

        def __init__(self, *args, **kwargs):
            _type_choices = kwargs.pop('data_list', None)
            super().__init__(*args, **kwargs)
            self.fields['type'].widget = ListTextWidget(
                data_list=_type_choices,
                name='qualification-type-choices',
            )
            self.fields['type'].label = _('Qualification type')

        class Meta:
            model = AssistantAdditionalQualification
            fields = ['type', 'status']

    form_class = AssistantQualificationForm

    def get_form_kwargs(self):
        qualification_type_choices = [
            option.name for option in AssistantAdditionalQualificationOption.objects.all().order_by('name')
        ]
        kwargs = super().get_form_kwargs()
        kwargs['data_list'] = qualification_type_choices
        return kwargs


class UsePersonResearchExperienceFormMixin:
    """
    Mixin for injecting PersonResearchExperienceForm into Create-/UpdateView for PersonResearchExperience models.
    """

    class ResearchExperienceForm(forms.ModelForm):

        involved_in_research_projects = forms.ChoiceField(label=_("Involved in research projects"), choices=YesNoUnknowType.choices)
        type = forms.CharField(
            required=False,
            help_text=DATALIST_INPUT_HELP_TEXT
        )

        def __init__(self, *args, **kwargs):
            _type_choices = kwargs.pop('data_list', None)
            super().__init__(*args, **kwargs)
            self.fields['type'].widget = ListTextWidget(
                data_list=_type_choices,
                name='experience-type-choices',
            )
            self.fields['type'].label = _('Experience type')

        class Meta:
            model = ResearchExperience
            fields = ['involved_in_research_projects', 'type']

    form_class = ResearchExperienceForm

    def get_form_kwargs(self):
        research_experience_type_choices = [
            option.name for option in ResearchExperienceOption.objects.all().order_by('name')
        ]
        kwargs = super().get_form_kwargs()
        kwargs['data_list'] = research_experience_type_choices
        kwargs['initial'] = {'involved_in_research_projects': Person.objects.get(pk=self.kwargs['pk']).involved_in_research_projects}
        return kwargs


class UsePersonResearchTrainingFormMixin:
    """
    Mixin for injecting PersonResearchTrainingForm into Create-/UpdateView for ResearchTraining models.
    """

    class ResearchTrainingForm(forms.ModelForm):
        type = forms.CharField(
            required=True,
            help_text=DATALIST_INPUT_HELP_TEXT
        )

        def __init__(self, *args, **kwargs):
            _type_choices = kwargs.pop('data_list', None)
            super().__init__(*args, **kwargs)
            self.fields['type'].widget = ListTextWidget(
                data_list=_type_choices,
                name='training-type-choices',
            )
            self.fields['type'].label = _('Training type')

        class Meta:
            model = ResearchTraining
            fields = ['type']

    form_class = ResearchTrainingForm

    def get_form_kwargs(self):
        research_training_type_choices = [
            option.name for option in ResearchTrainingOption.objects.all().order_by('name')
        ]
        kwargs = super().get_form_kwargs()
        kwargs['data_list'] = research_training_type_choices
        return kwargs
