from django.test import TestCase
from persons.models import Person, StudySpecificTrainingOption, FoPraNetTraining, FoPraNetTrainingOption
from persons.forms import StudySpecificTrainingForm, FoPraNetTrainingForm
from persons.models.additionals import StudySpecificTraining


class TestPersonForms(TestCase):

    def setUp(self) -> None:
        self.person = Person.objects.create(
            first_name="test",
            last_name="user",
        )

    def test_study_specific_training_form(self):
        option = StudySpecificTrainingOption.objects.create(name="testoption")
        option.save()
        data = {'type': 1}
        # Check if form is valid
        form = StudySpecificTrainingForm(data=data, person=self.person)
        self.assertTrue(form.is_valid())
        # Link training option to person
        training = StudySpecificTraining.objects.create(type=option, person=self.person)
        training.save()
        self.person.study_specific_trainings.add(training)
        # Form should not be valid because person already has this option
        form = StudySpecificTrainingForm(data=data, person=self.person)
        self.assertFalse(form.is_valid())

    def test_fopranet_training_form(self):
        option = FoPraNetTrainingOption.objects.create(name="testoption")
        option.save()
        data = {'type': 1}
        # Check if form is valid
        form = FoPraNetTrainingForm(data=data, person=self.person)
        self.assertTrue(form.is_valid())
        # Link training option to person
        training = FoPraNetTraining.objects.create(type=option, person=self.person)
        training.save()
        self.person.fopranet_trainings.add(training)
        # Form should not be valid because person already has this option
        form = FoPraNetTrainingForm(data=data, person=self.person)
        self.assertFalse(form.is_valid())