from django.test import TestCase
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy
from persons.constants import RoleActions, Roles, WorkRelationshipType
from persons.exceptions import RoleAlreadyAssigned
from persons.models import Person, ResearchExperience
from persons.types import GenderType
from practices.models import Practice


class PersonModelAttributesTest(TestCase):
    def test_person_has_weekly_working_hours(self):
        self.practice = Practice.objects.create(name="Not Really A Place To Be")

        try:
            self.random_employee = Person.objects.create(
                practice=self.practice,
                first_name="HP",
                last_name="Baxxter",
                weekly_working_hours=40
            )
        except AssertionError:
            self.fail()


class PersonsModelTest(TestCase):
    def setUp(self) -> None:
        self.practice = Practice.objects.create(name="RandomPractice in the City")

        self.professor = Person(
            practice=self.practice,
            title="Prof. Dr. med.",
            first_name="Max",
            last_name="Mustermann",
            gender=GenderType.MALE,

        )

        self.doc_med = Person(
            practice=self.practice,
            title="Dr. med.",
            first_name="Max",
            last_name="Mustermann",
            gender=GenderType.MALE,

        )

        self.female_assistant = Person(
            practice=self.practice,
            first_name="Martina",
            last_name="Müllerfrau",
            gender=GenderType.FEMALE,
        )

        self.doc_med_diverse = Person(
            practice=self.practice,
            title="Dr. med.",
            first_name="Curly",
            last_name="Schmitt",
            gender=GenderType.DIVERSE,
        )

    def test_full_name(self):
        self.assertEqual(self.professor.full_name, "Max Mustermann")



    def test_salutation(self):
        s_male, sfemale, sdiverse = pgettext_lazy("male", "Dear"), pgettext_lazy("female", "Dear"), pgettext_lazy("diverse", "Dear")
        male, female = _("Mr."), _("Ms.")
        self.assertEqual(self.professor.form_of_address, f"{s_male} {male}")
        self.assertEqual(self.doc_med.form_of_address, f"{s_male} {male}")
        self.assertEqual(self.female_assistant.form_of_address, f"{sfemale} {female}")
        self.assertEqual(self.doc_med_diverse.form_of_address, f"{sdiverse}")

    def test_full_name_with_title(self):
        self.assertEqual(self.professor.full_name_with_title, "Prof. Dr. med. Max Mustermann")


class ResearchTandemTest(TestCase):
    def setUp(self) -> None:
        self.practice = Practice.objects.create(name="A Nice Practice With Researchers")
        self.doc = Person.objects.create(
            practice=self.practice,
            title="Dr. med.",
            first_name="Fritz",
            last_name="Forscher",
            gender=GenderType.MALE,
        )
        self.doc.make_doctor()

        self.assistant = Person.objects.create(
            practice=self.practice,
            first_name="Astrid",
            last_name="Astrogirl",
            gender=GenderType.FEMALE,
        )
        self.assistant.make_assistant()

    def test_make_tandem_role_is_in_allowed_actions(self):
        self.assertIn(RoleActions.MAKE_TANDEM_DOCTOR.name, self.doc.allowed_role_actions)
        self.assertIn(RoleActions.MAKE_TANDEM_ASSISTANT.name, self.assistant.allowed_role_actions)

    def test_make_tandem_role_not_in_allowed_actions_because_tandem_already_set(self):
        self.practice.research_doctor = Person.objects.create(first_name="WasHere", last_name="Before")
        self.practice.research_assistant = Person.objects.create(first_name="WasHere", last_name="Before")

        self.assertNotIn(RoleActions.MAKE_TANDEM_DOCTOR.name, self.doc.allowed_role_actions)
        self.assertNotIn(RoleActions.MAKE_TANDEM_ASSISTANT.name, self.assistant.allowed_role_actions)

    def test_make_tandem_role_not_in_allowed_actions_because_wrong_base_roles(self):
        self.doc.remove_doctor()
        self.assistant.remove_assistant()
        self.assertNotIn(RoleActions.MAKE_TANDEM_DOCTOR.name, self.doc.allowed_role_actions)
        self.assertNotIn(RoleActions.MAKE_TANDEM_ASSISTANT.name, self.assistant.allowed_role_actions)

    def test_remove_tandem_role_in_allowed_actions(self):
        self.practice.research_doctor = self.doc
        self.practice.research_assistant = self.assistant

        self.assertIn(RoleActions.REMOVE_TANDEM_DOCTOR.name, self.doc.allowed_role_actions)
        self.assertIn(RoleActions.REMOVE_TANDEM_ASSISTANT.name, self.assistant.allowed_role_actions)

    def test_remove_role(self):
        self.practice.research_doctor = self.doc
        self.practice.research_assistant = self.assistant

        self.doc.remove_tandem_doctor()
        self.assistant.remove_tandem_assistant()

        self.assertIsNone(self.practice.research_doctor)
        self.assertIsNone(self.practice.research_assistant)
        self.assertNotIn(Roles.TANDEM_DOCTOR, self.doc.roles)
        self.assertNotIn(Roles.TANDEM_ASSISTANT, self.assistant.roles)

    def test_tandem_role_not_listed_in_roles(self):
        self.assertNotIn(Roles.TANDEM_DOCTOR, self.doc.roles)
        self.assertNotIn(Roles.TANDEM_ASSISTANT, self.assistant.roles)

    def test_tandem_role_is_listed_in_roles(self):
        self.practice.research_doctor = self.doc
        self.practice.research_assistant = self.assistant

        self.assertIn(Roles.TANDEM_DOCTOR, self.doc.roles)
        self.assertIn(Roles.TANDEM_ASSISTANT, self.assistant.roles)

    def test_make_person_tandem_role_if_none_set_yet(self):
        self.doc.make_tandem_doctor()
        self.assertIn(Roles.TANDEM_DOCTOR, self.doc.roles)

        self.assistant.make_tandem_assistant()
        self.assertIn(Roles.TANDEM_ASSISTANT, self.assistant.roles)

    def test_make_person_tandem_role_if_already_set(self):
        self.practice.research_doctor = Person.objects.create(first_name="Aetsch", last_name="Baetsch")
        self.practice.research_assistant = Person.objects.create(first_name="Aetsch", last_name="Baetsch")

        self.assertRaises(RoleAlreadyAssigned, self.doc.make_tandem_doctor)
        self.assertRaises(RoleAlreadyAssigned, self.assistant.make_tandem_assistant)

    def test_make_and_remove_owner_sets_work_relation_ship_attribute(self):
        self.assertEqual(self.doc.work_relationship_type, "")
        self.doc.make_owner()
        self.assertEqual(self.doc.work_relationship_type, WorkRelationshipType.OWNER.label)
        self.doc.remove_owner()
        self.assertEqual(self.doc.work_relationship_type, "")


class ResearchExperienceTests(TestCase):
    def setUp(self) -> None:
        self.practice = Practice.objects.create(name="RandomPractice in the City")
        self.person = Person.objects.create(
            practice=self.practice,
            title="Prof. Dr. med.",
            first_name="Max",
            last_name="Mustermann",
            gender=GenderType.MALE,
        )

    def test_assigning_and_removing_roles_for_persons_is_idempotent_for_research_experiences(self):
        """
        Test was introduced because of a "wrong direction" of OneToOne relations and unintended delete cascades.
        """

        ResearchExperience.objects.create(person=self.person, type="Pharma")

        self.assertIn("Pharma", [exp.type for exp in self.person.research_experiences.all()])
        self.person.make_owner()
        self.assertIn("Pharma", [exp.type for exp in self.person.research_experiences.all()])
        self.person.remove_owner()
        self.assertIn("Pharma", [exp.type for exp in self.person.research_experiences.all()])
