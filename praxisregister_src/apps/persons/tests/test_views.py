from django.test import TestCase, Client
from django.urls import reverse

from persons.constants import Roles
from persons.models import Person
from persons.types import GenderType
from persons.exceptions import RoleNotAssigned
from persons.models.roles import DoctorRole
from persons.models.additionals import DoctorSpecialty
from utils.utils_test import setup_test_basics


class PersonsViewsTest(TestCase):
    def setUp(self) -> None:
        self.institute, self.group, self.user, [self.practice_1, self.practice_2] = setup_test_basics()
        self.client = Client()
        self.client.login(username='tester', password='password')

    def test_persons_page_available(self):
        response = self.client.get(reverse('practices:persons', kwargs={"pk": self.practice_1.id}))
        self.assertEqual(response.status_code, 200)

    def test_add_person(self):
        data = {
            "first_name": "Max",
            "last_name": "Mustermann",
            "gender": GenderType.MALE,
            "year_of_birth": 1976,
            "phone": "1234",
            "email": "max.mustermann@praxis.de",
        }
        self.client.post(reverse('practices:person-add', kwargs={"pk": self.practice_1.id}), data=data)
        self.assertEqual(Person.objects.count(), 1)

    def test_practice_in_person_detail_context(self):
        p = Person.objects.create(
            practice=self.practice_1,
            first_name="Max",
            last_name="Maxmann",
            gender=GenderType.MALE,
        )
        response = self.client.get(reverse('persons:detail', kwargs={"pk": p.id}))
        self.assertEqual(self.practice_1, response.context['practice'])

    def test_delete_person(self):
        p = Person.objects.create(
            practice=self.practice_1,
            first_name="Toby",
            last_name="ToBeDeleted",
            gender=GenderType.MALE,
        )
        self.assertEqual(Person.objects.count(), 1)
        response = self.client.post(reverse('persons:delete', kwargs={"pk": p.id}))
        self.assertEqual(Person.objects.count(), 0)

    def test_make_and_remove_owner_view(self):
        p = Person.objects.create(
            practice=self.practice_1,
            first_name="King",
            last_name="Kool",
            gender=GenderType.DIVERSE,
        )
        self.client.post(reverse('persons:make-owner', kwargs={"pk": p.id}))
        self.assertIn(p, self.practice_1.owners)
        response = self.client.post(reverse('persons:make-owner', kwargs={"pk": p.id}))
        #expect RoleAlreadyAssignedError
        self.assertEqual(response.status_code, 400)

        self.client.post(reverse('persons:remove-owner', kwargs={"pk": p.id}))
        self.assertNotIn(p, self.practice_1.owners)

    def test_make_and_remove_doctor_view(self):
        p = Person.objects.create(
            practice=self.practice_1,
            first_name="Doctor",
            last_name="Good",
            gender=GenderType.MALE,
        )
        response = self.client.post(reverse('persons:make-doctor', kwargs={"pk": p.id}))
        p.refresh_from_db()
        self.assertIn(Roles.DOCTOR, p.roles)

        response = self.client.post(reverse('persons:remove-doctor', kwargs={"pk": p.id}))
        p.refresh_from_db()
        self.assertNotIn("Doctor", p.roles)

    def test_make_and_remove_assistant_view(self):
        p = Person.objects.create(
            practice=self.practice_1,
            first_name="Assi",
            last_name="Alexa",
            gender=GenderType.MALE,
        )
        response = self.client.post(reverse('persons:make-assistant', kwargs={"pk": p.id}))
        p.refresh_from_db()
        self.assertIn(Roles.ASSISTANT, p.roles)

        response = self.client.post(reverse('persons:remove-assistant', kwargs={"pk": p.id}))
        p.refresh_from_db()
        self.assertNotIn(Roles.ASSISTANT, p.roles)

    def test_make_and_remove_tandem_assistant_view(self):
        p = Person.objects.create(
            practice=self.practice_1,
            first_name="TandemTamara",
            last_name="Turbotussi",
            gender=GenderType.FEMALE,
        )
        p.make_assistant()
        response = self.client.post(reverse('persons:make-tandem-assistant', kwargs={"pk": p.id}))
        p.refresh_from_db()
        self.assertIn(Roles.TANDEM_ASSISTANT, p.roles)

        response = self.client.post(reverse('persons:remove-tandem-assistant', kwargs={"pk": p.id}))
        p.refresh_from_db()
        self.assertNotIn(Roles.TANDEM_ASSISTANT, p.roles)

    def test_make_and_remove_tandem_doctor_view(self):
        p = Person.objects.create(
            practice=self.practice_1,
            first_name="TandemTamara",
            last_name="Turbotussi",
            gender=GenderType.FEMALE,
        )
        p.make_doctor()
        response = self.client.post(reverse('persons:make-tandem-doctor', kwargs={"pk": p.id}))
        p.refresh_from_db()
        self.assertIn(Roles.TANDEM_DOCTOR, p.roles)
        response = self.client.post(reverse('persons:remove-tandem-doctor', kwargs={"pk": p.id}))
        p.refresh_from_db()
        self.assertNotIn(Roles.TANDEM_DOCTOR, p.roles)

    def test_people_search(self):
        p = Person.objects.create(practice=self.practice_1, first_name="Max", last_name="Maxmann", gender=GenderType.MALE,)
        monika = Person.objects.create(first_name="Monika", last_name="Musterfrau", gender=GenderType.FEMALE,)
        monika.assign_perms(self.user.assigned_institute)

        response = self.client.get(reverse('persons:search')) #standard search
        self.assertEqual(len(response.context['table'].data), 2)
        response = self.client.get(reverse('persons:search'), data={'first_name': "Max"}) # search by first name
        self.assertEqual(len(response.context['table'].data), 1)
        response = self.client.get(reverse('persons:search'), data={'part_of_practice': False}) #search by part of practice
        self.assertEqual(len(response.context['table'].data), 1)
        # search by role (doctor)
        response = self.client.get(reverse('persons:search'), data={'roles': Roles.DOCTOR})
        self.assertEqual(len(response.context['table'].data), 0)
        p.make_doctor()
        response = self.client.get(reverse('persons:search'), data={'roles': Roles.DOCTOR})
        self.assertEqual(len(response.context['table'].data), 1) 

    def test_new_person(self):
        response = self.client.get(reverse('persons:new'))
        self.assertEqual(response.status_code, 200)
        response_post = self.client.post(reverse('persons:new'), data={
            "first_name": "Tina",
            "last_name": "Tinker",
            "gender" : GenderType.FEMALE,
            "year_of_birth": 1976,
            "email": "Tina.Tinker@testmail.com",
        })
        self.assertRedirects(response_post, reverse('persons:search'))
        self.assertEqual(response_post.status_code, 302)

    
    def test_edit_person(self):
        p = Person.objects.create_person_for_practice(first_name="Max", last_name="Maxmann", practice=self.practice_1)
        response = self.client.get(reverse('persons:edit', kwargs={"pk": p.id}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Person.objects.get(pk=p.id).first_name, "Max")
        response_post = self.client.post(reverse('persons:edit', kwargs={"pk": p.id}), data={
            "first_name": "Maximilian",
            "last_name": "Maxmann",
            "gender": GenderType.MALE,
            "year_of_birth": 1976
        })
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Person.objects.get(pk=p.id).first_name, "Maximilian")
        self.assertRedirects(response_post, reverse('persons:detail', kwargs={"pk": p.id}))

    def test_person_doctor_speciality_add(self):
        p = Person.objects.create_person_for_practice(first_name="Doctor", last_name="Neumann", practice=self.practice_1)
        self.client.post(reverse('persons:make-doctor', kwargs={"pk": p.id}))
        p.refresh_from_db()
        self.assertIn(Roles.DOCTOR, p.roles)
        self.assertEqual(Person.objects.get(pk=p.id).role_doctor.specialties.count(), 0)
        response = self.client.get(reverse('persons:doctor-specialties-add', kwargs={"pk": p.role_doctor.id}))
        self.assertEqual(response.status_code, 200)
        response_post = self.client.post(reverse('persons:doctor-specialties-add', kwargs={"pk": p.role_doctor.id}), data={
            "status": DoctorSpecialty.StatusType.IN_TRAINING,
            "type": "Facharzt für Allgemeinchirurgie",
        })
        self.assertEqual(response_post.status_code, 302)
        self.assertRedirects(response_post, reverse('persons:detail', kwargs={"pk": p.id}))
        self.assertEqual(Person.objects.get(pk=p.id).role_doctor.specialties.count(), 1)

    def test_person_contact_times_edit(self):
        p = Person.objects.create_person_for_practice(first_name="Doctor", last_name="Neumann", practice=self.practice_1)
        # get request edit-contact-times
        response = self.client.get(reverse('persons:edit-contact-times', kwargs={"pk": p.id}))
        self.assertEqual(response.status_code, 200)
        response_post = self.client.post(reverse('persons:edit-contact-times', kwargs={"pk": p.id}), data={
            "contact_times-TOTAL_FORMS": 1,
            "contact_times-INITIAL_FORMS": 0,
            "contact_times-MIN_NUM_FORMS": 0,
            "contact_times-MAX_NUM_FORMS": 1000,
            "contact_times-0-day": 0,
            "contact_times-0-start_time": "09:00",
            "contact_times-0-end_time": "12:00",
        })
        self.assertEqual(response_post.status_code, 302)
        self.assertRedirects(response_post, reverse('persons:detail', kwargs={"pk": p.id}))
               