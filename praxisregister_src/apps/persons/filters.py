
import logging

from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML
from crispy_forms.layout import Column as FormCol
from crispy_forms.layout import Layout, Row, Submit
from django.forms import NullBooleanSelect, Select, TextInput
from django.utils.translation import gettext_lazy as _
from django_filters import (BooleanFilter, CharFilter, ChoiceFilter,
                            ModelChoiceFilter)
from django_property_filter import PropertyFilterSet
from events.models import EventInfo, ParticipatingPerson
from events.models.options import ParticipatingPersonInvitationChoices
from events.filters import EventParticipationFilter
from persons.constants import Roles
from persons.models import Person
from practices.models import Practice
from studies.models import Study, PersonStudyParticipation
from studies.filters import StudyPeopleParticipationFilter

logger = logging.getLogger(__name__)

def get_events_for_user(request):
    if request:
        return EventInfo.objects.get_events_for_user(request.user).distinct()
    else:
        return EventInfo.objects.none()

def get_studies_for_user(request):
    if request:
        return Study.objects.get_studies_for_user(request.user).distinct()
    else:
        return Study.objects.none()

class PeopleFilter(PropertyFilterSet):
    title = CharFilter(
        lookup_expr='icontains',
        field_name='title',
        label=_('Title'),
        widget=TextInput()
    )
    first_name = CharFilter(
        lookup_expr='icontains',
        field_name='first_name',
        label=_('First name'),
        widget=TextInput()
    )
    last_name = CharFilter(
        lookup_expr='icontains',
        field_name='last_name',
        label=_('Last name'),
        widget=TextInput()
    )

    part_of_practice = BooleanFilter(
        method= lambda queryset, name, value : queryset.filter(practice__isnull=not value),
        label=_("Part of a practice"),
        widget=NullBooleanSelect(),
    )

    practice = CharFilter(
        lookup_expr='icontains',
        field_name='practice__name',
        label=_('Practice'),
        widget=TextInput()
    )

    roles = ChoiceFilter(
        method='filter_person_role',
        label=_('Role'),
        choices=Roles.choices,
        widget=Select()
    )

    practice__tags__name = CharFilter(
        lookup_expr='iexact',
        label=_('Practice tags'),
        widget=TextInput(),
        distinct=True,
    )

    part_of_event = ModelChoiceFilter(
        method='filter_part_of_event',
        queryset=get_events_for_user,
        label=_('Part of event'),
    )

    event_participation_status = ChoiceFilter(
        method='filter_participation_status',
        label=_('Participation status'),
        choices=ParticipatingPersonInvitationChoices.choices,
        widget=Select()
    )

    part_of_study = ModelChoiceFilter(
        method='filter_part_of_study',
        queryset=get_studies_for_user,
        label=_('Part of study'),
    )

    study_participation_status = ChoiceFilter(
        method='filter_study_participation_status',
        label=_('Study participation status'),
        choices=ParticipatingPersonInvitationChoices.choices,
        widget=Select()
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        show_people_options = self._is_people_option_selected()
        show_other_options = self._is_other_option_selected()
        show_event_options = self._is_event_option_selected()
        self.form.helper = PeopleFilterFormHelper(show_people_options, show_other_options, show_event_options)

    def _has_field(self, field_name) -> bool:
        return field_name in self.form.data \
            and bool(self.form.data[field_name]) \
            and self.form.data[field_name] != "unknown"

    def _is_people_option_selected(self):
        fields = ["title", "first_name", "last_name", "roles"]
        return any([self._has_field(field) for field in fields])
    
    def _is_other_option_selected(self):
        fields = ["practice", "part_of_practice", "practice__tags__name"]
        return any([self._has_field(field) for field in fields])
    
    def _is_event_option_selected(self):
        fields = ["part_of_event", "event_participation_status"]
        return any([self._has_field(field) for field in fields])
    
    def _is_study_option_selected(self):
        fields = ["part_of_study", "study_participation_status"]
        return any([self._has_field(field) for field in fields])

    class Meta:
        model = Person
        fields = ('title', 'first_name', 'last_name', 'roles', 
                  'part_of_practice', 'practice', 
                  'part_of_event', 'event_participation_status',
                  'part_of_study', 'study_participation_status',
                  'practice__tags__name')

    @staticmethod
    def filter_part_of_event(queryset, name, value):
        return queryset.filter(participatingperson__event_participation__event=value)
    
    def filter_participation_status(self, queryset, name, value):
        if not "part_of_event" in self.data:
            return queryset

        # First finds the ID of all event participants matching the status.
        participating_people = ParticipatingPerson.objects.filter(event_participation__event_id = self.data["part_of_event"])
        filtered_participants = EventParticipationFilter.filter_participation_status(participating_people, name, value)
        filtered_ids = filtered_participants.values_list("id", flat = True)
        
        # Then filters the Person queryset by those IDs.
        return queryset.filter(participatingperson__id__in = filtered_ids)

    @staticmethod
    def filter_part_of_study(queryset, name, value):
        return queryset.filter(study_participations__study_participation__study=value)
    
    def filter_study_participation_status(self, queryset, name, value):
        if not "part_of_study" in self.data:
            return queryset

        # First finds the ID of all study participants matching the status.
        participating_people = PersonStudyParticipation.objects.filter(study_participation__study_id = self.data["part_of_study"])
        filtered_participants = StudyPeopleParticipationFilter.filter_participation_status(participating_people, name, value)
        filtered_ids = filtered_participants.values_list("id", flat = True)
        
        # Then filters the Person queryset by those IDs.
        return queryset.filter(study_participations__id__in = filtered_ids)


    @staticmethod
    def filter_person_role(queryset, name, value):
            qs = None
            match value:
                case Roles.OWNER:
                    qs = queryset.filter(role_owner__isnull=False)
                case Roles.DOCTOR:
                    qs = queryset.filter(role_doctor__isnull=False)
                case Roles.ASSISTANT:
                    qs = queryset.filter(role_assistant__isnull=False)
                case Roles.TANDEM_DOCTOR:
                    tandem_doctors=Practice.objects.filter(research_doctor__isnull=False).values_list("research_doctor", flat=True)
                    qs = queryset.filter(id__in=tandem_doctors)
                case Roles.TANDEM_ASSISTANT:
                    tandem_assistants=Practice.objects.filter(research_assistant__isnull=False).values_list("research_assistant", flat=True)
                    qs = queryset.filter(id__in=tandem_assistants)
            return qs

class PeopleFilterFormHelper(FormHelper):

    def __init__(self, show_people_options: bool = False, 
                 show_other_options: bool = False, 
                 show_event_options: bool = False, 
                 show_study_options: bool = False, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.show_people_options = "show" if show_people_options else ""
        self.show_other_options = "show" if show_other_options else ""
        self.show_event_options = "show" if show_event_options else ""
        self.show_study_options = "show" if show_study_options else ""
        self.filterFunctionsTranslation = _('Filter functions')
        self.personRelatedTranslation = _('Person related')
        self.eventRelatedTranslation = _('Events')
        self.studyRelatedTranslation = _('Studies')
        self.othersTranslation = _('Others')

    form_method = 'GET'
    layout = Layout(
        HTML("""<h6 class="mb-2">{{ filterFunctionsTranslation }}:</h6>"""),
        HTML("""<div class="d-flex gap-2 mb-2">"""),
        HTML("""<button class="btn btn-outline-primary" type="button" data-bs-toggle="collapse" data-bs-target="#personRelated" aria-expanded="false" aria-controls="personRelated">{{personRelatedTranslation}}</button>"""),
        HTML("""<button class="btn btn-outline-primary" type="button" data-bs-toggle="collapse" data-bs-target="#eventRelated" aria-expanded="false" aria-controls="eventRelated">{{ eventRelatedTranslation }}</button>"""),
        HTML("""<button class="btn btn-outline-primary" type="button" data-bs-toggle="collapse" data-bs-target="#studyRelated" aria-expanded="false" aria-controls="studyRelated">{{ studyRelatedTranslation }}</button>"""),
        HTML("""<button class="btn btn-outline-primary" type="button" data-bs-toggle="collapse" data-bs-target="#others" aria-expanded="false" aria-controls="others">{{ othersTranslation }}</button>"""),
        HTML("""</div>"""),
        HTML("""<div class="collapse multi-collapse {{ show_people_options }}" id="personRelated">"""),
        Row(
            FormCol('title'),
            FormCol('first_name'),
            FormCol('last_name'),
            FormCol('roles'),
        ),
        HTML("""</div>"""),
        HTML("""<div class="collapse multi-collapse {{ show_event_options }}" id="eventRelated">"""),
        Row(
            FormCol('part_of_event'),
            FormCol('event_participation_status'),
        ),
        HTML("""</div>"""),
        HTML("""<div class="collapse multi-collapse {{ show_study_options }}" id="studyRelated">"""),
        Row(
            FormCol('part_of_study'),
            FormCol('study_participation_status'),
        ),
        HTML("""</div>"""),
        HTML("""<div class="collapse multi-collapse {{ show_other_options }}" id="others">"""),
        Row(
            FormCol('practice'),
            FormCol('practice__tags__name'),
            FormCol('part_of_practice'),
        ),
        HTML("""</div>"""),
        FormCol(Submit('submit', value=_('Apply Filter')), css_class="mb-3", style="align-self:flex-end")
    )