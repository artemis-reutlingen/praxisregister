from django import forms
from django.utils.translation import gettext_lazy as _
from django.forms.widgets import DateInput
from persons.models import (DoctorRole, FoPraNetTraining,
                            FoPraNetTrainingOption, Person,
                            StudySpecificTrainingOption)
from persons.types import YesNoUnknowType
from utils.constants import DATALIST_INPUT_HELP_TEXT
from utils.fields import ListTextWidget

from .constants import DoctorTitles, WorkRelationshipType
from .models.additionals import StudySpecificTraining


class PersonAddForm(forms.ModelForm):
    work_relationship_type = forms.CharField(required=False, help_text=DATALIST_INPUT_HELP_TEXT, label = _("Work relationship"))
    practice = forms.ModelChoiceField(required=False, label=_('Practice'), queryset=None)
    email = forms.EmailField(required=False, label=_("Email"))
    phone = forms.CharField(required=False, label=_("Phone"))
                               
    def __init__(self, *args, **kwargs):
        current_practice = kwargs.pop('current_practice', None)
        practices_selection = kwargs.pop('practices_selection', None)
        super().__init__(*args, **kwargs)
        self.fields['practice'].queryset = practices_selection
        self.fields['practice'].initial = self.instance.practice if self.instance.practice else current_practice

    class Meta:
        model = Person
        fields = [
            'questionnaire_date',
            'import_id',
            'practice',
            'title',
            'first_name',
            'last_name',
            'former_name',
            'gender',
            'year_of_birth',
            'phone',
            'email',
            'work_relationship_type',
            'estimated_weekly_working_hours',
            'weekly_working_hours',
            'starting_year_in_gp_care',
        ]

        widgets = {
            "work_relationship_type": ListTextWidget(data_list=[type_.label for type_ in WorkRelationshipType], name='work-relationship-type-choices'),
            "title": ListTextWidget(data_list=[title.label for title in DoctorTitles], name='doctor-titles-choices'),
            "questionnaire_date": forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            )
        }
        

class PersonSelectBaseForm(forms.Form):
    """
    Providing a form to select multiple persons.
    The queryset for persons should be overwritten inside the view (form.initial_data) in order to limit the returned
    objects to the context/permissions of the requesting user.
    """
    persons = forms.ModelMultipleChoiceField(
        widget=forms.MultipleHiddenInput(attrs={'disabled': "true"}),
        label="persons",
        queryset=Person.objects.all(),
    )


class FoPraNetTrainingForm(forms.ModelForm):
    class Meta:
        model = FoPraNetTraining
        fields = ['type']

    def __init__(self, *args, **kwargs):
        person = kwargs.pop('person')
        super(FoPraNetTrainingForm, self).__init__(*args, **kwargs)
        inner_qs = Person.objects.filter(id=person.id).values_list(
            "fopranet_trainings__type", flat=True)
        if inner_qs.first():
            self.fields['type'].queryset = FoPraNetTrainingOption.objects.exclude(
                id__in=inner_qs)
        else:
            self.fields['type'].queryset = FoPraNetTrainingOption.objects.all()


class StudySpecificTrainingForm(forms.ModelForm):
    class Meta:
        model = StudySpecificTraining
        fields = ['type']

    def __init__(self, *args, **kwargs):
        person = kwargs.pop('person')
        super(StudySpecificTrainingForm, self).__init__(*args, **kwargs)
        inner_qs = Person.objects.filter(id=person.id).values_list(
            "study_specific_trainings__type", flat=True)
        if inner_qs.first():
            self.fields['type'].queryset = StudySpecificTrainingOption.objects.exclude(
                id__in=inner_qs)
        else:
            self.fields['type'].queryset = StudySpecificTrainingOption.objects.all()

class DoctorGeneralInfoForm(forms.ModelForm):

    class Meta:
        model = DoctorRole
        fields = ['lanr', 'year_of_licensure', 'attended_research_training', 'efn']