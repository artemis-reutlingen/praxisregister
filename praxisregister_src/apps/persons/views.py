from django import forms
from django.forms.models import BaseModelForm
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView, TemplateView
from django_tables2 import SingleTableMixin, RequestConfig
from django.dispatch import Signal
from django.core.exceptions import PermissionDenied
from rest_framework import viewsets
from guardian.mixins import PermissionRequiredMixin
from contacttimes.view_mixins import ContactTimesEditMixin
from practices.models import Practice
from utils.constants import DATALIST_INPUT_HELP_TEXT
from utils.fields import ListTextWidget
from utils.view_mixins import ManagePracticePermissionRequired, AbstractPracticeDetailView
from .constants import WorkRelationshipType, DoctorTitles
from .decorators import person_http_post_wrapper
from .exceptions import RoleAlreadyAssigned, RoleNotAssigned
from .forms import PersonAddForm, FoPraNetTrainingForm, StudySpecificTrainingForm, DoctorGeneralInfoForm
from .models import Person, DoctorRole, AssistantRole, AssistantVocationalTrainingOption, \
    AssistantAdditionalQualification, DoctorAdditionalTraining, ResearchExperience, ResearchTraining, FoPraNetTraining
from .models.additionals import DoctorSpecialty, StudySpecificTraining
from .serializers import PersonSerializer
from .tables import PeopleTable, PersonsInPracticeTable
from persons.filters import PeopleFilter, PeopleFilterFormHelper
from .view_mixins import RoleUpdateMixin, UseDoctorAdditionalTrainingFormMixin, UseAssistantQualificationFormMixin, \
    UsePersonResearchExperienceFormMixin, UsePersonResearchTrainingFormMixin, UseDoctorSpecialtyFormMixin

persons_crud_log=Signal()
persons_change_role_log = Signal()
persons_additional_info_log = Signal()

#manage_practice > manage_person permission
def check_user_permissions(request, person : Person):
        if not request.user.has_perm('persons.manage_person', person):
            if not person.practice or not request.user.has_perm('practices.manage_practice', person.practice):
                raise PermissionDenied

class PersonViewSet(viewsets.ReadOnlyModelViewSet):
    """
    DRF REST endpoint for person model. Currently only used in combination with datatables library in frontend.
    """
    serializer_class = PersonSerializer

    def get_queryset(self):
        return Person.objects.get_person_objects_for_user(self.request.user)


class PeopleSearch(TemplateView):
    template_name = 'persons/people_search.html'

    def get_queryset(self):
        return Person.objects.get_person_objects_for_user(user=self.request.user).order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super(PeopleSearch, self).get_context_data(**kwargs)
        people_filter = PeopleFilter(self.request.GET, queryset=self.get_queryset(), request=self.request)
        table = PeopleTable(people_filter.qs)
        RequestConfig(self.request).configure(table)
        context['filter'] = people_filter
        context['table'] = table
        return context

def person_table(request):
    person_filter = PeopleFilter(
            request.GET,
            queryset=Person.objects.get_person_objects_for_user(user=request.user).order_by('-updated_at'),
            request=request
        )
    table = PeopleTable(person_filter.qs)
    RequestConfig(request).configure(table)
    context = {'table' : table}
    return render(request, "global/basic_table.html", context)

class PersonsInPractice(SingleTableMixin, AbstractPracticeDetailView):
    template_name = 'persons/all_persons_in_practice.html'
    table_class = PersonsInPracticeTable

    def get_table_data(self):
        return self.object.persons


class PersonAdd(ManagePracticePermissionRequired, CreateView):
    model = Person
    form_class = PersonAddForm
    template_name = 'persons/forms/person_form.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def get_success_url(self):
        persons_crud_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.id, practice_id=self.kwargs['pk'])
        self.object.assign_perms(self.request.user.assigned_institute)
        return reverse_lazy('persons:detail', kwargs={'pk': self.object.id})

    def get_form_kwargs(self):
        current_practice = Practice.objects.get(pk=self.kwargs['pk'])
        practices_selection = Practice.objects.get_practice_objects_for_user(self.request.user)
        kwargs = super().get_form_kwargs()
        kwargs['current_practice'] = current_practice
        kwargs['practices_selection'] = practices_selection
        return kwargs

class PersonNew(CreateView):
    model = Person
    form_class = PersonAddForm
    template_name = 'persons/forms/new_person_form.html'

    def form_valid(self, form):
        # if practice is not None check if user has permission to add person to practice
        if form.cleaned_data['practice'] is not None:
            if not self.request.user.has_perm('practices.manage_practice', form.cleaned_data['practice']):
                raise PermissionDenied
        return super().form_valid(form)
    
    def get_success_url(self):
        practice_id = self.object.practice.id if self.object.practice else None
        persons_crud_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.id, practice_id=practice_id)
        self.object.assign_perms(self.request.user.assigned_institute)
        return reverse_lazy('persons:search')

    def get_form_kwargs(self):
        practices_selection = Practice.objects.get_practice_objects_for_user(self.request.user)
        kwargs = super().get_form_kwargs()
        kwargs['practices_selection'] = practices_selection
        return kwargs   

class PersonEdit(UpdateView):
    model = Person
    form_class = PersonAddForm
    template_name = 'persons/forms/person_form.html'

    def get(self, request, *args, **kwargs):
        check_user_permissions(request, self.get_object())
        return super().get(request, *args, **kwargs)

    def get_success_url(self):
        practice_id = self.get_object().practice.id if self.get_object().practice else None
        persons_crud_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.id, practice_id=practice_id )
        return reverse_lazy('persons:detail', kwargs={'pk': self.object.id})

    def get_form_kwargs(self):
        practices_selection = Practice.objects.get_practice_objects_for_user(self.request.user)
        kwargs = super().get_form_kwargs()
        kwargs['practices_selection'] = practices_selection
        return kwargs


def person_show_detail(request, pk):
    template_name = 'persons/person_detail.html'
    person = Person.objects.get(pk=pk)

    check_user_permissions(request, person)
    context = {
        'person': person
    }
    if person.practice:
        context['actions'] = person.allowed_role_actions
        context['practice'] = person.practice
        context['roles'] = person.roles
        context['person'] = person
        template_name = 'persons/person_detail_practice.html'

    return render(request, template_name, context)

class PersonDelete(DeleteView):
    model = Person
    template_name = 'global/forms/generic_confirm_delete.html'

    def get(self, request, *args, **kwargs):
        check_user_permissions(request, self.get_object())
        return super().get(request, *args, **kwargs)

    def get_success_url(self):
        persons_crud_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.id, practice_id=self.object.practice.id if self.object.practice else None)
        return reverse_lazy('persons:search')


@person_http_post_wrapper
def person_make_owner(request, pk):
    person = Person.objects.get(id=pk)

    try:
        person.make_owner()
        class MakeOwner: pass
        persons_change_role_log.send(sender=MakeOwner, username=request.user.username, person_id=person.id)
        return redirect(reverse('persons:detail', kwargs={"pk": pk}))

    except RoleAlreadyAssigned:
        return HttpResponseBadRequest("Person already has owner role")


@person_http_post_wrapper
def person_remove_owner(request, pk):
    person = Person.objects.get(id=pk)

    try:
        person.remove_owner()
        class RemoveOwner: pass
        persons_change_role_log.send(sender=RemoveOwner, username=request.user.username, person_id=person.id)
        return redirect(reverse('persons:detail', kwargs={"pk": pk}))
    except RoleNotAssigned:
        return HttpResponseBadRequest("Can not remove unassigned role")


@person_http_post_wrapper
def person_make_doctor(request, pk):
    person = Person.objects.get(id=pk)  # get called second time after the decorator :/
    try:
        person.make_doctor()
        class MakeDoctor: pass
        persons_change_role_log.send(sender=MakeDoctor, username=request.user.username, person_id=person.id)        
        return redirect(reverse('persons:detail', kwargs={"pk": pk}))
    except RoleAlreadyAssigned:
        return HttpResponseBadRequest("Person already has role Doctor")


@person_http_post_wrapper
def person_remove_doctor(request, pk):
    person = Person.objects.get(id=pk)  # get called second time after the decorator :/
    try:
        person.remove_doctor()
        class RemoveDoctor: pass
        persons_change_role_log.send(sender=RemoveDoctor, username=request.user.username, person_id=person.id)       
        return redirect(reverse('persons:detail', kwargs={"pk": pk}))
    except RoleNotAssigned:
        return HttpResponseBadRequest("Can not remove unassigned role")


@person_http_post_wrapper
def person_make_assistant(request, pk):
    person = Person.objects.get(id=pk)  # get called second time after the decorator :/
    try:
        person.make_assistant()
        class MakeAssistant: pass
        persons_change_role_log.send(sender=MakeAssistant, username=request.user.username, person_id=person.id) 
        return redirect(reverse('persons:detail', kwargs={"pk": pk}))
    except RoleAlreadyAssigned:
        return HttpResponseBadRequest("Person already has role Assistant")


@person_http_post_wrapper
def person_remove_assistant(request, pk):
    person = Person.objects.get(id=pk)  # get called second time after the decorator :/
    try:
        person.remove_assistant()
        class RemoveAssistant: pass
        persons_change_role_log.send(sender=RemoveAssistant, username=request.user.username, person_id=person.id) 
        return redirect(reverse('persons:detail', kwargs={"pk": pk}))
    except RoleNotAssigned:
        return HttpResponseBadRequest("Can not remove unassigned role")


@person_http_post_wrapper
def person_make_tandem_doctor(request, pk):
    person = Person.objects.get(id=pk)  # get called second time after the decorator :/
    try:
        person.make_tandem_doctor()
        class MakeTandemDoctor: pass
        persons_change_role_log.send(sender=MakeTandemDoctor, username=request.user.username, person_id=person.id) 
        return redirect(reverse('persons:detail', kwargs={"pk": pk}))
    except Exception:
        return HttpResponseBadRequest("Action not possible")


@person_http_post_wrapper
def person_remove_tandem_doctor(request, pk):
    person = Person.objects.get(id=pk)  # get called second time after the decorator :/
    try:
        person.remove_tandem_doctor()
        class RemoveTandemDoctor: pass
        persons_change_role_log.send(sender=RemoveTandemDoctor, username=request.user.username, person_id=person.id) 
        return redirect(reverse('persons:detail', kwargs={"pk": pk}))
    except Exception:
        return HttpResponseBadRequest("Action not possible")


@person_http_post_wrapper
def person_make_tandem_assistant(request, pk):
    person = Person.objects.get(id=pk)  # get called second time after the decorator :/
    try:
        person.make_tandem_assistant()
        class MakeTandemAssistant: pass
        persons_change_role_log.send(sender=MakeTandemAssistant, username=request.user.username, person_id=person.id) 
        return redirect(reverse('persons:detail', kwargs={"pk": pk}))
    except Exception:
        return HttpResponseBadRequest("Action not possible")


@person_http_post_wrapper
def person_remove_tandem_assistant(request, pk):
    person = Person.objects.get(id=pk)  # get called second time after the decorator :/
    try:
        person.remove_tandem_assistant()
        class RemoveTandemAssistant: pass
        persons_change_role_log.send(sender=RemoveTandemAssistant, username=request.user.username, person_id=person.id) 
        return redirect(reverse('persons:detail', kwargs={"pk": pk}))
    except Exception:
        return HttpResponseBadRequest("Action not possible")


class DoctorGeneralInfoEdit(RoleUpdateMixin):
    model = DoctorRole
    form_class = DoctorGeneralInfoForm

class DoctorSpecialtyAdd(UseDoctorSpecialtyFormMixin, ManagePracticePermissionRequired, CreateView):
    model = DoctorSpecialty
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        doctor_role = DoctorRole.objects.get(pk=self.kwargs['pk'])
        return Practice.objects.get(pk=doctor_role.person.practice.id)

    def form_valid(self, form):
        form.instance.doctor_role = DoctorRole.objects.get(pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.doctor_role.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.object.doctor_role.person.id})


class DoctorSpecialtyEdit(UseDoctorSpecialtyFormMixin, ManagePracticePermissionRequired, UpdateView):
    model = DoctorSpecialty
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().doctor_role.person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.doctor_role.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().doctor_role.person.id})


class DoctorSpecialtyDelete(ManagePracticePermissionRequired, DeleteView):
    model = DoctorSpecialty
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().doctor_role.person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.doctor_role.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().doctor_role.person.id})


class DoctorResearchExperienceLastYearEdit(RoleUpdateMixin):
    model = DoctorRole
    fields = [
        'research_experience_year_of_last_project_participation',
    ]


class DoctorAdditionalTrainingAdd(UseDoctorAdditionalTrainingFormMixin, ManagePracticePermissionRequired, CreateView):
    model = DoctorAdditionalTraining
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        doctor_role = DoctorRole.objects.get(pk=self.kwargs['pk'])
        return Practice.objects.get(pk=doctor_role.person.practice.id)

    def form_valid(self, form):
        form.instance.doctor_role = DoctorRole.objects.get(pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.doctor_role.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.object.doctor_role.person.id})


class DoctorAdditionalTrainingEdit(UseDoctorAdditionalTrainingFormMixin, ManagePracticePermissionRequired, UpdateView):
    model = DoctorAdditionalTraining
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().doctor_role.person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.doctor_role.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().doctor_role.person.id})


class DoctorAdditionalTrainingDelete(ManagePracticePermissionRequired, DeleteView):
    model = DoctorAdditionalTraining
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().doctor_role.person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.doctor_role.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().doctor_role.person.id})


class AssistantVocationalTrainingEdit(RoleUpdateMixin):
    class AssistantTrainingForm(forms.ModelForm):
        vocational_training_type = forms.CharField(
            required=True,
            help_text=DATALIST_INPUT_HELP_TEXT
        )

        def __init__(self, *args, **kwargs):
            _training_type_choices = kwargs.pop('data_list', None)
            super().__init__(*args, **kwargs)
            self.fields['vocational_training_type'].widget = ListTextWidget(
                data_list=_training_type_choices,
                name='training-type-choices',
            )
            self.fields['vocational_training_type'].label = _('Vocational training type')

        class Meta:
            model = AssistantRole
            fields = ['vocational_training_type']

    form_class = AssistantTrainingForm
    template_name = 'global/forms/generic.html'
    model = AssistantRole

    def get_form_kwargs(self):
        training_type_choices = [
            option.name for option in AssistantVocationalTrainingOption.objects.all().order_by('name')
        ]
        kwargs = super().get_form_kwargs()
        kwargs['data_list'] = training_type_choices
        return kwargs


class AssistantResearchExperienceLastYearEdit(RoleUpdateMixin):
    model = AssistantRole
    fields = [
        'research_experience_year_of_last_project_participation',
    ]


class AssistantQualificationAdd(UseAssistantQualificationFormMixin, ManagePracticePermissionRequired, CreateView):
    model = AssistantAdditionalQualification
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        assistant_role = AssistantRole.objects.get(pk=self.kwargs['pk'])
        return Practice.objects.get(pk=assistant_role.person.practice.id)

    def form_valid(self, form):
        form.instance.assistant_role = AssistantRole.objects.get(pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.assistant_role.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.object.assistant_role.person.id})


class AssistantQualificationEdit(UseAssistantQualificationFormMixin, ManagePracticePermissionRequired, UpdateView):
    model = AssistantAdditionalQualification
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().assistant_role.person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.assistant_role.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().assistant_role.person.id})


class AssistantQualificationDelete(ManagePracticePermissionRequired, DeleteView):
    model = AssistantAdditionalQualification
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().assistant_role.person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.assistant_role.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().assistant_role.person.id})


class ResearchExperienceAdd(UsePersonResearchExperienceFormMixin, ManagePracticePermissionRequired, CreateView):
    model = ResearchExperience
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        person = Person.objects.get(pk=self.kwargs['pk'])
        return Practice.objects.get(pk=person.practice.id)
    
    def form_valid(self, form):
        if form.is_valid():
            person = Person.objects.get(pk=self.kwargs['pk'])
            person.involved_in_research_projects = form.cleaned_data['involved_in_research_projects']
            person.save()
            if form.cleaned_data["type"].strip() != "":
                form.instance.person = person
                form.instance.type = form.cleaned_data["type"]
                form.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('persons:detail', kwargs={'pk': self.kwargs['pk']})


class ResearchExperienceEdit(UsePersonResearchExperienceFormMixin, ManagePracticePermissionRequired, UpdateView):
    model = ResearchExperience
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().person.id})


class ResearchExperienceDelete(ManagePracticePermissionRequired, DeleteView):
    model = ResearchExperience
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().person.id})


class ResearchTrainingAdd(UsePersonResearchTrainingFormMixin, ManagePracticePermissionRequired, CreateView):
    model = ResearchTraining
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        person = Person.objects.get(pk=self.kwargs['pk'])
        return Practice.objects.get(pk=person.practice.id)

    def form_valid(self, form):
        form.instance.person = Person.objects.get(pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.object.person.id})


class ResearchTrainingEdit(UsePersonResearchTrainingFormMixin, ManagePracticePermissionRequired, UpdateView):
    model = ResearchTraining
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().person.id})


class ResearchTrainingDelete(ManagePracticePermissionRequired, DeleteView):
    model = ResearchTraining
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().person.id})


class FoPraNetTrainingAdd(ManagePracticePermissionRequired, CreateView):
    """
    Adds a FoPraNet training entry for a person.
    """
    template_name = 'global/forms/generic.html'
    form_class = FoPraNetTrainingForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['person'] = Person.objects.get(pk=self.kwargs['pk'])
        return kwargs

    def get_permission_object(self):
        person = Person.objects.get(pk=self.kwargs['pk'])
        return Practice.objects.get(pk=person.practice.id)

    def form_valid(self, form):
        form.instance.person = Person.objects.get(pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.object.person.id})


class FoPraNetTrainingDelete(ManagePracticePermissionRequired, DeleteView):
    model = FoPraNetTraining
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().person.id})


class PersonContactTimesEdit(ContactTimesEditMixin, UpdateView):
    """
    UpdateView using formset for editing all contact time slot objects for a person at the same time.
    """
    template_name = 'contacttimes/forms/contact_times_person.html'

    model = Person
    form_class = forms.modelform_factory(model=Person, fields=[])

    def get(self, request, *args, **kwargs):
        check_user_permissions(request, self.get_object())
        return super().get(request, *args, **kwargs)
    
    def get_timeslots(self):
        return self.get_object().contact_times_ordered

    def get_success_url(self):
        practice_id = self.get_object().practice.id if self.get_object().practice else None
        persons_crud_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.id, practice_id=practice_id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().id})


class StudySpecificTrainingAdd(ManagePracticePermissionRequired, CreateView):

    template_name = 'global/forms/generic.html'
    form_class = StudySpecificTrainingForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['person'] = Person.objects.get(pk=self.kwargs['pk'])
        return kwargs

    def get_permission_object(self):
        person = Person.objects.get(pk=self.kwargs['pk'])
        return Practice.objects.get(pk=person.practice.id)

    def form_valid(self, form):
        form.instance.person = Person.objects.get(pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.object.person.id})


class StudySpecificTrainingEdit(ManagePracticePermissionRequired, UpdateView):
    model = StudySpecificTraining
    template_name = 'global/forms/generic.html'
    fields = ['type']

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().person.id})


class StudySpecificTrainingDelete(ManagePracticePermissionRequired, DeleteView):
    model = StudySpecificTraining
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().person.practice.id)

    def get_success_url(self):
        persons_additional_info_log.send(sender=self.__class__, username=self.request.user.username, person_id=self.object.person.id, model_id=self.object.id)
        return reverse_lazy('persons:detail', kwargs={'pk': self.get_object().person.id})
