from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.utils.html import format_html
from django.urls import reverse
from guardian.admin import GuardedModelAdmin
from guardian.shortcuts import get_objects_for_group

from institutes.models import Institute
from practices.models import Practice
from persons.models import AssistantVocationalTrainingOption, AssistantAdditionalQualificationOption, \
    DoctorAdditionalTrainingOption, ResearchExperienceOption, ResearchExperience, Person

class PersonInstituteFilter(admin.SimpleListFilter):
    title = _('Institute')
    parameter_name = 'institute'

    def lookups(self, request, model_admin):
        institutes = []
        for institute in Institute.objects.all():
            institutes.append((institute.id, institute.full_name))
        return institutes

    def queryset(self, request, queryset):
            if request.GET.get(self.parameter_name):
                institute = Institute.objects.get(pk=request.GET.get(self.parameter_name))
                queryset = get_objects_for_group(institute.group, 'persons.manage_person')
            return queryset
        
class PersonRolesFilter(admin.SimpleListFilter):
    title = _('Roles Filter')
    parameter_name = 'person'

    def lookups(self, request, model_admin):
        return [ ('OWNER', _('OWNER')),
                 ('DOCTOR', _('DOCTOR')),
                 ('ASSISTANT', _('ASSISTANT')),
                 ('TANDEM_DOCTOR', _('TANDEM_DOCTOR')),
                 ('TANDEM_ASSISTANT', _('TANDEM_ASSISTANT'))]

    def queryset(self, request, queryset):
        if self.value() == 'OWNER':
            return queryset.filter(role_owner__isnull=False)
        if self.value() == 'DOCTOR':
            return queryset.filter(role_doctor__isnull=False)
        if self.value() == 'ASSISTANT':
            return queryset.filter(role_assistant__isnull=False)
        if self.value() == 'TANDEM_DOCTOR':
            return queryset.filter(pk__in=Practice.objects.filter(research_doctor__isnull=False).values_list('research_doctor', flat=True))
        if self.value() == 'TANDEM_ASSISTANT':
            return queryset.filter(pk__in=Practice.objects.filter(research_assistant__isnull=False).values_list('research_doctor', flat=True))
        
class PersonAdmin(GuardedModelAdmin):
    model = Person
    list_display = ('id', 'first_name', 'last_name' , 'practice_link', 'gender', 'email', 'roles') 
    list_per_page = 25
    list_select_related = ['practice']
    list_filter = ['updated_at', 'created_at', PersonRolesFilter, PersonInstituteFilter]
    ordering = ('-updated_at',)
    search_fields = ['practice__name__icontains', 'first_name__istartswith', 'last_name__istartswith', 'email__istartswith']

    @admin.display(ordering='practice__name', description=_('Practice'))
    def practice_link(self, person):
        if(person.practice):
            url = reverse('admin:practices_practice_change', args=[person.practice.id])
            return format_html('<a href="{}">{}<a>', url, person.practice.name)
        else:
            return '-'
        
PersonAdmin.model._meta.verbose_name_plural = "1. " + _("Persons")

admin.site.register(Person, PersonAdmin)

class ResearchExperienceAdmin(admin.ModelAdmin):
    model = ResearchExperience


admin.site.register(ResearchExperience, ResearchExperienceAdmin)


class ResearchExperienceOptionAdmin(admin.ModelAdmin):
    model = ResearchExperienceOption


admin.site.register(ResearchExperienceOption, ResearchExperienceOptionAdmin)


class AssistantVocationalTrainingOptionAdmin(admin.ModelAdmin):
    model = AssistantVocationalTrainingOption


admin.site.register(AssistantVocationalTrainingOption, AssistantVocationalTrainingOptionAdmin)


class AssistantAdditionalQualificationOptionAdmin(admin.ModelAdmin):
    model = AssistantAdditionalQualificationOption


admin.site.register(AssistantAdditionalQualificationOption, AssistantAdditionalQualificationOptionAdmin)


class DoctorAdditionalTrainingOptionAdmin(admin.ModelAdmin):
    model = DoctorAdditionalTrainingOption


admin.site.register(DoctorAdditionalTrainingOption, DoctorAdditionalTrainingOptionAdmin)
