from django.db.models.signals import post_save
from django.dispatch import receiver

from contacttimes.models import DayContactTimeSlot
from persons.models import Person

@receiver(post_save, sender=Person)
def create_standard_contact_timeslots_for_new_person(sender, instance, created, **kwargs):
    if created:
        days = [
            DayContactTimeSlot.Weekday.MONDAY,
            DayContactTimeSlot.Weekday.TUESDAY,
            DayContactTimeSlot.Weekday.WEDNESDAY,
            DayContactTimeSlot.Weekday.THURSDAY,
            DayContactTimeSlot.Weekday.FRIDAY,
            DayContactTimeSlot.Weekday.SATURDAY,
            DayContactTimeSlot.Weekday.SUNDAY,
        ]
        contact_times = [DayContactTimeSlot(weekday = day, content_object = instance) for day in days]
        DayContactTimeSlot.objects.bulk_create(contact_times)