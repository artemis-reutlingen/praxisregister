# Generated by Django 4.1.7 on 2023-09-04 13:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('persons', '0033_alter_person_phone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='title',
            field=models.CharField(blank=True, default='', max_length=256, null=True, verbose_name='Title'),
        ),
    ]
