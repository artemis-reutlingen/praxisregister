# Generated by Django 4.2.7 on 2024-02-19 10:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('persons', '0042_ownerrole_import_by_ownerrole_import_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='gender',
            field=models.CharField(choices=[('UNKNOWN', 'unknown'), ('MALE', 'male'), ('FEMALE', 'female'), ('DIVERSE', 'diverse')], default='UNKNOWN', max_length=128, verbose_name='Gender'),
        ),
    ]
