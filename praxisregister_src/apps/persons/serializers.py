from rest_framework import serializers

from persons.models import Person
from practices.serializers import PracticeSerializer


class PersonSerializer(serializers.ModelSerializer):
    practice = PracticeSerializer()

    class Meta:
        model = Person
        fields = ['id', 'title', 'first_name', 'last_name', 'former_name', 'practice', 'roles', ]
