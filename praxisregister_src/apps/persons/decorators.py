from django.core.exceptions import PermissionDenied
from django.http import HttpResponseNotAllowed

from persons.models import Person


def person_http_post_wrapper(func):
    """ Simple decorator for adding permission checks for function based POST views on person models. """
    def wrapper(*args, **kwargs):
        request, pk = args[0], kwargs['pk']
        if request.method == "POST":
            person = Person.objects.get(id=pk)
            if not person.practice or not request.user.has_perm("practices.manage_practice", person.practice):
                raise PermissionDenied
            return func(*args, **kwargs)
        else:
            return HttpResponseNotAllowed(permitted_methods=['POST'])
    return wrapper
