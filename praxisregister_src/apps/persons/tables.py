from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django_tables2 import Table, Column, CheckBoxColumn

from persons.models import Person

def concat_person_roles(roles):
    concated_roles = ""
    if len(roles) > 1:
    #Last role without comma
        for role in roles[:-1]:
            concated_roles += str(role.label) + ", "
        concated_roles += str(roles[-1].label)
    else:
        for role in roles:
            concated_roles += str(role.label)
    return concated_roles

class PeopleTable(Table):
    full_name_with_title = Column(verbose_name='Name', orderable=False)
    roles = Column(verbose_name=_('Roles'), orderable=False)
    practice = Column(verbose_name=_('Practice'))

    def render_full_name_with_title(self, value, record):
        person_url = reverse("persons:detail", kwargs={"pk": record.id})
        return mark_safe(f'<a href="{person_url}" id="id_person_{record.id}">{value}</a>')

    def render_roles(self, value, record):
        return concat_person_roles(value)

    def render_practice(self, value, record):
        html = "-"
        if record.practice:
            practice_url = reverse("practices:detail", kwargs={"pk": record.practice.id})
            html = mark_safe(f'<a href="{practice_url}" id="id_practice_{record.practice.id}_link">{record.practice}</a>')
        return html

    class Meta:
        model = Person
        template_name = "django_tables2/bootstrap5.html"
        fields = ("full_name_with_title", "roles", "practice")


class PersonsInPracticeTable(Table):
    full_name_with_title = Column(verbose_name='Name', orderable=False)
    roles = Column(verbose_name=_('Roles'), orderable=False)

    def render_full_name_with_title(self, value, record):
        person_url = reverse("persons:detail", kwargs={"pk": record.id})
        return mark_safe(f'<a href="{person_url}" id="id_person_{record.id}">{value}</a>')

    def render_roles(self, value, record):
        return concat_person_roles(value)

    class Meta:
        model = Person
        template_name = "django_tables2/bootstrap.html"
        fields = ("id", "full_name_with_title", "roles")

class SelectPeopleTable(Table):
    selection = CheckBoxColumn(verbose_name="", accessor='pk')
    full_name_with_title = Column(verbose_name='Name', orderable=False)
    roles = Column(verbose_name=_('Roles'), orderable=False)
    practice = Column(verbose_name=_('Practice'))

    def render_full_name_with_title(self, value, record):
        person_url = reverse("persons:detail", kwargs={"pk": record.id})
        return mark_safe(f'<a href="{person_url}" id="id_person_{record.id}">{value}</a>')

    def render_roles(self, value, record):
        html = "-"
        if len(value) > 0:
            concated_roles = ""
            # Concatenate all roles with comma except the last one
            for role in value[:-1]:
                concated_roles += str(role.label) + ", "
            html = concated_roles + str(value[-1].label)
        return html

    def render_practice(self, value, record):
        html = "-"
        if record.practice:
            practice_url = reverse("practices:detail", kwargs={"pk": record.practice.id})
            html = mark_safe(f'<a href="{practice_url}" id="id_practice_{record.practice.id}_link">{record.practice}</a>')
        return html

    class Meta:
        model = Person
        template_name = "django_tables2/bootstrap5.html"
        fields = ("selection", "full_name_with_title", "title", "first_name", "last_name", "roles", "practice")

