from django.urls import path
from changelogs import views
from django.views.generic import TemplateView

app_name = 'changelogs'

urlpatterns = [
    path('v0-7-0', views.latest_changelog, name='latest-changelog'),
    path('v0-6-0', TemplateView.as_view(template_name="changelogs/changelog_0.6.0.html"), name='changelog-v0.6.0'),
    path('v0-4-0', TemplateView.as_view(template_name="changelogs/changelog_0.4.0.html"), name='changelog-v0.4.0'),
    path('v0-3-0', TemplateView.as_view(template_name="changelogs/changelog_0.3.0.html"), name='changelog-v0.3.0'),
    #with v0-3-0 started to only make changelogs for major versions and extend them in the html file
    path('v0-2-2', TemplateView.as_view(template_name="changelogs/changelog_0.2.2.html"), name='changelog-v0.2.2'),
    path('v0-2-1', TemplateView.as_view(template_name="changelogs/changelog_0.2.1.html"), name='changelog-v0.2.1'),
    path('v0-1-4', TemplateView.as_view(template_name="changelogs/changelog_0.1.4.html"), name='changelog-v0.1.4'),

    path('notification', views.changelog_notification, name='changelog-notification'),
]
