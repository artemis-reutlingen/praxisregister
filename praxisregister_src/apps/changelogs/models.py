from django.db import models
from django.utils.translation import gettext_lazy as _


class ChangelogNotification(models.Model):

    animate_changelog = models.BooleanField(default=False, verbose_name=_('Animate changelog'))
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, verbose_name=_('User'))