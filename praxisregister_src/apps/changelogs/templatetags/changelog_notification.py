from django import template
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from changelogs.models import ChangelogNotification

register = template.Library()

@register.filter("changelog_notification", is_safe=True)
def changelog_notification(value):
    if ChangelogNotification.objects.filter(user=value, animate_changelog=False).exists():
        result = '<i class="fa-solid fa-circle-info"></i>'
    else:
        result = '<i class="fa-solid fa-circle-info fa-beat text-warning"></i>'
    return mark_safe(result)