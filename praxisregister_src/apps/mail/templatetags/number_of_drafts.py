from django import template
from mail.models import EmailObject
from guardian.shortcuts import get_objects_for_user

register = template.Library()

@register.filter
def number_of_drafts(value):
    return get_objects_for_user(value, 'mail.view_emailobject').filter(draft=True).count()