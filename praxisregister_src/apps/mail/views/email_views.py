from django.conf import settings
from django.contrib import messages as django_messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.http import HttpRequest, HttpResponse, HttpResponseForbidden
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django_tables2 import SingleTableMixin
from django.views.generic import UpdateView, CreateView, ListView, DeleteView, TemplateView
from django.core.exceptions import PermissionDenied
from django.core.mail import BadHeaderError
from django.utils.translation import gettext_lazy as _
from mail.models import *
from mail.tables import *
from mail.forms import *
from mail.constants import CONTEXT_MARKER_OPEN, CONTEXT_MARKER_CLOSE
from mail.exceptions import NoEmailAddressException
from mail.helper import emailhelper
from guardian.mixins import PermissionRequiredMixin
from threading import Thread
import logging
import os

from events.models.events import ParticipatingPerson
user_logger=logging.getLogger('user_activity')
general_logger=logging.getLogger('general')

# ----------------- EmailObject -----------------

class EmailObjectList(SingleTableMixin, ListView):
    model = EmailObject
    paginate_by = 15
    template_name = 'mails/tables/email_object_list.html'
    table_class = EmailObjectTable
    
    def get_queryset(self):
        return EmailObject.objects.get_emailobjects_for_user(self.request.user).filter(draft=False).order_by('-updated_at')
    
class EmailObjectDraftList(SingleTableMixin, ListView):
    model = EmailObject
    paginate_by = 15
    template_name = 'mails/tables/email_draft_list.html'
    table_class = EmailObjectDraftTable

    def get_queryset(self):
        return EmailObject.objects.get_emailobjects_for_user(self.request.user).filter(draft=True).order_by('-updated_at')
    
class EmailObjectNew(CreateView):
    model = EmailObject
    form_class = EmailObjectForm
    template_name = 'mails/forms/email_object.html'

    def form_valid(self, form):
        form.instance.author = self.request.user
        email_object = form.save()
        email_object.assign_perms(self.request.user)
        emailhelper.update_email_object(form, self.request, email_object)
        if not form.is_valid():
            return super().form_invalid(form)
        return super().form_valid(form)

    def get_success_url(self):
        user_logger.info(f"{self.request.user} created EmailObject with id {self.object.id}")
        return reverse_lazy('mail:emailobject-recipients', kwargs={'pk': self.object.pk})
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["context_marker_open"] = CONTEXT_MARKER_OPEN
        context["context_marker_close"] = CONTEXT_MARKER_CLOSE
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

class EmailObjectDetail(PermissionRequiredMixin, UpdateView):
    model = EmailObject
    form_class = EmailObjectForm
    template_name = 'mails/forms/email_object.html'
    permission_required = "mail.change_emailobject"
    context_object_name = 'email_object'
    return_403 = True

    def form_valid(self, form):
        emailhelper.update_email_object(form, self.request, form.instance)
        if not form.is_valid():
            return super().form_invalid(form)
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["context_marker_open"] = CONTEXT_MARKER_OPEN
        context["context_marker_close"] = CONTEXT_MARKER_CLOSE
        return context

    def get_success_url(self):
        user_logger.info(f"{self.request.user} updated EmailObject with id {self.object.id}")
        return reverse_lazy('mail:emailobject-recipients', kwargs={'pk': self.object.pk})

class EmailObjectSendDetail(PermissionRequiredMixin, TemplateView):
    template_name = 'mails/email_object_detail.html'
    permission_required = "mail.view_emailobject"
    context_object_name = 'email_object'
    return_403 = True

    def get_permission_object(self):
        return EmailObject.objects.get(id=self.kwargs['pk'])
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["email_object"] = EmailObject.objects.get(id=self.kwargs['pk'])
        return context

class EmailObjectDelete(PermissionRequiredMixin, DeleteView):
    model = EmailObject
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = "mail.delete_emailobject"
    return_403 = True

    def get_success_url(self):
        user_logger.info(f"{self.request.user} deleted EmailObject with id {self.object.id} and subject {self.object.subject}")
        return reverse_lazy('mail:emailobject-draft-list')

# ----------------- Attachement -----------------

class AttachementDelete(PermissionRequiredMixin, DeleteView):
    model = EmailAttachement
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = 'mail.delete_emailobject'
    return_403 = True

    def get_permission_object(self):
        return EmailObject.objects.get(id=self.get_object().email_object.id)

    def get_success_url(self):
        return reverse_lazy('mail:emailobject-detail', kwargs={'pk':self.get_object().email_object.id})

def download_attachement(request, email_object_id, file_id):
    email_object = EmailObject.objects.get(id=email_object_id)

    if not request.user.has_perm("mail.view_emailobject", email_object):
        raise PermissionDenied
    file = email_object.emailattachement_set.get(id=file_id).file
    file_name = file.name.split("/")[-1]

    response = HttpResponse()
    response["Content-Disposition"] = f"attachment; filename={file_name}"
    response['X-Accel-Redirect'] = file.url
    return response

# ------------------- Send Email -------------------

def send_email_to_recipients(request, pk):
    if request.method == "GET":
        email_object = EmailObject.objects.get(pk=pk)
        if not request.user.has_perm('mail.change_emailobject', email_object):
            raise PermissionDenied
        if settings.EMAIL_ENABLE and email_object.draft and email_object.from_email.strip() != "":
            #Prepare messages and send emails
            user_logger.info(f"{request.user} sends EmailObject with id {email_object.id} to recipients - Preparing messages")
            try:
                messages_dic = emailhelper.prepare_messages(email_object)
            except BadHeaderError:
                general_logger.error(f"BadHeaderError. {request.user} tryed to send email_object with id {email_object.id}!")
                return HttpResponse('Invalid header found.')
            except NoEmailAddressException:
                error_message = _("Not all recipients have an email address!")
                general_logger.error(f"NoEmailAddressException. {request.user} tryed to send email_object with id {email_object.id}!")
                django_messages.error(request, error_message)
                return redirect('mail:emailobject-recipients', pk=pk)
            
            user_logger.info(f"{request.user} sends EmailObject with id {email_object.id} to recipients - open connection")
            send_emails_thread = Thread(target=emailhelper.open_connection_to_send_multiple_emails, args=[messages_dic, email_object], name="send_multiple_emails")
            send_emails_thread.start()
            
            #save email object as sent
            email_object.draft = False
            email_object.save()
            #After email was send, no more changes are allowed
            if not request.user.is_superuser:
                email_object.remove_edit_perms(request.user)
            user_logger.info(f"{request.user} sends EmailObject with id {email_object.id} to recipients - Emails sent")
            return redirect('mail:emailobject-list')
    return redirect('mail:emailobject-recipients', pk=pk)

class AdminTestMail(UserPassesTestMixin, CreateView):
    model = EmailObject
    form_class = AdminEmailObjectForm
    template_name = 'mails/forms/admin_test_email.html'

    def test_func(self):
        return self.request.user.is_superuser

    def handle_no_permission(self):
        return HttpResponseForbidden()
    
    def form_valid(self, form):
        self.email_object = form.save()
        emailhelper.add_attachements_to_email_object(form, self.request, self.email_object)
        admins_email = os.environ.get('ADMINS_EMAIL', "").split(",")
        if admins_email == None or len(admins_email) == 0:
            form.add_error(None, _("No admin email address found in env variables!"))
            return super().form_invalid(form)
        for a_mail in admins_email:
            recipient_group = RecipientGroup.objects.create(email_object=self.email_object)
            recipient = Recipient.objects.create(recipient_email_address=a_mail, recipient_group=recipient_group)
            emailhelper.set_recipient_message(self.email_object, self.request, recipient)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('mail:emailobject-send-email', kwargs={'pk' : self.email_object.pk})
     