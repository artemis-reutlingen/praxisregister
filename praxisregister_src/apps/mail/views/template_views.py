import logging
from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django_tables2 import SingleTableMixin
from django.views.generic import UpdateView, CreateView, ListView, DeleteView
from django.core.exceptions import PermissionDenied
from django.utils.translation import gettext_lazy as _
from guardian.mixins import PermissionRequiredMixin
from mail.models import *
from mail.tables import *
from mail.forms import *
from mail.validators import get_valid_context_fields_from_text
from mail.constants import CONTEXT_MARKER_OPEN, CONTEXT_MARKER_CLOSE

user_logger=logging.getLogger('user_activity')

class EmailTemplateList(SingleTableMixin, ListView):
    model = EmailTemplate
    paginate_by = 15
    template_name = 'mails/tables/email_template_list.html'
    table_class = EmailTemplateTable

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("Templates")
        return context
    
    def get_queryset(self):
        return EmailTemplate.objects.get_email_templates_for_user(self.request.user).order_by('-updated_at')

class EmailTemplateNew(CreateView):
    model = EmailTemplate
    form_class = EmailTemplateForm
    template_name = 'mails/forms/email_template.html'
    context_object_name = 'email_template'

    def form_valid(self, form):
        form.instance.author = self.request.user
        email_template = form.save()
        email_template.assign_perms(self.request.user)
        create_or_update_email_template(form, self.request, email_template)
        if not form.is_valid():
            return super().form_invalid(form)
        return super().form_valid(form)

    def get_success_url(self):
        user_logger.info(f"{self.request.user} created EmailTemplate with id {self.object.id}")
        return reverse_lazy('mail:emailtemplate-list')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["context_marker_open"] = CONTEXT_MARKER_OPEN
        context["context_marker_close"] = CONTEXT_MARKER_CLOSE
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

class EmailTemplateDetail(PermissionRequiredMixin, UpdateView):
    model = EmailTemplate
    form_class = EmailTemplateForm
    template_name = 'mails/forms/email_template.html'
    permission_required = "mail.change_emailtemplate"
    context_object_name = 'email_template'
    return_403 = True

    def form_valid(self, form):
        create_or_update_email_template(form, self.request, form.instance)
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["context_marker_open"] = CONTEXT_MARKER_OPEN
        context["context_marker_close"] = CONTEXT_MARKER_CLOSE
        return context

    def get_success_url(self):
        user_logger.info(f"{self.request.user} updated EmailTemplate with id {self.object.id}")
        return reverse_lazy('mail:emailtemplate-detail', kwargs={'pk': self.object.pk})
    
def create_or_update_email_template(form, request, email_template):
        email_template.last_editor = request.user
        email_template.save()
        #save attachements
        files = request.FILES.getlist('attachements')
        for file in files:
            EmailTemplateAttachement.objects.create(email_template=email_template, file=file)
        #add internal_context_fields to email_template
        email_template.context_fields.clear()
        context_message = form.cleaned_data['context_message']
        for context_field in get_valid_context_fields_from_text(context_message):
            for value, label in EmailContextFieldChoices.choices:
                if label == context_field:
                    obj, created = EmailContextField.objects.get_or_create(key=value)
                    email_template.context_fields.add(obj)

class EmailTemplateDelete(PermissionRequiredMixin, DeleteView):
    model = EmailTemplate
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = 'mail.delete_emailtemplate'
    return_403 = True

    def get_success_url(self):
        user_logger.info(f"{self.request.user} deleted EmailTemplate with id {self.object.id} and name {self.object.template_name}")
        return reverse_lazy('mail:emailtemplate-list')

def copy_email_template(request, pk):
    email_template = EmailTemplate.objects.get(id=pk)
    if not request.user.has_perm("mail.view_emailtemplate", email_template):
        raise PermissionDenied
    email_template.pk = None
    email_template.author = request.user
    email_template.save()
    orignial_email_template = EmailTemplate.objects.get(id=pk)
    for attachement in orignial_email_template.emailtemplateattachement_set.all():
        attachement.pk = None
        attachement.email_template = email_template
        attachement.save()
    email_template.assign_perms(request.user)
    user_logger.info(f"{request.user} copied EmailTemplate with id {orignial_email_template.id}")
    return redirect('mail:emailtemplate-list')

class AttachementDelete(PermissionRequiredMixin, DeleteView):
    model = EmailTemplateAttachement
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = 'mail.change_emailtemplate'
    return_403 = True

    def get_permission_object(self):
        return EmailTemplate.objects.get(id=self.get_object().email_template.id)

    def get_success_url(self):
        return reverse_lazy('mail:emailtemplate-detail', kwargs={'pk':self.get_object().email_template.id})
    
def download_attachement(request, email_template_id, file_id):
    email_template = EmailTemplate.objects.get(id=email_template_id)

    if not request.user.has_perm("mail.view_emailtemplate", email_template):
        raise PermissionDenied
    file = email_template.emailtemplateattachement_set.get(id=file_id).file
    file_name = file.name.split("/")[-1]

    response = HttpResponse()
    response["Content-Disposition"] = f"attachment; filename={file_name}"
    response['X-Accel-Redirect'] = file.url

    return response

def use_template_for_email(request, pk):
    emailtemplate = EmailTemplate.objects.get(id=pk)
    user = request.user
    if not user.has_perm("mail.view_emailtemplate", emailtemplate):
        raise PermissionDenied

    email_address = user.assigned_institute.email_address if user.assigned_institute and user.assigned_institute.email_address else user.email if user.email else ""
    email_object = EmailObject.objects.create(author=user, last_editor=user, from_email=email_address, reply_to=email_address, subject=emailtemplate.subject, context_message=emailtemplate.context_message)
    
    for context_field in emailtemplate.context_fields.all():
        email_object.context_fields.add(context_field)

    for attachement in emailtemplate.emailtemplateattachement_set.all():
        EmailAttachement.objects.create(email_object=email_object, file=attachement.file)

    user_logger.info(f"{request.user} used EmailTemplate with id {emailtemplate.id} for new EmailObject with id {email_object.id}")
    email_object.assign_perms(user)
    return redirect('mail:emailobject-detail', pk=email_object.id)