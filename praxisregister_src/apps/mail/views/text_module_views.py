import logging
from typing import Any
from django.http import HttpRequest, HttpResponse
from django.urls import reverse_lazy
from django_tables2 import SingleTableMixin
from django.views.generic import CreateView, ListView, DeleteView
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
from guardian.mixins import PermissionRequiredMixin
from mail.models import EmailContextText
from mail.tables import TextModuleTable
from mail.forms import EmailContextTextForm

user_logger=logging.getLogger('user_activity')

# ----------------- Text Module -----------------

class TextModuleList(SingleTableMixin, ListView):
    model = EmailContextText
    paginate_by = 15
    template_name = 'mails/tables/text_modules_list.html'
    table_class = TextModuleTable
    
    def get_queryset(self):
        return EmailContextText.objects.get_text_modules_for_user(self.request.user).order_by('-key')

class TextModuleCreate(CreateView):
    model = EmailContextText
    form_class = EmailContextTextForm
    template_name = 'mails/forms/email-generic.html'

    def get_success_url(self):
        self.object.assign_perms(self.request.user)
        user_logger.info(f"{self.request.user} created EmailContextText with id {self.object.id} and key {self.object.key}")
        messages.success(self.request, _("Text module created successfully."))
        return reverse_lazy('mail:textmodule-list')

class TextModuleDelete(PermissionRequiredMixin, DeleteView):
    model = EmailContextText
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = 'mail.delete_emailcontexttext'
    return_403 = True

    def get_success_url(self):
        user_logger.info(f"{self.request.user} deleted EmailContextText with id {self.object.id} and key {self.object.key}")
        messages.error(self.request, _("Text module deleted."))
        return reverse_lazy('mail:textmodule-list')