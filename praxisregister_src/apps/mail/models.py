from datetime import datetime
from django.db import models
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from django.db.models.signals import post_delete
from mail.types import EmailContextFieldChoices
from config.settings import UPLOAD_DIRECTORY
from guardian.shortcuts import assign_perm, get_objects_for_user, remove_perm
from guardian.models import UserObjectPermissionBase, GroupObjectPermissionBase

from django_ckeditor_5.fields import CKEditor5Field

from persons.models.person import Person
from users.models import User
from events.views.participation_views import get_invitation_link

def generate_attachment_upload_path(instance, filename):
    path = f"{UPLOAD_DIRECTORY}/email_attachements/{filename}"
    return path

def generate_template_attachment_upload_path(instance, filename):
    path = f"{UPLOAD_DIRECTORY}/template_attachements/{filename}"
    return path

class EmailManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()

    @staticmethod
    def get_emailobjects_for_user(user):
        return get_objects_for_user(user, 'mail.view_emailobject')

class TemplateManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()

    @staticmethod
    def get_email_templates_for_user(user):
        return get_objects_for_user(user, 'mail.view_emailtemplate')
    
class TextModuleManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()

    @staticmethod
    def get_text_modules_for_user(user):
        return get_objects_for_user(user, 'mail.view_emailcontexttext')
    
class EmailContextText(models.Model):
    key = models.CharField(max_length=255, blank=False, verbose_name=_('Name'))
    value = models.TextField(blank=False, verbose_name=_('Text value'))

    objects = TextModuleManager()    
    def assign_perms(self, user):
        assign_perm('mail.add_emailcontexttext', user.group, self)
        assign_perm('mail.view_emailcontexttext', user.group, self)
        assign_perm('mail.change_emailcontexttext', user.group, self)
        assign_perm('mail.delete_emailcontexttext', user.group, self)

    def __str__(self) -> str:
        s = _("Custom email context text: ")
        return f"{s}"

class EmailContextField(models.Model):
    key = models.CharField(choices=EmailContextFieldChoices.choices, max_length=255, blank=False, verbose_name=_('Key'), primary_key=True)

# Explicit definition of EmailContextField object permissions. This ensures permission objects are also deleted when a context field is deleted instead of being orphaned.
# ? I'm not sure why this model needs object permissions.
class EmailContextFieldUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(EmailContextField, on_delete = models.CASCADE)

class EmailContextFieldGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(EmailContextField, on_delete = models.CASCADE)

class EmailObject(models.Model):
    author = models.ForeignKey('users.User', verbose_name=_('Author'), on_delete=models.SET_NULL, null=True, blank=True)
    last_editor = models.ForeignKey('users.User', verbose_name=_('Last editor'), on_delete=models.SET_NULL, null=True, blank=True, related_name="email_edits")
    event = models.ForeignKey('events.EventInfo', verbose_name=_('Event'), on_delete=models.SET_NULL, null=True, blank=True)
    subject = models.CharField(max_length=255, blank=False, verbose_name=_('Subject'))
    context_message = CKEditor5Field(verbose_name=_("Message"), blank=False, config_name='extends')
    from_email = models.EmailField(verbose_name=_('From email'), blank=True)
    reply_to = models.EmailField(verbose_name=_('Reply to'), blank=True)
    draft = models.BooleanField(verbose_name=_("Draft"), default=True)
    sent = models.BooleanField(verbose_name=_("Sent"), default=False)
    sent_time = models.DateTimeField(verbose_name=_("Sent time"), null=True, blank=True)
    created_at = models.DateTimeField(verbose_name=_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    context_fields = models.ManyToManyField(EmailContextField, verbose_name=_('Context fields'), blank=True)

    objects = EmailManager()
    def assign_perms(self, user):
        assign_perm('mail.view_emailobject', user.group, self)
        assign_perm('mail.change_emailobject', user.group, self)
        assign_perm('mail.delete_emailobject', user.group, self)

    def remove_edit_perms(self, user):
        remove_perm('mail.change_emailobject', user.group, self)
        remove_perm('mail.delete_emailobject', user.group, self)

    def get_email_context_field_dic(self, request, person : Person = None, event = None):
        dic = {}
        #Person related
        if person != None:
            dic[EmailContextFieldChoices.PERSON_SALUTATION_FULL] = person.form_of_address
            dic[EmailContextFieldChoices.PERSON_SALUTATION_FORMAL] = person.form_of_address_formal
            dic[EmailContextFieldChoices.PERSON_SALUTATION_GENDER] = person.form_of_address_gender
            dic[EmailContextFieldChoices.PERSON_TITLE] = person.title
            dic[EmailContextFieldChoices.PERSON_FIRST_NAME] = person.first_name
            dic[EmailContextFieldChoices.PERSON_LAST_NAME] = person.last_name
            dic[EmailContextFieldChoices.PERSON_EMAIL] = person.email
            dic[EmailContextFieldChoices.PERSON_FULL_NAME] = person.full_name
            dic[EmailContextFieldChoices.PERSON_FULL_NAME_WITH_TITLE] = person.full_name_with_title
            if person.practice != None:
                #practice related
                practice = person.practice
                dic[EmailContextFieldChoices.PRACTICE_NAME] = practice.name
                dic[EmailContextFieldChoices.PRACTICE_BSNR] = practice.identification_number
                dic[EmailContextFieldChoices.PRACTICE_COUNTY] = practice.county
                dic[EmailContextFieldChoices.PRACTICE_CITY] = practice.city
                dic[EmailContextFieldChoices.PRACTICE_ZIP_CODE] = practice.zip_code
                dic[EmailContextFieldChoices.PRACTICE_STREET] = practice.street
                dic[EmailContextFieldChoices.PRACTICE_STREET_NUMBER] = practice.house_number
                dic[EmailContextFieldChoices.PRACTICE_ADDRESS] = f"{practice.street} {practice.house_number} \r\n {practice.zip_code} {practice.city}"
                dic[EmailContextFieldChoices.PRACTICE_PHONE] = practice.contact_phone
                dic[EmailContextFieldChoices.PRACTICE_FAX] = practice.contact_fax
                dic[EmailContextFieldChoices.PRACTICE_EMAIL] = practice.contact_email
                dic[EmailContextFieldChoices.PRACTICE_RESEARCH_DOCTOR] = practice.research_doctor
                dic[EmailContextFieldChoices.PRACTICE_RESEARCH_ASSISTANT] = practice.research_assistant
        if event != None:
            dic[EmailContextFieldChoices.EVENT_NAME] = event.title
            dic[EmailContextFieldChoices.EVENT_DESCRIPTION] = event.description
            dic[EmailContextFieldChoices.EVENT_LOCATION] = event.location
            dic[EmailContextFieldChoices.EVENT_TAKES_PLACE] = event.takes_place
            dic[EmailContextFieldChoices.EVENT_VIDEO_CONFERENCE_LINK] = event.video_call_url
            dic[EmailContextFieldChoices.EVENT_CERTIFICATES] = event.certificates
            dic[EmailContextFieldChoices.EVENT_TYPE] = event.type
        if event != None and person != None:
            invitation_url = get_invitation_link(request, event, person)
            if invitation_url != None and invitation_url.strip() != "": 
                dic[EmailContextFieldChoices.EVENT_INVITATION_LINK] = invitation_url
        #Institute related
        user = request.user
        if user.assigned_institute:
            dic[EmailContextFieldChoices.NAME_OF_MY_INSTITUTION] = user.assigned_institute.name
            dic[EmailContextFieldChoices.NAME_OF_MY_NETWORK] = user.assigned_institute.organisation
        #User related
        dic[EmailContextFieldChoices.MY_NAME] = user.full_name
        dic[EmailContextFieldChoices.MY_MEDICAL_TITLE] = user.medical_title
        dic[EmailContextFieldChoices.MY_PHONE_NUMBER] = user.phone_number
        dic[EmailContextFieldChoices.MY_EMAIL] = user.email
        #others
        dic[EmailContextFieldChoices.CURRENT_DATE] = datetime.today().strftime('%d/%m/%Y')
        return dic

    def __str__(self):
        email = _("New Email")
        return f"{self.author} : {self.subject}" if (self.author and self.subject) else f"{email}"

# Explicit definition of email object permissions. This ensures permission objects are also deleted when an email object is deleted instead of being orphaned.
class EmailObjectUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(EmailObject, on_delete = models.CASCADE)

class EmailObjectGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(EmailObject, on_delete = models.CASCADE)


class RecipientGroup(models.Model):
    email_object = models.ForeignKey(EmailObject, verbose_name=_('Email'), on_delete=models.CASCADE)

class AbstractRecipient(models.Model):
    # If a person is not registered in the system, only the email address is saved here.
    recipient_email_address = models.EmailField(verbose_name=_('Recipient email address'), null=True, blank=True)
    recipient_person = models.ForeignKey('persons.Person', verbose_name=_('Email recipient'), on_delete=models.CASCADE , null=True, blank=True)
    recipient_name = models.CharField(max_length=255, verbose_name=_('Recipient name'), null=True, blank=True)
    email_received = models.BooleanField(verbose_name=_("Email received"), default=False)
    sent_time = models.DateTimeField(verbose_name=_("Sent at the time"), null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        person_name = self.recipient_person.full_name_with_title + "," if self.recipient_person else ""
        person_email = self.recipient_person.email if self.recipient_person else self.recipient_email_address
        email_address = _("Email address")
        return f'{person_name} {email_address}: {person_email}'

class Recipient(AbstractRecipient):
    received_email_message = CKEditor5Field(verbose_name=_("Received email message"), config_name='extends')
    recipient_group = models.OneToOneField(RecipientGroup, related_name="recipient", verbose_name=_('Recipient group'), on_delete=models.CASCADE, null=True, blank=True)
    unfullfilled_context_fields = models.BooleanField(verbose_name=_("Unfullfilled context fields"), default=False)

    class Meta:
        verbose_name = _('Recipient')
        verbose_name_plural = _('Recipients')

@receiver(post_delete, sender=Recipient)
def delete_recipient_group(sender, instance, **kwargs):
    try:
        instance.recipient_group.delete()
    except:
        #Recipient group is already deleted --> pass
        pass

class CarbonCopyRecipient(AbstractRecipient):
    recipient_group = models.ForeignKey(RecipientGroup, verbose_name=_('Recipient group'), on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name = _('Carbon copy recipient')
        verbose_name_plural = _('Carbon copy recipients')

class BlindCarbonCopyRecipient(AbstractRecipient):
    recipient_group = models.ForeignKey(RecipientGroup, verbose_name=_('Recipient group'), on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name = _('Blind carbon copy recipient')
        verbose_name_plural = _('Blind carbon copy recipients')

class EmailTemplate(models.Model):
    template_name = models.CharField(max_length=255, blank=False, verbose_name=_('Template name'))
    author = models.ForeignKey('users.User', verbose_name=_('Author'), on_delete=models.SET_NULL, null=True, blank=True)
    subject = models.CharField(max_length=255, blank=False, verbose_name=_('Subject'))
    context_message = CKEditor5Field(verbose_name=_("Context message"), null=False, blank=False, config_name='extends')
    created_at = models.DateTimeField(verbose_name=_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    context_fields= models.ManyToManyField(EmailContextField, verbose_name=_('Context fields'), blank=True)
    
    objects = TemplateManager()
    
    def assign_perms(self, user):
        assign_perm('mail.view_emailtemplate', user.group, self)
        assign_perm('mail.change_emailtemplate', user.group, self)
        assign_perm('mail.delete_emailtemplate', user.group, self)
    
    def __str__(self):
        new_template = _("New Email template")
        return f"{self.template_name}" if self.template_name else f"{new_template}"
    
# Explicit definition of EmailTemplate object permissions. This ensures permission objects are also deleted when a template is deleted instead of being orphaned.
class EmailTemplateUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(EmailTemplate, on_delete = models.CASCADE)

class EmailTemplateGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(EmailTemplate, on_delete = models.CASCADE)
       
class EmailAttachement(models.Model):
    file = models.FileField(upload_to=generate_attachment_upload_path, verbose_name=_('Email attachement'))
    email_object = models.ForeignKey(EmailObject, verbose_name=_('Email'), on_delete=models.CASCADE)
    
    def __str__(self):
        return self.file.name
    
class EmailTemplateAttachement(models.Model):
    email_template = models.ForeignKey(EmailTemplate, verbose_name=_('Email template'), on_delete=models.CASCADE)
    file = models.FileField(upload_to=generate_template_attachment_upload_path, verbose_name=_('Email attachement'))
    
    def __str__(self):
        return self.file.name