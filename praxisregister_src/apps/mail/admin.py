from typing import Any, Optional
from django.contrib import admin
from django.db.models.query import QuerySet
from django.http.request import HttpRequest
from django.contrib import admin
from guardian.admin import GuardedModelAdmin
from . import models
from django.db.models import Count
from django.utils.html import format_html
from django.utils.http import urlencode
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

    
#RecipientGroup Filter
class RecipientGroupFilter(admin.SimpleListFilter):
    title = _('Recipient Group')
    parameter_name = 'recipient_group'

    def lookups(self, request, model_admin):
        return [ ('<10', _('Less than 10 recipient groups')), ('10-25', _('10 to 25 recipient groups')), ('25-50', _('25 to 50 recipient groups')), ('>50', _('More than 50 recipient groups'))]

    def queryset(self, request, queryset):
        if self.value() == '<10':
            return queryset.filter(recipient_group_count__lt=10)
        if self.value() == '10-25':
            return queryset.filter(recipient_group_count__gte=10, recipient_group_count__lt=25)
        if self.value() == '25-50':
            return queryset.filter(recipient_group_count__gte=25, recipient_group_count__lt=50)
        if self.value() == '>50':
            return queryset.filter(recipient_group_count__gte=50)
    
class EmailObjectAdmin(GuardedModelAdmin):
    model = models.EmailObject
    list_display = ('id', 'updated_at', 'author', 'subject', 'draft', 'attachements_count', 'recipient_group_count', 'sent', 'sent_time', 'event_title') 
    list_per_page = 20
    list_select_related = ['author', 'event']
    list_filter = ['draft', 'sent', 'updated_at', RecipientGroupFilter]
    ordering = ('-updated_at',)
    search_fields = ['subject__startswith', 'author__username__startswith', 'event__title__startswith']

    @admin.display(ordering='event__title', description=_('Event'))
    def event_title(self, email_object):
        if(email_object.event):
            url = reverse('admin:events_eventinfo_change', args=[email_object.event.id])
            return format_html('<a href="{}">{}<a>', url, email_object.event.title)
        else:
            return '-'
    
    @admin.display(ordering='attachements_count', description=_('Attachements'))
    def attachements_count(self, email_object):
        url = (reverse('admin:mail_emailattachement_changelist')
        + '?'
        + urlencode({'email_object__id': f'{email_object.id}'}))
        return format_html('<a href="{}">{}<a>', url, email_object.attachements_count)

    @admin.display(ordering='recipient_group_count', description=_('Recipient groups'))
    def recipient_group_count(self, email_object):
        url = (reverse('admin:mail_recipientgroup_changelist')
        + '?'
        + urlencode({'email_object__id': f'{email_object.id}'}))
        return format_html('<a href="{}">{}<a>', url, email_object.recipient_group_count)

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request).annotate(
            attachements_count=Count('emailattachement', distinct=True),
            recipient_group_count=Count('recipientgroup', distinct=True)
        )
        return qs
    
class AbstractEmailObjectAdmin(GuardedModelAdmin):

    @admin.display(ordering='email_object__subject', description=_('Email'))
    def email_object_link(self, email_attachement):
        if(email_attachement.email_object):
            url = reverse('admin:mail_emailobject_change', args=[email_attachement.email_object.id])
            return format_html('<a href="{}">{}<a>', url, email_attachement.email_object)
        else:
            return '-'

class EmailAttachementAdmin(AbstractEmailObjectAdmin):
    model = models.EmailAttachement
    list_display = ('id', 'file', 'email_object_link')
    list_per_page = 20
    ordering = ('-email_object__updated_at',)
    search_fields = ['email_object__author__last_name__startswith', 'email_object__subject__startswith']

class EmailTemplateAdmin(GuardedModelAdmin):
    model = models.EmailTemplate
    list_display = ('id', 'updated_at', 'template_name', 'author', 'subject', 'attachements_count')
    list_per_page = 20
    list_select_related = ['author']
    ordering = ('-updated_at',)
    search_fields = ['template_name__startswith', 'subject__startswith', 'author__username__startswith']

    @admin.display(ordering='attachements_count', description=_('Attachements'))
    def attachements_count(self, email_template):
        url = (reverse('admin:mail_emailtemplateattachement_changelist')
        + '?'
        + urlencode({'email_template__id': f'{email_template.id}'}))
        return format_html('<a href="{}">{}<a>', url, email_template.attachements_count)
    
    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request).annotate(
            attachements_count=Count('emailtemplateattachement', distinct=True)
            )
        return qs
    
class EmailTemplateAttachementAdmin(GuardedModelAdmin):
    model = models.EmailTemplateAttachement
    list_display = ('id', 'file', 'email_template_link')
    list_per_page = 20
    ordering = ('-email_template__updated_at',)
    search_fields = ['email_template__template_name__startswith', 'email_template__subject__startswith']

    @admin.display(ordering='email_template__template_name', description=_('Email template'))
    def email_template_link(self, email_template_attachement):
        if(email_template_attachement.email_template):
            url = reverse('admin:mail_emailtemplate_change', args=[email_template_attachement.email_template.id])
            return format_html('<a href="{}">{}<a>', url, email_template_attachement.email_template)
        else:
            return '-'

class RecipientGroupAdmin(AbstractEmailObjectAdmin):
    model = models.RecipientGroup
    list_display = ('id', 'email_object_link', 'recipient_link', 'recipient_cc_count', 'recipient_bcc_count')
    list_per_page = 20
    ordering = ('-email_object__updated_at',)
    search_fields = ['email_object__subject__startswith', 'recipient__recipient_person__last_name__startswith']
    list_filter = ['recipient__sent_time', 'email_object__draft']

    @admin.display(ordering='recipient__recipient_person__last_name', description=_('Recipient'))
    def recipient_link(self, recipient_group):
        if(recipient_group.recipient and recipient_group.recipient.recipient_person):
            url = reverse('admin:mail_recipient_change', args=[recipient_group.recipient.id])
            return format_html('<a href="{}">{}<a>', url, recipient_group.recipient.recipient_person.full_name_with_title)
        else:
            return '-'
        
    @admin.display(ordering='recipient_cc_count', description=_('CC'))
    def recipient_cc_count(self, recipient_group):
        url = (reverse('admin:mail_carboncopyrecipient_changelist')
        + '?'
        + urlencode({'recipient_group__id': f'{recipient_group.id}'}))
        return format_html('<a href="{}">{}<a>', url, recipient_group.recipient_cc_count)

    @admin.display(ordering='recipient_bcc_count', description=_('BCC'))
    def recipient_bcc_count(self, recipient_group):
        url = (reverse('admin:mail_blindcarboncopyrecipient_changelist')
        + '?'
        + urlencode({'recipient_group__id': f'{recipient_group.id}'}))
        return format_html('<a href="{}">{}<a>', url, recipient_group.recipient_bcc_count)

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request).annotate(
            recipient_cc_count=Count('carboncopyrecipient', distinct=True),
            recipient_bcc_count=Count('blindcarboncopyrecipient', distinct=True)
        )
        return qs
        
class AbstractRecipientAdmin(GuardedModelAdmin):

    @admin.display(ordering='recipient_person__last_name', description=_('Person'))
    def recipient_link(self, recipient):
        if(recipient.recipient_person):
            url = reverse('admin:persons_person_change', args=[recipient.recipient_person.id])
            return format_html('<a href="{}">{}<a>', url, recipient.recipient_person.full_name_with_title)
        else:
            return '-'
        
class RecipientAdmin(AbstractRecipientAdmin):
    model = models.Recipient
    list_display = ('id', 'recipient_link', 'recipient_group_link', 'sent_time', 'unfullfilled_context_fields', )
    list_per_page = 20
    ordering = ('-sent_time', '-recipient_person__last_name','-id')
    search_fields = ['email_template__template_name__startswith', 'email_template__subject__startswith']
    list_filter = ['sent_time', 'recipient_group__email_object__draft']

    @admin.display(ordering='recipient_group__id', description=_('Recipient group'))
    def recipient_group_link(self, recipient):
        if(recipient.recipient_group):
            url = (reverse('admin:mail_recipientgroup_changelist')
            + '?'
            + urlencode({'recipient__id': f'{recipient.id}'}))
            return format_html('<a href="{}">{}<a>', url, recipient.recipient_group.id)
        else:
            return '-'

class CarbonCopyRecipientAdmin(AbstractRecipientAdmin):
    model = models.CarbonCopyRecipient
    list_display = ('id', 'recipient_link', 'recipient_group_link', 'sent_time' )
    list_per_page = 20
    ordering = ('-sent_time', '-recipient_person__last_name','-id')
    search_fields = ['email_template__template_name__startswith', 'email_template__subject__startswith']
    list_filter = ['sent_time', 'recipient_group__email_object__draft']

    @admin.display(ordering='recipient_group__id', description=_('Recipient group'))
    def recipient_group_link(self, cc_recipient):
        if(cc_recipient.recipient_group):
            url = (reverse('admin:mail_recipientgroup_changelist')
            + '?'
            + urlencode({'carboncopyrecipient__id': f'{cc_recipient.id}'}))
            return format_html('<a href="{}">{}<a>', url, cc_recipient.recipient_group.id)
        else:
            return '-'

class BlindCarbonCopyRecipientAdmin(AbstractRecipientAdmin):
    model = models.BlindCarbonCopyRecipient
    list_display = ('id', 'recipient_link', 'recipient_group_link', 'sent_time')
    list_per_page = 20
    ordering = ('-sent_time', '-recipient_person__last_name','-id')
    search_fields = ['email_template__template_name__startswith', 'email_template__subject__startswith']
    list_filter = ['sent_time', 'recipient_group__email_object__draft']

    @admin.display(ordering='recipient_group__id', description=_('Recipient group'))
    def recipient_group_link(self, bcc_recipient):
        if(bcc_recipient.recipient_group):
            url = (reverse('admin:mail_recipientgroup_changelist')
            + '?'
            + urlencode({'blindcarboncopyrecipient__id': f'{bcc_recipient.id}'}))
            return format_html('<a href="{}">{}<a>', url, bcc_recipient.recipient_group.id)
        else:
            return '-'

# Changing the order of the admin menu
EmailObjectAdmin.model._meta.verbose_name_plural = "1. " + _("Email objects")
EmailAttachementAdmin.model._meta.verbose_name_plural = "2. " + _("Email attachements")
EmailTemplateAdmin.model._meta.verbose_name_plural = "3. " + _("Email templates")
EmailTemplateAttachementAdmin.model._meta.verbose_name_plural = "4. " + _("Email template attachements")
RecipientGroupAdmin.model._meta.verbose_name_plural = "5. " + _("Recipient groups")
RecipientAdmin.model._meta.verbose_name_plural = "6. " + _("Recipients")
CarbonCopyRecipientAdmin.model._meta.verbose_name_plural = "7. " + _("Carbon copy recipients")
BlindCarbonCopyRecipientAdmin.model._meta.verbose_name_plural = "8. " + _("Blind carbon copy recipients")


#Registering the models
admin.site.register(models.EmailObject, EmailObjectAdmin)
admin.site.register(models.EmailAttachement, EmailAttachementAdmin)
admin.site.register(models.EmailTemplate, EmailTemplateAdmin )
admin.site.register(models.EmailTemplateAttachement, EmailTemplateAttachementAdmin)
admin.site.register(models.RecipientGroup, RecipientGroupAdmin)
admin.site.register(models.Recipient, RecipientAdmin)
admin.site.register(models.CarbonCopyRecipient, CarbonCopyRecipientAdmin)
admin.site.register(models.BlindCarbonCopyRecipient, BlindCarbonCopyRecipientAdmin)