from django.db import models
from django.utils.translation import gettext_lazy as _ 

class EmailContextChoices(models.TextChoices):
    TEXT = 'text', _('Custom text')
    IMAGE = 'image', _('Image')
    FIELD = 'field', _('Field')

def get_context_fields_that_allow_empty_values():
    return [EmailContextFieldChoices.PERSON_TITLE, EmailContextFieldChoices.PERSON_SALUTATION_GENDER]

#Fields that can be used in email templates.
#Care with translations, use ä = ae, ö = oe, ü = ue, ß = ss and do not use special characters (Covered by test)
#This is because the ckeditor transforms the text to html. e.g. ä = &auml; and this is not covered the regular expression
class EmailContextFieldChoices(models.TextChoices):
    # Person related fields
    PERSON_SALUTATION_FULL = 'salutation', _('Person Full salutation')
    PERSON_SALUTATION_GENDER = 'salutation_gender', _('Person Gender salutation')
    PERSON_SALUTATION_FORMAL = 'salutation_formal', _('Person Formal salutation')
    PERSON_TITLE = 'title', _('Person Title')
    PERSON_FIRST_NAME = 'first_name', _('Person First name')
    PERSON_LAST_NAME = 'last_name', _('Person Last name')
    PERSON_FULL_NAME = 'full_name', _('Person Full name')
    PERSON_FULL_NAME_WITH_TITLE = 'full_name_with_title', _('Person Full name with title')
    PERSON_EMAIL = 'email', _('Person Email')
    # Institution related fields
    NAME_OF_MY_INSTITUTION = 'institution_name', _('Institution Name')
    NAME_OF_MY_NETWORK = 'institution_network', _('Institution Network')
    # Practice related fields
    PRACTICE_NAME = 'practice_name', _('Practice Name')
    PRACTICE_BSNR = 'practice_bsnr', _('Practice BSNR')
    PRACTICE_ADDRESS = 'practice_address', _('Practice Address')
    PRACTICE_ZIP_CODE = 'practice_zip_code', _('Practice Zip code')
    PRACTICE_CITY = 'practice_city', _('Practice City')
    PRACTICE_COUNTY = 'practice_county', _('Practice County')
    PRACTICE_STREET = 'practice_street', _('Practice Street')
    PRACTICE_STREET_NUMBER = 'practice_street_number', _('Practice Street number')
    PRACTICE_PHONE = 'practice_phone', _('Practice Phone')
    PRACTICE_FAX = 'practice_fax', _('Practice Fax')
    PRACTICE_EMAIL = 'practice_email', _('Practice Email')
    PRACTICE_RESEARCH_DOCTOR = 'practice_research_doctor', _('Practice Research doctor as full name with title')
    PRACTICE_RESEARCH_ASSISTANT = 'practice_research_assistant', _('Practice Research assistant as full name with title')
    # Event related fields
    EVENT_NAME = 'event_name', _('Event Name')
    EVENT_LOCATION = 'event_location', _('Event Location')
    EVENT_DESCRIPTION = 'event_description', _('Event Description')
    EVENT_TAKES_PLACE = 'event_takes_place', _('Event Takes place')
    EVENT_VIDEO_CONFERENCE_LINK = 'event_video_conference_link', _('Event Video conference link')
    EVENT_CERTIFICATES = 'event_certificates', _('Event Certificates')
    EVENT_TYPE = 'event_type', _('Event Type')
    EVENT_INVITATION_LINK = 'event_invitation_link', _('Event Invitation link')
    # User related fields
    MY_NAME = 'my_name', _('My first and last name')
    MY_MEDICAL_TITLE = 'my_medical_title', _('My medical title')
    MY_PHONE_NUMBER = 'my_phone_number', _('My phone number')
    MY_EMAIL = 'my_email', _('My email address')

    # other fields
    CURRENT_DATE = 'todays_date', _('Todays date')

    'Possible Fields'
    """
    INSTITUTION_ADDRESS = 'institution_address', _('Institution address')
    INSTITUTION_ZIP_CODE = 'institution_zip_code', _('Institution zip code')
    INSTITUTION_CITY = 'institution_city', _('Institution city')
    INSTITUTION_COUNTRY = 'institution_country', _('Institution country')
    INSTITUTION_PHONE = 'institution_phone', _('Institution phone')
    INSTITUTION_FAX = 'institution_fax', _('Institution fax')
    INSTITUTION_EMAIL = 'institution_email', _('Institution email')
    INSTITUTION_WEBSITE = 'institution_website', _('Institution website')
    INSTITUTION_CONTACT_PERSON = 'institution_contact_person', _('Institution contact person')
    INSTITUTION_CONTACT_PERSON_PHONE = 'institution_contact_person_phone', _('Institution contact person phone')
    INSTITUTION_CONTACT_PERSON_EMAIL = 'institution_contact_person_email', _('Institution contact person email')
    """
class EmailRecipientChoices(models.TextChoices):
    CC = 'CC', _('CC')
    BCC = 'BCC', _('BCC')
