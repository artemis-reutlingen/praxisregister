from django.test import TestCase
from django.urls import reverse, resolve
from mail.views.email_views import *

class TestUrls(TestCase):

    def setUp(self):
        self.email_object = EmailObject.objects.create(subject='Test', context_message='Test', draft=False)
        self.email_object_draft = EmailObject.objects.create(subject='Test', context_message='Test', draft=True)


    # ----------------------- EmailObject -----------------------
    
    def test_emailobject_list_url_is_resolved(self):
        url = reverse('mail:emailobject-list')
        self.assertEqual(resolve(url).func.view_class, EmailObjectList)

    def test_emailobject_draft_list_url_is_resolved(self):
        url = reverse('mail:emailobject-draft-list')
        self.assertEqual(resolve(url).func.view_class, EmailObjectDraftList)

    def test_emailobject_new_url_is_resolved(self):
        url = reverse('mail:emailobject-new')
        self.assertEqual(resolve(url).func.view_class, EmailObjectNew)

    def test_emailobject_detail_url_is_resolved(self):
        url = reverse('mail:emailobject-detail', args=[self.email_object.pk])
        self.assertEqual(resolve(url).func.view_class, EmailObjectDetail)

    def test_emailobject_delete_url_is_resolved(self):
        url = reverse('mail:emailobject-delete', args=[self.email_object.pk])
        self.assertEqual(resolve(url).func.view_class, EmailObjectDelete)

    def test_emailobject_send_detail_url_is_resolved(self):
        url = reverse('mail:emailobject-send-detail', args=[self.email_object.pk])
        self.assertEqual(resolve(url).func.view_class, EmailObjectSendDetail)

    def  test_emailobject_send_url_is_resolved(self):
        url = reverse('mail:emailobject-send-email', args=[self.email_object.pk])
        self.assertEqual(resolve(url).func, send_email_to_recipients)