import re
from mail.constants import CONTEXT_MARKER_OPEN_SINGLE, CONTEXT_MARKER_CLOSE_SINGLE

def get_valid_context_fields_from_text(context_message) -> list:
        pattern = f"\{CONTEXT_MARKER_OPEN_SINGLE}\{CONTEXT_MARKER_OPEN_SINGLE}\s*(\w+(?:\s+\w+)*)\s*\{CONTEXT_MARKER_CLOSE_SINGLE}\{CONTEXT_MARKER_CLOSE_SINGLE}"
        return re.findall(pattern, context_message)
        
def get_valid_context_fields_with_marker_from_text(context_message) -> list:
        pattern = f"\{CONTEXT_MARKER_OPEN_SINGLE}\{CONTEXT_MARKER_OPEN_SINGLE}\s*\w+(?:\s+\w+)*\s*{CONTEXT_MARKER_CLOSE_SINGLE}{CONTEXT_MARKER_CLOSE_SINGLE}"
        return re.findall(pattern, context_message)