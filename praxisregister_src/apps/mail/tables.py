from django.urls import reverse
from django.utils.safestring import mark_safe
from django_tables2 import Table, Column
from django.utils.translation import gettext_lazy as _
from django.middleware.csrf import get_token
from django.utils import timezone, dateformat

from mail.models import EmailObject, RecipientGroup, EmailTemplate, EmailContextText


def get_delete_button_html(delete_url, request):
    csrf_token = mark_safe(f'<input type="hidden" name="csrfmiddlewaretoken" value="{get_token(request)}">')
    return f'<form action="{delete_url}" method="post" style="display:inline;">{csrf_token}<button class="btn btn-sm btn-outline-danger" type="submit" style="transform: scale(0.7);"><i class="fa fa-trash"></i></button></form>'
    
def get_copy_button_html(copy_url, request):
    csrf_token = mark_safe(f'<input type="hidden" name="csrfmiddlewaretoken" value="{get_token(request)}">')
    return f'<form action="{copy_url}" method="post" style="display:inline;">{csrf_token}<button class="btn btn-sm btn-outline-primary" type="submit" style="transform: scale(0.7);"><i class="fa-solid fa-clone"></i></button></form>'

def get_person_link(request, person, delete_url = None):
    html = ""
    if not person.email:
        html += '<span style="color: red;"> ' + _("No email address") + '</span> <i class="fa fa-exclamation-triangle" style="color: red;"></i><br>'
    person_url = reverse("persons:detail", kwargs={"pk": person.id})
    html += mark_safe(f'<a href="{person_url}" id="id_person_{person.id}">{person}</a>')
    if delete_url:
        html += f" {get_delete_button_html(delete_url, request)}"
    return mark_safe(html)

def get_draft_cc_bcc_html(request, recipient_group, cc_or_bcc_recipients, delete_url_name):
        b_cc_recipients_html = ""
        for b_cc_recipient in cc_or_bcc_recipients:
            delete_url = reverse(delete_url_name, kwargs={"email_object_id" : recipient_group.email_object.id, "recipient_group_id" : recipient_group.id, "pk": b_cc_recipient.id})
            b_cc_recipients_html += get_person_link(request, b_cc_recipient.recipient_person, delete_url) + "<br>"
        add_cc_url = reverse("mail:emailobject-recipients-add-cc-bcc", kwargs={"email_object_id" : recipient_group.email_object.id, "recipient_group_id": recipient_group.id})
        return mark_safe(f'{b_cc_recipients_html}<a href="{add_cc_url}"><i class="fa fa-plus"></i></a>')

def get_cc_bcc_html(request, cc_or_bcc_recipients):
        b_cc_recipients_html = ""
        for b_cc_recipient in cc_or_bcc_recipients:
            b_cc_recipients_html += get_person_link(request, b_cc_recipient.recipient_person) + "<br>"
        return mark_safe(f'{b_cc_recipients_html}')

class EmailObjectTable(Table):

    link_to_recipients = Column(verbose_name=_("Recipients"), empty_values=(), orderable=False)
    link_to_detail_view = Column(verbose_name=_("Details"), empty_values=(), orderable=False)
    number_of_attachements = Column(verbose_name=_("Number of Attachements"), empty_values=(), orderable=False)

    def render_number_of_attachements(self, value, record):
        return len(record.emailattachement_set.all())
    
    def render_link_to_recipients(self, value, record):
        recipient_list = '<i class="fa-solid fa-user"></i>'
        url = "mail:emailobject-send-recipients"
        return mark_safe(f'<a href="{reverse(url, kwargs={"pk": record.pk})}">{recipient_list}</a>')
    
    def render_link_to_detail_view(self, value, record):
        detail_link = '<i class="fa-solid fa-circle-info"></i>'
        url = "mail:emailobject-send-detail"
        return mark_safe(f'<a href="{reverse(url, kwargs={"pk": record.pk})}">{detail_link}</a>')
    
    class Meta:
        model = EmailObject
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("subject", "author", "reply_to", "created_at", "updated_at", "number_of_attachements", "link_to_recipients", "link_to_detail_view")


class EmailObjectDraftTable(Table):

    link_to_recipients = Column(verbose_name=_("Recipients"), empty_values=(), orderable=False)
    link_to_detail_view = Column(verbose_name=_("Edit"), empty_values=(), orderable=False)
    number_of_attachements = Column(verbose_name=_("Number of Attachements"), empty_values=(), orderable=False)
    delete_draft = Column(verbose_name=_("Delete"), empty_values=(), orderable=False)

    def render_number_of_attachements(self, value, record):
        return len(record.emailattachement_set.all())
    
    def render_link_to_recipients(self, value, record):
        recipient_list = '<i class="fa-solid fa-user"></i>'
        url = "mail:emailobject-recipients"
        return mark_safe(f'<a href="{reverse(url, kwargs={"pk": record.pk})}">{recipient_list}</a>')
    
    def render_link_to_detail_view(self, value, record):
        detail_link = '<i class="fa-solid fa-pen-to-square"></i>'
        url = "mail:emailobject-detail"
        return mark_safe(f'<a href="{reverse(url, kwargs={"pk": record.pk})}">{detail_link}</a>')
    
    def render_delete_draft(self, value, record):
        delete_url = reverse("mail:emailobject-delete", kwargs={"pk": record.pk})
        return mark_safe(get_delete_button_html(delete_url, self.request))

    class Meta:
        model = EmailObject
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("subject", "author", "reply_to", "created_at", "updated_at", "number_of_attachements", "link_to_recipients", "link_to_detail_view", "delete_draft")

class RecipientGroupDraftTable(Table):

    is_event_participant = Column(verbose_name=_("Event Participant"), empty_values=(), orderable=False)
    cc = Column(verbose_name=_("CC"), empty_values=(), orderable=False)
    bcc = Column(verbose_name=_("BCC"), empty_values=(), orderable=False)
    recipient_message = Column(verbose_name=_("Message"), empty_values=(), orderable=False)

    
    def render_recipient(self, value, record):
        delete_url = reverse("mail:emailobject-recipients-remove", kwargs={"email_object_id" : record.email_object.id , "pk": record.id})
        return get_person_link(self.request, value.recipient_person, delete_url) if value.recipient_person else value.recipient_email_address
    
    def render_is_event_participant(self, value, record):
        is_event_participant = record.email_object.event and record.recipient.recipient_person.participatingperson_set.filter(event_participation__event=record.email_object.event).exists()
        return _("Yes") if is_event_participant else _("No")
    
    def render_cc(self, value, record):
        return get_draft_cc_bcc_html(self.request, record, record.carboncopyrecipient_set.all(), "mail:emailobject-recipients-remove-cc")
    
    def render_bcc(self, value, record):
        return get_draft_cc_bcc_html(self.request, record, record.blindcarboncopyrecipient_set.all(), "mail:emailobject-recipients-remove-bcc")

    def render_recipient_message(self, value, record):
        message_url = reverse("mail:emailobject-recipient-message", kwargs={"email_object_id" : record.email_object.id, "pk": record.recipient.id})
        message_display = _("Message")
        html = mark_safe(f'<a href="{message_url}">{message_display}</a>')
        if record.recipient.unfullfilled_context_fields:
            html += '<i class="fa-solid text-warning fa-triangle-exclamation"></i>'
        return mark_safe(f"{html}")

    class Meta:
        model = RecipientGroup
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ('recipient', 'is_event_participant')

class RecipientGroupTable(Table):

    is_event_participant = Column(verbose_name=_("Event Participant"), empty_values=(), orderable=False)
    cc = Column(verbose_name=_("CC"), empty_values=(), orderable=False)
    bcc = Column(verbose_name=_("BCC"), empty_values=(), orderable=False)
    recipient_message = Column(verbose_name=_("Message"), empty_values=(), orderable=False)
    sent_time = Column(verbose_name=_("Sent time"), empty_values=(), orderable=False)
    
    def render_recipient(self, value, record):
        return get_person_link(self.request, value.recipient_person) if value.recipient_person else value.recipient_email_address
    
    def render_is_event_participant(self, value, record):
        is_event_participant = record.email_object.event and record.recipient.recipient_person.participatingperson_set.filter(event_participation__event=record.email_object.event).exists()
        return _("Yes") if is_event_participant else _("No")

    def render_cc(self, value, record):
        return get_cc_bcc_html(self.request, record.carboncopyrecipient_set.all())
    
    def render_bcc(self, value, record):
        return get_cc_bcc_html(self.request, record.blindcarboncopyrecipient_set.all())

    def render_recipient_message(self, value, record):
        message_url = reverse("mail:emailobject-send-recipient-message", kwargs={"email_object_id" : record.email_object.id, "pk": record.recipient.id})
        message_display = _("Message")
        html = mark_safe(f'<a href="{message_url}">{message_display}</a>')
        if record.recipient.unfullfilled_context_fields:
            html += '<i class="fa-solid text-warning fa-triangle-exclamation"></i>'
        return mark_safe(f"{html}")

    def render_sent_time(self, value, record):
        html = _("Email not sent")
        if record.recipient.sent_time:
            html = record.recipient.sent_time.strftime("%d-%m-%Y %H:%M:%S")
        return mark_safe(f"{html}")

    class Meta:
        model = RecipientGroup
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ('recipient', 'is_event_participant')

class EmailTemplateTable(Table):

    number_of_attachements = Column(verbose_name=_("Number of Attachements"), empty_values=(), orderable=False)
    link_to_detail_view = Column(verbose_name=_("Details"), empty_values=(), orderable=False)
    use_for_email = Column(verbose_name=_("Use for new email"), empty_values=(), orderable=False)
    delete_template = Column(verbose_name=_("Delete"), empty_values=(), orderable=False)
    copy_template = Column(verbose_name=_("Copy"), empty_values=(), orderable=False)

    def render_number_of_attachements(self, value, record):
        return len(record.emailtemplateattachement_set.all())
    
    def render_link_to_detail_view(self, value, record):
        detail_link = _("Show template")
        url = "mail:emailtemplate-detail"
        return mark_safe(f'<a href="{reverse(url, kwargs={"pk": record.pk})}">{detail_link}</a>')
    
    def render_use_for_email(self, value, record):
        url = "mail:emailtemplate-use-for-email"
        text = _("Use template")
        return mark_safe(f'<a href="{reverse(url, kwargs={"pk": record.pk})}"><i class="fa-solid fa-envelope"></i> {text}</a>')

    def render_delete_template(self, value, record):
        url = reverse("mail:emailtemplate-delete", kwargs={"pk": record.pk})
        return mark_safe(get_delete_button_html(url, self.request))
    
    def render_copy_template(self, value, record):
        url = reverse("mail:emailtemplate-copy", kwargs={"pk": record.pk})
        return mark_safe(get_copy_button_html(url, self.request))
    
    class Meta:
        model = EmailTemplate
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("template_name", "subject", "author", "created_at", "updated_at", "number_of_attachements", "link_to_detail_view", "use_for_email", "copy_template", "delete_template")

class TextModuleTable(Table):

    delete_text_module = Column(verbose_name=_("Delete"), empty_values=(), orderable=False)

    def render_delete_text_module(self, value, record):
        url = reverse("mail:textmodule-delete", kwargs={"pk": record.pk})
        return mark_safe(get_delete_button_html(url, self.request))
    
    class Meta:
        model = EmailContextText
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("key", "value",)