import logging
from django.db import transaction
from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from mail.models import *
from mail.tables import *
from mail.forms import *
from mail.helper import emailhelper

from persons.models.person import Person

user_logger=logging.getLogger('user_activity')

#Helper function
def get_persons_that_are_not_already_recipients(user, email_object):
    persons_user_has_permission_to_see = Person.objects.get_person_objects_for_user(user=user)
    #Exclude persons that are already in recipient list
    is_already_recipient = RecipientGroup.objects.filter(Q(email_object=email_object) & Q(recipient__isnull=False)).values_list('recipient__recipient_person__id', flat=True)
    possible_recipients = persons_user_has_permission_to_see.exclude(id__in=is_already_recipient)
    return possible_recipients

#Helper function
def add_recipients(request, email_object, possible_recipients, persons_to_add_list):
    with transaction.atomic():
        filtered_persons = persons_to_add_list.intersection(possible_recipients)  
        bulk_recipients_list = []
        for person in filtered_persons:
            recipient_group = RecipientGroup.objects.create(email_object=email_object)
            bulk_recipients_list.append(Recipient(recipient_person=person, recipient_group=recipient_group))
        created_recipients = Recipient.objects.bulk_create(bulk_recipients_list)
        #Add message to recipients
        for recipient in created_recipients:
            emailhelper.set_recipient_message(email_object, request, recipient)
            user_logger.info(f"{request.user} added recipient {get_recipient_info(recipient)} to EmailObject with id {email_object.id}")

def check_if_all_recipients_have_an_email_address(email_object):
    recipients = Recipient.objects.filter(recipient_group__email_object=email_object)
    cc_recipients = CarbonCopyRecipient.objects.filter(recipient_group__email_object=email_object)
    bcc_recipients = BlindCarbonCopyRecipient.objects.filter(recipient_group__email_object=email_object)
    return recipients_have_email_address(recipients) and recipients_have_email_address(cc_recipients) and recipients_have_email_address(bcc_recipients)

def recipients_have_email_address(recipients):
    result = True
    #Are there persons without an email address?
    persons_with_no_email = recipients.filter(Q(recipient_person__email="") | Q(recipient_person__email__isnull=True))
    if persons_with_no_email.count() > 0:
        #Was the email address added manually?
        result = persons_with_no_email.filter(Q(recipient_email_address="") | Q(recipient_email_address__isnull=True)).count() == 0
    return result

def get_recipient_info(recipient):
    email = "No email stored!"
    if recipient.recipient_person and recipient.recipient_person.email:
        email = recipient.recipient_person.email
    elif recipient.recipient_email_address:
        email = recipient.recipient_email_address
    return f"id: {recipient.id}, email: {email}"
