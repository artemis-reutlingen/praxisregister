from django.conf import settings
from django.core.files import File
from django.core.mail import EmailMessage, get_connection
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from mail.models import *
from mail.tables import *
from mail.forms import *
from mail.types import get_context_fields_that_allow_empty_values
from mail.validators import get_valid_context_fields_from_text, get_valid_context_fields_with_marker_from_text
from mail.constants import CONTEXT_MARKER_OPEN, CONTEXT_MARKER_OPEN_SINGLE
from mail.exceptions import NoEmailAddressException
from email.mime.image import MIMEImage
from bs4 import BeautifulSoup
import re
import os
import logging

from events.models.events import ParticipatingPerson
user_logger=logging.getLogger('user_activity')
general_logger=logging.getLogger('general')

def update_email_object(form, request, email_object):
        email_object.last_editor = request.user
        email_object.save()
        add_attachements_to_email_object(form, request, email_object)
        add_context_fields_to_email_object(form, email_object)
        add_event_participants_to_email_object(form, request, email_object)

        #Change messages of ALL recipients
        for recipient_group in email_object.recipientgroup_set.all():
            set_recipient_message(email_object, request, recipient_group.recipient)

#save attachements
def add_attachements_to_email_object(form, request, email_object):
    attachements_size = 0
    for existing_attachement in email_object.emailattachement_set.all():
        attachements_size += existing_attachement.file.size 
    files = request.FILES.getlist('attachements')
    for file in files:
        attachements_size += file.size
        if attachements_size > settings.MAX_EMAIL_ATTACHMENTS_SIZE:
            form.add_error('attachements', f"The maximum size of all attachements is {settings.MAX_EMAIL_ATTACHMENTS_SIZE / (1024 * 1024)} mb.")
        else:
            EmailAttachement.objects.create(email_object=email_object, file=file)

#add internal_context_fields to email_object
def add_context_fields_to_email_object(form, email_object):
    email_object.context_fields.clear()
    context_message = form.cleaned_data['context_message']
    for context_field in get_valid_context_fields_from_text(context_message):
        for value, label in EmailContextFieldChoices.choices:
            if label == context_field:
                obj, created = EmailContextField.objects.get_or_create(key=value)
                email_object.context_fields.add(obj)
                
#if event is selected, add all participants to recipient list
def add_event_participants_to_email_object(form, request, email_object):
    event = form.cleaned_data['event']
    if event is not None:
        user_logger.info(f"The participants of the event with id {event.id} were added to EmailObject with id {email_object.id} by user {request.user}")
        for event_participation in event.eventparticipation_set.all():
            for participating_person in event_participation.participatingperson_set.all():
                    #check if person is already in recipient list
                if not email_object.recipientgroup_set.filter(recipient__recipient_person=participating_person.person).exists():
                    recipient_group = RecipientGroup.objects.create(email_object=email_object)      
                    Recipient.objects.create(recipient_person=participating_person.person, recipient_group=recipient_group)


#This method replaces the context fields in the message with the sytem internal values for the given recipient.
def set_recipient_message(email_object : EmailObject, request, recipient):
    #Reset unfullfilled context fields
    recipient.unfullfilled_context_fields=False
    recipient.save()
    dic = email_object.get_email_context_field_dic(request, recipient.recipient_person, email_object.event)
    context_message = email_object.context_message
    context_fields = get_valid_context_fields_from_text(context_message)
    context_fields_with_markers = get_valid_context_fields_with_marker_from_text(context_message)
    for index in range(len(context_fields)):
        #Insert context field values into the message
        for value, label in EmailContextFieldChoices.choices:
            try:
                if label == context_fields[index]:
                    #Current context tokens "[[" need to be escaped for re.sub
                    replace_this = context_fields_with_markers[index].replace(f"{CONTEXT_MARKER_OPEN}", f"\{CONTEXT_MARKER_OPEN_SINGLE}\{CONTEXT_MARKER_OPEN_SINGLE}")
                    if dic[value] == "":
                        #Known exceptions where empty context fields are allowed
                        if value in get_context_fields_that_allow_empty_values():
                            #Prevent double whitespace
                            #replace_this = f"{replace_this}&nbsp;"
                            #NOTE currently ignoring this, because we dont know if the user adds a whitespace
                            pass
                        else:
                            raise KeyError
                    if dic[value] is None:
                        raise KeyError
                    context_message = re.sub(replace_this, dic[value], context_message)
                    break
            except KeyError:
                #Recipient does not have an internal value for this context field.
                recipient.unfullfilled_context_fields=True
                recipient.save()
    recipient.received_email_message=context_message
    recipient.save()

def prepare_messages(email_object):
    messages_dic = {}
    for recipient_group in email_object.recipientgroup_set.all():
        messages_dic[recipient_group]= get_email_for_recipient(recipient_group)
    return messages_dic

def get_email_for_recipient(recipient_group : RecipientGroup):
    email_object = recipient_group.email_object
    email = EmailMessage()
    email.from_email = email_object.from_email
    email.to = [get_email_address_for_recipient(recipient_group.recipient)]
    email.subject = email_object.subject
    email.body = attach_image_as_cid_to_email_body(email, recipient_group.recipient.received_email_message)
    email.cc = [get_email_address_for_recipient(cc_recipient) for cc_recipient in recipient_group.carboncopyrecipient_set.all()]
    email.bcc = [get_email_address_for_recipient(bcc_recipient) for bcc_recipient in recipient_group.blindcarboncopyrecipient_set.all()]
    email.reply_to = [email_object.reply_to]
    for attachement in email_object.emailattachement_set.all():
        file = File(open(attachement.file.path, 'rb'))
        email.attach(os.path.basename(attachement.file.name), file.read())
    email.content_subtype = "html"
    return email

def attach_image_as_cid_to_email_body(email, email_body):
    replaced_image_body = email_body
    # search for img tags in body
    if email_body.find("<img") != -1:
        soup = BeautifulSoup(email_body, 'html.parser') # TODO only usage of B4 so far. Do we need this here?
        img_tags = soup.find_all('img')
        for img_tag in img_tags:
            # Remove first "/"
            old_src = img_tag['src'][1:]
            # Extract the image name from the old src
            image_name = os.path.basename(old_src)
            img_tag['src'] = f"cid:{image_name}"
            with open(old_src, 'rb') as f:
                img = MIMEImage(f.read())
                img.add_header('Content-ID', '<{name}>'.format(name=image_name))
                img.add_header('Content-Disposition', 'inline', filename=image_name)
            email.attach(img)
        replaced_image_body = str(soup)
    return replaced_image_body

def get_email_address_for_recipient(recipient):
    email_address = None
    if recipient.recipient_person and recipient.recipient_person.email:
        email_address = recipient.recipient_person.email
    elif recipient.recipient_email_address:
        email_address = recipient.recipient_email_address
    if not email_address:
        raise NoEmailAddressException
    return email_address

def open_connection_to_send_multiple_emails(messages_dic, email_object):
    connection = get_connection()
    connection.open()
    for recipient_group, message in messages_dic.items():
        try:
            message.send()
            update_recipient_data(recipient_group)
        except Exception as e:                             
            general_logger.error(f"Could not send Email to {message.to}! Email_object id: {email_object.id} Error: {e}")
    connection.close()
    #set sent status when thread is finished
    email_object.sent = True
    email_object.sent_time = timezone.now()
    email_object.save()

def update_recipient_data(recipient_group):
    recipient = recipient_group.recipient
    recipient.sent_time = timezone.now()
    recipient.save()
    for cc_recipient in recipient_group.carboncopyrecipient_set.all():
        cc_recipient.sent_time = timezone.now()
        cc_recipient.save()
    for bcc_recipient in recipient_group.blindcarboncopyrecipient_set.all():
        bcc_recipient.sent_time = timezone.now()
        bcc_recipient.save()
    if recipient_group.email_object.event and (recipient_group.email_object.context_fields.filter(key=EmailContextFieldChoices.EVENT_INVITATION_LINK).count() > 0):
        ParticipatingPerson.objects.filter(person=recipient.recipient_person).update(invitation_sent=True, invitation_sent_at=timezone.now())