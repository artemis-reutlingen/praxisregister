from django.db.models import TextChoices


class ExportFormatOptions(TextChoices):
    EXCEL = 'EXCEL', 'Excel (*.xls)'
    CSV = 'CSV', 'CSV (*.csv)'