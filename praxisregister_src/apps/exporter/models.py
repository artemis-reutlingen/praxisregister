from django.db import models

class Export(models.Model):
    
    user = models.ForeignKey('users.User', on_delete=models.SET_NULL, null=True, blank=True)
    export_time = models.DateTimeField(auto_now=True)
    practices = models.ManyToManyField('practices.Practice')
    persons = models.ManyToManyField('persons.Person')