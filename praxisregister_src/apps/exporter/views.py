from django.http import HttpResponse
from django.shortcuts import render
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from django.views import View

from persons.forms import PersonSelectBaseForm
from persons.models import Person
from persons.tables import SelectPeopleTable
from persons.filters import PeopleFilter

from practices.forms.forms import PracticeSelectBaseForm
from practices.models import Practice
from practices.filters import SmallPracticeFilter
from practices.tables import SelectPracticesTable
from exporter.models import Export
from .resources import *

class ExporterIndexView(View):
    def get(self, request):
        return render(request, 'exporter/index.html')

def export_practices(request):

    practices = Practice.objects.get_practice_objects_for_user(user=request.user)
    practice_filter = SmallPracticeFilter(request.GET, queryset=practices)
    table = SelectPracticesTable(practice_filter.qs)

    if request.method == 'POST':
        selected_practices = Practice.objects.filter(id__in=request.POST.getlist("selection"))

        practice_resource = PracticeResource()
        dataset = practice_resource.export(selected_practices)
        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="practices.xls"'
        #save export data in db
        export = Export(user=request.user)
        export.save()
        export.practices.set(selected_practices)
        return response

    return render(request, 'exporter/forms/practices.html', {'table': table, 'filter': practice_filter})

def export_practice_structure_data(request):
    
    practices = Practice.objects.get_practice_objects_for_user(user=request.user)
    practice_filter = SmallPracticeFilter(request.GET, queryset=practices)
    table = SelectPracticesTable(practice_filter.qs)
    
    if request.method == 'POST':
        selected_practices = Practice.objects.filter(id__in=request.POST.getlist("selection"))

        practice_resource = PracticeStructureDataResource()
        dataset = practice_resource.export(selected_practices)
        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="practices.xls"'
        #save export data in db
        export = Export(user=request.user)
        export.save()
        export.practices.set(selected_practices)
        return response

    return render(request, 'exporter/forms/practices.html', {'table': table, 'filter': practice_filter})

def export_persons(request):

    people = Person.objects.get_person_objects_for_user(user = request.user)
    people_filter = PeopleFilter(request.GET, queryset = people, request = request)
    table = SelectPeopleTable(people_filter.qs)

    if request.method == 'POST':
        selected_people = Person.objects.filter(id__in=request.POST.getlist("selection"))
        person_resource = PersonResource()
        dataset = person_resource.export(selected_people)
        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="persons.xls"'
        #save export data in db
        export = Export(user=request.user)
        export.save()
        export.persons.set(selected_people)
        return response

    return render(request, 'exporter/forms/persons.html', {'table': table, 'filter': people_filter})
