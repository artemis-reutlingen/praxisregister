from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone

@receiver(pre_save)
def track_model_changes(sender, instance, **kwargs):
    if not instance.pk:
        # New model instance is being created
        instance.created_at = timezone.now()

    # Update the modified_at field
    instance.modified_at = timezone.now()
