from django.test import TestCase
from django.contrib.contenttypes.models import ContentType

from practices.models import Practice
from track_changes.models import ChangedField

class DataTrackTest(TestCase):

    def test_saving_data_track(self):
        changed_fields = ChangedField.objects.count()
        Practice.objects.create(name="Test Practice")
        changed_fields_after = ChangedField.objects.count()
        self.assertTrue(changed_fields_after > changed_fields)

    def test_changing_data(self):
        practice = Practice.objects.create(name="Test Practice")
        changed_practice_fields = ChangedField.objects.filter(content_type=ContentType.objects.get_for_model(practice), object_id=practice.id)
        practice.name = "Test Practice 2"
        practice.save()
        self.assertTrue(changed_practice_fields.filter(field_name="name").count() == 2)