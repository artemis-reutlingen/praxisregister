from django.contrib import admin
from django.contrib import admin

# Register your models here.
from addresses.models import County, FederalState


class CountyAdmin(admin.ModelAdmin):
    model = County


admin.site.register(County, CountyAdmin)


class FederalStateAdmin(admin.ModelAdmin):
    model = FederalState
    list_display = (
        'full_name',
    )


admin.site.register(FederalState, FederalStateAdmin)


