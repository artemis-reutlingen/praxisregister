from django.db import models
from django.utils.translation import gettext_lazy as _

class FederalState(models.Model):
    full_name = models.CharField(max_length=256, null=False, blank=False, unique=True)
    abbreviation = models.CharField(max_length=32, null=False, blank=True, default="")

    def __str__(self) -> str:
        return self.full_name


class County(models.Model):
    full_name = models.CharField(max_length=256, null=False, blank=False, unique=True)

    def __str__(self):
        return self.full_name


class AddressMixin(models.Model):
    """
    To be used in other models: e.g. Practice/Site, Person, Events etc.
    """
    street = models.CharField(verbose_name=_('Street'), max_length=256, null=False, blank=True, default="")
    house_number = models.CharField(verbose_name=_('House number'), max_length=256, null=False, blank=True, default="")
    city = models.CharField(verbose_name=_('City'), max_length=256, null=False, blank=True, default="")
    zip_code = models.CharField(verbose_name=_('ZIP code'), max_length=256, null=False, blank=True, default="")

    county = models.ForeignKey(
        County,
        verbose_name=_("County"),
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    federal_state = models.ForeignKey(
        FederalState,
        verbose_name=_("Federal state"),
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    class Meta:
        abstract = True
