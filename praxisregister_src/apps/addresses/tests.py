from django.db import IntegrityError
from django.test import TestCase

from addresses.models import FederalState, County

class CountyModelTest(TestCase):
    def test_duplicate_county_raises_integrity_error(self):

        County.objects.create(
            full_name="Tübingen"
            )

        with self.assertRaises(IntegrityError):
            County.objects.create(
                full_name="Tübingen"
                )

