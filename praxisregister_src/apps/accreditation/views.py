import datetime
from typing import Any

from django import forms
from django.forms import DateInput
from django.urls import reverse_lazy, reverse
from django.views.generic import FormView, DetailView, DeleteView, UpdateView
from django.dispatch import Signal
from practices.models import Practice
from accreditation.forms import AccreditationEditChecklistForm
from accreditation.types import AccreditationStatus
from utils.view_mixins import ManagePracticePermissionRequired
from .models import AccreditationTypeChoices, Accreditation, AccreditationAttribute, AccreditationAttributeChoices, AccreditationType
from django.utils.translation import gettext_lazy as _
from events.models import EventInfo

accreditation_log=Signal()

class AccreditationCreateForm(forms.Form):
    accreditation_type = forms.ChoiceField(
        label=_("Accreditation type"),
        widget=forms.RadioSelect(),
        choices=AccreditationTypeChoices.choices
    )

    def __init__(self, *args, **kwargs):
        """
        Removing already instantiated options, which are passed as kwarg from the view.
        """

        already_instantiated_options = kwargs.pop('already_instantiated_options', None)  # Set default None
        super().__init__(*args, **kwargs)

        if already_instantiated_options:
            for element in already_instantiated_options:
                self.fields['accreditation_type'].choices.remove(element)
                self.fields['accreditation_type'].widget.choices.remove(element)


class PracticeAccreditationCreate(ManagePracticePermissionRequired, FormView):
    form_class = AccreditationCreateForm
    template_name = 'global/forms/generic.html'

    def get_practice_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def get_permission_object(self):
        return self.get_practice_object()

    def get_form_kwargs(self):
        """
        Determining already instantiated accreditation options for this practice instance, and adding to form kwargs.
        """
        form_kwargs = super().get_form_kwargs()

        if self.get_practice_object().accreditations.all():
            already_instantiated_options = []
            for accreditation in self.get_practice_object().accreditations.all():
                element = (accreditation.accreditation_type.name, AccreditationTypeChoices[accreditation.accreditation_type.name].label)
                already_instantiated_options.append(element)
            form_kwargs['already_instantiated_options'] = already_instantiated_options
        return form_kwargs

    def form_valid(self, form):
        accreditation = Accreditation.objects.create_accreditation_for_practice(
            practice=self.get_practice_object(),
            accreditation_type=form.cleaned_data['accreditation_type'],
        )
        accreditation_log.send(sender=self.__class__, username=self.request.user.username, accreditation_id=accreditation.id, practice_id=self.get_practice_object().id)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('practices:accreditations', kwargs={'pk': self.get_practice_object().id})


class PracticeAccreditationList(ManagePracticePermissionRequired, DetailView):
    model = Practice
    template_name = 'practices/accreditations/overview.html'

    def get_permission_object(self):
        return self.get_object()
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['accreditation_types'] = AccreditationTypeChoices.choices
        context["download_url"] = reverse('practices:contract-download', kwargs={"pk": self.object.id})
        context["confirm_url"] = reverse('practices:contract-delete', kwargs={"pk": self.object.id})
        return context

class AccreditationDelete(ManagePracticePermissionRequired, DeleteView):
    model = Accreditation
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return self.get_object().practice

    def get_success_url(self):
        practice_id = self.get_object().practice.id
        accreditation_log.send(sender=self.__class__, username=self.request.user.username, accreditation_id=self.get_object().id, practice_id=practice_id)
        return reverse('practices:accreditations', kwargs={'pk': practice_id})

class AccreditationDetailChecklist(ManagePracticePermissionRequired, DetailView):
    model = Accreditation
    context_object_name = 'accreditation'
    template_name = 'practices/accreditations/detail_checklist.html'
    
    def get_permission_object(self):
        return self.get_object().practice

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['practice'] = self.get_object().practice
        attributes = self.get_object().accreditation_type.accreditationattribute_set.values_list("name", flat=True)
        attr_dict = {}
        criteria_dict = self.get_object().accreditation_criteria_dict
        labels_dict = dict(AccreditationAttributeChoices.choices)
        for attr in attributes:
            if attr == AccreditationAttributeChoices.OPERATES_UNTIL.value:
                context[AccreditationAttributeChoices.OPERATES_UNTIL] = {labels_dict[attr] : criteria_dict[attr]}
            elif attr == AccreditationAttributeChoices.PARTICIPATED_IN_EVENTS.value:
                context[AccreditationAttributeChoices.PARTICIPATED_IN_EVENTS] = {labels_dict[attr] : criteria_dict[attr]}
            else:
                attr_dict[labels_dict[attr]] = criteria_dict.get(attr, False)
        context['attr_dict'] = attr_dict

        return context
    
class AccreditationDetailChecklistEdit(ManagePracticePermissionRequired, UpdateView):
    model = Accreditation
    template_name = 'practices/accreditations/edit_checklist.html'
    form_class = AccreditationEditChecklistForm
   
    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_permission_object(self):
        return self.get_object().practice

    def form_valid(self, form):
        # Update basic accreditation attributes
        AccreditationAttribute.objects.filter(accreditation_type=self.get_object().accreditation_type).delete()
        attributes = []
        for attr in form.cleaned_data['attribute']:
            attributes.append(AccreditationAttribute(
                name=attr,
                accreditation_type=self.get_object().accreditation_type
            ))
        AccreditationAttribute.objects.bulk_create(attributes)

        # Update required events
        self.get_object().accreditation_type.eventinfo_set.clear()
        self.get_object().accreditation_type.eventinfo_set.add(*form.cleaned_data['event_participiation'])

        return super().form_valid(form)
    
    def get_success_url(self):
        accreditation_log.send(sender=self.__class__, username=self.request.user.username, accreditation_id=self.object.id, practice_id=self.get_object().practice.id)
        return reverse_lazy('accreditations:detail', kwargs={'pk': self.object.id})
    
class AccreditationStatusEdit(ManagePracticePermissionRequired, UpdateView):
    """
    Update status field of an accreditation.
    """
    model = Accreditation
    template_name = 'global/forms/generic.html'
    fields = ['status']

    def form_valid(self, form):
        """
        Set the accredited_at field when setting status to pending or finished.
        """
        if form.cleaned_data['status'] == AccreditationStatus.ACCREDITED \
                or form.cleaned_data['status'] == AccreditationStatus.PENDING:
            form.instance.accredited_at = datetime.datetime.now()
            form.instance.save()
        return super().form_valid(form=form)

    def get_permission_object(self):
        return self.get_object().practice

    def get_success_url(self):
        accreditation_log.send(sender=self.__class__, username=self.request.user.username, accreditation_id=self.object.status, practice_id=self.get_object().practice.id)
        return reverse_lazy('accreditations:detail', kwargs={'pk': self.object.id})

class AccreditationDateEdit(ManagePracticePermissionRequired, UpdateView):
    """
    Update just the accredited_at field of an accreditation. E.g. when the automatically set date is not appropriate.
    """

    class AccreditationDateForm(forms.ModelForm):
        class Meta:
            model = Accreditation
            fields = ['accredited_at']
            widgets = {
                'accredited_at': DateInput(
                    format='%Y-%m-%d',
                    attrs={
                        'class': 'form-control',
                        'placeholder': _('Select a date'),
                        'type': 'date'
                    }
                ),
            }

    model = Accreditation
    template_name = 'global/forms/generic.html'
    form_class = AccreditationDateForm

    def get_permission_object(self):
        return self.get_object().practice

    def get_success_url(self):
        accreditation_log.send(sender=self.__class__, username=self.request.user.username, accreditation_id=self.object.id, practice_id=self.get_object().practice.id)
        return reverse_lazy('accreditations:detail', kwargs={'pk': self.object.id})
