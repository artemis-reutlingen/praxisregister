from django.db import models
from django.utils.translation import gettext_lazy as _


class AccreditationStatus(models.TextChoices):
    NOT_ACCREDITED = 'NOT_ACCREDITED', _('Not accredited')
    PENDING = 'PENDING', _('Accreditation is pending')
    ACCREDITED = 'ACCREDITED', _('Accreditation process finished')


class AccreditationTypeChoices(models.TextChoices):
    RESEARCH_PRACTICE = "RESEARCH_PRACTICE", _("Research practice")
    RESEARCH_PRACTICE_PLUS = "RESEARCH_PRACTICE_PLUS", _("Research practice plus")

class AccreditationAttributeChoices(models.TextChoices):
    HAS_FULL_CARE_MANDATE = "HAS_FULL_CARE_MANDATE", _("Full care mandate")
    HAS_MORE_THAN_500_TREATMENT_CERTIFICATES_PER_QUARTER = "HAS_MORE_THAN_500_TREATMENT_CERTIFICATES_PER_QUARTER", _("At least 500 treatment certificates per quarter")
    IS_OPEN_AT_LEAST_35_HOURS_PER_WEEK = "IS_OPEN_AT_LEAST_35_HOURS_PER_WEEK", _("At least 35 hours opened per week")
    OFFERS_HOME_VISITS = "OFFERS_HOME_VISITS", _("Regular home visits")
    HAS_ANY_DMP_IMPLEMENTED = "HAS_ANY_DMP_IMPLEMENTED", _("Disease Management Programs")
    NUMBER_OF_IMPLEMENTED_DMPS = "NUMBER_OF_IMPLEMENTED_DMPS", _("Number of implemented Disease Management Programs")
    OPERATES_UNTIL = "OPERATES_UNTIL", _("Operation of practice ensured until")
    HAS_AUTOMATED_BACKUPS = "HAS_AUTOMATED_BACKUPS", _("Backup processes established")
    HAS_WORKSTATION_AVAILABLE = "HAS_WORKSTATION_AVAILABLE", _("Workstation with access to PDMS and internet")
    IS_USING_EPA = "IS_USING_EPA", _("EPA is used for medical documentation")
    IS_OPEN_FOR_DATA_EXPORT = "IS_OPEN_FOR_DATA_EXPORT", _("Open for automated data export")
    TANDEM_DOCTOR_IS_AVAILABLE= "TANDEM_DOCTOR_IS_AVAILABLE", _("Research doctor available")
    TANDEM_DOCTOR_HOURS_GTE_15 = "TANDEM_DOCTOR_HOURS_GTE_15", _("Research doctor works more than 15h per week")
    TANDEM_ASSISTANT_IS_AVAILABLE = "TANDEM_ASSISTANT_IS_AVAILABLE", _("Research assistant available")
    TANDEM_ASSISTANT_HOURS_GTE_15 ="TANDEM_ASSISTANT_HOURS_GTE_15", _("Research assistant works more than 15h per week")
    HAS_GP_SERVICE_SPECTRUM = "HAS_GP_SERVICE_SPECTRUM", _("GP range of services more than 50%")
    HAS_SIGNED_CONTRACT = "HAS_SIGNED_CONTRACT", _("Contract signed")
    TANDEM_DOCTOR_ATTENDED_TRAINING = "TANDEM_DOCTOR_ATTENDED_TRAINING", _("Research doctor attended FoPraNet-BW training")
    TANDEM_ASSISTANT_ATTENDED_TRAINING = "TANDEM_ASSISTANT_ATTENDED_TRAINING", _("Research assistant attended FoPraNet-BW training")
    PARTICIPATED_IN_EVENTS = "PARTICIPATED_IN_EVENTS", _("Practice has participated in events")