from django.db import models
from polymorphic.models import PolymorphicModel, PolymorphicManager
from django.utils.translation import gettext_lazy as _

from accreditation.types import AccreditationTypeChoices, AccreditationAttributeChoices, AccreditationStatus
from accreditation.model_mixins import AccreditationMixin
from persons.models import FoPraNetTraining, Person
from practices.models import Practice
from importer.models import ImportableMixin
from utils.constants import FoPraNetTrainingType

class AccreditationManager(models.Manager):
    @staticmethod
    def create_accreditation_for_practice(practice: Practice, accreditation_type: AccreditationTypeChoices):
        accreditation_type, _ = AccreditationType.objects.get_or_create(
            name = accreditation_type, 
            institute = practice.assigned_institute
        )
        return Accreditation.objects.create(
            practice = practice, 
            accreditation_type = accreditation_type, 
            practice_has_to_operate_until = accreditation_type.practice_has_to_operate_until
        )
    
    @staticmethod
    def create_accreditation_attribute_for_practice(practice: Practice, accreditation_type: AccreditationTypeChoices, accreditation_attribute: AccreditationAttributeChoices):
        acc = Accreditation.objects.filter(practice=practice, accreditation_type__name=accreditation_type.name).first()
        if acc.accreditation_type.name == accreditation_type.name:
            accrType, created = AccreditationAttribute.objects.get_or_create(name=accreditation_attribute, accreditation_type=acc.accreditation_type)
        return accrType

"""
An AccreditationType like "Forschungspraxis" belongs to one institute.
It can have multiple real accreditations (connected to practices).
Every AccreditationType has a set of attributes, which can be defined by the institute.
"""
class AccreditationType(ImportableMixin):

    name = models.CharField(
        verbose_name=_("Type"),
        max_length=128,
        choices=AccreditationTypeChoices.choices,
        blank=False,
    )

    institute = models.ForeignKey('institutes.Institute', verbose_name=_('Institute'), on_delete=models.CASCADE)
    practice_has_to_operate_until = models.DateField(verbose_name=_('Operation must be guaranteed until'), null=True, blank=False)

    def __str__(self) -> str:
        return f"{AccreditationTypeChoices[self.name].label}"

class Accreditation(ImportableMixin, AccreditationMixin):

    class Meta:
        verbose_name = _("Accreditation")
        verbose_name_plural = _("Accreditations")
        
    objects = AccreditationManager()

    accreditation_type = models.ForeignKey(AccreditationType, null=True, on_delete=models.SET_NULL)

    status = models.CharField(
        verbose_name=_("Status"),
        max_length=128,
        choices=AccreditationStatus.choices,
        default=AccreditationStatus.NOT_ACCREDITED,
        blank=False,
    )

    accredited_at = models.DateField(verbose_name=_("Last update"), null=True, blank=True)

    practice = models.ForeignKey('practices.Practice', related_name='accreditations', null=False, blank=False,
                                 on_delete=models.CASCADE)
    
    practice_has_to_operate_until = models.DateField(verbose_name=_('Operation must be guaranteed until'), null=True, blank=False)

    @property
    def is_accredited(self):
        return self.status == AccreditationStatus.ACCREDITED
    
    @property
    def _tandem_doctor_has_attended_fopranet_basic_training(self):
        return _person_has_attended_fopranet_training(person=self.practice.research_doctor, training_type=FoPraNetTrainingType.FOPRANET_BASIC_TRAINING)

    @property
    def _tandem_assistant_has_attended_fopranet_basic_training(self):
        return _person_has_attended_fopranet_training(person=self.practice.research_assistant, training_type=FoPraNetTrainingType.FOPRANET_BASIC_TRAINING)

    def __str__(self):
        return str(self.accreditation_type)

def _person_has_attended_fopranet_training(person: Person, training_type: FoPraNetTrainingType):
    """
    Helper function for determining if a person has attended a certain training identified by FoPraNetTrainingType.
    """
    try:
        training = person.fopranet_trainings.filter(type__name=training_type.value)
    except FoPraNetTraining.DoesNotExist:
        training = None
    return bool(training)

"""
An accreditation attribute is a specific requirement for a certain accreditation type.
"""
class AccreditationAttribute(models.Model):

    name = models.CharField(
        verbose_name=_("Accreditation attribute"),
        max_length=128,
        choices=AccreditationAttributeChoices.choices,
        blank=True,
    )

    accreditation_type = models.ForeignKey(AccreditationType, null=True, on_delete=models.CASCADE)

