from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from guardian.admin import GuardedModelAdmin

from accreditation.models import Accreditation, AccreditationType, AccreditationAttribute

class AccreditationAdmin(GuardedModelAdmin):
    model = Accreditation
    list_display = ('id', 'practice', 'accreditation_type', 'status') 
    list_per_page = 20
    list_select_related = ['accreditation_type', 'practice']
    list_filter = ['accredited_at', 'status', 'accreditation_type', 'practice__assigned_institute']
    ordering = ('-accredited_at',)
    search_fields = ['practice__name__icontains']

class AccreditationTypeAdmin(GuardedModelAdmin):
    model = AccreditationType
    list_display = ('id', 'name', 'institute') 
    list_per_page = 20
    list_select_related = ['institute']
    list_filter = ['institute']
    search_fields = ['name__icontains']


class AccreditationAttributeAdmin(GuardedModelAdmin):
    model = AccreditationAttribute
    list_display = ('id', 'name', 'accreditation_type') 
    list_per_page = 20
    list_select_related = ['accreditation_type']
    list_filter = ['accreditation_type__institute']
    search_fields = ['name__icontains']

AccreditationAdmin.model._meta.verbose_name_plural = "1. " + _("Accreditations")
AccreditationTypeAdmin.model._meta.verbose_name_plural = "2. " + _("Accreditation types")
AccreditationAttributeAdmin.model._meta.verbose_name_plural = "3. " + _("Accreditation attributes")

admin.site.register(Accreditation, AccreditationAdmin)
admin.site.register(AccreditationType, AccreditationTypeAdmin)
admin.site.register(AccreditationAttribute, AccreditationAttributeAdmin)