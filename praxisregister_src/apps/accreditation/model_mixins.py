from accreditation.types import AccreditationAttributeChoices
from events.models import EventInfo

class AccreditationMixin:

    @property
    def has_any_DMP_implemented(self) -> bool:
        return any(dmp.is_available for dmp in self.practice.disease_management_programs.all())

    @property
    def number_of_implemented_DMPs(self) -> str:
        """
        Returns number of DMPs as a string representation "x out of y": e.g. "3 / 10"
        """
        number_of_implemented_DMPs = sum(
            dmp.is_available for dmp in self.practice.disease_management_programs.all()
        )
        number_of_possible_DMPs = len(self.practice.disease_management_programs.all())

        return f"{number_of_implemented_DMPs} / {number_of_possible_DMPs}"

    @property
    def operation_ensured_until_01_2025_display_value(self):
        value = None
        if self.practice.structure_practice_operating_until and self.practice_has_to_operate_until:
            value = self.practice.structure_practice_operating_until >= self.practice_has_to_operate_until
        return value
    
    @property
    def required_events(self):
        return EventInfo.objects.filter(accreditation_requirement=self.accreditation_type)

    @property
    def accreditation_criteria_dict(self):
        criteria = {}
        practice = self.practice

        has_tandem_assistant = practice.research_assistant is not None
        if has_tandem_assistant:
            criteria[AccreditationAttributeChoices.TANDEM_ASSISTANT_IS_AVAILABLE] = True
            criteria[AccreditationAttributeChoices.TANDEM_ASSISTANT_HOURS_GTE_15] = practice.research_assistant.weekly_working_hours >= 15
            criteria[AccreditationAttributeChoices.TANDEM_ASSISTANT_ATTENDED_TRAINING] = self._tandem_assistant_has_attended_fopranet_basic_training
        else:
            criteria[AccreditationAttributeChoices.TANDEM_ASSISTANT_IS_AVAILABLE] = False
            criteria[AccreditationAttributeChoices.TANDEM_ASSISTANT_HOURS_GTE_15] = False
            criteria[AccreditationAttributeChoices.TANDEM_ASSISTANT_ATTENDED_TRAINING] = False

        has_tandem_doctor = practice.research_doctor is not None
        if has_tandem_doctor:
            criteria[AccreditationAttributeChoices.TANDEM_DOCTOR_IS_AVAILABLE] = True
            criteria[AccreditationAttributeChoices.TANDEM_DOCTOR_HOURS_GTE_15] = practice.research_doctor.weekly_working_hours >= 15
            criteria[AccreditationAttributeChoices.TANDEM_DOCTOR_ATTENDED_TRAINING] = self._tandem_doctor_has_attended_fopranet_basic_training
        else:
            criteria[AccreditationAttributeChoices.TANDEM_DOCTOR_IS_AVAILABLE] = False
            criteria[AccreditationAttributeChoices.TANDEM_DOCTOR_HOURS_GTE_15] = False
            criteria[AccreditationAttributeChoices.TANDEM_DOCTOR_ATTENDED_TRAINING] = False

        criteria[AccreditationAttributeChoices.OPERATES_UNTIL] = self.operation_ensured_until_01_2025_display_value
        criteria[AccreditationAttributeChoices.HAS_FULL_CARE_MANDATE] = practice.structure_full_care_mandate
        criteria[AccreditationAttributeChoices.HAS_GP_SERVICE_SPECTRUM] = practice.structure_gp_range_of_services
        criteria[AccreditationAttributeChoices.IS_OPEN_AT_LEAST_35_HOURS_PER_WEEK] = practice.structure_at_least_35_hours_openend
        criteria[AccreditationAttributeChoices.OFFERS_HOME_VISITS] = practice.services_home_visits
        criteria[AccreditationAttributeChoices.NUMBER_OF_IMPLEMENTED_DMPS] = self.number_of_implemented_DMPs
        criteria[AccreditationAttributeChoices.HAS_ANY_DMP_IMPLEMENTED] = self.has_any_DMP_implemented
        criteria[AccreditationAttributeChoices.IS_USING_EPA] = practice.it_epa_used_for_medical_documentation
        criteria[AccreditationAttributeChoices.HAS_AUTOMATED_BACKUPS] = practice.it_automatic_backups_established
        criteria[AccreditationAttributeChoices.HAS_WORKSTATION_AVAILABLE] = practice.it_fopranet_ready_workstation_available
        criteria[AccreditationAttributeChoices.IS_OPEN_FOR_DATA_EXPORT] = practice.it_openness_data_export
        criteria[AccreditationAttributeChoices.HAS_MORE_THAN_500_TREATMENT_CERTIFICATES_PER_QUARTER] = practice.structure_more_than_500_bills_per_quarter
        criteria[AccreditationAttributeChoices.HAS_SIGNED_CONTRACT] = practice.is_contract_signed
        criteria[AccreditationAttributeChoices.PARTICIPATED_IN_EVENTS] = {r_event: r_event.has_pratice_participated(practice) for r_event in self.required_events}
        return criteria
