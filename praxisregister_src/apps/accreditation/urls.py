from django.urls import path

from . import views

app_name = 'accreditations'
urlpatterns = [
    path('<int:pk>/', views.AccreditationDetailChecklist.as_view(), name='detail'),
    path('<int:pk>/edit-checklist', views.AccreditationDetailChecklistEdit.as_view(), name='edit-checklist'),
    path('<int:pk>/delete', views.AccreditationDelete.as_view(), name='delete'),
    path('<int:pk>/edit', views.AccreditationStatusEdit.as_view(), name='edit'),
    path('<int:pk>/date/edit', views.AccreditationDateEdit.as_view(), name='edit-accredited-at'),
]
