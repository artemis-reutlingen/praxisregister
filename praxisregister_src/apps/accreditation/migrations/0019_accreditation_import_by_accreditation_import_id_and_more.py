# Generated by Django 4.2.7 on 2024-02-20 07:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accreditation', '0018_alter_accreditationattribute_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='accreditation',
            name='import_by',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Import by'),
        ),
        migrations.AddField(
            model_name='accreditation',
            name='import_id',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Import ID'),
        ),
        migrations.AddField(
            model_name='accreditationtype',
            name='import_by',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Import by'),
        ),
        migrations.AddField(
            model_name='accreditationtype',
            name='import_id',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Import ID'),
        ),
        migrations.AlterUniqueTogether(
            name='accreditationtype',
            unique_together={('import_id', 'import_by')},
        ),
    ]
