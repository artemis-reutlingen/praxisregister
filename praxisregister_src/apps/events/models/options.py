from django.db import models
from django.utils.translation import gettext_lazy as _

class AbstractChoiceOption(models.Model):
    """
    Abstract Option model. Just encapsulating a simple name attribute, but allowing DB driven management
    of data-list / choices style behavior.
    """
    name = models.CharField(max_length=256, blank=False, default="", unique=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class EventTypeOption(AbstractChoiceOption):
    """
    Represents a single option as used for type field in AbstractResearchExperience.
    """

class ParticipatingPersonInvitationChoices(models.TextChoices):
    INVITATION_PENDING = 'INVITATION_PENDING', _('Invitation pending')
    INVITATION_SENT = 'INVITATION_SENT', _('Invitation sent')
    SIGNED_UP =  'SIGNED_UP', _('Signed up')
    CANCELLED = 'CANCELLED', _('Cancelled')
    NO_RESPONSE = 'NO_RESPONSE', _('No response')
    PARTICIPATED = 'PARTICIPATED', _('Participated')
