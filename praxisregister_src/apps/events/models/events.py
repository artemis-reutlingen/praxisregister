import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _
from events.types import EventType, HasSignedUpStatus, Status
from guardian.shortcuts import assign_perm, get_objects_for_user
from guardian.models import UserObjectPermissionBase, GroupObjectPermissionBase
from importer.models import ImportableMixin
from django.core.validators import URLValidator
from track_changes.models import TrackChangesMixin


class EventInfoManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()

    @staticmethod
    def get_events_for_user(user):
        return get_objects_for_user(user, 'events.view_eventinfo')


class EventInfo(TrackChangesMixin, ImportableMixin, models.Model):
    objects = EventInfoManager()

    title = models.CharField(max_length=512, blank=False, verbose_name=_('Name'))
    description = models.TextField(verbose_name=_('Description'), blank=True, null=True)
    
    takes_place = models.CharField(
        verbose_name=_('Takes place'),
        max_length=128,
        choices=EventType.choices,
        default=EventType.ONLINE,
        blank=False,
    )
    video_call_url = models.CharField(verbose_name=_('Video call link'), validators=[URLValidator], max_length=256, null=True, blank=True)
    location = models.CharField(verbose_name=_('Location'), max_length=256, blank=True, null=True)
    type = models.CharField(verbose_name=_('Type of event'), max_length=128, blank=True, null=True)
    
    participant_group = models.CharField(verbose_name=_('Participant group'), max_length=256, blank=True, null=True)
    max_participation = models.PositiveSmallIntegerField(verbose_name=_('Maximum number of participants'), blank=True, null=True)
    prerequisites = models.CharField(verbose_name=_('Prerequisites'), max_length=512, blank=True, null=True)
    organization = models.CharField(verbose_name=_('Organization'), max_length=256, blank=True, null=True)
    lecturers = models.CharField(verbose_name=_('Lecturers'), max_length=256, blank=True, null=True)
    certificates = models.CharField(verbose_name=_('Certificates'), max_length=256, blank=True, null=True)
    cme_points = models.BooleanField(verbose_name=_('CME points requested'), default=False)
    status = models.CharField(
        verbose_name=_("Current status"),
        max_length=128,
        choices=Status.choices,
        default=Status.PLANNED,
        blank=False,
    )
    additional_notes = models.TextField(verbose_name=_('Additional notes'), blank=True, null=True)
    fixed_dates = models.BooleanField(verbose_name=_('Fixed dates'), default=True)
    is_draft = models.BooleanField(verbose_name=_('Is a draft'), default=False)

    assigned_institute = models.ForeignKey('institutes.Institute', verbose_name=_('Institute'),
                                           on_delete=models.SET_NULL,
                                           null=True)

    author = models.ForeignKey('users.User', verbose_name=_('Created by'), on_delete=models.SET_NULL, null=True)

    accreditation_requirement = models.ManyToManyField('accreditation.AccreditationType', verbose_name=_('Accreditation requirement'))

    def __str__(self):
        return f"{_('Event')}  {str(self.id)}: {self.title}"

    def assign_perms(self, user):
        assign_perm('events.view_eventinfo', user.group, self)
        assign_perm('events.change_eventinfo', user.group, self)
        assign_perm('events.delete_eventinfo', user.group, self)

    # we check if any person of this practice has participated in the event
    def has_pratice_participated(self, practice):
        return ParticipatingPerson.objects.filter(event_participation__event=self, person__practice=practice).exists()

# Explicit definition of EventInfo object permissions. This ensures permission objects are also deleted when an event is deleted instead of being orphaned.
class EventInfoUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(EventInfo, on_delete = models.CASCADE)

class EventInfoGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(EventInfo, on_delete = models.CASCADE)

class EventParticipation(TrackChangesMixin, ImportableMixin, models.Model):
    created_by = models.ForeignKey('users.User', verbose_name=_('Created by'), on_delete=models.SET_NULL, null=True)
    event = models.ForeignKey('events.EventInfo', verbose_name=_('Event'), on_delete=models.CASCADE, null=False)
    practice = models.ForeignKey('practices.Practice', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = _("Event participation")
        verbose_name_plural = _("Event participations")
        unique_together = ['event', 'practice']

    def add_participating_person(self, person):
        if person in self.participatingperson_set.all().values('person'):
            raise Exception("This Person is already added to this EventParticipation")
        participating_person = ParticipatingPerson.objects.create(person=person, event_participation=self)
        self.participatingperson_set.add(participating_person)
        self.save()

    def remove_participating_person(self, person):
        participating_person_related_persons = [pp.person for pp in self.participatingperson_set.all()]
        if person in participating_person_related_persons:
            self.participatingperson_set.all().get(person=person).delete()
            self.save()
        else:
            raise Exception("Cannot remove this Person from EventParticipation")

    def __str__(self) -> str:
        return _("Event participation") + f" {self.event.title}: {self.practice.name}"

class ParticipatingPerson(TrackChangesMixin, ImportableMixin, models.Model):
    person = models.ForeignKey('persons.Person', on_delete=models.CASCADE, null=False)
    event_participation = models.ForeignKey('events.EventParticipation', on_delete=models.CASCADE, null=False)

    invitation_sent = models.BooleanField(verbose_name=_('Invitation sent'), default=False)
    invitation_sent_at = models.DateTimeField(verbose_name=_("Invitation sent at"), null=True, blank=True)

    has_signed_up = models.CharField(verbose_name=_('Registration status'), max_length=128, default=HasSignedUpStatus.UNKNOWN, choices=HasSignedUpStatus.choices)
    
    invitation_code = models.UUIDField(default=uuid.uuid4, unique=True, editable=False, verbose_name=_('Invitation code'))
    responded_to_invitation_at = models.DateTimeField(verbose_name=_("Responded to invitation at"), null=True, blank=True)

    has_participated = models.BooleanField(verbose_name=_('Has participated'), default=False)

    class Meta:
        verbose_name = _("Participating person")
        verbose_name_plural = _("Participating persons")
        unique_together = ['person', 'event_participation']
    
    def __str__(self) -> str:
        return _("Participating persons") + f" {self.person.full_name_with_title}: {self.event_participation.event.title}"
