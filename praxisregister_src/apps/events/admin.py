from django.urls import reverse
from django.utils.html import format_html
from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from guardian.admin import GuardedModelAdmin
from guardian.shortcuts import get_objects_for_group

from institutes.models import Institute
from events.models.events import EventInfo, ParticipatingPerson, EventParticipation

#RecipientGroup Filter
class PersonInstituteFilter(admin.SimpleListFilter):
    title = _('Institute')
    parameter_name = 'institute'

    def lookups(self, request, model_admin):
        institutes = []
        for institute in Institute.objects.all():
            institutes.append((institute.id, institute.full_name))
        return institutes

    def queryset(self, request, queryset):
            if request.GET.get(self.parameter_name):
                institute = Institute.objects.get(pk=request.GET.get(self.parameter_name))
                queryset = ParticipatingPerson.objects.filter(pk__in=get_objects_for_group(institute.group, 'persons.manage_person'))
            return queryset
    
class EventAdmin(GuardedModelAdmin):
    model = EventInfo
    list_display = ('id', 'title', 'status', 'is_draft', 'author', 'assigned_institute', 'updated_at') 
    list_per_page = 20
    list_select_related = ['author', 'assigned_institute']
    list_filter = ['status', 'is_draft', 'assigned_institute']
    ordering = ('-updated_at',)
    search_fields = ['title__icontains', 'author__first_name__icontains', 'author__last_name__icontains']

class EventParticipationAdmin(GuardedModelAdmin):
    model = EventParticipation
    list_display = ('id', 'created_by', 'event', 'practice', 'updated_at') 
    list_per_page = 20
    list_select_related = ['created_by', 'event', 'practice']
    list_filter = ['event__status', 'practice__assigned_institute']
    ordering = ('-updated_at',)
    search_fields = ['event__title__icontains', 'created_by__first_name__icontains', 'created_by__last_name__icontains']

class ParticipatingPersonAdmin(GuardedModelAdmin):
    model = ParticipatingPerson
    list_display = ('id', 'person', 'event_participation__id', 'invitation_sent', 'has_signed_up', 'has_participated') 
    list_per_page = 20
    list_select_related = ['person', 'event_participation']
    list_filter = [PersonInstituteFilter]
    search_fields = ['event_participation__event__title__icontains', 'person__first_name', 'person__last_name']

    @admin.display(description=_('Event participation'))
    def event_participation__id(self, participating_person):
        if(participating_person.event_participation):
            url = reverse('admin:events_eventparticipation_change', args=[participating_person.event_participation.id])
            return format_html('<a href="{}">{}<a>', url, participating_person.event_participation.id)
        else:
            return '-'

EventAdmin.model._meta.verbose_name_plural = "1. " + _("Events")
EventParticipationAdmin.model._meta.verbose_name_plural = "2. " + _("Event participations")
ParticipatingPersonAdmin.model._meta.verbose_name_plural = "3. " + _("Participating persons")

admin.site.register(EventInfo, EventAdmin)
admin.site.register(EventParticipation, EventParticipationAdmin)
admin.site.register(ParticipatingPerson, ParticipatingPersonAdmin)
