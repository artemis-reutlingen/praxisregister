from django import forms
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse_lazy, reverse
from django.utils.translation import gettext_lazy as _
from django.views import View
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView, FormView
from django_tables2 import RequestConfig
from django.dispatch import Signal
from django.core.exceptions import PermissionDenied
from django.utils import timezone
from guardian.mixins import PermissionRequiredMixin
from events.filters import EventParticipationFilter, EventParticipationFormHelper
from events.models.events import EventInfo, EventParticipation, ParticipatingPerson
from events.tables import EventParticipationsTable
from events.types import Status, HasSignedUpStatus
from events.forms import *
from persons.models import Person
from persons.filters import PeopleFilter, PeopleFilterFormHelper
from persons.models import Person
from persons.tables import SelectPeopleTable
from practices.models import Practice
from practices.tables import SelectPracticesTable
from practices.filters import PracticeFilter
from utils.view_mixins import ManagePracticePermissionRequired, ManagePersonPermissionRequired
from login_required import login_not_required

events_edit_log=Signal()
events_participation_edit_log=Signal()
events_bulk_creation_log=Signal()

class PracticeEventParticipationList(ManagePracticePermissionRequired, ListView):
    """
    For listing all event participations of a specific practice.
    """
    model = EventParticipation
    template_name = 'practices/event_participations/eventparticipation_list.html'
    paginate_by = 5

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['practice'] = self.get_practice_object()
        return context

    def get_queryset(self):
        queryset = super().get_queryset().filter(
            practice=self.get_practice_object()
        ).order_by('-created_at')
        return queryset


class EventParticipationCreate(ManagePracticePermissionRequired, CreateView):
    model = EventParticipation
    template_name = 'global/forms/generic.html'
    form_class = EventParticipationForm

    def get_permission_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def form_valid(self, form):
        form.instance.practice = Practice.objects.get(pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_form_class(self):
        # Injecting request context into form class: selectable options/queryset of events
        # Removing events as selectable choices for events where the current practice already has a participation

        practice = Practice.objects.get(pk=self.kwargs['pk'])  # get practice via URL param

        existing_event_participations_for_practice = EventParticipation.objects.filter(practice=practice)
        already_participating_event_ids_for_practice = existing_event_participations_for_practice.values('event_id')

        available_choices = EventInfo.objects.get_events_for_user(self.request.user).distinct().exclude(pk__in=already_participating_event_ids_for_practice)

        form_class = EventParticipationForm
        form_class.base_fields['event'] = forms.ModelChoiceField(
            label=_('Event'),
            queryset=available_choices,
            required=True,
        )
        return form_class

    def get_success_url(self):
        events_participation_edit_log.send(sender=self.__class__, username=self.request.user.username, participation_id=self.object.id)
        return reverse_lazy('practices:event-participation-list', kwargs={'pk': self.object.practice_id})


class EventParticipationDelete(ManagePracticePermissionRequired, DeleteView):
    """ Deletes an EventParticipation. """
    model = EventParticipation
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return self.get_object().practice

    def get_success_url(self):
        events_participation_edit_log.send(sender=self.__class__, username=self.request.user.username, participation_id=self.object.id)
        return reverse_lazy('practices:event-participation-list', kwargs={'pk': self.object.practice_id})


class RemoveParticipatingPerson(ManagePersonPermissionRequired, SuccessMessageMixin, View):
    success_message = _('Removed person from this EventParticipation')

    def get_participating_person(self):
        return ParticipatingPerson.objects.get(pk=self.kwargs['participating_person_id'])

    def get_permission_object(self):
        return self.get_participating_person().person

    def post(self, request, *args, **kwargs):
        event_participation = EventParticipation.objects.get(pk=self.kwargs['event_participation_id'])
        person = self.get_participating_person().person
        event_participation.remove_participating_person(person)
        return HttpResponseRedirect(redirect_to=reverse('events:participation-list', kwargs={'pk': event_participation.event.id}))


class AddParticipatingPerson(ManagePracticePermissionRequired, FormView):
    template_name = 'global/forms/generic.html'

    def get_event_participation(self):
        return EventParticipation.objects.get(pk=self.kwargs['pk'])

    def get_permission_object(self):
        return self.get_event_participation().practice

    def form_valid(self, form):
        self.get_event_participation().add_participating_person(form.cleaned_data['person'])
        return super().form_valid(form)

    def get_form_class(self):
        # Injecting request context into form class: selectable options/queryset of practice persons
        practice = self.get_event_participation().practice

        already_participating_persons_ids = self.get_event_participation().participatingperson_set.all().values(
            'person_id'
        )
        practice_persons = Person.objects.all().filter(practice=practice)
        available_choices = practice_persons.exclude(pk__in=already_participating_persons_ids)

        form_class = ParticipatingPersonForm
        form_class.base_fields['person'] = forms.ModelChoiceField(
            queryset=available_choices,
            required=True,
        )
        return form_class

    def __str__(self) -> str:
        return f"{_('Participating person')}"

    def get_success_url(self):
        return reverse_lazy(
            'practices:event-participation-list',
            kwargs={'pk': self.get_event_participation().practice.id}
        )


class EditParticipatingPerson(ManagePersonPermissionRequired, UpdateView):
    form_class = ParticipatingPersonEditForm
    template_name = 'events/forms/edit_participating_person.html'
    model = ParticipatingPerson

    def get_event_participation(self):
        return EventParticipation.objects.get(pk=self.kwargs['event_participation_id'])

    def get_permission_object(self):
        person = Person.objects.get(pk=self.get_object().person.id)
        return person

    def get_success_url(self):
        return reverse_lazy(
            'events:participation-list',
            kwargs={'pk': self.get_event_participation().event.id}
        )


def bulk_create_event_participations_for_practices(request, pk):
    """
    View for creating event participations from event context. Allowing users to bulk create for selected practices.
    Seems to be cleaner with function based view.
    """
    event = EventInfo.objects.get(pk=pk)
    if not request.user.has_perm('events.view_eventinfo', event):
        raise PermissionDenied
    practices = Practice.objects.get_practice_objects_for_user(user=request.user).exclude(id__in=EventParticipation.objects.filter(event=event).filter(practice__isnull=False).values_list('practice__id', flat=True))
    practice_filter = PracticeFilter(request, queryset = practices)
    table = SelectPracticesTable(practice_filter.qs)
    RequestConfig(request,paginate={"per_page": 12}).configure(table)

    if request.method == 'POST':
        # TODO: Refactor create action into helper function.
        with transaction.atomic():
            selectedPractices = Practice.objects.filter(id__in=request.POST.getlist("selection"))
            if len(selectedPractices):
                bulk_participation_list = []
                filtered_practices = selectedPractices.exclude(id__in=EventParticipation.objects.filter(event=event).values_list('practice__id', flat=True))
                for practice in filtered_practices:
                    if not request.user.has_perm('practices.manage_practice', practice):
                        raise PermissionDenied
                    bulk_participation_list.append(EventParticipation(
                        event=event,
                        practice=practice,
                        created_by=request.user
                    ))
                EventParticipation.objects.bulk_create(bulk_participation_list)
                class BulkCreateEventParticipationsForPractices: pass
                events_bulk_creation_log.send(sender=BulkCreateEventParticipationsForPractices, username=request.user.username, event_id=event.id, participations="".join([str(participation.practice.id) + " " for participation in bulk_participation_list]))
                messages.success(request, _("Successfully added practice to the event"))
                return redirect('events:participation-practice-bulk-create', pk=pk)
    return render(request, 'events/forms/create_event_practice_participations.html', {'event': event, 'table': table, 'filter': practice_filter, 'add_practices': "active"})

def bulk_create_event_participations_for_persons(request, pk):
    event = EventInfo.objects.get(pk=pk)
    if not request.user.has_perm('events.view_eventinfo', event):
        raise PermissionDenied
    persons = Person.objects.get_person_objects_for_user(user=request.user).exclude(id__in=ParticipatingPerson.objects.filter(event_participation__event=event).values_list('person__id', flat=True))
    
    people_filter = PeopleFilter(request.GET, queryset=persons, request=request)
    table = SelectPeopleTable(people_filter.qs)
    RequestConfig(request,paginate={"per_page": 12}).configure(table)
    if request.method == 'POST':
        # TODO: Refactor create action into helper function.
        with transaction.atomic():
            #Get checked persons    
            selectedPersons = Person.objects.filter(id__in=request.POST.getlist("selection"))
            bulk_participating_practice_list = []
            bulk_participating_person_list = []
            #prevent duplicate creation
            filtered_persons = selectedPersons.exclude(id__in=ParticipatingPerson.objects.filter(event_participation__event=event).values_list('person__id', flat=True))
            practice_ids = filtered_persons.filter(practice__isnull=False).values_list('practice__id', flat=True).distinct()
            #Filter practices which already exist in EventParticipation
            practice_ids_of_event_to_exclude = EventParticipation.objects.filter(Q(event=event) & Q(practice__id__in=practice_ids)).values_list('practice__id', flat=True)
            practices = Practice.objects.filter(Q(id__in=practice_ids) & ~Q(id__in=practice_ids_of_event_to_exclude))
            #Create praticipations for practices
            for practice in practices:
                if not request.user.has_perm('practices.manage_practice', practice):
                    raise PermissionDenied                   
                bulk_participating_practice_list.append(EventParticipation(
                    event=event,
                    practice=practice,
                    created_by=request.user
                ))
            EventParticipation.objects.bulk_create(bulk_participating_practice_list)
            #Extra query to get EventParticipation from already saved practices
            event_participations = EventParticipation.objects.filter(Q(event=event) & Q(practice__id__in=practice_ids))
            #Create participations for persons with practice
            event_participations_dic = {participation.practice.id : participation for participation in event_participations}
            for person in filtered_persons.filter(practice__isnull=False):
                bulk_participating_person_list.append(ParticipatingPerson(
                    person=person,
                    event_participation=event_participations_dic.get(person.practice.id)
                ))
            #Check if there are any persons without a practice
            not_part_of_practice = filtered_persons.filter(practice__isnull=True)
            if len(not_part_of_practice) > 0:
                event_participation_without_practice = EventParticipation.objects.create(event=event, created_by=request.user)
                for person in not_part_of_practice:
                    bulk_participating_person_list.append(ParticipatingPerson(
                        person=person,
                        event_participation=event_participation_without_practice
                    ))
            #Create all participating persons
            ParticipatingPerson.objects.bulk_create(bulk_participating_person_list)

            if len(bulk_participating_person_list) > 0:
                class BulkCreateEventParticipationsForPersons: pass
                events_bulk_creation_log.send(sender=BulkCreateEventParticipationsForPersons, username=request.user.username, event_id=event.id, participations="".join([str(participation.person.id) + " " for participation in bulk_participating_person_list]))
                messages.success(request, _("Successfully added people to the event"))
                return redirect('events:participation-persons-bulk-create', pk=pk)
    return render(request, 'events/forms/create_event_person_participations.html', {'event': event, 'table': table, 'filter': people_filter, 'add_participations': "active"})

class EventParticipationList(PermissionRequiredMixin, DetailView):
    """ Lists all participating persons for specific event in event context. """
    model = EventInfo
    template_name = 'events/participation_list.html'
    permission_required = 'events.view_eventinfo'
    return_403 = True
    context_object_name = 'event'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        event_participation_filter = EventParticipationFilter(
            self.request.GET,
            queryset=ParticipatingPerson.objects.filter(event_participation__in=self.object.eventparticipation_set.all())
        )
        event_participation_filter.form.helper = EventParticipationFormHelper()

        table = EventParticipationsTable(event_participation_filter.qs, request=self.request)

        RequestConfig(self.request).configure(table)

        context['filter'] = event_participation_filter
        context['table'] = table
        context['participations'] = "active"

        return context

class EventParticipationsInvited(View):
  
    def get(self, request, *args, **kwargs):
        get_participating_persons_by_event(request.user, self.kwargs['pk']).update(invitation_sent=True)
        return HttpResponseRedirect(redirect_to=reverse('events:participation-list', kwargs={'pk': self.kwargs['pk']}))
    
class EventParticipationsSignedIn(View):
  
    def get(self, request, *args, **kwargs):
        get_participating_persons_by_event(request.user, self.kwargs['pk']).update(has_signed_up=HasSignedUpStatus.CONFIRMATION)
        return HttpResponseRedirect(redirect_to=reverse('events:participation-list', kwargs={'pk': self.kwargs['pk']}))

class EventParticipationsParticipated(View):

    def get(self, request, *args, **kwargs):
        get_participating_persons_by_event(request.user, self.kwargs['pk']).update(has_participated=True)
        return HttpResponseRedirect(redirect_to=reverse('events:participation-list', kwargs={'pk': self.kwargs['pk']}))

def get_participating_persons_by_event(user, pk):
        if not user.has_perm('events.view_eventinfo', EventInfo.objects.get(pk=pk)):
            raise PermissionDenied
        return ParticipatingPerson.objects.filter(event_participation__event__id=pk)

def get_invitation_link(request, event, person):
    absolute_url = ""
    pp = ParticipatingPerson.objects.filter(event_participation__event=event, person=person).first()
    if pp:
        relative_url = reverse('events:invitation-link', kwargs={'invitation_code': pp.invitation_code})
        absolute_url = request.build_absolute_uri(relative_url)
        link_text = _("Invitation link")
        absolute_url = f'<a href="{absolute_url}">{link_text}</a>'
    return absolute_url

@login_not_required
def eventParticipantLink(request, invitation_code):
    html_template = 'events/forms/invitation/invitation_link.html'
    if event_is_invalid(invitation_code):
        html_template = 'events/forms/invitation/invitation_invalid.html'
    return render(request, html_template, {'invitation_code': invitation_code})

@login_not_required
def eventParticipantLinkConfirm(request, invitation_code):
    html_template = "events/forms/invitation/invitation_link_feedback.html"
    if event_is_invalid(invitation_code):
        html_template = 'events/forms/invitation/invitation_invalid.html'
    else:
        ParticipatingPerson.objects.filter(invitation_code=invitation_code).update(responded_to_invitation_at=timezone.now(), has_signed_up=HasSignedUpStatus.CONFIRMATION)
    return render(request, html_template)

@login_not_required
def eventParticipantLinkCancel(request, invitation_code):
    html_template = "events/forms/invitation/invitation_link_feedback.html"
    if event_is_invalid(invitation_code):
        html_template = 'events/forms/invitation/invitation_invalid.html'
    else:
        ParticipatingPerson.objects.filter(invitation_code=invitation_code).update(responded_to_invitation_at=timezone.now(), has_signed_up=HasSignedUpStatus.CANCELLATION)
    return render(request, html_template)

def event_is_invalid(invitation_code):
    event = EventInfo.objects.filter(eventparticipation__participatingperson__invitation_code=invitation_code).first()
    return event and event.status != Status.PLANNED