from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.utils.translation import gettext_lazy as _
from events.forms import DateForm
from events.models.dates import DateModel
from events.models.events import EventInfo

def create_date(request, pk):
    dates = DateModel.objects.filter(event__id=pk)
    form = DateForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            date = form.save(commit=False)
            date.event = EventInfo.objects.get(id=pk)
            date.save()
            return redirect("events:detail-date", pk=date.id)
        else:
            return render(request, "events/forms/event_date.html", {
                "form": form
            })

    context = {
        "form": form,
        "dates": dates,
        "event_id" : pk
    }
    return render(request, "events/forms/event_dates.html", context)

def create_date_form(request):
    context = {
        "form": DateForm()
    }
    return render(request, "events/forms/event_date.html", context)

def detail_date(request, pk):
    date = DateModel.objects.get(pk=pk)
    context = {
        "date": date
    }
    return render(request, "events/forms/event_date_detail.html", context)

def update_date(request, pk):
    date = DateModel.objects.get(pk=pk)
    form = DateForm(request.POST or None, instance=date)

    if request.method == "POST":
        if form.is_valid():
            date = form.save()
            return redirect("events:detail-date", pk=date.id)

    context = {
        "form": form,
        "date": date
    }
    return render(request, "events/forms/event_date.html", context)

def delete_date(request, pk):
    date = DateModel.objects.get(pk=pk)
    date.delete()
    return HttpResponse('')