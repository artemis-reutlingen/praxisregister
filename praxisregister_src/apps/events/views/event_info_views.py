from django.contrib.auth.mixins import PermissionRequiredMixin as NonObjectPermissionRequiredMixin
from django.db.models import Min
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from django_tables2 import SingleTableMixin, RequestConfig
from django.dispatch import Signal
from guardian.mixins import PermissionRequiredMixin
from events.filters import EventFilter, EventInfoFormHelper
from events.models.options import EventTypeOption
from events.models.events import EventInfo
from events.tables import EventsTable
from events.forms import EventInfoForm

events_edit_log=Signal()
events_participation_edit_log=Signal()
events_bulk_creation_log=Signal()

class EventList(SingleTableMixin, ListView):
    """ Main event list accessed from main navbar. """
    model = EventInfo
    paginate_by = 15
    template_name = 'events/list.html'

    def get_queryset(self):
        return EventInfo.objects.get_events_for_user(self.request.user).distinct().annotate(min_date=Min('datemodel__date')).order_by('-min_date')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        event_filter = EventFilter(
            self.request.GET,
            queryset=EventInfo.objects.get_events_for_user(self.request.user).distinct().annotate(min_date=Min('datemodel__date')).order_by('-min_date')
        )
        event_filter.form.helper = EventInfoFormHelper()
        table = EventsTable(event_filter.qs)
        RequestConfig(self.request).configure(table)

        context['filter'] = event_filter
        context['table'] = table

        return context
    
def event_table(request):
    event_filter = EventFilter(
            request.GET,
            queryset=EventInfo.objects.get_events_for_user(request.user).distinct().order_by('-id')
        )
    table = EventsTable(event_filter.qs)
    RequestConfig(request).configure(table)
    context = {'table' : table}
    return render(request, "events/event_table.html", context)

class EventDetail(PermissionRequiredMixin, DetailView):
    """ Event detail view. """
    model = EventInfo
    permission_required = 'events.view_eventinfo'
    return_403 = True
    context_object_name = 'event'
    template_name = "events/detail.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        actions = []
        if self.request.user.has_perm('events.change_eventinfo', self.get_object()):
            actions.append("EDIT_EVENT")
        if self.request.user.has_perm('events.change_eventinfo', self.get_object()):
            actions.append("DELETE_EVENT")

        context['actions'] = actions
        context['details'] = "active"

        return context
    
class EventCreate(CreateView):
    """ Create an event. """
    model = EventInfo
    form_class = EventInfoForm
    template_name = 'events/forms/event.html'
    
    def form_valid(self, form):
        form.instance.assigned_institute = self.request.user.assigned_institute
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        self.object.assign_perms(self.request.user)
        events_edit_log.send(sender=self.__class__, username=self.request.user.username, event=self.object)
        return reverse_lazy('events:add_dates', kwargs={'pk': self.object.id})

class EventEdit(PermissionRequiredMixin, UpdateView):
    """ Edit an event. """
    model = EventInfo
    form_class = EventInfoForm
    template_name = 'events/forms/event.html'
    permission_required = 'events.change_eventinfo'
    return_403 = True

    def get_success_url(self):
        events_edit_log.send(sender=self.__class__, username=self.request.user.username, event=self.object)
        return reverse_lazy('events:add_dates', kwargs={'pk': self.object.id})


class EventDelete(PermissionRequiredMixin, DeleteView):
    """ Delete an event. """
    model = EventInfo
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = 'events.delete_eventinfo'
    return_403 = True

    def get_success_url(self):
        events_edit_log.send(sender=self.__class__, username=self.request.user.username, event=self.object)
        return reverse_lazy('events:list')
