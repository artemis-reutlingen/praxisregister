from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column as FormCol, Submit, Div
from django.forms import TextInput, Select, DateInput
from django.db.models import Q, Count
from django.utils.translation import gettext_lazy as _
from django_filters import CharFilter, ChoiceFilter, DateFilter, BooleanFilter
from django_property_filter import PropertyFilterSet, PropertyCharFilter
from practices.models import Practice
from events.models import EventInfo, EventParticipation, ParticipatingPersonInvitationChoices
from events.types import HasSignedUpStatus


class EventFilter(PropertyFilterSet):

    title = CharFilter(
        lookup_expr='icontains',
        label=_('Name'),
        widget=TextInput()
    )

    type = CharFilter(
        lookup_expr='icontains',
        label=_("Type of event"),
        widget=TextInput()
    )

    location = CharFilter(
        lookup_expr='icontains',
        label=_('Location'),
        widget=TextInput()
    )

    participant_group = CharFilter(
        lookup_expr='icontains',
        label=_('Participant group'),
        widget=TextInput()
    )

    prerequisites = CharFilter(
        lookup_expr='icontains',
        label=_('Prerequisites'),
        widget=TextInput()
    )

    date_widget = DateInput(
        format='%Y-%m-%d',
        attrs={'class': 'form-control', 'type': 'date'}
    )

    date_specific = DateFilter(
        method= lambda queryset, name, value : queryset.filter(datemodel__date=value),
        label=_("Exact date"),
        widget=date_widget
    )

    date_from = DateFilter(
        method= lambda queryset, name, value : queryset.filter(Q(datemodel__date__isnull=False) & Q(datemodel__date__gte=value)),
        label=_("Date from"),
        widget=date_widget
    )

    date_to = DateFilter(
        method= lambda queryset, name, value : queryset.filter(Q(datemodel__date__isnull=False) & Q(datemodel__date__lte=value)),
        label=_("Date to"),
        widget=date_widget
    )

    one_day = BooleanFilter(
        method='filter_count_dates',
        label=_("One day")
    )

    class Meta:
        model = EventInfo
        fields = ('title', 'one_day', 'type', 'location', 'takes_place', 'participant_group',
                  'prerequisites', 'status', 'date_specific', 'date_from', 'date_to')
    @staticmethod
    def filter_count_dates(queryset, name, value):
        one_day=queryset.annotate(num_dates=Count('datemodel__date')).filter(num_dates__exact=1)
        more_days=queryset.annotate(num_dates=Count('datemodel__date')).filter(num_dates__gt=1)
        return one_day if value else more_days

class EventInfoFormHelper(FormHelper):
    form_method = 'GET'
    layout = Layout(
        Div(
            Row(
                FormCol('title'),
                FormCol('one_day'),
                FormCol('type'),
                FormCol('location'),
            ),
            Row(
                FormCol('takes_place'),
                FormCol('participant_group'),
                FormCol('prerequisites'),
                FormCol('status'),
            ),
            css_class="collapse multi-collapse",
            css_id="filter_by_base_data"),
        Div(
            Row(
                FormCol('date_specific'),
                FormCol('date_from'),
                FormCol('date_to'),
            ),
            css_class="collapse multi-collapse",
            css_id="filter_by_date"),
        Row(
            FormCol(Submit('submit', value=_('Apply Filter'))),
        )
    )


class EventParticipationFilter(PropertyFilterSet):

    person__full_name = CharFilter(
        method='filter_persons_full_name',
        label=_('Person'),
        widget=TextInput()
    )

    event_participation__practice__name = CharFilter(
        lookup_expr='icontains',
        label=_('Practice name'),
        widget=TextInput()
    )

    participation_status = ChoiceFilter(
        method='filter_participation_status',
        label=_('Participation status'),
        choices=ParticipatingPersonInvitationChoices.choices,
        widget=Select()
    )

    class Meta:
        model = EventParticipation
        fields = ('person__full_name',
                  'event_participation__practice__name', 'participation_status')

    @staticmethod
    def filter_persons_full_name(queryset, name, value):
        return queryset.filter(Q(person__first_name__icontains=value) | Q(person__last_name__icontains=value))

    @staticmethod
    def filter_participation_status(queryset, name, value):
        qs = None
        match value:
            case ParticipatingPersonInvitationChoices.INVITATION_PENDING:
                qs = queryset.filter(has_participated = False, invitation_sent = False)
            case ParticipatingPersonInvitationChoices.INVITATION_SENT:
                qs = queryset.filter(has_participated = False, invitation_sent = True)
            case ParticipatingPersonInvitationChoices.NO_RESPONSE:
                qs = queryset.filter(has_participated = False, invitation_sent = True, has_signed_up = HasSignedUpStatus.UNKNOWN)
            case ParticipatingPersonInvitationChoices.SIGNED_UP:
                qs = queryset.filter(has_participated = False, has_signed_up = HasSignedUpStatus.CONFIRMATION)
            case ParticipatingPersonInvitationChoices.CANCELLED:
                qs = queryset.filter(has_participated = False, has_signed_up = HasSignedUpStatus.CANCELLATION)
            case ParticipatingPersonInvitationChoices.PARTICIPATED:
                qs = queryset.filter(has_participated = True)
        return qs.distinct()


class EventParticipationFormHelper(FormHelper):
    form_method = 'GET'
    layout = Layout(
        Row(
            FormCol('person__full_name'),
            FormCol('event_participation__practice__name'),
            FormCol('participation_status'),
            FormCol(Submit('submit', value=_('Apply Filter')),
                    css_class="mb-3", style="align-self:flex-end")
        )
    )

class EventPracticeFilter(PropertyFilterSet):

    name = CharFilter(
        lookup_expr='icontains',
        label="",
        widget=TextInput(attrs={'placeholder': _('Practice name')})
    )

    identification_number = CharFilter(
        lookup_expr='icontains',
        label="",
        widget=TextInput(attrs={'placeholder': _('BSNR')})
    )
    owners_names_formatted = PropertyCharFilter(
        lookup_expr='icontains',
        label="",
        field_name='owners_names_formatted',
        widget=TextInput(attrs={'placeholder': _('Owner')}),
    )
    city = CharFilter(
        lookup_expr='icontains',
        label="",
        widget=TextInput(attrs={'placeholder': _('City')})
    )

    county__full_name = CharFilter(
        lookup_expr='icontains',
        label="",
        widget=TextInput(attrs={'placeholder': _('County')})
    )

    class Meta:
        model = Practice
        fields = ('name', 'identification_number', 'owners_names_formatted',
                  'city', 'county__full_name')

class EventPracticeFilterFormHelper(FormHelper):
    form_method = 'GET'
    layout = Layout(
        Row(
            FormCol('name'),
            FormCol('identification_number'),
            FormCol('owners_names_formatted'),
            FormCol('city'),
            FormCol('county__full_name'),
            FormCol(Submit('submit', value=_('Apply Filter')))
        )
    )
