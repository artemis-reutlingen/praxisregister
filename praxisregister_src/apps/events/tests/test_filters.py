from django.test import TestCase
from events.models import EventInfo, EventParticipation, DateModel, ParticipatingPerson, ParticipatingPersonInvitationChoices
from events.filters import EventParticipationFilter
from persons.models import Person
from practices.models import Practice
from utils.utils_test import setup_test_basics
from events.types import HasSignedUpStatus

def basic_event_test_set_up(self):
    institute, group, user, [practice_1, practice_2] = setup_test_basics()
    self.practice1=practice_1

    practice_3 = Practice.objects.create(name="Practice_C")
    practice_3.identification_number = "ABC-3"
    practice_3.save()

    self.person1 = Person.objects.create(practice=practice_1)
    self.person2 = Person.objects.create(practice=practice_2)

    # practice_3 should not be visible for our user
    self.event = EventInfo.objects.create(title="Testpraxis")
    self.date = DateModel.objects.create(date="2021-10-01", start_time="12:00", end_time="14:00", event=self.event)

    self.event.assign_perms(user=user)

    self.client.login(username="tester", password="password")

class FilterTest(TestCase):

    def setUp(self) -> None:
        basic_event_test_set_up(self)
        #Create participating person
        self.practice_participation1 = EventParticipation.objects.create(practice=self.practice1, event=self.event)
        self.participating_person = ParticipatingPerson.objects.create(person=self.person1, event_participation=self.practice_participation1)

    def test_participation_status_filter_invitation_pending(self):
        #test default
        self.assertTrue(len(EventParticipation.objects.first().participatingperson_set.all()) > 0)
        qs = EventParticipationFilter.filter_participation_status(queryset=ParticipatingPerson.objects.filter(event_participation=self.practice_participation1), name="", value=ParticipatingPersonInvitationChoices.INVITATION_PENDING)
        self.assertEqual(self.person1, qs.first().person)

    def test_participation_status_filter_invitation_send(self):
        self.participating_person.invitation_sent = True
        self.participating_person.save()
        qs = EventParticipationFilter.filter_participation_status(queryset=ParticipatingPerson.objects.filter(event_participation=self.practice_participation1), name="", value=ParticipatingPersonInvitationChoices.INVITATION_SENT)
        self.assertEqual(self.person1, qs.first().person)
        
    def test_participation_status_filter_signed_up(self):
        self.participating_person.has_signed_up = HasSignedUpStatus.CONFIRMATION
        self.participating_person.save()
        qs = EventParticipationFilter.filter_participation_status(queryset=ParticipatingPerson.objects.filter(event_participation=self.practice_participation1), name="", value=ParticipatingPersonInvitationChoices.SIGNED_UP)
        self.assertTrue(self.person1, qs.first().person)
        
    def test_participation_status_filter_invitation_send_and_participated(self):
        self.participating_person.invitation_sent = True
        self.participating_person.has_participated = True
        self.participating_person.save()
        qs = EventParticipationFilter.filter_participation_status(queryset=ParticipatingPerson.objects.filter(event_participation=self.practice_participation1), name="", value=ParticipatingPersonInvitationChoices.INVITATION_SENT)
        self.assertTrue(len(qs.all()) == 0)
        
    def test_participation_status_filter_participated(self):
        self.participating_person.has_participated = True
        self.participating_person.save()
        qs = EventParticipationFilter.filter_participation_status(queryset=ParticipatingPerson.objects.filter(event_participation=self.practice_participation1), name="", value=ParticipatingPersonInvitationChoices.PARTICIPATED)
        self.assertEqual(self.person1, qs.first().person)

