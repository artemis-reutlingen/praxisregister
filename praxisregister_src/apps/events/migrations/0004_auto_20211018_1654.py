# Generated by Django 3.2.7 on 2021-10-18 14:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('persons', '0010_alter_studyspecifictraining_type'),
        ('practices', '0007_auto_20211014_2128'),
        ('events', '0003_participatingperson_event_participation'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='eventparticipation',
            unique_together={('event', 'practice')},
        ),
        migrations.AlterUniqueTogether(
            name='participatingperson',
            unique_together={('person', 'event_participation')},
        ),
    ]
