# Generated by Django 4.1.6 on 2023-02-14 08:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0007_alter_eventparticipation_event'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='additional_notes',
            field=models.TextField(blank=True, null=True, verbose_name='Additional notes'),
        ),
        migrations.AddField(
            model_name='event',
            name='catering',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Catering'),
        ),
        migrations.AddField(
            model_name='event',
            name='certificates',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Certificates'),
        ),
        migrations.AddField(
            model_name='event',
            name='cme_points',
            field=models.BooleanField(default=False, verbose_name='CME points requested'),
        ),
        migrations.AddField(
            model_name='event',
            name='lecturers',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Lecturers'),
        ),
        migrations.AddField(
            model_name='event',
            name='max_participation',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Maximum number of participants'),
        ),
        migrations.AddField(
            model_name='event',
            name='multiple_times',
            field=models.BooleanField(default=False, verbose_name='Multiple times'),
        ),
        migrations.AddField(
            model_name='event',
            name='participant_group',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Participant group'),
        ),
        migrations.AddField(
            model_name='event',
            name='prerequisites',
            field=models.CharField(blank=True, max_length=512, null=True, verbose_name='Prerequisites'),
        ),
        migrations.AddField(
            model_name='event',
            name='several_days',
            field=models.BooleanField(default=False, verbose_name='Several days'),
        ),
        migrations.AddField(
            model_name='event',
            name='status',
            field=models.CharField(choices=[('planned', 'Planned'), ('in_progress', 'In Progress'), ('finished', 'Finished'), ('cancelled', 'Cancelled')], default='planned', max_length=128, verbose_name='Current status'),
        ),
        migrations.AddField(
            model_name='event',
            name='takes_place',
            field=models.CharField(choices=[('online', 'Online'), ('onsite', 'Onsite'), ('hybrid', 'Hybrid')], default='online', max_length=128, verbose_name='Takes place'),
        ),
        migrations.AlterField(
            model_name='event',
            name='location',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Location'),
        ),
        migrations.AlterField(
            model_name='event',
            name='title',
            field=models.CharField(max_length=512, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='event',
            name='type',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Type of event'),
        ),
        migrations.AlterField(
            model_name='event',
            name='video_call_url',
            field=models.URLField(blank=True, null=True, verbose_name='Video call link'),
        ),
    ]
