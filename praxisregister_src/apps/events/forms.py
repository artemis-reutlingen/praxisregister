from accreditation.models import AccreditationType
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Column, Layout, Row
from django import forms
from django.forms import DateInput, ModelForm, Textarea, TimeInput
from django.forms.widgets import RadioSelect, CheckboxSelectMultiple
from django.utils.translation import gettext_lazy as _
from events.models import EventParticipation
from events.models.dates import DateModel
from events.models.events import EventInfo, ParticipatingPerson
from events.models.options import EventTypeOption
from utils.fields import ListTextWidget


class EventInfoForm(ModelForm):

    fixed_dates = forms.BooleanField(
        required=False,
        label=_("Period of the event(s)"),
        widget=RadioSelect(
        choices=[(True, _("Fixed dates")),(False, _("Time period"))]
        )
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.fields['type'].widget = ListTextWidget(
            data_list=[option.name for option in EventTypeOption.objects.all().order_by('name')],
            name='event-type'
        )
        self.fields['description'].widget = Textarea(attrs={'rows':1, 'cols':40})
        self.fields['fixed_dates'].initial = self.instance.fixed_dates if self.instance.fixed_dates else True

    class Meta:
        model = EventInfo
        exclude = ['assigned_institute', 'author', 'accreditation_requirement', 'import_id', 'import_by']

class EventParticipationForm(forms.ModelForm):
    class Meta:
        model = EventParticipation
        fields = [
            'event',
        ]

class ParticipatingPersonForm(forms.ModelForm):
    class Meta:
        model = ParticipatingPerson
        fields = ['person']

class ParticipatingPersonEditForm(forms.ModelForm):
    class Meta:
        model = ParticipatingPerson
        fields = ['invitation_sent', 'invitation_sent_at', 'has_signed_up', 'responded_to_invitation_at', 'has_participated']
        widgets = {
            'invitation_sent_at': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            ),
            'responded_to_invitation_at': forms.DateTimeInput(
                format='%Y-%m-%d %H:%M',
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'datetime-local'
                }
            ),
        }
        
class DateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Column('date'),
                Column('start_time'),
                Column('end_time')
            )
        )

        self.fields['date'] = forms.DateField(
            widget=DateInput(
                format='%Y-%m-%d',
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            ),
            label=_("Date")
        )
        time_input = TimeInput(
            format='%H:%M',
            attrs={
                'type': 'time',
                'class': 'form-control',
            }
        )
        self.fields['start_time'] = forms.TimeField(
            widget=time_input,
            label=_("Start time")
        )
        self.fields['end_time'] = forms.TimeField(
            widget=time_input,
            label=_("End time")
        )

    class Meta:
        model = DateModel
        fields = ['date', 'start_time', 'end_time']
