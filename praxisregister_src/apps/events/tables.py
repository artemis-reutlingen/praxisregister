from django.urls import reverse
from django.db.models import Min
from django.utils.safestring import mark_safe
from django_tables2 import Table, Column, CheckBoxColumn
from django.utils.translation import gettext_lazy as _
from django.middleware.csrf import get_token
from persons.models import Person
from practices.models import Practice
from events.models.events import EventInfo, ParticipatingPerson
from events.types import HasSignedUpStatus
from utils.tables import CheckBoxColumnWithTitle

class EventsTable(Table):
    datemodel_set = Column(verbose_name=_('Dates'))

    def order_datemodel_set(self, queryset, is_descending):
        qs = queryset.annotate(min_date=Min('datemodel__date')).order_by(
            ("-" if is_descending else "") + "min_date")
        return (qs, True)
    
    def render_title(self, value, record):
        event_url = reverse('events:detail', kwargs={"pk": record.id})
        event_link = mark_safe(f'<a href="{event_url}">{value}</a>')
        return event_link
    
    def render_datemodel_set(self, value, record):
        render_html = ""
        for date in value.values_list('date', flat=True):
            render_html += f'{date.strftime("%d-%m-%Y")}</br>'
        return mark_safe(render_html)

    class Meta:
        model = EventInfo
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("datemodel_set", "title", "type", "status", "takes_place", "location", "is_draft" )


class EventParticipationsTable(Table):
    person = Column(verbose_name=_("Participants"))
    event_participation__practice__name = Column(verbose_name=_('Practice name'))
    edit_participation = Column(verbose_name=_('Edit Participation'), orderable=False, empty_values=())
    
    def render_person(self, value, record):
        person_url = reverse("persons:detail", kwargs={"pk": value.pk})
        return mark_safe(f'<a href="{person_url}" style="text-decoration:none;">{Person.objects.get(pk=value.pk)}</a></br>')

    def render_event_participation__practice__name(self, value, record):
        html = "-"
        if record.event_participation and record.event_participation.practice:
            practice = record.event_participation.practice
            practice_url = reverse("practices:event-participation-list", kwargs={"pk": practice.id})
            html = mark_safe(f'<a href="{practice_url}" id="id_practice_{practice.id}_link" style="text-decoration:none;">{practice}</a>')
        return html
    
    def render_invitation_sent(self, value, record):
        if record.invitation_sent == True:
            html = '<i class="fa fa-check-circle text-success"></i>'
            html += f'<p>{record.invitation_sent_at.strftime("%d.%m.%Y, %H:%M")}</p>' if record.invitation_sent_at else ""
        else:
            html = '<i class="fa fa-times-circle text-danger"></i>'
        return mark_safe(html)
    
    def render_has_signed_up(self, value, record):
        html = ""
        if record.has_signed_up == HasSignedUpStatus.CONFIRMATION:
            html = '<i class="fa fa-check-circle text-success"></i> ' + _("Confirmation")
            html += f'<p>{record.responded_to_invitation_at.strftime("%d.%m.%Y, %H:%M")}</p>' if record.responded_to_invitation_at else ""
        elif record.has_signed_up == HasSignedUpStatus.CANCELLATION:
            html = '<i class="fa fa-times-circle text-danger"></i> ' + _("Cancellation")
            html += f'<p>{record.responded_to_invitation_at.strftime("%d.%m.%Y, %H:%M")}</p>' if record.responded_to_invitation_at else ""
        else:
            html = '<i class="fa-solid fa-circle-question text-secondary"></i>'
        return mark_safe(html)
    
    def render_has_participated(self, value, record):
        html = '<i class="fa fa-check-circle text-success"></i>' if record.has_participated == True else '<i class="fa fa-times-circle text-danger"></i>'
        return mark_safe(html)

    def render_edit_participation(self, value, record):
        edit_url = reverse("events:participation-edit-person", kwargs={"event_participation_id" : record.event_participation.id, "pk": record.id})
        delete_url = reverse("events:participation-remove-person", kwargs={"event_participation_id" : record.event_participation.id , "participating_person_id": record.id})
        csrf_token = mark_safe(f'<input type="hidden" name="csrfmiddlewaretoken" value="{get_token(self.request)}">')
        form = mark_safe(f'<form action="{delete_url}" method="post" style="margin: 0; padding: 0; display:inline;">{csrf_token}<button class="btn btn-sm btn-outline-danger" type="submit" name="removePerson"><i class="fa fa-trash"></i></button></form>') 
        return mark_safe(f'<a href="{edit_url}" class="btn btn-sm btn-outline-primary text-nowrap"><i class="fa fa-edit"></i></a>{form}')

    class Meta:
        model = ParticipatingPerson
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("person", "event_participation__practice__name", "invitation_sent", "has_signed_up", "has_participated", "edit_participation")
