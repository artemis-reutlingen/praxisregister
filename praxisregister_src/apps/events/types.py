from django.db import models
from django.utils.translation import gettext_lazy as _


class EventType(models.TextChoices):
    ONLINE = 'online', _('Online')
    ONSITE = 'onsite', _('Onsite')
    HYBRID = 'hybrid', _('Hybrid')

class Status(models.TextChoices):
    PLANNED = 'planned', _('Planned')
    INPROGRESS = 'in_progress', _('In Progress')
    FINISHED = 'finished', _('Finished')
    CANCELLED = 'cancelled', _('Cancelled')

class HasSignedUpStatus(models.TextChoices):
    CONFIRMATION = "Confirmation", _("Confirmation")
    CANCELLATION = "Cancellation", _("Cancellation")
    UNKNOWN = "UNKNOWN", _("Unknown")
