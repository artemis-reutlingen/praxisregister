from django.db import models

class KbvImportMixin(models.Model):

    kbv_import_id = models.CharField(max_length=256, verbose_name="Kbv import ID", null=True, blank=True)
    kbv_import_by = models.CharField(max_length=256, verbose_name="Kbv import by", null=True, blank=True)

    class Meta:
        abstract = True
        unique_together = ('kbv_import_id', 'kbv_import_by')