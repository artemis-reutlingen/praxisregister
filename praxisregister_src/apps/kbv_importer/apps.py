from django.apps import AppConfig


class KbvImporterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kbv_importer'
