from django.urls import path
from kbv_importer import views
from django.views.generic import TemplateView

app_name = 'kbv_importer'

urlpatterns = [

    path('', views.KbvImportView.as_view(), name="overview"),
    path('config', views.kbv_import_config, name="config"),
    path('check', views.check_for_existing_kbv_import, name="check_for_existing_import"),
    path('import-preview', views.KbvImportView.as_view(), name="import-preview"),
    path('import', views.process_kbv_confirm_form, name="import"),
]
