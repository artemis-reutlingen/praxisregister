import logging

from django.contrib import messages
from django.db import transaction
from django.shortcuts import redirect, render
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView
from django.contrib.contenttypes.models import ContentType
from django.views.decorators.http import require_POST
from importer.views.importer_utils import create_dataset_from_import_form, handle_import_result, check_for_ongoing_import, prepare_confirm_form
from importer.models import MapperField, ChunkImport, Mapper, ImportSettings
from importer.views.importer_views import create_data_import_object
from importer.views.chunk_views import *
from practice_importer.views.default_importer_utils import (
    add_related_model_as_foreignkey_to_mapper, createMapperField,
    get_or_create_mapper)
from practices.models import Practice
from practice_importer.views.views import custom_import
from practice_importer.views.views import load_dataset_from_temp_storage
from practice_importer.forms import CustomImportFileForm
from kbv_importer.forms import KBVMapperFieldsImportForm, KbvImportConfirmForm
from kbv_importer.resources import KbvResource
from kbv_importer.import_builder import *
from Levenshtein import distance as levenshtein_distance
from practice_importer.views.views import ImportView
from typing import Any


logger = logging.getLogger(__name__)

IMPORT_TYPE = "kbv_importer"
#Field name and score for levenshtein distance
MAPPER_SEARCH_FIELDS = {"name" : 4, "contact_email" : 4, "zip_code" : 2}

class KbvImportView(ImportView):
    template_name = "kbv_importer/import_file_kbv.html"
    form_class = CustomImportFileForm

    def __init__(self, **kwargs: Any) -> None:
        self.import_type = IMPORT_TYPE
        self.import_url = "kbv_importer:import-preview"
        self.import_method = kbv_detailed_import
        self.confirm_form = KbvImportConfirmForm
        super().__init__(**kwargs)

# Real import (dry_run=False) is done here
@require_POST
def process_kbv_confirm_form(request, *args, **kwargs):
    kwargs = {"groups": request.user.groups.all()}

    logger.debug("Processing import")
    confirm_form = KbvImportConfirmForm(data=request.POST, **kwargs)

    if confirm_form.is_valid():
        logger.debug("Confirm form is valid")

        storage_dataset = load_dataset_from_temp_storage(confirm_form.cleaned_data["input_format"], confirm_form.cleaned_data["original_file_name"], remove_storage=False)
        kbv_import = ChunkImport.objects.get(institute=request.user.assigned_institute, import_type=IMPORT_TYPE)
        
        import_object = ImportObject().set_confirm_form_data(confirm_form) \
            .set_user(request.user) \
            .set_storage_dataset(storage_dataset) \
            .set_chunk_import(kbv_import) \
            .set_data_import(create_data_import_object(request.user, confirm_form.cleaned_data["groups"])) \
            .set_import_url("kbv_importer:import-preview") \
            .set_html_template("kbv_importer/import_file_kbv.html") \
                    
        import_result = kbv_detailed_import(import_object, dry_run=False)
        
        handle_import_result(request, import_result , import_object.data_import)
        
        return check_for_ongoing_import(import_object, import_result, request, kbv_detailed_import)

    else:
        messages.error(request, _('Form invalid.'))
        logger.error("Confirm form invalid")
    return redirect(import_object.import_url)


def kbv_detailed_import(import_object: ImportObject, dry_run=True):
        
        mappers = Mapper.objects.filter(import_type=IMPORT_TYPE, institute=import_object.user.assigned_institute)
        practice_mapper = mappers.get(content_type=ContentType.objects.get_for_model(Practice))
        import_object.set_main_content_type(practice_mapper.content_type)

        if MapperField.objects.filter(mapper=practice_mapper, internal_fieldname="kbv_import_id").exists():
            logger.debug("Found kbv_import_id field in mapper")
            
            import_object.set_resource(KbvResource(practice_mapper, import_object.groups)) \
            .set_mappers(mappers.exclude(pk=practice_mapper.pk))

            import_result = custom_import(import_object, dry_run, import_id_field_name="kbv_import_id", import_by_field_name="kbv_import_by")
        else:
            logger.debug("No kbv_import_id field provided")
            #get highest kbv_import_id
            kbv_import_ids = Practice.objects.filter(kbv_import_by=import_object.user.assigned_institute).values_list("kbv_import_id", flat=True)
            id_list = [int(id_str) for id_str in kbv_import_ids if id_str.isdigit()]
            next_id = max(id_list) + 1 if id_list else 1
            import_object.sub_dataset.append_col([next_id + i for i in range(import_object.sub_dataset.height)], header="kbv_import_id")
            mapper_field_kbv_import_id, _ = MapperField.objects.get_or_create(mapper=practice_mapper, internal_fieldname="kbv_import_id", import_fieldname="kbv_import_id")
            
            # preapare db objects of mapper model to safe db queries
            mapper_model_fields_dict = {}
            change_kbv_import_id_objs = {}
            for mapper in mappers:
                db_fields = mapper.content_type.model_class().objects.filter(assigned_institute=import_object.user.assigned_institute)
                mapper_model_fields_dict[mapper] = db_fields
                logger.debug(f"preapared db_fields from model: {mapper.content_type.model_class()}")
                change_kbv_import_id_objs[mapper] = []
            
            ignore_case = ImportSettings.objects.filter(institute=import_object.user.assigned_institute, levenshtein_ignore_case=True).exists()
            relevant_fields_available = False
            # Iterate over rows in dataset, look at each field and try to find a match in the db
            logger.debug(f"starting to search for matches in {MAPPER_SEARCH_FIELDS.keys()}")
            for row in import_object.sub_dataset.dict:
                skip_remaining_fields = False
                for mapper in mappers:
                    if skip_remaining_fields:
                        break
                    for mapperField in mapper.mapperfield_set.all().filter(internal_fieldname__in=MAPPER_SEARCH_FIELDS.keys()): # TODO: use select_related to reduce db queries?
                        relevant_fields_available = True # TODO: Maybe find a better solution to check this
                        logger.debug(f"Searching a match for field: {mapperField.internal_fieldname} : {mapperField.import_fieldname}")
                        import_value = row[mapperField.import_fieldname] # Value of the field in the import file
                        logger.debug(f"value in import file: {import_value}")
                        db_fields = mapper_model_fields_dict[mapper]
                        
                        best_match = None
                        best_score = 100000
                        tolerance = MAPPER_SEARCH_FIELDS[mapperField.internal_fieldname]
                        for db_field in list(db_fields):
                            db_value = getattr(db_field, mapperField.internal_fieldname)
                            logger.debug(f"value in database: {db_value}")
                            to_string_processor = lambda x: str(x)
                            if ignore_case:
                                to_string_processor = lambda x: str(x).lower()
                            score = levenshtein_distance(db_value, import_value, score_cutoff=tolerance, processor=to_string_processor) 
                            if score <= tolerance and score < best_score:
                                best_match = db_field
                                best_score = score
                                if score == 0:
                                    # exact match found
                                    break
                        if best_match:
                            # OPtion 1 keine kbv_import_id vorhanden: dataset kbv_import_id in der Datenbank setzen -> Importvorschau -> Löschen -> für Real import wieder setzen -> bleibt gesetzt
                            # Option 2 kbv_import_id vorhanden: dataset id mit db_id überschreiben -> done 
                            skip_remaining_fields = True
                            if best_match.kbv_import_id is None:
                                logger.debug("setting kbv_import_id into database")
                                best_match.kbv_import_id = row["kbv_import_id"]
                                best_match.kbv_import_by = str(import_object.user.assigned_institute)
                                change_kbv_import_id_objs[mapper].append(best_match)
                            else:
                                logger.debug("kbv_import_id already set -> change in dataset")
                                row["kbv_import_id"] = best_match.kbv_import_id
                            break

                        # If no match was found, then import as new object                            

            if not relevant_fields_available:
                mapper_field_kbv_import_id.delete()
                raise Exception("No relevant fields available in import file. Please provide at least on of the following fields: " + ", ".join(MAPPER_SEARCH_FIELDS.keys()) + ".")
            
            # temporary save changed kbv_import_ids in database in order for import/export library to work properly
            for mapper in mappers:
                logger.debug(f"updating {mapper} kbv_import_ids with bulk_update")
                mapper.content_type.model_class().objects.bulk_update(change_kbv_import_id_objs[mapper], ["kbv_import_id", "kbv_import_by"])

            logger.debug("creating resource")
            # exclude main mapper from foreign key mappers
            import_object.set_resource(KbvResource(practice_mapper, import_object.groups)) \
            .set_mappers(mappers.exclude(pk=practice_mapper.pk))

            try:
                import_result = custom_import(import_object, dry_run, import_id_field_name="kbv_import_id", import_by_field_name="kbv_import_by")
                logger.debug("import result available")
            except Exception as e:
                logger.debug(f"Error while importing (detailed importer:) {e}")
            finally:
                # revert db changes
                mapper_field_kbv_import_id.delete()
                for mapper in mappers:
                    for mapper_model in change_kbv_import_id_objs[mapper]:
                        mapper_model.kbv_import_id = None
                        mapper_model.kbv_import_by = None
                    mapper.content_type.model_class().objects.bulk_update(change_kbv_import_id_objs[mapper], ["kbv_import_id", "kbv_import_by"])
                logger.debug("cleaned up")

        return import_result

def check_for_existing_kbv_import(request):
    return check_for_existing_import(request, IMPORT_TYPE, "kbv_importer:import-preview")

def kbv_import_config(request):
    institute = request.user.assigned_institute

    kbv_practice_mapper, created = get_or_create_mapper(
        Practice, institute, import_type=IMPORT_TYPE
    )
    kbv_practice_mapper.assign_perms(request.user)

    '''
    # related models
    federal_state_mapper, created = get_or_create_mapper(
        FederalState, institute, Practice, import_type=IMPORT_TYPE
    )

        federal_state_mapper: "practice",
    '''
    foreignkey_dict = {
    }

    kbv_tuples = [
        (kbv_practice_mapper, Practice._meta.get_field("kbv_import_id")),
        (kbv_practice_mapper, Practice._meta.get_field("contact_phone")),
        (kbv_practice_mapper, Practice._meta.get_field("contact_email")),
        (kbv_practice_mapper, Practice._meta.get_field("street")),
        (kbv_practice_mapper, Practice._meta.get_field("house_number")),
        (kbv_practice_mapper, Practice._meta.get_field("city")),
        (kbv_practice_mapper, Practice._meta.get_field("zip_code")),
        (kbv_practice_mapper, Practice._meta.get_field("federal_state")),
    ]

    kwargs = {"kbv_tuples": kbv_tuples}

    if request.method == "POST":
        import_form = KBVMapperFieldsImportForm(request.POST, **kwargs)
        if import_form.is_valid():
            mappers = set([kbv_tuple[0] for kbv_tuple in kbv_tuples])

            with transaction.atomic():
                # Clean mapper fields
                MapperField.objects.filter(mapper__in=mappers).delete()
                for fieldname, importname in import_form.cleaned_data.items():
                    # Ignore empty fields
                    if importname is None or importname.strip() == "":
                        continue
                    # Create mapper fields
                    for mapper in mappers:
                        if fieldname.startswith(mapper.name):
                            createMapperField(mapper, fieldname, importname)
                            break

                for related_mapper, field_name in foreignkey_dict.items():
                    add_related_model_as_foreignkey_to_mapper(
                        kbv_practice_mapper, related_mapper, field_name
                    )

            return redirect("kbv_importer:config")

    elif request.method == "GET":
        initial = {}
        for kbv_tuple in kbv_tuples:
            mapper_field = (
                kbv_tuple[0]
                .mapperfield_set.filter(internal_fieldname=kbv_tuple[1].name)
                .first()
            )
            if mapper_field:
                initial[
                    kbv_tuple[0].name + mapper_field.internal_fieldname
                ] = mapper_field.import_fieldname

        context = {"form": KBVMapperFieldsImportForm(initial=initial, **kwargs)}
        return render(request, "kbv_importer/generic_form.html", context)