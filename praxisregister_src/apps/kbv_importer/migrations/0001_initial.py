# Generated by Django 4.2.7 on 2024-01-15 12:12

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('institutes', '0004_alter_institute_name_alter_institute_organisation'),
    ]

    operations = [
        migrations.CreateModel(
            name='KbvImport',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ongoing', models.BooleanField(default=False)),
                ('chunk_size', models.IntegerField(default=50)),
                ('current_chunk', models.IntegerField(default=0)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('institute', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='institutes.institute')),
                ('last_user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
