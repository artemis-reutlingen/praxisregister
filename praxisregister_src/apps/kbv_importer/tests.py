from django.test import TestCase
from django.urls import reverse
from institutes.models import Institute
from django.contrib.auth.models import Group
from importer.models import ChunkImport
from users.models import User
from Levenshtein import distance
from kbv_importer.views import IMPORT_TYPE

class Test(TestCase):
    username = "testuser"
    password = "12345"

    @classmethod
    def setUpTestData(self):
        group = Group.objects.create(name="Group")
        institute = Institute.objects.create(
            organisation="Organization X", name="institute_name", group=group
        )
        self.user = User.objects.create(username=self.username)
        self.user.set_password(self.password)
        self.user.groups.add(group)
        self.user.assigned_institute = institute
        self.user.save()

    def setUp(self):
        self.client.login(username=self.username, password=self.password)

    def test_check_for_existing_import(self):
        self.assert_new_import_redirect()
        kbv_import, _ = ChunkImport.objects.update_or_create(
            institute=self.user.assigned_institute,
            import_type=IMPORT_TYPE,
            defaults={"last_user": self.user},
        )
        self.assert_new_import_redirect()
        kbv_import.ongoing = True
        kbv_import.save()
        # current_chunk is still 0 so we should redirect to import
        self.assert_new_import_redirect()
        kbv_import.ongoing = False
        kbv_import.current_chunk = 25
        kbv_import.save()
        self.assert_new_import_redirect()

    def assert_new_import_redirect(self):
        response = self.client.get("/kbv_importer/check")
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/kbv_importer/import-preview")

    def test_check_for_existing_import_with_ongoing_import(self):
        kbv_import, _ = ChunkImport.objects.update_or_create(
            institute=self.user.assigned_institute,
            import_type=IMPORT_TYPE,
            defaults={"last_user": self.user},
        )
        kbv_import.ongoing = True
        kbv_import.current_chunk = 25
        kbv_import.save()
        response = self.client.get("/kbv_importer/check")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "importer/continue_import.html")

    # This test is more for personal understanding of the Levenshtein distance algorithm
    def test_levenshtein_distance(self):
        distance_1 = distance("test", "test")
        distance_2 = distance("test", "test2")
        self.assertEqual(distance_1, 0)
        self.assertEqual(distance_2, 1)
        distance_3 = distance("test", "tstw", score_cutoff=2)
        self.assertEqual(distance_3, 2)
        # If the distance is bigger than score_cutoff, score_cutoff + 1 is returned instead
        distance_4 = distance("test", "abcd", score_cutoff=2)
        self.assertEqual(distance_4, 3)
        distance_5 = distance("test", "TEST")
        self.assertEqual(distance_5, 4)
        distance_6 = distance("test", "TEST", processor=lambda x: x.lower())
        self.assertEqual(distance_6, 0)
