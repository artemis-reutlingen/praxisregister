import logging
from collections import OrderedDict

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from guardian.shortcuts import assign_perm
from import_export.fields import Field
from import_export.resources import ModelResource
from import_export.results import RowResult
from import_export.widgets import ForeignKeyWidget, Widget
from importer.views.importer_utils import get_foreignkey_mapperField_import_fieldname
from importer.models import (ContentTypePostImportPermission, ImportedModel,
                             MapperFieldChoice)

logger = logging.getLogger(__name__)

IMPORT_BY_COLUMN_NAME = "import_by"

# This class creates a resource for the import of a data. The fields are defined in the Mapper.
class CustomResource(ModelResource):
    
    def __init__(self, mapper, groups, dry_run=False, save_null_fields=False, import_id="import_id", import_by="import_by"):
        self.groups = groups
        fields = []
        for mapper_field in mapper.mapperfield_set.all():
            field = Field(attribute=mapper_field.internal_fieldname, column_name=mapper_field.import_fieldname, saves_null_values=save_null_fields)

            if mapper_field.is_foreign_key():
                field.widget=ForeignKeyWidget(mapper_field.foreign_key_model.model_class())
                field.column_name = get_foreignkey_mapperField_import_fieldname(mapper_field)
                if dry_run:
                    """
                    Dry run is used to show the preview of changes (only temporary import).
                    In the preview it can be the case, that a model is new and so there is no database entry yet.
                    In conclusion we can't create a Foreignkey widget, that references to an object, that does no exists yet -> skip the field.
                    When the final import is done, the new model is created and after that the foreignkey widget can be used.
                    """
                    continue
            if mapper_field.is_one_to_one_field():
                field.widget=ForeignKeyWidget(mapper_field.foreign_key_model.model_class())
                if dry_run:
                    continue

            mapper_field_choices = MapperFieldChoice.objects.filter(mapper_field=mapper_field)
            if mapper_field_choices.count() > 0:  
                field.widget=ChoicesWidget(mapper_field_choices)
            setattr(self, mapper_field.internal_fieldname, field)
            fields.append((mapper_field.internal_fieldname, field))
        
        #Every import will be assigned to the user's institute. This is set automatically before every import --> see before_import method
        field = Field(attribute=import_by, column_name=IMPORT_BY_COLUMN_NAME)
        setattr(self, import_by, field)
        fields.append((import_by, field))

        self._meta.import_id_fields = (import_id, import_by)
        self._meta.model = mapper.content_type.model_class()
        self.fields = OrderedDict(fields)
        self.imported_models = []


        content_type = ContentType.objects.get_for_model(self._meta.model)
        self.post_import_permissions = ContentTypePostImportPermission.objects.filter(content_type = content_type)

        logger.debug("CustomResource created")
        super().__init__()

    class Meta:
        skip_unchanged = False
        store_instance = True
    
    # overwrite default import/export methods
        
    def before_import(self, dataset, using_transactions, dry_run, **kwargs):
        if not dry_run:
            self.groups = kwargs.pop("groups", None)
        self.user = kwargs.pop("user")

        # To separate imports with same import_id we add the user's institute to the import_by field
        import_by = self.user.assigned_institute.full_name
        dataset.headers.append(IMPORT_BY_COLUMN_NAME)
        dataset.append_col([import_by for row in range(len(dataset))], header=IMPORT_BY_COLUMN_NAME)
        
    def before_save_instance(self, instance, using_transactions, dry_run):
        if not dry_run and self.user:
            if hasattr(instance, "assigned_institute"):
                instance.assigned_institute = self.user.assigned_institute
            elif hasattr(instance, "institute"):
                instance.institute = self.user.assigned_institute

    def after_import_row(self, row, row_result, row_number=None, **kwargs):
        if kwargs.get("create_import_history", False):
            content_type = kwargs.get("content_type")
            model_class = content_type.model_class()
            model_name = model_class._meta.verbose_name if model_class else content_type.model 
            self.imported_models.append(
                ImportedModel(
                    object_id=row_result.object_id,
                    content_type=content_type,
                    data_import=kwargs.get("data_import"),
                    diff=row_result.diff,
                    import_type=row_result.import_type,
                    model_name=model_name,
                ))
            
    def after_import(self, dataset, result, using_transactions, dry_run, **kwargs):
        logger.info("After import")
        if kwargs.get("create_import_history", False):
            logger.info("Bulk create import objects for history")
            ImportedModel.objects.bulk_create(self.imported_models)

        if settings.IMPORT_WITH_DJANGO_GUARDIAN:
            # Permissions only need to be assigned for new instances.
            instances = [row.instance for row in result.rows if row.import_type == RowResult.IMPORT_TYPE_NEW]
            logger.info(f"Bulk creating {len(instances)} object permissions.")

            if not instances:
                return

            # Bulk assigns each permission to a group for all instances at once
            for post_import_permission in self.post_import_permissions:
                for group in self.groups:
                    assign_perm(post_import_permission.permission, group, instances)



    
    def import_field(self, field, obj, data, is_m2m=False, **kwargs):
        # If a value for updated_at is imported (e.g. the date of the last change made in the old system),
        # the time of day is set to 12 o'clock (midday) to avoid switchovers due to timezones.
        # Without this, 24.12.2024 might be imported as "23.12.2024 23:00:00" with a 1 hour timezone difference.
        if field.attribute and field.attribute == "updated_at":
            updated_at_value = data.get(field.column_name)

            if isinstance(updated_at_value, str):
                try:
                    updated_at_value = timezone.datetime.fromisoformat(updated_at_value)
                except ValueError:
                    updated_at_value = timezone.datetime.strptime(updated_at_value, "%d.%m.%Y")

            if updated_at_value:
                updated_at_value = updated_at_value.replace(hour=12, minute=0, second=0, microsecond=0)
                obj.updated_at = updated_at_value
        else:
            super().import_field(field, obj, data, is_m2m, **kwargs)

    # skips a row if the updated_at field from the import is older than the one already in the system.
    def skip_row(self, instance, original, row, import_validation_errors=None):
        # If there's no original primary key or no updated_at field, this instance is new and should never be skipped.
        if original.pk and hasattr(instance, "updated_at") and instance.updated_at:
            if isinstance(instance.updated_at, str):
                try:
                    last_update = timezone.datetime.fromisoformat(instance.updated_at)
                except ValueError:
                    last_update = timezone.datetime.strptime(last_update, "%d.%m.%Y")
            else:
                last_update = instance.updated_at

            if timezone.is_naive(last_update):
                last_update = timezone.make_aware(last_update, timezone.get_default_timezone())

            if last_update.date() < original.updated_at.date():
                return True

        return super().skip_row(instance, original, row, import_validation_errors)

class ChoicesWidget(Widget):
    """
    Widget that uses choice display values in place of database values
    """
    def __init__(self, mapper_field_choice_qs, *args, **kwargs):
        """
        Creates a self.choices dict with a key, display value, and value,
        db value, e.g. {'Chocolate': 'CHOC'}
        """
        self.choices = {}
        for mapper_field_choice in list(mapper_field_choice_qs):
            self.choices[mapper_field_choice.key] = mapper_field_choice.value.lower().strip()

        self.revert_choices = dict((v, k) for k, v in self.choices.items())

    def clean(self, value, row=None, *args, **kwargs):
        """Returns the db value given the display value"""
        return self.revert_choices.get(value.lower().strip(), value) if value else None

    def render(self, value, obj=None):
        """Returns the display value given the db value"""
        return self.choices.get(value, '')