from django.shortcuts import redirect, render
from django.views.generic import TemplateView
from importer.models import ChunkImport
from django.urls import reverse_lazy

def check_for_existing_import(request, import_type, import_url):
    chunk_import, created = ChunkImport.objects.update_or_create(
        institute=request.user.assigned_institute,
        import_type=import_type,
        defaults={"last_user": request.user},
    )
    if created or chunk_import.current_chunk == 0 or not chunk_import.ongoing:
        return redirect(import_url)
    else:
        template_name = "importer/continue_import.html"
        return render(
            request,
            template_name=template_name,
            context={
                "chunk_import": chunk_import,
                "new_import_url": reverse_lazy("importer:reset-import", kwargs={"import_type": import_type, "import_url": import_url}),
                "continue_url": reverse_lazy(import_url),
            },
        )

def reset_chunk_import(request, import_type, import_url):
    ChunkImport.objects.update_or_create(
        institute=request.user.assigned_institute, import_type=import_type, defaults={"last_user": request.user, "ongoing": False, "current_chunk": 0}
    )
    return redirect(reverse_lazy(import_url))
