import logging
from typing import Any
from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.contrib import messages
from django.db.models.query import QuerySet
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.decorators.http import require_POST
from django.views.generic import CreateView, DeleteView, FormView, ListView
from django_tables2 import SingleTableMixin
from import_export.admin import ImportMixin
from import_export.formats import base_formats
from import_export.forms import ImportForm
from importer.forms import *
from importer.models import Mapper, MapperField, MapperFieldChoice
from importer.tables import MapperFieldTable, MapperTable
from importer.views.importer_utils import *

logger = logging.getLogger(__name__)

class MapperNew(CreateView):
    model = Mapper
    form_class = MapperNewForm
    template_name = 'importer/forms/mapper_new.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs
    
    def get_success_url(self):
        if settings.IMPORT_WITH_DJANGO_GUARDIAN:
            self.object.assign_perms(self.request.user)
        logger.info(f"Mapper {self.object.name} created by {self.request.user}")
        return reverse_lazy('importer:mapper-fields', kwargs={"pk": self.object.pk})

class MapperDelete(DeleteView):
    model = Mapper
    template_name = 'global/forms/generic_confirm_delete.html'
    success_url = reverse_lazy('importer:mapper-list')

    def post(self, request: HttpRequest, *args: str, **kwargs: Any) -> HttpResponse:
        if settings.IMPORT_WITH_DJANGO_GUARDIAN:
            if not self.get_object().has_delete_permission(request.user):
                raise PermissionDenied
        return super().post(request, *args, **kwargs)

    def get_success_url(self) -> str:
        logger.info(f"Mapper {self.object.name} deleted by {self.request.user}")
        return super().get_success_url()

class MapperFieldDelete(DeleteView):
    model = MapperField
    template_name = 'global/forms/generic_confirm_delete.html'

    def post(self, request: HttpRequest, *args: str, **kwargs: Any) -> HttpResponse:
        if settings.IMPORT_WITH_DJANGO_GUARDIAN:
            if not self.get_object().mapper.has_delete_permission(request.user):
                raise PermissionDenied
        return super().post(request, *args, **kwargs) 
    
    def get_success_url(self) -> str:
        return reverse_lazy('importer:mapper-fields', kwargs={"pk": self.kwargs.get("mapper_id")})
    
class MapperFieldsView(CreateView):
    model = MapperField
    form_class = MapperFieldsForm
    template_name = 'importer/forms/mapper_fields.html'

    def get(self, request: HttpRequest, *args: str, **kwargs: Any) -> HttpResponse:
        if settings.IMPORT_WITH_DJANGO_GUARDIAN:
            if not Mapper.objects.get(pk=self.kwargs.get("pk")).has_view_permission(request.user):
                raise PermissionDenied
        return super().get(request, *args, **kwargs)
    
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["content_type"] = Mapper.objects.get(pk=self.kwargs.get("pk")).content_type
        kwargs["mapper"] = Mapper.objects.get(pk=self.kwargs.get("pk"))
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context ["mapper"] = Mapper.objects.get(pk=self.kwargs.get("pk"))
        context["table"] = MapperFieldTable(MapperField.objects.filter(mapper=self.kwargs.get("pk")))
        context["import_form"] = ImportForm(import_formats=base_formats.DEFAULT_FORMATS)
        return context
    
    def form_valid(self, form):
        if form.is_valid():
            mapper = Mapper.objects.get(pk=self.kwargs.get("pk"))
            internal_fieldname = form.cleaned_data.get("internal_fieldname")
            field = get_field_by_field_name_dict(mapper.content_type)[internal_fieldname]
            if field.get_internal_type() == "ForeignKey" and field.many_to_one:
                form.instance.foreign_key_model=ContentType.objects.get_for_model(field.related_model)
            form.instance.internal_type = field.get_internal_type()
            form.instance.mapper = mapper
            mapper_field = form.save()
            if field.get_internal_type() == "CharField" and field.choices:
                for item in field.choices:
                    obj, created = mapper_field_choice = MapperFieldChoice.objects.get_or_create(key=item[0], value=item[1])
                    obj.mapper_field.add(mapper_field)
                    
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('importer:mapper-fields', kwargs={"pk": self.kwargs.get("pk")})
    
class MapperList(SingleTableMixin, ListView):
    model = Mapper
    paginate_by = 15
    template_name = 'importer/tables/mapper_list.html'
    table_class = MapperTable
    ordering = ["-updated_at"]

    def get_queryset(self) -> QuerySet[Any]:
        if settings.IMPORT_WITH_DJANGO_GUARDIAN:
            return Mapper.objects.get_mappers_for_user(self.request.user).order_by("-updated_at")
        return super().get_queryset()

class MapperFieldsHeaderImport(ImportMixin, FormView):
    template_name = 'importer/forms/import_header_fields.html'
    form_class = ImportForm

    def get_success_url(self):
        return reverse_lazy('importer:mapper-fields', kwargs={"pk": self.kwargs.get("pk")})

def create_mapper_fields_by_import(request, pk):
    if settings.IMPORT_WITH_DJANGO_GUARDIAN:
        if not Mapper.objects.get(pk=pk).has_view_permission(request.user):
            raise PermissionDenied
    if request.method == 'POST':
        form = ImportFileForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            #Get the headers out of the import_file
            import_file, dataset = create_dataset_from_import_form(form)
            
            filtered_headers = [head for head in dataset.headers if head is not None] 

            mapper = Mapper.objects.get(id=pk)
            choices = get_choices_from_content_type(mapper.content_type)
            choices.append(("", "---------")) #Add empty choice to skip field
            context = {
                "headers": filtered_headers,
                "model_field_choices": choices,
                "mapper": mapper,
            }
            return render(request, 'importer/forms/field_mapping.html', context)
        else:
            logger.error(f"Invalid form for import file by {request.user}")
    else:
        form = ImportFileForm()

    return render(request, 'importer/forms/import_file.html', {"form": form})

@require_POST
def create_multiple_fields(request, pk):
    user_mapping = request.POST.copy()
    mapper = Mapper.objects.get(id=pk)
    if settings.IMPORT_WITH_DJANGO_GUARDIAN:
        if not mapper.has_view_permission(request.user):
            raise PermissionDenied
    field_dic = get_field_by_field_name_dict(mapper.content_type)
    logger.info(f"Create multiple fields for mapper {mapper.name} by {request.user}")

    fields_to_create = []
    existing_internal_fieldnames = mapper.mapperfield_set.all().values_list("internal_fieldname", flat=True)
    existing_import_fieldnames = mapper.mapperfield_set.all().values_list("import_fieldname", flat=True)

    class_field_names = [choice[0] for choice in get_choices_from_content_type(mapper.content_type)]
    for header_name, internal_name in user_mapping.items():
        if not internal_name:
            #Ignore empty fields
            continue

        if header_name in existing_import_fieldnames:
            error = f"Import fieldname {header_name} already exists for mapper {mapper.name} (skipped)"
            messages.error(request, error)
            continue

        if internal_name in existing_internal_fieldnames:  
                error = f"Internal fieldname {internal_name} already exists for mapper {mapper.name} (skipped)"
                messages.error(request, error)
                continue
        
        if internal_name in class_field_names:
            internal_field = field_dic[internal_name]
            fields_to_create.append(MapperField(
                mapper=mapper,
                import_fieldname=header_name,
                internal_fieldname=internal_name,
                internal_type=internal_field.get_internal_type(),
            ))

    mapper_fields = MapperField.objects.bulk_create(fields_to_create)

    # Add MapperFieldChoices afterwards
    for mapper_field in mapper_fields:
        field = field_dic[mapper_field.internal_fieldname]
        if field.get_internal_type() == "CharField" and field.choices:
            for item in field.choices:
                obj, created = mapper_field_choice = MapperFieldChoice.objects.get_or_create(key=item[0], value=item[1])
                obj.mapper_field.add(mapper_field)

    return redirect('importer:mapper-fields', pk=pk)


