import logging

from django.conf import settings
from django.utils.module_loading import import_string
from import_export.formats import base_formats
from import_export.tmp_storages import MediaStorage, TempFolderStorage
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from kbv_importer.import_builder import ImportObject
from django.shortcuts import render, redirect
from typing import Callable

logger = logging.getLogger(__name__)

def get_tmp_storage_class():
    tmp_storage_class = getattr(
        settings,
        "IMPORT_EXPORT_TMP_STORAGE_CLASS",
        TempFolderStorage,
    )
    logger.debug("Tmp storage class: %s", tmp_storage_class)
    if isinstance(tmp_storage_class, str):
        tmp_storage_class = import_string(tmp_storage_class)
    return tmp_storage_class

def write_to_tmp_storage(import_file, input_format):
    from_encoding = "utf-8-sig"
    encoding = None
    if not input_format.is_binary():
        encoding = from_encoding

    tmp_storage_cls = get_tmp_storage_class()
    tmp_storage = tmp_storage_cls(encoding=encoding, read_mode=input_format.get_read_mode())
    data = bytes()
    for chunk in import_file.chunks():
        data += chunk

    if tmp_storage_cls == MediaStorage and not input_format.is_binary():
        data = data.decode(from_encoding)

    logger.debug("Write to tmp storage")
    tmp_storage.save(data)
    return tmp_storage

def load_dataset_from_temp_storage(input_format, import_file_name, remove_storage=True):
    input_format = get_import_formats(int(input_format))
    encoding = None if input_format.is_binary() else "utf-8-sig"
    tmp_storage_cls = get_tmp_storage_class()
    tmp_storage = tmp_storage_cls(
            name=import_file_name,
            encoding=encoding,
            read_mode=input_format.get_read_mode(),
        )
    data = tmp_storage.read()
    dataset = input_format.create_dataset(data)
    logger.debug("Storage dataset loaded")
    if remove_storage:
        tmp_storage.remove()
    return dataset 

def get_import_formats(input_format_number):
        import_formats = [f for f in base_formats.DEFAULT_FORMATS if f().can_import()]
        input_format = import_formats[input_format_number]()
        if not input_format.is_binary():
            input_format.encoding = "utf-8-sig"
        logger.debug("Input format: %s", input_format)
        return input_format

def create_dataset_from_import_form(import_form):
        input_format = get_import_formats(int(import_form.cleaned_data["input_format"]))
        import_file = import_form.cleaned_data["import_file"]
        return create_basic_dataset(import_file, input_format)

def create_dataset_from_json_data(import_file):
    input_format = base_formats.JSON()
    return create_basic_dataset(import_file, input_format)

def create_basic_dataset(import_file, input_format):
        # first always write the uploaded file to disk as it may be a
        # memory file or else based on settings upload handlers
        tmp_storage = write_to_tmp_storage(import_file, input_format)
        # allows get_confirm_form_initial() to include both the
        # original and saved file names from form.cleaned_data
        import_file.tmp_storage_name = tmp_storage.name
        try:
            # then read the file, using the proper format-specific mode
            # warning, big files may exceed memory
            data = tmp_storage.read()
            dataset = input_format.create_dataset(data)
        except Exception as e:
            logger.error("Error encountered while trying to read file: %s", import_file.name)
            logger.exception(e)
        return import_file, dataset

def get_choices_from_content_type(content_type):
    choices = []
    for field in content_type.model_class()._meta.get_fields():
        i_type = field.get_internal_type() 
        """We dont want the user to choose the real pk of the model. We want to use the field "import_id" as pk.
        The model we want to import need to inherent from the ImportableMixin."""
        if (i_type == "ForeignKey" and field.many_to_one) or (i_type == "OneToOneField") or (not field.related_model and not field.primary_key and field.name not in ["import_by", "created_at"]):
            field_name = field.verbose_name if hasattr(field, 'verbose_name') else field.name
            choices.append((field.name, field_name))
            choices.sort(key=lambda tuple: tuple[1])
    return choices

def get_field_by_field_name_dict(content_type):
    dictionary = {}
    for field in content_type.model_class()._meta.get_fields():
        dictionary[field.name] = field
    return dictionary

def check_user_permissions(user, mapper, import_object_ids, permission):
        no_object_permission = []
        import_model_instances = mapper.content_type.model_class().objects.filter(id__in=import_object_ids)
        change_perm = mapper.content_type.app_label + '.' + permission + '_' + mapper.content_type.model
        for obj in import_model_instances:
            if user.has_perm(change_perm, obj) == False:
                no_object_permission.append(obj.id)
        return no_object_permission

# Get new column name for foreignKey ids.
def get_foreignkey_mapperField_import_fieldname(mapper_field):
     if mapper_field.import_fieldname and mapper_field.import_fieldname.startswith("PRM_"):
        return mapper_field.import_fieldname
     return f"PRM_{mapper_field.import_fieldname}_id"

# This function is used to add a column with the primary keys of the foreign key models.
def add_primary_key_column_from_import_id(institute, dataset, mapper):
    for mapper_field in mapper.mapperfield_set.all():
        if mapper_field.is_foreign_key():
            fk_import_ids = [str(field) for field in dataset[mapper_field.import_fieldname]]
            existing_related_models = mapper_field.foreign_key_model.model_class().objects.filter(import_id__in=fk_import_ids, import_by=institute)
            primary_keys = []
            #This seems odd, but we need to keep the order and therefore add None values if not found.
            for fk_import_id in fk_import_ids:
                if existing_related_models.filter(import_id=fk_import_id).exists():
                    primary_keys.append(existing_related_models.get(import_id=fk_import_id).pk)
                else:
                    primary_keys.append(None)
            #Add column with primary keys
            new_column_name = get_foreignkey_mapperField_import_fieldname(mapper_field)
            dataset.headers.append(new_column_name)
            dataset.append_col(primary_keys, header=new_column_name)

def handle_import_result(request, result, data_import):
        if not result.has_errors():
            logger.info("Import successful")
            messages.success(request, _('Import successful.'))
        else:
            logger.error("Import failed")
            logger.debug(result.row_errors())
            messages.error(request, _('Import failed.'))
            data_import.success = False
            data_import.save()

def check_for_ongoing_import(import_object: ImportObject, import_result, request, detailed_import: Callable):
        if not import_result.has_errors():                  
            import_object.chunk_import.increase_current_chunk(import_object.storage_dataset.height)
            if import_object.chunk_import.ongoing:
                logger.info("Import ongoing")
                # import next chunk
                import_object.check_for_other_fields()
                context = prepare_confirm_form(request, import_object, detailed_import)
                return render(request, import_object.html_template, context)
            else:
                logger.info("Import finished")
                #clean storage
                load_dataset_from_temp_storage(import_object.input_format, import_object.original_file_name)
                return redirect(import_object.import_url)

def prepare_confirm_form(request, import_object : ImportObject, detailed_import : Callable):
        logger.info("Prepare confirm form")
        try:
            import_result = detailed_import(import_object)
        except KeyError as keyError:
            messages.error(request, _('The specified import fields do not match those in your file.'))
            logger.error(f"KeyError while importing data for {import_object.import_type}: {keyError}")
            return redirect(import_object.import_url)
        except Exception as e:
            logger.exception(f"Error while importing with type {import_object.import_type}")
            messages.error(request, _('An error occurred during the import. Please check your settings.'))
            return redirect(import_object.import_url)

        context = {}
        context["result"] = import_result
        context["confirm_form"] = import_object.confirm_form_instance
        
        return context
