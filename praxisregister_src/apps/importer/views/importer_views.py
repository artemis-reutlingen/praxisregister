import logging

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import Permission
from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django.views.decorators.http import require_POST
from django.views.generic import FormView
from guardian.shortcuts import get_objects_for_user
from importer.forms import CustomConfirmImportForm, CustomImportFileForm
from importer.models import ContentTypeRestriction, Mapper, ImportSettings, ChunkImport
from importer.views.importer_utils import *
from importer.views.importer_history_views import create_data_import_object
from importer.resources import CustomResource
from importer.forms import ImportSettingsForm

logger = logging.getLogger(__name__)


class ImportView(FormView):
    template_name = "importer/forms/import_file.html"
    form_class = CustomImportFileForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # Let the user decide to which of his groups the imported objects should belong
        kwargs["groups"] = self.request.user.groups.all()
        kwargs["user"] = self.request.user
        return kwargs

    def form_valid(self, form) -> HttpResponse:
        import_file, dataset = create_dataset_from_import_form(form)

        mapper = form.cleaned_data["mapper"]
        if not form.errors:
            add_primary_key_column_from_import_id(self.request.user.assigned_institute, dataset, mapper)
            save_null_fields = ImportSettings.objects.get(
                institute=self.request.user.assigned_institute
            ).save_null_fields

            groups = self.get_form_kwargs()["groups"]
            resource = CustomResource(mapper=mapper, groups=groups, save_null_fields=save_null_fields)
            # This is just a dry import (data temporary stored)
            logger.info(
                f"Import: Dry run with file {import_file.name}  by user {self.request.user}"
            )
            result = resource.import_data(
                dataset,
                dry_run=True,
                raise_errors=False,
                file_name=import_file.name,
                user=self.request.user,
            )

            # Using Django Guardian allows object based permission check
            # Permissions for models that needs to be imported can be configured in the admin interface
            if settings.IMPORT_WITH_DJANGO_GUARDIAN and not result.has_errors():
                logger.info("Checking object permissions for import")
                objects_to_create = []
                objects_to_update = set()
                objects_to_delete = set()
                for row in result.valid_rows():
                    if row.import_type == "new":
                        objects_to_create.append(row.object_id)
                    if row.import_type == "update":
                        objects_to_update.add(row.object_id)
                    if row.import_type == "delete":
                        objects_to_delete.add(row.object_id)

                error_message = []
                # get the permissions the user needs for accessing the content type
                ct_restrictions = ContentTypeRestriction.objects.filter(
                    allowed_content_types=mapper.content_type,
                    group__in=self.request.user.groups.all(),
                ).distinct()
                # Check if the user fulfills the permissions for creating the objects
                self.check_create_permission(
                    mapper, objects_to_create, error_message, ct_restrictions
                )

                self.check_update_permission(
                    mapper, objects_to_update, error_message, ct_restrictions
                )

                self.check_delete_permission(
                    mapper, objects_to_delete, error_message, ct_restrictions
                )

                for error in error_message:
                    messages.error(self.request, error)
                    logger.error(error)
                    return self.form_invalid(form)
        else:
            logger.error(form.errors)
            messages.error(
                self.request,
                _("An error occurred during the import. Please check your settings."),
            )
            return redirect("importer:import-preview")

        context = self.get_context_data()
        context["result"] = result
        kwargs = {"groups": self.request.user.groups.all()}
        logger.debug("Preparing confirm form")
        context["confirm_form"] = CustomConfirmImportForm(
            initial={
                "import_file_name": import_file.tmp_storage_name,
                "original_file_name": import_file.name,
                "input_format": form.cleaned_data["input_format"],
                "mapper_id": mapper.id,
                "groups": form.cleaned_data["groups"],
            },
            **kwargs,
        )

        # We render the same template but pass the confirm form with the context
        return self.render_to_response(context)

    # For importing new data, we only have to check the global permission (no object permissions yet)
    def check_create_permission(self, mapper, objects_to_create, error_message, ct_restrictions):
        if len(objects_to_create) > 0:
            has_permission = True
            restiction = ct_restrictions.filter(import_type__name="new")
            create_permissions = restiction.filter(
                auth_permission__isnull=False
            ).values_list("auth_permission", flat=True)
            if len(create_permissions) > 0:
                for permission in list(
                    Permission.objects.filter(id__in=create_permissions)
                ):
                    # check perm (format: app_label.codename)
                    if not self.request.user.has_perm(
                        mapper.content_type.app_label + "." + permission.codename
                    ):
                        has_permission = False
                        break
            else:
                if restiction.count() == 0:
                    has_permission = False
                else:
                    # We allow creating objects here, because the admin created a restriction without permissions
                    pass
            if not has_permission:
                error_message.append(
                    mark_safe(
                        _("You have no Permission to create objects of type ")
                        + f"{str(mapper.content_type)} "
                        + _(
                            "Please adjust your import file or contact the administrator."
                        )
                    )
                )

    # Update check needs to be done on object level
    def check_update_permission(
        self, mapper, objects_to_update, error_message, ct_restrictions
    ):
        import_type_name = "update"
        self.object_based_permission_check(
            mapper, objects_to_update, error_message, ct_restrictions, import_type_name
        )

    def check_delete_permission(
        self, mapper, objects_to_delete, error_message, ct_restrictions
    ):
        import_type_name = "delete"
        self.object_based_permission_check(
            mapper, objects_to_delete, error_message, ct_restrictions, import_type_name
        )

    def object_based_permission_check(
        self, mapper, objects, error_message, ct_restrictions, import_type_name
    ):
        if len(objects) > 0:
            has_permission = True
            # Get all permissions the user needs for updating the objects
            restriction = ct_restrictions.filter(import_type__name=import_type_name)
            permissions = list(
                Permission.objects.filter(
                    id__in=list(restriction.values_list("auth_permission", flat=True))
                )
            )
            if len(permissions) > 0:
                # Get all objects_ids the user has permission to
                permitted_object_ids = get_objects_for_user(
                    self.request.user,
                    [
                        mapper.content_type.app_label + "." + permission.codename
                        for permission in permissions
                    ],
                ).values_list("pk", flat=True)
                # Equal means all elements of A are present in B
                if not objects.intersection(permitted_object_ids) == objects:
                    has_permission = False
            else:
                if restriction.count() == 0:
                    has_permission = False
                else:
                    # We allow creating objects here, because the admin created a restriction without permissions
                    logger.info(
                        "No permissions set for updating objects, but restriction exists"
                    )

            if not has_permission:
                error_message.append(
                    mark_safe(
                        _("You have no Permission to")
                        + f" {import_type_name} "
                        + _("some objects with type")
                        + f" {mapper.content_type}. "
                        + _(
                            "Please adjust your import file or contact the administrator."
                        )
                    )
                )


@require_POST
def process_import(request, *args, **kwargs):
    """
    Perform the actual import action (after the user has confirmed the import)
    """
    # NOTE: The handling of kwargs doesn't make much sense in this module. It should be refactored at some point.
    kwargs = {"groups": request.user.groups.all()}

    logger.debug("Processing import")
    confirm_form = CustomConfirmImportForm(data=request.POST, **kwargs)

    if confirm_form.is_valid():
        dataset = load_dataset_from_temp_storage(
            confirm_form.cleaned_data["input_format"],
            confirm_form.cleaned_data["import_file_name"],
        )

        mapper = Mapper.objects.get(id=confirm_form.cleaned_data["mapper_id"])
        add_primary_key_column_from_import_id(request.user.assigned_institute, dataset, mapper)
        save_null_fields = ImportSettings.objects.get(
            institute=request.user.assigned_institute
        ).save_null_fields
        resource = CustomResource(mapper=mapper, groups=kwargs["groups"], save_null_fields=save_null_fields)
        # Create a DataImport for import history
        groups = confirm_form.cleaned_data["groups"]
        data_import = create_data_import_object(request.user, groups)

        file_name = confirm_form.cleaned_data["original_file_name"]
        logger.info(
            f"Import {file_name} with mapper {mapper.name} by user {request.user}"
        )

        result = resource.import_data(
            dataset,
            dry_run=False,
            file_name=file_name,
            user=request.user,
            rollback_on_validation_errors=True,
            # extra kwargs for import history
            groups=groups,
            create_import_history=True,
            content_type=mapper.content_type,
            data_import=data_import,
        )
        data_import.headers = result.diff_headers
        data_import.save()

        handle_import_result(request, result, data_import)

    else:
        logger.error("Confirm form invalid")
    return redirect("importer:import-preview")


class SettingsView(FormView):
    template_name = "practice_importer/import_settings.html"
    form_class = ImportSettingsForm
    success_url = reverse_lazy("importer:import-settings")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        institute = self.request.user.assigned_institute
        import_settings, _ = ImportSettings.objects.get_or_create(institute=institute)
        chunk_import = ChunkImport.objects.filter(
            institute=institute
        ).first()  # Is equal for all import types
        chunk_size = chunk_import.chunk_size if chunk_import else 50
        kwargs["initial"] = {
            "save_null_fields": import_settings.save_null_fields,
            "chunk_size": chunk_size,
            "levenshtein_ignore_case": import_settings.levenshtein_ignore_case,
        }
        return kwargs

    def form_valid(self, form) -> HttpResponse:
        if form.is_valid():
            institute = self.request.user.assigned_institute

            ImportSettings.objects.update_or_create(
                institute=institute,
                defaults={
                    "save_null_fields": form.cleaned_data["save_null_fields"],
                    "levenshtein_ignore_case": form.cleaned_data["levenshtein_ignore_case"],
                },
            )

            chunk_size = form.cleaned_data["chunk_size"]
            ChunkImport.objects.filter(institute=institute).update(
                chunk_size=chunk_size
            )

            messages.success(self.request, _("Settings saved."))
        return super().form_valid(form)
