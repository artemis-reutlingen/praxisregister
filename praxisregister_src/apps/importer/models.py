
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import gettext_lazy as _
from guardian.shortcuts import assign_perm, get_objects_for_user
from guardian.models import UserObjectPermissionBase, GroupObjectPermissionBase

class ImportableMixin(models.Model):

    import_id = models.CharField(max_length=256, verbose_name=_("Import ID"), null=True, blank=True)
    import_by = models.CharField(max_length=256, verbose_name=_("Import by"), null=True, blank=True)

    class Meta:
        abstract = True
        unique_together = ('import_id', 'import_by')

class MapperManager(models.Manager):
    
    @staticmethod
    def get_mappers_for_user(user):
        return get_objects_for_user(user, 'importer.view_mapper')

class Mapper(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False, unique=True, verbose_name=_("Mapper name"))
    created_at = models.DateTimeField(verbose_name=_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_("Updated at"), auto_now=True)
    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
        verbose_name=_("content type")
    )
    import_type = models.CharField(max_length=255, null=True, blank=True, verbose_name=_("Import type"), default="")
    institute = models.ForeignKey("institutes.Institute", on_delete=models.CASCADE, verbose_name=_("Institute"), null=True, blank=True)

    objects = MapperManager()

    def __str__(self):
        translation = _("New Mapper")
        return f"{self.name}" if self.name else f"{translation}"
        
    def assign_perms(self, user):
        if user.group:
            assign_perm('importer.view_mapper', user.group, self)
            assign_perm('importer.delete_mapper', user.group, self)
    
    def has_view_permission(self, user):
        return user.has_perm('importer.view_mapper', self)
    
    def has_delete_permission(self, user):
        return user.has_perm('importer.delete_mapper', self)
    

# Explicit definition of Mapper object permissions. This ensures permission objects are also deleted when a mapper is deleted instead of being orphaned.
class MapperUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(Mapper, on_delete = models.CASCADE)

class MapperGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(Mapper, on_delete = models.CASCADE)

class MapperField(models.Model):
    internal_fieldname = models.CharField(max_length=255, verbose_name=_("Internal fieldname"))
    internal_type = models.CharField(max_length=255, verbose_name=_("Internal type"))
    import_fieldname = models.CharField(max_length=255, verbose_name=_("Import fieldname"))
    mapper = models.ForeignKey(Mapper, on_delete=models.CASCADE, null=False)
    foreign_key_model = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, verbose_name=_("Foreign key model"))

    def __str__(self):
        translation = _("Mapper fields")
        return f"{translation}" 
    
    def is_foreign_key(self):
        return self.internal_type == "ForeignKey"
    
    def is_one_to_one_field(self):
        return self.internal_type == "OneToOneField"

    class Meta:
        unique_together = ('internal_fieldname', 'mapper')

class MapperFieldChoice(models.Model):
    mapper_field = models.ManyToManyField(MapperField)
    key = models.CharField(max_length=255, verbose_name=_("Key"))
    value = models.CharField(max_length=255, verbose_name=_("Value"))

    unique_together = ('key', 'value')

# This represents an import type or action like "create" or "update"
# Made this a model instead of constants in order for a simple use in the admin interface
class ImportType(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)

    def __str__(self):
        return self.name

#Used by admin to restrict content types for a group
class ContentTypeRestriction(models.Model):
    group = models.ForeignKey("auth.Group", on_delete=models.CASCADE, verbose_name=_("Group"))
    allowed_content_types = models.ForeignKey(ContentType, verbose_name=_("Allowed content types"), on_delete=models.CASCADE)
    auth_permission = models.ManyToManyField("auth.Permission", verbose_name=_("Auth permission"), blank=True)
    import_type = models.ManyToManyField(ImportType, verbose_name=_("Import type"))

    class Meta:
        verbose_name = _("Content type restriction")
        verbose_name_plural = _("Content type restrictions")

class ContentTypePostImportPermission(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    permission = models.ForeignKey("auth.Permission", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Post import permission")
        verbose_name_plural = _("Post import permissions")

class DataImportManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().prefetch_related("groups")
    
    @staticmethod
    def get_data_imports_for_user(user):
        return get_objects_for_user(user, 'importer.view_dataimport')

#This model represents one full data import (for example a file)
class DataImport(models.Model):
    imported_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name=_("Imported by"))
    imported_at = models.DateTimeField(verbose_name=_("Created at"), auto_now_add=True)
    success = models.BooleanField(default=False, verbose_name=_("Success"))
    groups = models.ManyToManyField("auth.Group", verbose_name=_("Groups"), blank=True)
    headers = models.TextField(default="[]", verbose_name=_("Headers"))

    objects = DataImportManager()

    def assign_perms(self, group):
        assign_perm('importer.view_dataimport', group, self)

    def has_view_permission(self, user):
        return user.has_perm('importer.view_dataimport', self)
    

# Explicit definition of DataImport object permissions. This ensures permission objects are also deleted when a data import is deleted instead of being orphaned.
class DataImportUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(DataImport, on_delete = models.CASCADE)

class DataImportGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(DataImport, on_delete = models.CASCADE)

# This model represents one line of the imported data (for example in an exel file).
class ImportedModel(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=False)
    object_id = models.PositiveIntegerField() # This is the original id of the imported resource
    imported_resource = GenericForeignKey('content_type', 'object_id')
    data_import = models.ForeignKey(DataImport, on_delete=models.CASCADE, verbose_name=_("Data import"))
    diff = models.TextField()
    import_type = models.CharField(max_length=255, null=False, blank=False, verbose_name=_("Import type"))
    model_name = models.CharField(max_length=255, null=False, blank=False, verbose_name=_("Model name"), default=_("Unknown model"))

class ChunkImport(models.Model):
    
    ongoing = models.BooleanField(default=False)
    chunk_size = models.IntegerField(default=50)
    current_chunk = models.IntegerField(default=0)

    # DB, Standard, KBV    
    import_type = models.CharField(max_length=255, null=False, blank=False, verbose_name=_("Import type"))
    institute = models.ForeignKey('institutes.Institute', on_delete=models.CASCADE)
    file_name = models.CharField(max_length=256, null=True, blank=True)

    last_updated = models.DateTimeField(auto_now=True)
    last_user = models.ForeignKey('users.User', on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        unique_together = ('institute', 'import_type')

    def increase_current_chunk(self, max_size):
        self.current_chunk += self.chunk_size
        self.ongoing = True
        if self.current_chunk >= max_size:
            self.current_chunk = 0
            self.ongoing = False
        self.save()
    
    def create_sub_dataset(self, dataset):
        to = min([self.current_chunk + self.chunk_size, dataset.height])
        dataset = dataset.subset(rows=list(range(self.current_chunk, to)))
        return dataset

    def __str__(self) -> str:
        obj_name = _("Chunk Import settings")
        return f"{obj_name}"
    
class ImportSettings(models.Model):

    save_null_fields = models.BooleanField(default=False, verbose_name=_("Save null fields"))
    institute = models.OneToOneField('institutes.Institute', on_delete=models.CASCADE)
    levenshtein_ignore_case = models.BooleanField(default=False, verbose_name=_("Ignore capitalization"))