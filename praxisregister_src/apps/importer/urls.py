from django.urls import path
from importer.views import mapper_views, importer_views, importer_history_views, chunk_views

app_name = 'importer'

urlpatterns = [
    path('mapper', mapper_views.MapperList.as_view(), name="mapper-list"),
    path('mapper/new', mapper_views.MapperNew.as_view(), name="mapper-new"),
    path('mapper/<int:pk>/delete', mapper_views.MapperDelete.as_view(), name="mapper-delete"),
    path('mapper/<int:pk>/fields', mapper_views.MapperFieldsView.as_view(), name="mapper-fields"),
    path('mapper/<int:pk>/fields/new', mapper_views.create_multiple_fields, name="mapper-fields-bulk-create"),
    path('mapper/<int:mapper_id>/field/<int:pk>/delete', mapper_views.MapperFieldDelete.as_view(), name="mapper-field-delete"),
    path('mapper/<int:pk>/fields/import', mapper_views.create_mapper_fields_by_import, name="mapper-fields-import"),

    path('', importer_views.ImportView.as_view(), name="import-preview"),
    path('import', importer_views.process_import, name="import"),
    path('settings', importer_views.SettingsView.as_view(), name="import-settings"),

    path('history', importer_history_views.ImportList.as_view(), name="import-history"),
    path('history/detail/<int:pk>', importer_history_views.ImportDetailList.as_view(), name="import-history-detail"),
    path('history/model/<int:pk>', importer_history_views.ImportModelList.as_view(), name="import-history-model"),

    path('reset/<import_type>/<import_url>', chunk_views.reset_chunk_import, name="reset-import"),
]
