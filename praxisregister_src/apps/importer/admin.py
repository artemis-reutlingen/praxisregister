from django.contrib import admin
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from importer.models import (ContentTypePostImportPermission,
                             ContentTypeRestriction, DataImport, Mapper)


class ContentTypeRestrictionAdmin(admin.ModelAdmin):
    model = ContentTypeRestriction
    list_display = ("id", "group", "allowed_content_types")
    list_per_page = 20


    def save_model(self, request, obj, form, change):
        allowed_content_type = request.POST.get('allowed_content_types')
        # Add view, change and delete permission for the allowed content type
        # This safes us from having to add the permissions manually
        content_type = ContentType.objects.get(id=allowed_content_type)
        permission, _ = ContentTypePostImportPermission.objects.get_or_create(
            content_type=content_type,
            permission=Permission.objects.get(content_type=content_type, codename=f"view_{content_type.model}")
        )
        permission, _ = ContentTypePostImportPermission.objects.get_or_create(
            content_type=content_type,
            permission=Permission.objects.get(content_type=content_type, codename=f"change_{content_type.model}")
        )
        permission, _ = ContentTypePostImportPermission.objects.get_or_create(
            content_type=content_type,
            permission=Permission.objects.get(content_type=content_type, codename=f"delete_{content_type.model}")
        )
        permission.save()
        super().save_model(request, obj, form, change)


class ContentTypePostImportPermissionAdmin(admin.ModelAdmin):
    model = ContentTypePostImportPermission
    list_display = ("id", "content_type", "permission")

class MapperAdmin(admin.ModelAdmin):
    model = Mapper
    list_display = ("name", "created_at", "updated_at", "content_type")

class DataImportAdmin(admin.ModelAdmin):
    model = DataImport
    list_display = ("id", "imported_by", "imported_at", "success")

admin.site.register(ContentTypeRestriction, ContentTypeRestrictionAdmin)
admin.site.register(ContentTypePostImportPermission, ContentTypePostImportPermissionAdmin)
admin.site.register(Mapper, MapperAdmin)
admin.site.register(DataImport, DataImportAdmin)