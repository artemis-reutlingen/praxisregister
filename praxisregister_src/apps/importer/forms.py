from django import forms
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext_lazy as _
from import_export.formats import base_formats
from import_export.forms import ConfirmImportForm, ImportForm
from importer.models import ContentTypeRestriction, Mapper, MapperField
from importer.views.importer_utils import get_choices_from_content_type


class MapperNewForm(forms.ModelForm):
    
    content_type = forms.ModelChoiceField(
        label=_('Content type'),
        queryset=None, # set in __init__
    )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        super().__init__(*args, **kwargs)
        if user.is_superuser:
            allowed_c_t_id = ContentType.objects.all().values_list("id", flat=True)
        else:
            allowed_c_t_id = ContentTypeRestriction.objects.filter(group__in=user.groups.all()).values_list("allowed_content_types__id", flat=True)
        self.fields['content_type'].queryset = ContentType.objects.filter(id__in=allowed_c_t_id) # could use content type model name here

    class Meta:
        model = Mapper
        fields = ["name", "content_type"]

class MapperFieldsForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.mapper = kwargs.pop("mapper")
        content_type = kwargs.pop("content_type")
        super().__init__(*args, **kwargs)
        choices = get_choices_from_content_type(content_type)
        self.fields['internal_fieldname'] = forms.ChoiceField(choices=choices)

    class Meta:
        model = MapperField
        fields = ["import_fieldname", "internal_fieldname"]

    def clean_internal_fieldname(self):
        internal_fieldname = self.cleaned_data['internal_fieldname']
        if MapperField.objects.filter(mapper=self.mapper, internal_fieldname=internal_fieldname).exists():
            msg = _("This internal fieldname is already used in this mapper.")
            self.add_error("internal_fieldname", msg)
        return internal_fieldname

class ImportFileForm(ImportForm):

    def __init__(self, *args, **kwargs):
        import_formats = [f for f in base_formats.DEFAULT_FORMATS if f().can_import()]
        super().__init__(import_formats, *args, **kwargs)

class CustomImportFileForm(ImportFileForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        groups = kwargs.pop("groups")
        super().__init__(*args, **kwargs)
        if settings.IMPORT_WITH_DJANGO_GUARDIAN:
            self.fields['mapper'].queryset = Mapper.objects.get_mappers_for_user(user)   
        self.fields['groups'].queryset = groups

    mapper = forms.ModelChoiceField(
        label=_('Mapper'),
        queryset=Mapper.objects.all(),
        required=True,
    )

    groups = forms.ModelMultipleChoiceField(
        label=_('Groups'),
        queryset=None, # set in __init__
        required=True,
    )

    def clean_mapper(self):
        mapper = self.cleaned_data['mapper']
        if mapper.mapperfield_set.filter(internal_fieldname__isnull=False).count() == 0:
            msg = _("To import data, a mapper must have at least one mapping field.")
            self.add_error("mapper", msg)
        if mapper.mapperfield_set.filter(internal_fieldname="import_id").count() == 0:
                msg = _("To import data, a mapper must have a field with the internal name 'import_id'. If you cant choose this field, a administator first has to add it to the model.")
                self.add_error("mapper", msg)
        return mapper

class CustomConfirmImportForm(ConfirmImportForm):

    def __init__(self, *args, **kwargs):
        groups = kwargs.pop("groups", None)
        super().__init__(*args, **kwargs)
        self.fields['groups'].queryset = groups
    
    mapper_id = forms.IntegerField(widget=forms.HiddenInput())
    groups = forms.ModelMultipleChoiceField(widget=forms.MultipleHiddenInput, queryset=None)
    
class ImportSettingsForm(forms.Form):

    chunk_size = forms.IntegerField(
        label=_('Chunk size'),
        help_text=_('How many objects should be imported in one chunk?'),
        required=True,
        min_value=1,
        max_value=200,
        initial=50,
    )

    save_null_fields = forms.BooleanField(
        label=_('Save empty fields'),
        help_text=_('If checked, the import will attempt to store null values in the database. This can lead to errors if the database field does not allow it.'),
        required=False,
    )

    levenshtein_ignore_case = forms.BooleanField(
        label=_('Ignore case when importing without ID'),
        help_text=_('This is for importing without an ID field. If checked, the import will ignore case when comparing fields.'),
        required=False,
    )