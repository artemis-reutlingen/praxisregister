from django import forms
from feedback.models import Feedback, Workflow, WorkflowStep, FeedbackComment
from utils.fields import ListTextWidget

class FeedbackForm(forms.ModelForm):

    class Meta:
        model = Feedback
        fields = ["topic", "step", "unclarities", "suggestions", "error_reporting", "file"]
        widgets = {
            'file' : forms.FileInput()
        }
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        feedback_type_choices = Workflow.objects.all().values_list('topic', flat=True)
        step_type_choices = WorkflowStep.objects.all().values_list('name', flat=True)
        self.fields['topic'].widget = ListTextWidget(
                data_list=feedback_type_choices,
                name='feedback-type-choices',
            )
        self.fields['step'].widget = ListTextWidget(
                data_list=step_type_choices,
                name='step-type-choices',
            )

class FeedbackCommentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["comment"].required = False

    class Meta:
        model = FeedbackComment
        fields = ["comment"]