# Generated by Django 4.2.7 on 2024-02-13 08:22

from django.db import migrations
import django_ckeditor_5.fields


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0005_remove_feedback_comment_remove_feedback_comment_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedbackcomment',
            name='comment',
            field=django_ckeditor_5.fields.CKEditor5Field(default='', verbose_name='Comment'),
        ),
    ]
