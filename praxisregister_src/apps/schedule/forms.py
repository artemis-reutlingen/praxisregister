from django import forms
from django.utils.translation import gettext_lazy as _

from schedule.models import Event, Occurrence
from schedule.widgets import ColorInput
from events.models.events import EventInfo


class SpanForm(forms.ModelForm):
    start = forms.SplitDateTimeField(label=_("start"))
    end = forms.SplitDateTimeField(
        label=_("end"), help_text=_("The end time must be later than start time.")
    )

    def clean(self):
        if "end" in self.cleaned_data and "start" in self.cleaned_data:
            if self.cleaned_data["end"] <= self.cleaned_data["start"]:
                raise forms.ValidationError(
                    _("The end time must be later than start time.")
                )
        return self.cleaned_data


class EventForm(SpanForm):
    def __init__(self, *args, **kwargs):
        event_infos = kwargs.pop('event_infos', None)
        super().__init__(*args, **kwargs)
        self.fields['event_info'].queryset = event_infos if event_infos else EventInfo.objects.none()
        self.fields['event_info'].required = False

    end_recurring_period = forms.DateTimeField(
        label=_("End recurring period"),
        help_text=_("This date is ignored for one time only events."),
        required=False,
    )

    title = forms.CharField(label=_("Display name"), max_length=255, required=False)
        
    class Meta:
        model = Event
        exclude = ("creator", "created_on", "calendar", "description")

    def clean(self):
        if (self.cleaned_data["title"] == None or self.cleaned_data["title"] == "") and self.cleaned_data["event_info"] == None:
            raise forms.ValidationError(_("Please enter a title or select an event info."))
        return super().clean()

class OccurrenceForm(SpanForm):
    class Meta:
        model = Occurrence
        exclude = ("original_start", "original_end", "event", "cancelled")


class EventAdminForm(forms.ModelForm):
    class Meta:
        exclude = []
        model = Event
        widgets = {"color_event": ColorInput}
