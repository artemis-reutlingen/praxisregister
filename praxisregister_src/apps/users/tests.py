from django.test import TestCase
from django.urls import reverse

from institutes.models import Institute
from users.models import User


class ViewTests(TestCase):
    def setUp(self) -> None:
        self.institute = Institute.objects.create_institute_with_group(
            organisation="Uni.X",
            name="Dep.Y",
            group_name="unix-depy"
        )
        self.user = User.objects.create_user_for_institute(username="tester0", password="x", institute=self.institute)
        self.client.login(username='tester0', password='x')

    def test_edit_user_info_page_gets_rendered_correctly(self):
        response = self.client.get(reverse('users:user-update', kwargs={'pk': self.user.id}))

        self.assertTemplateUsed(
            response,
            template_name='global/forms/generic.html'
        )
