from django import forms
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm, UserCreationForm
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView
from django.views.generic.edit import UpdateView, FormView
from django.dispatch import Signal

from institutes.models import Institute
from users.models import User
from utils.view_mixins import AdminStaffRequiredMixin

users_profil_log=Signal()

class UserUpdateView(SuccessMessageMixin, UpdateView):
    model = User
    fields = [
        'medical_title',
        'first_name',
        'last_name',
        'phone_number',
        'email',
    ]
    template_name = 'global/forms/generic.html'
    success_message = _('Your profile data has been updated.')

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        form.fields["first_name"].required = True
        form.fields["last_name"].required = True
        form.fields["email"].required = True
        return form

    def get_success_url(self) -> str:
        users_profil_log.send(sender=self.__class__, username=self.object.username)
        return reverse_lazy('users:member-page')


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, _('Your password has been changed.'))
            class ChangePassword: pass
            users_profil_log.send(sender=ChangePassword, username=user.get_username())
            return redirect('users:member-page')
        else:
            messages.error(request, _('The following errors occurred.'))
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'global/forms/generic.html', {'form': form})


class MemberPageView(TemplateView):
    template_name = 'users/user_profile_page.html'


class CreateUserFormView(AdminStaffRequiredMixin, SuccessMessageMixin, FormView):
    """
    This view represents a convenience action for usage in the admin panel.
    Creates a new user instance from given username name, password and the selected institute.
    Associates the new user with corresponding group of the given institute.

    """

    class CreateUserForm(UserCreationForm):
        """
        Extends standard Django UserCreationForm.
        """
        institute = forms.ModelChoiceField(
            queryset=Institute.objects.all()
        )

        class Meta:
            model = User
            fields = ('username',)

    template_name = 'users/create_user.html'
    form_class = CreateUserForm
    success_url = '/headquarters/'  # TODO: proper redirect with reverse func
    success_message = "New user instance created successfully"

    def form_valid(self, form):
        # custom user creation code
        username = form.cleaned_data['username']
        password = form.cleaned_data['password1']
        institute = form.cleaned_data['institute']

        User.objects.create_user_for_institute(
            username=username,
            password=password,
            institute=institute
        )

        return super().form_valid(form)
