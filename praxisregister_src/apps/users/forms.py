from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import gettext_lazy as _

from institutes.models import Institute
from users.models import User

class CreateUserForm(UserCreationForm):
        """
        Extends standard Django UserCreationForm.
        """
        institute = forms.ModelChoiceField(
            queryset=Institute.objects.all()
        )

        class Meta:
            model = User
            fields = ["username"]

        def save(self, commit = True):
            instance = super(CreateUserForm, self).save(commit=False)

            return User.objects.create_user_for_institute(
                username=instance.username,
                password=instance.password1,
                institute=instance.institute
            )
        