from django.urls import path

from .views import CreateInstituteFormView

app_name = 'institutes'

urlpatterns = [
    path('create', CreateInstituteFormView.as_view(), name='create'),
]
