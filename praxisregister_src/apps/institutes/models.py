from django.db import models
from django.contrib.auth.models import Group
from guardian.shortcuts import assign_perm
from guardian.models import UserObjectPermissionBase, GroupObjectPermissionBase
from django.utils.translation import gettext_lazy as _

class InstituteManager(models.Manager):
    def create_institute_with_group(self, organisation: str, name: str, group_name: str, email_address : str = None):
        """
        Creating a new institute and a corresponding group.
        Assigns basic permissions for the new group.

        @param organisation: Used for constructing institute model.
        @param name: Used for constructing institute model.
        @param group_name: Used for creating the group for this insitute.
        @return: New institute object.
        """
        institute_group, created = Group.objects.get_or_create(name=group_name)
        institute, created = self.get_or_create(
            organisation=organisation,
            name=name,
            group=institute_group,
            email_address=email_address
        )
        assign_perm('practices.add_practice', institute.group)

        return institute
    

class Institute(models.Model):
    """
    Institute model.
    Each user and each practice has an associated institute.

    An institute has a specific group associated with it. All users corresponding to a certain institute are members
    of that group as well.

    Group association allows collective permissions based on institute membership by utilizing
    django groups feature with django-guardian.
    """
    objects = InstituteManager()
    #Name of the network
    organisation = models.CharField(max_length=256, null=True, blank=False, verbose_name=_("Network name"))
    name = models.CharField(max_length=256, null=False, blank=False, verbose_name=_("Institute name"))
    email_address = models.EmailField(max_length=256, null=True, blank=True)
    group = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True)

    class Meta:
        unique_together = ('organisation', 'name')
        permissions = [
            ("can_clear_data", "Can clear data")
        ]

    @property
    def full_name(self):
        return f"{self.organisation}_{self.name}"

    def __repr__(self):
        return f"{self.organisation}_{self.name}"

    def __str__(self):
        return f"{self.organisation}_{self.name}"

# Explicit definition of Institute object permissions. This ensures permission objects are also deleted when an institute is deleted instead of being orphaned.
class InstituteUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(Institute, on_delete = models.CASCADE)

class InstituteGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(Institute, on_delete = models.CASCADE)

