from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from institutes.models import Institute


class CreateInstituteForm(forms.ModelForm):
        
        class Meta:
            model = Institute
            fields = '__all__'

        def clean(self):
            if not self.instance.pk:
                #Checks for naming conflicts with existing institutes.
                cleaned_data = super().clean()
                organisation = cleaned_data.get("organisation")
                name = cleaned_data.get("name")

                # Prerequisite check if both fields are valid themselves
                if organisation and name:
                    # Check if an institute with identical naming exists
                    if Institute.objects.filter(organisation=organisation, name=name).exists():
                        raise ValidationError(_("Naming conflict. This combination of organization name and department name already exists!"))
        
        
        def save(self, commit=True):
            if self.instance.pk:
                 return super().save(commit)
            else:
                instance = super(CreateInstituteForm, self).save(commit=False)
                
                # Generate a group name from given names in the form
                group_name = f"{instance.organisation}_{instance.name}"

                return Institute.objects.create_institute_with_group(
                    group_name=group_name,
                    organisation=instance.organisation,
                    name=instance.name,
                    email_address=instance.email_address
                )
