from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from guardian.admin import GuardedModelAdmin

from todos.models import ToDo

class ToDoAdmin(GuardedModelAdmin):
    model = ToDo
    list_display = ('id', 'title', 'author', 'practice') 
    list_per_page = 20
    list_select_related = ['author', 'practice']
    list_filter = ['created_at', 'practice__assigned_institute']
    ordering = ('-created_at',)
    search_fields = ['title__startswith', 'practice__name__icontains', 'author__first_name__startswith', 'author__last_name__startswith']

admin.site.register(ToDo, ToDoAdmin)
