from django.db import models
from django.utils.translation import gettext_lazy as _

from todos.types import ToDoStatusType
from track_changes.models import TrackChangesMixin


class ToDo(TrackChangesMixin, models.Model):

    title = models.CharField(verbose_name=_('Title'), max_length=256)
    description = models.TextField(verbose_name=_('Description'), blank=True, default="")
    author = models.ForeignKey(
        'users.User',
        related_name="todos",
        verbose_name=_('Created by'),
        on_delete=models.CASCADE,
        null=True
    )
    created_at = models.DateTimeField(auto_now=True)
    assignees = models.ManyToManyField('users.User', through='ToDoStatus', through_fields=('todo', 'user'))

    practice = models.ForeignKey('practices.Practice', verbose_name=_('Concerns practice'), on_delete=models.CASCADE,
                                 null=True, blank=True)

    def get_desciption_length(self):
        return len(self.description)

    def __str__(self) -> str:
        return f"Todo: {self.title}"
    
    class Meta:
        verbose_name = _("Todo")
        verbose_name_plural = _("Todos")

class ToDoStatus(models.Model):

    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    todo = models.ForeignKey(ToDo, on_delete=models.CASCADE)

    seen_by_user = models.BooleanField(default=False)
    status = models.CharField(
        verbose_name=_("Current status"),
        max_length=128,
        choices=ToDoStatusType.choices,
        default=ToDoStatusType.OPEN,
    )
    done_at = models.DateTimeField(blank=True, null=True)

    