from django.db import models
from django.utils.translation import gettext_lazy as _


class ToDoStatusType(models.TextChoices):
    OPEN = 'OPEN', _('Open')
    DONE = 'DONE', _('Done')