from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView, CreateView, DetailView, View, UpdateView, DeleteView
from django.dispatch import Signal
from django.db.models import Q

from practices.models import Practice
from todos.forms import ToDoForm
from todos.models import ToDo, ToDoStatus
from todos.types import ToDoStatusType
from utils.view_mixins import ManagePracticePermissionRequired

todo_log=Signal()

class ToDoDetail(DetailView):
    model = ToDo

class ToDoList(ListView):
    model = ToDo
    paginate_by = 5

    def get_queryset(self):
        ToDoStatus.objects.filter(user=self.request.user).update(seen_by_user=True)
        return ToDo.objects.filter(todostatus__user=self.request.user).order_by('-created_at')

class PracticeToDoCreate(ManagePracticePermissionRequired, CreateView):
    model = ToDo
    form_class = ToDoForm
    template_name = 'global/forms/generic.html'

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["practice"] = self.get_practice_object()
        return kwargs
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        self.object.practice = Practice.objects.get(pk=self.kwargs['pk'])
        self.object.save()
        form.save_m2m()
        return super().form_valid(form)        
    
    def get_success_url(self):
        todo_log.send(sender=self.__class__, username=self.request.user.username, todo=self.object)
        return reverse_lazy('practices:todo-list', kwargs={'pk': self.object.practice.id})


class ToDoEdit(ManagePracticePermissionRequired, UpdateView):
    model = ToDo
    template_name = 'global/forms/generic.html'
    form_class = ToDoForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["practice"] = ToDo.objects.get(pk=self.kwargs['pk']).practice
        return kwargs

    def get_permission_object(self):
        return self.get_object().practice

    def get_success_url(self):
        todo_log.send(sender=self.__class__, username=self.request.user.username, todo=self.object)
        return reverse_lazy('practices:todo-list', kwargs={'pk': self.object.practice.id})


class ToDoMarkAsDone(ManagePracticePermissionRequired, SuccessMessageMixin, View):
    def post(self, request, *args, **kwargs):
        todoStatus_object = ToDoStatus.objects.get(id=kwargs.get('pk'))
        if todoStatus_object.user == request.user:
            if todoStatus_object.status == ToDoStatusType.OPEN:
                todoStatus_object.status = ToDoStatusType.DONE    
                todoStatus_object.done_at = timezone.now()
                todoStatus_object.save()
                messages.success(request, _('ToDo marked as done.'))
            else:
                todoStatus_object.status = ToDoStatusType.OPEN    
                todoStatus_object.done_at = None
                todoStatus_object.save()
                messages.success(request, _('ToDo reopened'))  
            todo_log.send(sender=self.__class__, username=self.request.user.username, todo=todoStatus_object)
        else:
            messages.error(request, _("Only assignee can mark ToDos as done."))
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))
    
    def get_permission_object(self):
        return ToDoStatus.objects.get(id=self.kwargs.get('pk')).todo.practice

class PracticeToDoList(ManagePracticePermissionRequired, ListView):
    model = ToDo
    template_name = 'practices/todos/practice_todo_list.html'
    paginate_by = 5

    def get_permission_object(self):
        return self.get_practice_object()

    def get_practice_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['practice'] = self.get_practice_object()
        return context

    def get_queryset(self):
        return super().get_queryset().filter(Q(practice=self.get_practice_object()) & Q(author=self.request.user)).order_by('-created_at')

class ToDoDelete(ManagePracticePermissionRequired, DeleteView):
    model = ToDo
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return self.get_object().practice

    def get_success_url(self):
        return reverse_lazy('practices:todo-list', kwargs={'pk': self.object.practice.id})
