from typing import Any, Dict

from addresses.models import County
from contacttimes.view_mixins import ContactTimesEditMixin
from django import forms
from django.contrib import messages
from django.dispatch import Signal
from django.forms.models import BaseModelForm
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, UpdateView
from practices.forms.practice_base_data_forms import PracticeInfoForm
from practices.models import Practice
from practices.views.utils import get_save_or_save_and_continue_url
from utils.view_mixins import (ManagePracticePermissionRequired,
                               PracticeUpdateView)

practice_base_data_log=Signal()

class PracticeDetail(ManagePracticePermissionRequired, DetailView):
    model = Practice
    template_name = 'practices/base_data/detail.html'
    context_object_name = 'practice'


class UpdatePracticeBaseData(ManagePracticePermissionRequired, UpdateView):
    model = Practice
    template_name = 'practices/base_data/forms/info.html'
    form_class = PracticeInfoForm

    def get_context_data(self, **kwargs: Any):
        context = super().get_context_data(**kwargs)
        context['practice_id'] = self.kwargs['pk']
        return context
    
    def get_form_kwargs(self) -> Dict[str, Any]:
        kwargs = super().get_form_kwargs()
        kwargs['county'] = self.object.county
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        if form.is_valid():
            county_name = form.cleaned_data['county_name']
            if county_name != None:
                county, created = County.objects.get_or_create(full_name=county_name.strip())
                form.instance.county = county
            if form.cleaned_data['import_id'] != None:
                form.instance.import_by = self.request.user.assigned_institute.full_name
        return super().form_valid(form)
    
    def get_success_url(self) -> str:
        class EditPracticeBaseData: pass
        practice_base_data_log.send(sender=EditPracticeBaseData, username=self.request.user.username, id=self.kwargs['pk'])
        messages.success(self.request, _("Practice information successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:detail', 'practices:create-site')
        return reverse_lazy(redirect_url, kwargs={'pk': self.kwargs['pk']})


class PracticeOpeningHoursEdit(ManagePracticePermissionRequired, ContactTimesEditMixin, UpdateView):
    """
    UpdateView using formset for editing all contact time slot objects for a practice at the same time.
    """
    template_name = 'contacttimes/forms/contact_times.html'

    model = Practice
    form_class = forms.modelform_factory(model=Practice, fields=[])

    def get_permission_object(self):
        return self.get_object()

    def get_timeslots(self):
        return self.get_object().contact_times_ordered

    def get_success_url(self):
        practice_id = self.get_object().id
        practice_base_data_log.send(sender=self.__class__, username=self.request.user.username, id=self.object.id)
        messages.success(self.request, _("Opening hours successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:detail', 'practices:structure-general-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': practice_id})


class PracticeContactEdit(PracticeUpdateView):
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice_id'] = context['practice'].pk
        return context
    
    def get_success_url(self):
        practice_base_data_log.send(sender=self.__class__, username=self.request.user.username, id=self.object.id)
        messages.success(self.request, _("Contact information successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:detail', 'practices:edit-opening-hours')
        return reverse_lazy(redirect_url, kwargs={'pk': self.kwargs['pk']})

    class ContactForm(forms.ModelForm):
        class Meta:
            model = Practice
            fields = [
                'contact_phone',
                'contact_phone_alt',
                'contact_fax',
                'contact_email',
                'contact_email_alt',
                'contact_website',
                'contact_preferred_medium',
            ]

    form_class = ContactForm
