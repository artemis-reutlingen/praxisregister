from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import DetailView

from utils.view_mixins import ManagePracticePermissionRequired
from ..models import Practice


class TandemDoctorDetail(ManagePracticePermissionRequired, DetailView):
    """
    Providing access to tandem doctor of specific practice. Redirecting to person model, otherwise rendering
    tandem_role_not_set template.
    """
    model = Practice
    template_name = 'persons/tandem_role_not_set.html'
    context_object_name = 'practice'

    def get(self, request, *args, **kwargs):
        if self.get_object().research_doctor:
            return redirect(reverse('persons:detail', kwargs={'pk': self.get_object().research_doctor.id}))
        else:
            return super(TandemDoctorDetail, self).get(request, *args, **kwargs)


class TandemAssistantDetail(ManagePracticePermissionRequired, DetailView):
    """
    Providing access to tandem assistant of specific practice. Redirecting to person model, otherwise rendering
    tandem_role_not_set template.
    """
    model = Practice
    template_name = 'persons/tandem_role_not_set.html'
    context_object_name = 'practice'

    def get(self, request, *args, **kwargs):
        if self.get_object().research_assistant:
            return redirect(reverse('persons:detail', kwargs={'pk': self.get_object().research_assistant.id}))
        else:
            return super(TandemAssistantDetail, self).get(request, *args, **kwargs)
