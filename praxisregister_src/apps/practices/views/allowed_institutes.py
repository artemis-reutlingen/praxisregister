from django.urls import reverse_lazy
from django.views.generic import DetailView, UpdateView
from django.dispatch import Signal
from django.forms.models import ModelChoiceIterator
from institutes.models import Institute
from practices.forms import forms
from practices.models import Practice
from utils.view_mixins import ManagePracticePermissionRequired, AllowOtherInstitutesPermissionRequired

allowed_institutes_removed_log = Signal()
allowed_institutes_added_log = Signal()


class AllowedInstitutes(ManagePracticePermissionRequired, DetailView):
    model = Practice
    template_name = 'practices/allowed_institutes/detail.html'
    context_object_name = 'practice'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(AllowedInstitutes, self).get_context_data()
        if self.request.user.has_perm("practices.allow_other_institutes", self.get_object()):
            context['actions'] = ['EDIT_ALLOWED_INSTITUTES']

        return context


class AllowedInstitutesEdit(AllowOtherInstitutesPermissionRequired, UpdateView):
    model = Practice
    form_class = forms.AllowedInstitutesForm
    template_name = 'practices/allowed_institutes/forms/edit.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['institutes'] = Institute.objects.exclude(id=self.get_object().assigned_institute.id)
        return kwargs

    def form_valid(self, form):
        before_save = list(self.get_object().allowed_institutes.all())
        form = super().form_valid(form)
        after_save = list(self.get_object().allowed_institutes.all())
        removed_institutes=set(before_save).difference(after_save)
        added_institutes=set(after_save).difference(before_save)
        if(len(removed_institutes) > 0):
            allowed_institutes_removed_log.send(sender=self.__class__, username=self.request.user.username, removed_institutes=removed_institutes)
        if(len(added_institutes) > 0):
            allowed_institutes_added_log.send(sender=self.__class__, username=self.request.user.username, added_institutes=added_institutes)
        return form

    def get_success_url(self):
        practice = self.get_object()
        practice.update_practices_access()
        return reverse_lazy('practices:allowed-institutes', kwargs={'pk': practice.id})
