from django.contrib.auth.mixins import PermissionRequiredMixin as NonObjectPermissionRequiredMixin
from django.urls import reverse_lazy, reverse
from django.shortcuts import render
from django.views.generic import CreateView
from django.views.generic import TemplateView
from django_tables2 import RequestConfig
from django.db.models import Q
from django.dispatch import Signal
from rest_framework import viewsets

from practices.filters import PracticeFilter, PracticeFilterFormHelper
from practices.models import Practice
from practices.serializers import PracticeSerializer
from practices.tables import PracticeTable
from processes.models import ProcessDefinition

practice_create_log = Signal()

class PracticeViewSet(viewsets.ReadOnlyModelViewSet):
    """
    REST viewset for practices model.
    Used in combination with datatables library in frontend for fetching practice data AJAX style.
    """
    serializer_class = PracticeSerializer

    def get_queryset(self):
        return Practice.objects.get_practice_objects_for_user(self.request.user)


class Practices(TemplateView):
    """
    Practices list/search. Listing practice objects by user's permissions capsuled in manager method:
    get_practice_objects_for_user.

    Uses django-tables2 with crispy-forms / FormHelper.
    """
    template_name = 'practices/practices_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        practice_filter = PracticeFilter(
            self.request,
            queryset=Practice.objects.get_practice_objects_for_user(user=self.request.user)
        )
        #Prefill process step selection options based on process selection choice
        if self.request.method == "GET":
            process_definition_pk = self.request.GET.get('process__process_definition', '')
            if process_definition_pk != '' \
            and ProcessDefinition.objects.get_process_definitions_for_user(self.request.user).filter(Q(pk=process_definition_pk) & Q(is_draft=False)) != None:
                process_definition = self.request.GET.get('process__process_definition', '')
                step_definitions = [(choice.pk, choice.name) for choice in ProcessDefinition.objects.filter(pk=process_definition).get().step_definitions]
                practice_filter.form.fields['process__step__step_definition__name'].widget.choices.extend(step_definitions)
                practice_filter.form.fields['processStep_not_fullfilled'].widget.choices.extend(step_definitions)
        #Fill in process options based on current user
        practice_filter.form.fields['process__process_definition'].widget.choices.extend([(choice.pk, choice.name) for choice in ProcessDefinition.objects.get_process_definitions_for_user(self.request.user).filter(is_draft=False)])
        practice_filter.form.fields['process__process_definition'].widget.attrs = {
                      'hx-get': reverse('practices:process-step-selection') , 'hx-trigger': 'change', 'hx-target': '#process_filters'}

        table = PracticeTable(practice_filter.qs)
        RequestConfig(self.request).configure(table)
        context['filter'] = practice_filter
        context['table'] = table

        return context


class PracticeCreate(NonObjectPermissionRequiredMixin, CreateView):
    """
    Create a new practice and assign perms and set the corresponding institute.
    """
    model = Practice
    template_name = 'global/forms/generic.html'
    permission_required = 'practices.add_practice'
    return_403 = True
    fields = ['name', ]

    def form_valid(self, form):
        form.instance.assigned_institute = self.request.user.assigned_institute
        return super().form_valid(form)

    def get_success_url(self):
        # TODO: Refactor into helper create function. This probably means not using a CreateView.
        practice_create_log.send(sender=self.__class__, username=self.request.user.username, id=self.object.id)
        self.object.assign_perms(self.request.user.assigned_institute)
        return reverse_lazy('practices:detail', kwargs={'pk': self.object.id})

def practice_table(request):
    practice_filter = PracticeFilter(
            request,
            queryset=Practice.objects.get_practice_objects_for_user(user=request.user)
        )
    table = PracticeTable(practice_filter.qs)
    RequestConfig(request).configure(table)
    context = {'table' : table}
    return render(request, "global/basic_table.html", context)

