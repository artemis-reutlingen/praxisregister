from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import UpdateView
from django.contrib import messages
from django.dispatch import Signal

from practices.forms.practice_it_infrastructure import PDMSForm, ITOtherForm
from practices.models import Practice, PDMSSoftwareOption
from practices.views.utils import get_save_or_save_and_continue_url
from utils.view_mixins import ManagePracticePermissionRequired

practice_it_infrastructure_log=Signal()

class ITInfrastructureUpdateView(ManagePracticePermissionRequired, UpdateView):
    model = Practice
    template_name = 'practices/it_infrastructure/it_infrastructure_generic.html'

class PracticeITDetail(SuccessMessageMixin, ITInfrastructureUpdateView):
    template_name = 'practices/it_infrastructure/it_infrastructure_detail.html'
    context_object_name = 'practice'
    fields = ['it_notepad', ]

    def get_success_url(self):
        practice_id = self.get_object().id
        messages.success(self.request, _('IT Notepad was updated.'))
        return reverse_lazy('practices:it-detail', kwargs={'pk': practice_id})


class PracticeITSystemHouseEdit(ITInfrastructureUpdateView):

    fields = [
        'it_system_house_name',
        'it_system_house_address_street',
        'it_system_house_address_house_number',
        'it_system_house_address_zip_code',
        'it_system_house_address_city',
        'it_system_house_contact_person_name',
        'it_system_house_contact_email',
        'it_system_house_contact_phone'
    ]
 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice_id'] = self.kwargs['pk']
        return context

    def get_success_url(self):
        practice_id = self.get_object().id
        practice_it_infrastructure_log.send(sender=self.__class__, username=self.request.user.username, id=practice_id)
        messages.success(self.request, _("System house data successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:it-detail', 'practices:it-other-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': practice_id})

class PracticeITPDMSEdit(ITInfrastructureUpdateView):
    form_class = PDMSForm

    def get_form_kwargs(self):
        pdms_choices = [option.name for option in PDMSSoftwareOption.objects.all().order_by('name')]
        kwargs = super().get_form_kwargs()
        kwargs['data_list'] = pdms_choices
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice_id'] = self.kwargs['pk']
        return context

    def get_success_url(self):
        practice_id = self.get_object().id
        practice_it_infrastructure_log.send(sender=self.__class__, username=self.request.user.username, id=practice_id)
        messages.success(self.request, _("Patient Data Management System successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:it-detail', 'practices:it-system-house-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': practice_id})

class PracticeITOtherEdit(ITInfrastructureUpdateView):
    form_class = ITOtherForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice_id'] = self.kwargs['pk']
        return context

    def get_success_url(self):
        practice_id = self.get_object().id
        practice_it_infrastructure_log.send(sender=self.__class__, username=self.request.user.username, id=practice_id)
        messages.success(self.request, _("Other informations successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:it-detail', 'practices:communication-spoken-language-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': practice_id})


class PracticeITNotepadEdit(ITInfrastructureUpdateView):
    fields = [
        'it_notepad'
    ]
