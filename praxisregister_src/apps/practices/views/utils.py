def get_save_or_save_and_continue_url(request, save_url, save_and_continue_url):
    redirect_url = save_url
    if 'save_and_continue' in request.POST:
        redirect_url = save_and_continue_url
    return redirect_url