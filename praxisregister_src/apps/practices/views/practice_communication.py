from typing import Any, Dict

from django.contrib import messages
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import DeleteView, DetailView, UpdateView
from practices.forms.practice_communication_forms import (
    ColleaguesCommunicationForm, LanguageCommunicationForm,
    PatientsCommunicationForm)
from practices.models.practice import Practice
from practices.models.practice_communication import (
    CommunicationDigitalWithColleagues, CommunicationDigitalWithPatients,
    CommunicationInterpreter, CommunicationSpokenLanguages)
from practices.views.utils import get_save_or_save_and_continue_url
from utils.view_mixins import ManagePracticePermissionRequired


class PracticeCommunicationView(ManagePracticePermissionRequired, DetailView):
    model = Practice
    template_name = 'practices/communication/communication_detail.html'

class PracticeCommunicationEdit(ManagePracticePermissionRequired, UpdateView):
    model = Practice
    template_name = 'practices/communication/forms/communication_generic.html'
    form_class = LanguageCommunicationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice_id'] = self.kwargs['pk']
        return context
    
    def form_valid(self, form):
        if form.is_valid():
            if form.cleaned_data['spoken_language']:
                CommunicationSpokenLanguages.objects.create(com_spoken_language=form.cleaned_data['spoken_language'], practice=form.instance)
            if form.cleaned_data['interpreter_language']:
                CommunicationInterpreter.objects.create(com_interpreter_language=form.cleaned_data['interpreter_language'], practice=form.instance)
        return super().form_valid(form)

    def get_success_url(self) -> str:
        messages.success(self.request, _("Communication data successfully saved!"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:communication-detail', 'practices:communication-digital-colleagues-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': self.kwargs['pk']})
    
class CommunicationSpokenLanguageDelete(ManagePracticePermissionRequired, DeleteView):
    model = CommunicationSpokenLanguages
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().practice.id)

    def get_success_url(self):
        return reverse_lazy('practices:communication-detail', kwargs={'pk': self.get_object().practice.id})
    
class CommunicationSpokenLanguagePercentage(ManagePracticePermissionRequired, UpdateView):
    model = CommunicationSpokenLanguages
    template_name = 'practices/communication/forms/communication_generic.html'
    fields = ['com_spoken_language_percentage']

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().practice.id)

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['practice_id'] = self.get_object().practice.id
        return context

    def get_success_url(self):
        return reverse_lazy('practices:communication-detail', kwargs={'pk': self.get_object().practice.id})
    
class CommunicationInterpreterDelete(ManagePracticePermissionRequired, DeleteView):
    model = CommunicationInterpreter
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().practice.id)

    def get_success_url(self):
        return reverse_lazy('practices:communication-detail', kwargs={'pk': self.get_object().practice.id})
    
class CommunicationInterpreterAmount(ManagePracticePermissionRequired, UpdateView):
    model = CommunicationInterpreter
    template_name = 'practices/communication/forms/communication_generic.html'
    fields = ['com_number_of_patients']

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().practice.id)

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['practice_id'] = self.get_object().practice.id
        return context

    def get_success_url(self):
        return reverse_lazy('practices:communication-detail', kwargs={'pk': self.get_object().practice.id})
    
class PracticeDigitalCommunicationWithColleaguesEdit(ManagePracticePermissionRequired, UpdateView):
    model = Practice
    template_name = 'practices/communication/forms/communication_generic.html'
    form_class = ColleaguesCommunicationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice_id'] = self.kwargs['pk']
        return context
    
    def form_valid(self, form):
        if form.is_valid():
            if form.cleaned_data['digital_communication']:
                CommunicationDigitalWithColleagues.objects.create(com_type_of_communication=form.cleaned_data['digital_communication'], practice=form.instance)
        return super().form_valid(form)

    def get_success_url(self) -> str:
        messages.success(self.request, _("Communication data successfully saved!"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:communication-detail', 'practices:communication-digital-patients-edit')
        return reverse_lazy(redirect_url, kwargs={'pk': self.kwargs['pk']})
    
class CommunicationWithColleaguesDelete(ManagePracticePermissionRequired, DeleteView):
    model = CommunicationDigitalWithColleagues
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().practice.id)

    def get_success_url(self):
        return reverse_lazy('practices:communication-detail', kwargs={'pk': self.get_object().practice.id})
    
class PracticeDigitalCommunicationWithPatientsEdit(ManagePracticePermissionRequired, UpdateView):
    model = Practice
    template_name = 'practices/communication/forms/communication_generic.html'
    form_class = PatientsCommunicationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['practice_id'] = self.kwargs['pk']
        return context
    
    def form_valid(self, form):
        if form.is_valid():
            if form.cleaned_data['digital_communication']:
                CommunicationDigitalWithPatients.objects.create(com_type_of_communication=form.cleaned_data['digital_communication'], practice=form.instance)
        return super().form_valid(form)

    def get_success_url(self) -> str:
        messages.success(self.request, _("Communication data successfully saved!"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:communication-detail', 'practices:communication-detail')
        return reverse_lazy(redirect_url, kwargs={'pk': self.kwargs['pk']})
    
class CommunicationWithPatientsDelete(ManagePracticePermissionRequired, DeleteView):
    model = CommunicationDigitalWithPatients
    template_name = 'global/forms/generic_confirm_delete.html'

    def get_permission_object(self):
        return Practice.objects.get(pk=self.get_object().practice.id)

    def get_success_url(self):
        return reverse_lazy('practices:communication-detail', kwargs={'pk': self.get_object().practice.id})