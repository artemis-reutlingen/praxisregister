from typing import Any, Dict

from django.contrib import messages
from django.dispatch import Signal
from django.forms import BaseModelForm
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DeleteView, UpdateView
from guardian.mixins import PermissionRequiredMixin
from practices.forms.forms import SiteForm
from practices.models import Practice, Site
from practices.views.utils import get_save_or_save_and_continue_url
from addresses.models import County

practice_additional_site_log=Signal()

class SiteMixin(PermissionRequiredMixin):
    model = Site
    form_class = SiteForm
    template_name = 'practices/base_data/forms/site.html'
    permission_required = "manage_practice"
    return_403 = True

    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        if form.is_valid():
            county_name = form.cleaned_data['county_name']
            if county_name != None:
                county, created = County.objects.get_or_create(full_name=county_name.strip())
                form.instance.county = county
        return super().form_valid(form)
    
class AdditionalSiteCreateView(SiteMixin, CreateView):

    def get_permission_object(self):
        return Practice.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        practice = Practice.objects.get(pk=self.kwargs['pk'])
        context['practice_id'] = practice.id
        context['practice'] = practice
        return context

    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        if form.is_valid():
            form.instance.practice = get_object_or_404(Practice, id=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self) -> str:
        practice_additional_site_log.send(AdditionalSiteCreateView, username=self.request.user.username, site_id=self.object.id, practice_id=self.object.practice.id)
        messages.success(self.request, _("Additional site successfully saved"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:detail', 'practices:edit-contact')
        return reverse_lazy(redirect_url, kwargs={'pk': self.object.practice.id})

class AdditionalSiteEdit(SiteMixin, UpdateView):

    def get_permission_object(self):
        return self.get_object().practice

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['practice_id'] = self.get_object().practice.id
        context['practice'] = self.get_object().practice
        context['edit'] = True
        return context
   
    def get_form_kwargs(self) -> Dict[str, Any]:
        kwargs = super().get_form_kwargs()
        kwargs['county'] = self.object.county
        return kwargs
    
    def get_success_url(self) -> str:
        practice_additional_site_log.send(AdditionalSiteEdit, username=self.request.user.username, site_id=self.object.id, practice_id=self.object.practice.id)
        messages.success(self.request, _("Additional site successfully updated"))
        redirect_url = get_save_or_save_and_continue_url(self.request, 'practices:detail', 'practices:edit-contact')
        return reverse_lazy(redirect_url, kwargs={'pk': self.object.practice.pk})

class AdditionalSiteDelete(PermissionRequiredMixin, DeleteView):
    """
    Delete additional site for practice.
    """
    model = Site
    template_name = 'global/forms/generic_confirm_delete.html'
    permission_required = "manage_practice"
    return_403 = True

    def get_permission_object(self):
        return self.get_object().practice

    def get_success_url(self):
        practice_id = self.get_object().practice.id
        practice_additional_site_log.send(sender=self.__class__, username=self.request.user.username, site_id=self.object.id, practice_id=practice_id)
        return reverse_lazy('practices:detail', kwargs={'pk': practice_id})
