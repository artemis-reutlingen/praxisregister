from contacttimes.models import DayContactTimeSlot
from practices.models import Practice, DiseaseManagementProgramEntry, DiseaseManagementProgramOption
from django.db.models.signals import post_save
from django.dispatch import receiver

@receiver(post_save, sender=Practice)
def create_dmp_entry_for_all_dmp_options_for_new_practice(sender, instance, created, **kwargs):
    if created:
        entries = [
            DiseaseManagementProgramEntry(practice=instance, type=dmp_option) for dmp_option
            in DiseaseManagementProgramOption.objects.all()
        ]
        DiseaseManagementProgramEntry.objects.bulk_create(entries)


@receiver(post_save, sender=DiseaseManagementProgramOption)
def create_dmp_entry_for_new_dmp_option_for_every_existing_practice(sender, instance, created, **kwargs):
    if created:
        entries = [
            DiseaseManagementProgramEntry(practice=practice, type=instance) for practice
            in Practice.objects.all()
        ]
        DiseaseManagementProgramEntry.objects.bulk_create(entries)


@receiver(post_save, sender=Practice)
def create_standard_contact_timeslots_for_new_practice(sender, instance, created, **kwargs):
    if created:
        days = [
            DayContactTimeSlot.Weekday.MONDAY,
            DayContactTimeSlot.Weekday.TUESDAY,
            DayContactTimeSlot.Weekday.WEDNESDAY,
            DayContactTimeSlot.Weekday.THURSDAY,
            DayContactTimeSlot.Weekday.FRIDAY,
            DayContactTimeSlot.Weekday.SATURDAY,
            DayContactTimeSlot.Weekday.SUNDAY,
        ]
        contact_times = [DayContactTimeSlot(weekday = day, content_object = instance) for day in days]
        DayContactTimeSlot.objects.bulk_create(contact_times)