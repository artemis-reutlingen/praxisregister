from addresses.models import County, FederalState
from django import forms
from django.core.exceptions import ValidationError
from django.forms import CheckboxInput
from django.utils.translation import gettext_lazy as _
from practices.models import Practice, Site
from utils.fields import ListTextWidget

class PracticeInfoForm(forms.ModelForm):
    is_active = forms.BooleanField(required=False, label=_('Is active'), widget=CheckboxInput(attrs={'onclick' : 'is_active_cb_clicked(this);'}))
    inactive_note = forms.CharField(required=False, label=_('Reason for inactivity'))
    county_name = forms.CharField(required=False, label=_('County'), widget=ListTextWidget(data_list=County.objects.values_list("full_name", flat=True).distinct(), name='county-choices'))
    federal_state = forms.ModelChoiceField(label=_('Federal state'), queryset=FederalState.objects.all())

    def __init__(self, *args, **kwargs):
        county = kwargs.pop('county', None)
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        if county:
            self.fields['county_name'].initial = county.full_name
        self.fields['inactive_note'].disabled = self.instance.is_active
        #datetime field widget
        self.fields['questionnaire_date'].widget = forms.DateInput(
            format='%Y-%m-%d',
            attrs={
                'class': 'form-control',
                'placeholder': 'Select a date',
                'type': 'date'
            }
        )
    class Meta:
        model = Practice
        fields = [
            'questionnaire_date',
            'import_id',
            'identification_number',
            'name',
            'street',
            'house_number',
            'zip_code',
            'city',
            'county_name',
            'federal_state',
            'type',
            'number_of_sites',
            'tags',
            'is_active',
            'inactive_note',
        ]

        labels = {
            "import_id": _("Practice ID given by the institute")
        }

    def clean_import_id(self):
        if self.cleaned_data['import_id'] and self.cleaned_data['import_id'].strip() != "":
            id_exists = Practice.objects.get_practice_objects_for_group(self.user.assigned_institute.group).filter(import_id=self.cleaned_data['import_id']).exclude(id=self.instance.id).exists()
            if id_exists:
                raise ValidationError(_("This ID is already in use. Please choose another one."))
        return self.cleaned_data['import_id']
    
    def clean_identification_number(self):
        """
        Check for system-wide (all institutes) conflicts for practice identification number
        and display error message to user.
        """
        entered_identification_number = self.cleaned_data['identification_number']
        
        if entered_identification_number and entered_identification_number.strip() == "":
            return ""

        try:
            conflicting_site = Site.objects.get(
                identification_number=entered_identification_number
            )
            if conflicting_site:
                if conflicting_site.practice != self.instance:
                    error_message = _(
                        "Identification number conflict: another practice is associated with this number."
                    )
                    conflicting_practice_name = conflicting_site.practice.name
                    conflicting_practice_zip_code = conflicting_site.zip_code
                    conflicting_practice_city = conflicting_site.city
                    conflicting_practices_institute = conflicting_site.practice.assigned_institute

                    raise ValidationError(
                        f"{error_message} "
                        f"({conflicting_practice_name}, {conflicting_practice_zip_code} {conflicting_practice_city} - "
                        f"{conflicting_practices_institute})"
                    )
        except Site.DoesNotExist:
            # No Site object conflict possible if there is no Site for this identification_number in the db
            pass

        return entered_identification_number
