from django import forms
from django.utils.translation import gettext_lazy as _
from practices.models.practice_structure import PracticeTreatmentByAgeGroupPerQuarter
from utils.constants import DATALIST_INPUT_HELP_TEXT
from utils.fields import ListTextWidget
from persons.models import AssistantAdditionalQualificationOption
from practices.models.additional_qualification import PracticeAssistantsAdditionalQualification
from practices.models import Practice, PracticeEngagement
from practices.types import ServicesPracticeOffersType, UrbanisationLevel, TreatmentsByAgeGroupType
from practices.models.engagement import ServicesPracticeOffers
from utils.constants import MULTIPLE_ENTRIES_HELP_TEXT

class PracticeAdditionalFunctionsForm(forms.ModelForm):
        
        practice_role = forms.CharField(required=False, help_text= MULTIPLE_ENTRIES_HELP_TEXT)

        def __init__(self, *args, **kwargs):
            self.practice_id = kwargs.pop('practice_id', None)
            _type_choices = kwargs.pop('functions_list', None)
            super().__init__(*args, **kwargs)
            self.fields['practice_role'].widget = ListTextWidget(
                data_list=_type_choices,
                name='engagement-type-choices',
            )
            self.fields['practice_role'].label = _('Function/role type')

        class Meta:
            model = PracticeEngagement
            fields = ['practice_role']

        def clean_practice_role(self):
            if self.cleaned_data.get('practice_role') and PracticeEngagement.objects.filter(practice__id=self.practice_id, type=self.cleaned_data.get('practice_role').strip()).exists():
                msg = _("This entry already exists!")
                self.add_error("practice_role", msg)
            return self.cleaned_data.get('practice_role')


class PracticeStructureGeneralForm(forms.ModelForm):
    structure_practice_type = forms.CharField(required=False, help_text=DATALIST_INPUT_HELP_TEXT)
    structure_practice_operating_until = forms.DateField(
        label=_('Operation is guaranteed until'),
        required=False,
        widget=forms.DateInput(
            format='%Y-%m-%d',
            attrs={
                'class': 'form-control',
                'placeholder': _('Select a date'),
                'type': 'date'
            }
        )
    )

    def __init__(self, *args, **kwargs):
        _type_choices = kwargs.pop('data_list', None)

        super().__init__(*args, **kwargs)
        self.fields['structure_practice_type'].widget = ListTextWidget(
            data_list=_type_choices,
            name='practice-type-choices',
        )
        self.fields['structure_practice_type'].label = _('Practice type')
        self.fields['structure_urbanisation_level'].widget = ListTextWidget(
            data_list=UrbanisationLevel.labels,
            name='urbanisation-level-choices',
        )

    class Meta:
        model = Practice
        fields = [
            'structure_practice_type',
            'structure_urbanisation_level',
            'structure_practice_operating_until',
            'structure_full_care_mandate',
            'structure_bills_per_quarter',
            'structure_at_least_35_hours_openend',
        ]    

class PracticeStructureStaffEditForm(forms.ModelForm):
        
        class Meta:
            model = Practice
            fields = [
                'structure_number_of_employed_medical_assistants',
                'structure_number_of_medical_assistants_in_training',
                'structure_number_of_employed_administrative_assistants',
                'structure_full_time_equivalent_for_assistants',
            ]

class PracticeAssistantsAdditionalQualificationForm(forms.ModelForm):
    type = forms.CharField(required=False, help_text=MULTIPLE_ENTRIES_HELP_TEXT)

    def __init__(self, *args, **kwargs):
        self.practice_id = kwargs.pop('practice_id', None)
        super().__init__(*args, **kwargs)
        self.fields['type'].widget = ListTextWidget(
            data_list=[option.name for option in AssistantAdditionalQualificationOption.objects.all().order_by('name')],
            name='qualification-type-choices',
        )
        self.fields['type'].label = _('Qualification type')

    class Meta:
        model = PracticeAssistantsAdditionalQualification
        fields = ['type']

class PracticeStructureServicesForm(forms.ModelForm):
    service = forms.CharField(required=False, help_text=MULTIPLE_ENTRIES_HELP_TEXT)

    def __init__(self, *args, **kwargs):
        self.practice_id = kwargs.pop('practice_id', None)
        super().__init__(*args, **kwargs)
        self.fields['service'].widget = ListTextWidget(
            data_list=ServicesPracticeOffersType.labels,
            name='service-choices',
        )
        self.fields['service'].label = _('Offers service')

    
    def clean_service(self):
        cl_service = self.cleaned_data.get('service')
        if cl_service and ServicesPracticeOffers.objects.filter(practice__id=self.instance.id, service_name=cl_service.strip()).exists():
            msg = _("This entry already exists!")
            self.add_error("service", msg)
        return cl_service

    class Meta:
        model = Practice
        fields = [
            'structure_gp_range_of_services',
            'services_home_visits',
            'services_nursing_home_visits',
            'service',
        ]

class PracticeTreatmentByAgeGroupPerQuarterForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        practice = kwargs.pop('practice', None)
        super().__init__(*args, **kwargs)
        #Remove existing choices from TreatmentsByAgeGroupType
        existing_choices = PracticeTreatmentByAgeGroupPerQuarter.objects.filter(practice=practice).values_list('age_group', flat=True)
        choices = []
        for choice in TreatmentsByAgeGroupType.choices:
            if choice[0] not in existing_choices:
                choices.append(choice)
        self.fields['age_group'].widget.choices = choices



    class Meta:
        model = PracticeTreatmentByAgeGroupPerQuarter
        fields = [
            'age_group',
            'amount_of_threatments',
        ]