from django.utils.translation import gettext_lazy as _
from django import forms

from utils.constants import DATALIST_INPUT_HELP_TEXT
from utils.fields import ListTextWidget
from practices.models import Practice, OperatingSystemOption, PDMSSoftwareOption

class PDMSForm(forms.ModelForm):
    it_pdms_software = forms.CharField(
        required=True,
        help_text=DATALIST_INPUT_HELP_TEXT
    )

    def __init__(self, *args, **kwargs):
        _it_pdms_software_choices = kwargs.pop('data_list', None)
        super().__init__(*args, **kwargs)
        self.fields['it_pdms_software'].widget = ListTextWidget(
            data_list=_it_pdms_software_choices,
            name='pdms-software-choices',
        )
        self.fields['it_pdms_software'].label = _('PDMS software')

    class Meta:
        model = Practice
        fields = [
            'it_pdms_software',
            'it_epa_used_for_medical_documentation',
            'it_pdms_connected_to_medical_devices',
            'it_checked_for_interoperability_interfaces',
        ]

class ITOtherForm(forms.ModelForm):
    it_operating_system = forms.ModelMultipleChoiceField(
        label=_('Operating System'),
        queryset=OperatingSystemOption.objects.all(),
        widget=forms.CheckboxSelectMultiple(),
        required=False,
    )

    class Meta:
        model = Practice
        fields = [
            'it_how_often_is_fax_used_for_communication',
            'it_is_email_checked_frequently',
            'it_automatic_backups_established',
            'it_fopranet_ready_workstation_available',
            'it_openness_data_export',
            'it_practice_networks',
            'it_security_permissions',
            'it_remote_maintenance',
            'it_operating_system',
            'it_internet_speed',
        ]
