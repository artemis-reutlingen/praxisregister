from django import forms
from practices.models.practice import Practice
from practices.types import CommunicationSpokenLanguageType, CommunicationDigitalWithColleaguesType, CommunicationTypeWithPatientsType
from utils.constants import MULTIPLE_ENTRIES_HELP_TEXT
from utils.fields import ListTextWidget
from practices.models.practice_communication import CommunicationSpokenLanguages, CommunicationInterpreter, CommunicationDigitalWithColleagues, CommunicationDigitalWithPatients
from django.db.models import Q
from django.utils.translation import gettext_lazy as _

class LanguageCommunicationForm(forms.ModelForm):

    spoken_language = forms.CharField(help_text=MULTIPLE_ENTRIES_HELP_TEXT, required=False, label=_("Spoken language in practice"))
    interpreter_language = forms.CharField(help_text=MULTIPLE_ENTRIES_HELP_TEXT, required=False, label=_("Language for which an interpreter is used "))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['spoken_language'].widget = ListTextWidget(
            data_list=CommunicationSpokenLanguageType.labels,
            name='spoken-language-choices',
        )
        self.fields['interpreter_language'].widget = ListTextWidget(
            data_list=CommunicationSpokenLanguageType.labels,
            name='interpreter-language-choices',
        )

    def clean(self):
        cl_spoken_language = self.cleaned_data.get('spoken_language')
        cl_interpreter_language = self.cleaned_data.get('interpreter_language')
        entry_exists = _("This entry already exists!")
        if cl_spoken_language and CommunicationSpokenLanguages.objects.filter(Q(practice__id=self.instance.id) & Q(com_spoken_language=cl_spoken_language.strip())).exists():
            msg = entry_exists
            self.add_error("spoken_language", msg)
        if cl_interpreter_language and CommunicationInterpreter.objects.filter(Q(practice__id=self.instance.id) & Q(com_interpreter_language=cl_interpreter_language.strip())).exists():
            msg = entry_exists
            self.add_error("interpreter_language", msg)
        return self.cleaned_data
    
    class Meta:
        model = Practice
        fields = ['spoken_language', 'interpreter_language']

class ColleaguesCommunicationForm(forms.ModelForm):

    digital_communication = forms.CharField(help_text=MULTIPLE_ENTRIES_HELP_TEXT, required=False, label=_("Digital communication with colleagues or ambulatory care providers"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['digital_communication'].widget = ListTextWidget(
            data_list=CommunicationDigitalWithColleaguesType.labels,
            name='digital_communication-choices',
        )

    def clean(self):
        digital_communication = self.cleaned_data.get('digital_communication')
        if digital_communication and CommunicationDigitalWithColleagues.objects.filter(Q(practice__id=self.instance.id) & Q(com_type_of_communication=digital_communication.strip())).exists():
            msg = _("This entry already exists!")
            self.add_error("digital_communication", msg)
        return self.cleaned_data
    
    class Meta:
        model = Practice
        fields = ['digital_communication', 'communication_type_with_colleagues']

class PatientsCommunicationForm(forms.ModelForm):

    digital_communication = forms.CharField(help_text=MULTIPLE_ENTRIES_HELP_TEXT, required=False, label=_("Digital communication with patients"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['digital_communication'].widget = ListTextWidget(
            data_list=CommunicationTypeWithPatientsType.labels,
            name='digital_communication-choices',
        )

    def clean(self):
        digital_communication = self.cleaned_data.get('digital_communication')
        if digital_communication and CommunicationDigitalWithPatients.objects.filter(Q(practice__id=self.instance.id) & Q(com_type_of_communication=digital_communication.strip())).exists():
            msg = _("This entry already exists!")
            self.add_error("digital_communication", msg)
        return self.cleaned_data
    
    class Meta:
        model = Practice
        fields = ['digital_communication']
