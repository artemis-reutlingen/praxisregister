from django.test import TestCase, Client
from django.urls import reverse

from addresses.models import FederalState, County
from institutes.models import Institute
from practices.models import Practice, Site
from practices.types import YesNoUnknowType
from users.models import User
from practices.types import SiteType


class BasePracticeViewTestCase(TestCase):
    def setUp(self) -> None:
        institute = Institute.objects.create_institute_with_group(
            organisation="Uni.X",
            name="Dep.Y",
            group_name="unix-depy"
        )

        self.test_practice = Practice.objects.create_practice_for_institute(institute=institute,
                                                                            practice_name="RandomPractice")
        User.objects.create_user_for_institute(username="tester0", password="x", institute=institute)

        self.client = Client()
        self.client.login(username='tester0', password='x')


class ITInfrastructureTests(BasePracticeViewTestCase):

    def test_it_infrastructure_page_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:it-detail', kwargs={'pk': self.test_practice.id}))
        self.assertTemplateUsed(
            response,
            template_name='practices/it_infrastructure/it_infrastructure_detail.html'
        )

    def test_it_infrastructure_page_context_has_form(self):
        response = self.client.get(reverse('practices:it-detail', kwargs={'pk': self.test_practice.id}))
        self.assertIn('form', response.context)

    def test_it_infrastructure_page_form_shows_initial_data(self):
        self.test_practice.it_notepad = "stuff"
        self.test_practice.save()
        response = self.client.get(reverse('practices:it-detail', kwargs={'pk': self.test_practice.id}))
        self.assertEqual(response.context['form']['it_notepad'].value(), 'stuff')

    def test_it_infrastructure_page_accepts_POST_with_notepad_data_and_redirects_to_same_page(self):
        self.assertEqual(self.test_practice.it_notepad, "")  # should be empty initially

        response = self.client.post(
            reverse('practices:it-detail', kwargs={'pk': self.test_practice.id}),
            data={'it_notepad': 'important stuff'}
        )

        # redirects to same page
        self.assertRedirects(response, reverse('practices:it-detail', kwargs={'pk': self.test_practice.id}))

        self.test_practice.refresh_from_db()
        self.assertEqual(self.test_practice.it_notepad, "important stuff")  # new information is stored

    def test_it_infrastructure_edit_pdms_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:it-pdms-edit', kwargs={'pk': self.test_practice.id}))
        self.assertTemplateUsed(
            response,
            template_name='practices/it_infrastructure/it_infrastructure_generic.html'
        )

    def test_it_infrastructure_edit_system_house_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:it-system-house-edit', kwargs={'pk': self.test_practice.id}))
        self.assertTemplateUsed(
            response,
            template_name='practices/it_infrastructure/it_infrastructure_generic.html'
        )

    def test_it_infrastructure_edit_other_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:it-other-edit', kwargs={'pk': self.test_practice.id}))
        self.assertTemplateUsed(
            response,
            template_name='practices/it_infrastructure/it_infrastructure_generic.html'
        )


class PracticeStructureTests(BasePracticeViewTestCase):
    def test_practice_structure_page_gets_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:structure-detail', kwargs={'pk': self.test_practice.id}))
        self.assertTemplateUsed(response, template_name='practices/practice_structure/practice_structure.html')

    def test_practice_structure_edit_general_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:structure-general-edit', kwargs={'pk': self.test_practice.id}))
        self.assertTemplateUsed(
            response,
            template_name='practices/practice_structure/forms/practice_structure_generic.html'
        )

    def test_practice_structure_edit_staff_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:structure-staff-edit', kwargs={'pk': self.test_practice.id}))
        self.assertTemplateUsed(
            response,
            template_name='practices/practice_structure/forms/practice_structure_generic.html'
        )

    def test_practice_structure_edit_services_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:structure-services-edit', kwargs={'pk': self.test_practice.id}))
        self.assertTemplateUsed(
            response,
            template_name='practices/practice_structure/forms/practice_structure_generic.html'
        )

    def test_practice_structure_edit_doctors_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:structure-doctors-edit', kwargs={'pk': self.test_practice.id}))
        self.assertTemplateUsed(
            response,
            template_name='practices/practice_structure/forms/practice_structure_generic.html'
        )


class ToDoTests(BasePracticeViewTestCase):
    def test_practice_add_todo_page_gets_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:create-todo', kwargs={'pk': self.test_practice.id}))
        self.assertTemplateUsed(
            response,
            template_name='global/forms/generic.html'
        )


class BaseDataTests(BasePracticeViewTestCase):

    def test_edit_practice_info_view_updates_practice(self):
        federal_state = FederalState.objects.create(full_name="Fantasy Federal State")
        county = County.objects.create(full_name="Fantasy County")
        response = self.client.post(
            reverse('practices:edit-info', kwargs={'pk': self.test_practice.id}),
            data={
                'name': "NewName",
                'street': "NewStreet",
                'house_number': "1",
                'zip_code': "123456",
                'city': "NewCity",
                'county_name': county.full_name,
                'federal_state': federal_state.id,
                'type': SiteType.PRIMARY,
                'identification_number': "654321",
                'number_of_sites': 2,
                'questionnaire_date': "2021-01-01",
            }
        )            

        self.assertRedirects(response, reverse('practices:detail', kwargs={'pk': self.test_practice.pk}))

        self.test_practice.refresh_from_db()

        self.assertEqual(self.test_practice.name, "NewName")
        self.assertEqual(self.test_practice.street, "NewStreet")
        self.assertEqual(self.test_practice.house_number, "1")
        self.assertEqual(self.test_practice.zip_code, "123456")
        self.assertEqual(self.test_practice.city, "NewCity")
        self.assertEqual(self.test_practice.county.full_name, "Fantasy County")
        self.assertEqual(self.test_practice.identification_number, "654321")
        self.assertEqual(self.test_practice.number_of_sites, 2)
        self.assertEqual(self.test_practice.questionnaire_date.strftime('%Y-%m-%d'), "2021-01-01")

    def test_edit_practice_info_views_form_has_correct_initial_data(self):
        county = County.objects.create(full_name="Fantasy County")

        self.test_practice.street = "SomeStreet"
        self.test_practice.house_number = "1"
        self.test_practice.zip_code = "123456"
        self.test_practice.city = "SomeCity"
        self.test_practice.county = county
        self.test_practice.federal_state = FederalState.objects.create(full_name="Fantasy Federal State")
        self.test_practice.identification_number = "654321"
        self.test_practice.save()

        response = self.client.get(reverse('practices:edit-info', kwargs={'pk': self.test_practice.id}))
        self.assertEqual(response.context['form']['name'].value(), 'RandomPractice')
        self.assertEqual(response.context['form']['street'].value(), 'SomeStreet')
        self.assertEqual(response.context['form']['house_number'].value(), '1')
        self.assertEqual(response.context['form']['zip_code'].value(), '123456')
        self.assertEqual(response.context['form']['city'].value(), 'SomeCity')
        self.assertEqual(response.context['form']['county_name'].value(), 'Fantasy County')
        self.assertEqual(response.context['form']['federal_state'].value(), self.test_practice.federal_state.id)
        self.assertEqual(response.context['form']['identification_number'].value(), "654321")
