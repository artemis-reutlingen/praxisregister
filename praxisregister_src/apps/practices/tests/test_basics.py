from django.test import TestCase
from django.urls import resolve, reverse

from config import settings
from institutes.models import Institute
from practices.views.practice import Practices
from users.models import User


class HelloServerTest(TestCase):

    def test_non_authenticated_user_gets_redirected_to_login_page(self):
        request_path = '/'
        response = self.client.get(request_path)

        # Login_required-middleware is active so we can expect 302 to login page
        self.assertEqual(response.status_code, 302)

        expected_redirect_url = f"{settings.LOGIN_URL}?next={request_path}"

        # Checking if redirect url is equal to login url as configured in site settings
        self.assertEqual(response.url, expected_redirect_url)

    def test_landing_page_for_authenticated_user_is_practice_list(self):
        institute = Institute.objects.create_institute_with_group(
            organisation="Uni.X",
            name="Dep.Y",
            group_name="unix-depy"
        )

        User.objects.create_user_for_institute(username="testi", password="testi123", institute=institute)

        self.client.login(username='testi', password='testi123')

        found_view = resolve('/')  # make sure right view is used
        self.assertEqual(found_view.func.__name__, Practices.as_view().__name__)

        # make sure right template is used
        response = self.client.get(reverse('landing-page'))
        self.assertTemplateUsed(
            response,
            template_name='practices/practices_list.html'
        )
