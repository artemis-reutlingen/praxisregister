from django.test import TestCase

from practices.models import Practice, DiseaseManagementProgramOption
# DiseaseManagementProgramEntry


class DiseaseManagementProgramModelsTest(TestCase):
    def setUp(self) -> None:
        self.dmps = [
            DiseaseManagementProgramOption(name="Back pain"),
            DiseaseManagementProgramOption(name="Cancer"),
            DiseaseManagementProgramOption(name="Something"),
        ]

        self.practice = Practice(name="Practice_A")

    def test_new_practice_has_dmp_entry_for_all_dmp_options(self):
        for dmp in self.dmps:
            dmp.save()

        self.practice.save()
        for dmp in self.dmps:
            self.assertIn(dmp, [_.type for _ in self.practice.disease_management_programs.all()])

    def test_new_dmp_has_entry_created_automatically_for_practices(self):
        self.practice.save()
        for dmp in self.dmps:
            dmp.save()
        for dmp in self.dmps:
            self.assertIn(dmp, [_.type for _ in self.practice.disease_management_programs.all()])
