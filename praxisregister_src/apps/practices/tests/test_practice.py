from django.contrib.auth.models import Group
from django.test import TestCase
from django.urls import reverse_lazy, reverse

from institutes.models import Institute
from persons.models import Person
from practices.models import Practice
from users.models import User


class PracticeAccessTest(TestCase):
    def setUp(self) -> None:
        self.institute_a = Institute.objects.create_institute_with_group("Uni X", "Dp. Med.", "uni_x_dep_md")
        self.institute_b = Institute.objects.create_institute_with_group("Uni Y", "Dp. Med.", "uni_y_dep_md")

        self.user_a = User.objects.create(username="test_a")
        self.user_a.set_password("x")
        self.user_a.assigned_institute = self.institute_a
        self.user_a.groups.add(Group.objects.get(name=self.institute_a.group))
        self.user_a.save()

        self.user_b = User.objects.create(username="test_b")
        self.user_b.set_password("x")
        self.user_b.assigned_institute = self.institute_b
        self.user_b.groups.add(Group.objects.get(name=self.institute_b.group))
        self.user_b.save()

        self.practice_1 = Practice.objects.create(name="Practice_A", assigned_institute=self.institute_a)
        self.practice_2 = Practice.objects.create(name="Practice_B", assigned_institute=self.institute_b)

        self.practice_1.assign_perms(self.institute_a)
        self.practice_2.assign_perms(self.institute_b)

    def test_allow_other_institutes_view(self):
        self.client.login(username="test_a", password="x")

        # Grant another institute access
        self.client.post(
            reverse('practices:allowed-institutes-edit', kwargs={"pk": self.practice_1.id}),
            data={'allowed_institutes': [self.institute_b.id]}
        )
        self.practice_1.refresh_from_db()
        self.assertIn(self.institute_b, self.practice_1.allowed_institutes.all())

        # Remove permissions again
        self.client.post(
            reverse('practices:allowed-institutes-edit', kwargs={"pk": self.practice_1.id}),
            data={'allowed_institutes': []}
        )
        self.practice_1.refresh_from_db()
        self.assertNotIn(self.institute_b, self.practice_1.allowed_institutes.all())


class PracticeModelTest(TestCase):
    def setUp(self) -> None:
        self.practice = Practice.objects.create(name="RandomPractice in the City")
        self.practice.street = "Parkstraße"
        self.practice.house_number = "3"
        self.practice.city = "Reutlingen"
        self.practice.zip_code = "72770"
        self.practice.save()

    def test_practice_type_default_is_set_to_not_specified(self):
        self.assertEqual(self.practice.structure_practice_type, "")

    def test_owners_last_names_formatted(self):
        p1 = Person.objects.create(
            practice=self.practice,
            title="Prof. Dr.",
            first_name="Heinrich",
            last_name="Mayer"
        )
        p2 = Person.objects.create(
            practice=self.practice,
            title="Dr.",
            first_name="Karl",
            last_name="Schmied"
        )
        p1.make_owner()
        p2.make_owner()
        self.assertEqual(self.practice.owners_names_formatted, "Prof. Dr. Heinrich Mayer, Dr. Karl Schmied")


class PracticeViewTests(TestCase):
    def setUp(self) -> None:
        self.institute_a = Institute.objects.create_institute_with_group("Uni A", "Dp. Med.", "uni_a_dep_md")

        self.user_a = User.objects.create(username="testi", assigned_institute=self.institute_a)
        self.user_a.set_password("x")
        self.user_a.groups.add(Group.objects.get(name=self.institute_a.group))
        self.user_a.save()

    def test_not_authenticated_user_cannot_create_practice(self):
        no_of_practices_in_db_before_post = Practice.objects.count()
        response = self.client.post(reverse_lazy('practices:create'), {'name': "Generic Practice Name"})
        no_of_practices_in_db_after_post = Practice.objects.count()
        # should be the same amount of practices
        self.assertEqual(no_of_practices_in_db_before_post, no_of_practices_in_db_after_post,
                          "practice count should stay the same")

    def test_authenticated_user_can_create_practice(self):
        self.client.login(username="testi", password="x")

        no_of_practices_in_db_before_post = Practice.objects.count()
        response = self.client.post(reverse_lazy('practices:create'), {'name': "Generic Practice Name"})
        no_of_practices_in_db_after_post = Practice.objects.count()

        # should be one more practice object in db
        self.assertEqual(no_of_practices_in_db_before_post + 1, no_of_practices_in_db_after_post,
                          "practice count should be incremented by 1")

        # client should be redirected (302)
        self.assertEqual(response.status_code, 302)

        # to practice-detail view with id of latest practice object
        created_practice = Practice.objects.latest('id')
        expected_redirect_url = reverse_lazy('practices:detail', args=[created_practice.id])
        self.assertEqual(response.url, expected_redirect_url)

        # client should be authorized for accessing newly created practice detail view (status code 200)
        response = self.client.get(expected_redirect_url)
        self.assertEqual(response.status_code, 200)
