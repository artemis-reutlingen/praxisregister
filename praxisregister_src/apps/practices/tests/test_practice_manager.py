from django.test import TestCase

from institutes.models import Institute
from practices.models import Practice
from users.models import User


class PracticeManagerTest(TestCase):
    def setUp(self) -> None:
        institute = Institute.objects.create_institute_with_group(
            organisation="Uni.X",
            name="Dep.Y",
            group_name="unix-depy"
        )

        self.practice_a = Practice.objects.create(name="A", assigned_institute=institute)
        self.practice_a.assign_perms(institute=institute)
        self.practice_b = Practice.objects.create(name="A", assigned_institute=institute)
        self.practice_b.assign_perms(institute=institute)

        u = User.objects.create(username="tester0", assigned_institute=institute)
        u.set_password("x")
        u.groups.add(institute.group)
        u.save()

    def test_get_practices_for_user(self):
        user = User.objects.get(username='tester0')
        all_practices_ids = [p.id for p in Practice.objects.all()]
        retrieved_via_util = [p.id for p in Practice.objects.get_practice_objects_for_user(user)]

        # user should have access to all practices
        for id in all_practices_ids:
            self.assertIn(id, retrieved_via_util)
