from django.core.exceptions import FieldDoesNotExist
from django.test import TestCase
from django.urls import reverse
from django.core.files.uploadedfile import SimpleUploadedFile

from practices.models import Practice
from utils.utils_test import setup_test_basics
from practices.forms.forms import ContractForm


class ContractTests(TestCase):
    def setUp(self) -> None:
        institute, group, user, [self.practice_1, self.practice_2] = setup_test_basics()

        self.client.login(username="tester", password="password")
        self.file = SimpleUploadedFile("testfile.txt", b"file_content")

    def test_contract_page_gets_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:accreditations', kwargs={'pk': self.practice_1.id}))
        self.assertTemplateUsed(
            response,
            template_name='practices/contract/detail.html'
        )

    def test_practice_model_has_contract_signed_indicator_attribute(self):
        try:
            Practice._meta.get_field('is_contract_signed')
        except FieldDoesNotExist:
            self.fail()

    def test_practice_model_has_contract_file(self):
        try:
            Practice._meta.get_field('contract_document')
        except FieldDoesNotExist:
            self.fail()

    def test_contract_edit_page_gets_rendered_with_correct_template(self):
        response = self.client.get(reverse('practices:contract-edit', kwargs={'pk': self.practice_1.id}))
        self.assertTemplateUsed(
            response,
            template_name='practices/contract/forms/edit.html'
        )

    def test_contract_form_without_signed(self):
        form = ContractForm(data={'contract_signed_date': "01.01.2022"}, files={'contract_document': self.file})
        self.assertFalse(form.is_valid())
    
    def test_contract_form_without_document(self):
        form = ContractForm(data={'is_contract_signed': True, 'contract_signed_date': "01.01.2022" })
        self.assertTrue(form.is_valid())

    def test_contract_form_without_signed_date(self):
        form = ContractForm(data={'is_contract_signed': True}, files={'contract_document': self.file})
        self.assertFalse(form.is_valid())
     
    def test_contract_form_with_only_document(self):
        form = ContractForm(files={'contract_document': self.file})
        self.assertTrue(form.is_valid())

    def test_contract_form_with_all_fields(self):
        form = ContractForm(data={'is_contract_signed': True, 'contract_signed_date': "01.01.2022"}, files={'contract_document': self.file})
        self.assertTrue(form.is_valid())