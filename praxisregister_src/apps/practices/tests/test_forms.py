from django.test import TestCase

from addresses.models import County, FederalState
from practices.forms.practice_base_data_forms import PracticeInfoForm
from practices.models import Practice, Site
from practices.types import SiteType, YesNoUnknowType

class PracticeInfoFormTest(TestCase):
    def test_practice_info_form_can_handle_practice_name_and_site_info_with_existing_county(self):
        federal_state = FederalState.objects.create(full_name="Fantasy Federal State")
        County.objects.create(full_name="Fantasy County")
        county_name = "Fantasy County"
        practice = Practice.objects.create(name="A random practice")
        form = PracticeInfoForm(
            instance=practice,
            data={
                'name': "NewName",
                'street': "NewStreet",
                'house_number': "1",
                'zip_code': "123456",
                'city': "NewCity",
                'county_name': county_name,
                'federal_state' : federal_state.id,
                'type': SiteType.PRIMARY,
                'identification_number': "654321",
                'number_of_sites': 2,
                'questionnaire_date': "2021-01-01",
            }
        )

        self.assertTrue(form.is_valid())
        form.save()

        self.assertEqual(practice.name, "NewName")

        self.assertEqual(practice.street, "NewStreet")
        self.assertEqual(practice.house_number, "1")
        self.assertEqual(practice.zip_code, "123456")
        self.assertEqual(practice.city, "NewCity")
        self.assertEqual(practice.identification_number, "654321")
        self.assertEqual(practice.number_of_sites, 2)
        self.assertEqual(practice.questionnaire_date.strftime('%Y-%m-%d'), "2021-01-01")
        self.assertEqual(practice.federal_state, federal_state)
        # We dont assert county because it is handled by the View

    def test_practice_info_form_can_handle_practice_name_and_site_info_with_none_existing_county(self):

        practice = Practice.objects.create(name="A random practice")
        form = PracticeInfoForm(
            instance=practice,
            data={
                'name': "NewName",
                'street': "NewStreet",
                'house_number': "1",
                'zip_code': "123456",
                'city': "NewCity",
                'county': "This_is_not_in_the_database_so_should_not_be_valid",
                'type': SiteType.PRIMARY,
                'identification_number': "654321"
            }
        )
       
        self.assertFalse(form.is_valid())
