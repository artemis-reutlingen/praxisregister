from django.urls import path

from accreditation.views import PracticeAccreditationCreate, PracticeAccreditationList
from contactnotes.views import ContactNoteCreate, PracticeContactNotes
from documents.views import DocumentCreate, PracticeDocuments
from events.views.participation_views import EventParticipationCreate, PracticeEventParticipationList
from persons.views import PersonsInPractice, PersonAdd
from processes.views import PracticeProcessList, ProcessParticipationCreate, process_step_select_option
from studies.views.practice_study_overview import PracticeStudyParticipations, StudyParticipationCreate
from todos.views import PracticeToDoCreate, PracticeToDoList, ToDoDelete
from .views.allowed_institutes import AllowedInstitutes, AllowedInstitutesEdit
from .views.practice_base_data import PracticeDetail, PracticeOpeningHoursEdit, \
    PracticeContactEdit, UpdatePracticeBaseData
from .views.contract import *
from .views.it_infrastructure import PracticeITDetail, PracticeITSystemHouseEdit, PracticeITOtherEdit, \
    PracticeITPDMSEdit, PracticeITNotepadEdit
from .views.notepad import PracticeNotepadEdit
from .views.practice import PracticeCreate, Practices, practice_table
from .views.sites import AdditionalSiteEdit, AdditionalSiteDelete, AdditionalSiteCreateView
from .views.practice_structure import *
from .views.research_tandem import TandemAssistantDetail, TandemDoctorDetail
from practices.views.practice_communication import *

app_name = 'practices'

practice_urls = [
    path('', Practices.as_view(), name='list'),
    path('htmx/practice-table/', practice_table, name='practice_table'),
    path('htmx/process-step-selection/', process_step_select_option, name='process-step-selection'),
    path('<int:pk>/', PracticeDetail.as_view(), name='detail'),
    path('new', PracticeCreate.as_view(), name='create'),
    path('<int:pk>/sites/new', AdditionalSiteCreateView.as_view(), name='create-site'),
    path('sites/<int:pk>/edit', AdditionalSiteEdit.as_view(), name='edit-site'),
    path('sites/<int:pk>/delete', AdditionalSiteDelete.as_view(), name='delete-site'),
    path('<int:pk>/notepad/', PracticeNotepadEdit.as_view(), name='edit-notepad'),
    path('<int:pk>/information/edit', UpdatePracticeBaseData.as_view(), name='edit-info'),
    path('<int:pk>/opening-hours/edit', PracticeOpeningHoursEdit.as_view(), name='edit-opening-hours'),
    path('<int:pk>/contact/edit', PracticeContactEdit.as_view(), name='edit-contact'),
]

todo_urls = [
    path('<int:pk>/todos/', PracticeToDoList.as_view(), name='todo-list'),
    path('<int:pk>/todos/new', PracticeToDoCreate.as_view(), name='create-todo'),
    path('<int:pk>/todos/delete', ToDoDelete.as_view(), name='todo-delete'),
]

accreditation_urls = [
    path('<int:pk>/accreditations/new', PracticeAccreditationCreate.as_view(), name='accreditation-add'),
    path('<int:pk>/accreditations/', PracticeAccreditationList.as_view(), name='accreditations'),
]

study_urls = [
    path('<int:pk>/study-participations/', PracticeStudyParticipations.as_view(),
         name='study-participation-list'),
    path('<int:pk>/study-participations/new', StudyParticipationCreate.as_view(),
         name='study-participation-create'),
]

event_urls = [
    path('<int:pk>/event-participations/', PracticeEventParticipationList.as_view(),
         name='event-participation-list'),
    path('<int:pk>/event-participations/new', EventParticipationCreate.as_view(),
         name='event-participation-create'),
]

process_urls = [
    path('<int:pk>/process-participations/', PracticeProcessList.as_view(), name='process-list'),
    path('<int:pk>/process-participations/new', ProcessParticipationCreate.as_view(),
         name='process-participation-create'),
]

contact_notes_urls = [
    path('<int:pk>/contact-notes/', PracticeContactNotes.as_view(), name='contactnote-list'),
    path('<int:pk>/contact-notes/new', ContactNoteCreate.as_view(), name='contactnote-create'),
]

documents_urls = [
    path('<int:pk>/documents/', PracticeDocuments.as_view(), name='document-list'),
    path('<int:pk>/documents/new', DocumentCreate.as_view(), name='document-create'),
]

structure_urls = [
    path('<int:pk>/structure/', PracticeStructureDetail.as_view(),
         name='structure-detail'),
    path('<int:pk>/structure/doctors/edit', PracticeStructureDoctorsEdit.as_view(),
         name='structure-doctors-edit'),
    path('<int:pk>/structure/staff/edit', PracticeStructureStaffEdit.as_view(),
         name='structure-staff-edit'),
    path('structure/assistant-qualifications/<int:pk>/delete',
         PracticeAssistantsAdditionalQualificationDelete.as_view(),
         name='structure-assistant-qualifications-delete'),
    path('<int:pk>/structure/services/edit', PracticeStructureServicesEdit.as_view(),
         name='structure-services-edit'),
    path('structure/engagements/<int:pk>/delete', PracticeEngagementDelete.as_view(),
         name='structure-engagement-delete'),
    path('<int:pk>/structure/general/edit', PracticeStructureGeneralEdit.as_view(),
         name='structure-general-edit'),
    path('structure/dmp-entries/<int:pk>/edit', DiseaseManagementProgramEntryEdit.as_view(),
         name='dmp-entry-edit'),
    path('structure/offers-service/<int:pk>/delete', ServicesPracticeOffersDelete.as_view(), name='practice-offers-delete'),
    path('<int:pk>/structure/treatment-by-age/edit', PracticeTreatmentByAgeGroupPerQuarterEdit.as_view(), name='structure-treatment-by-age-edit'),
    path('structure/treatment-by-age/<int:pk>/delete', PracticeTreatmentByAgeGroupPerQuarterDelete.as_view(), name='structure-treatment-by-age-delete'),
]

it_urls = [
    path('<int:pk>/it/', PracticeITDetail.as_view(), name='it-detail'),
    path('<int:pk>/it/system-house/edit', PracticeITSystemHouseEdit.as_view(), name='it-system-house-edit'),
    path('<int:pk>/it/pdms/edit', PracticeITPDMSEdit.as_view(), name='it-pdms-edit'),
    path('<int:pk>/it/other/edit', PracticeITOtherEdit.as_view(), name='it-other-edit'),
    path('<int:pk>/it/notepad/edit', PracticeITNotepadEdit.as_view(), name='it-notepad-edit'),
]

communication_urls = [
    path('<int:pk>/communication/', PracticeCommunicationView.as_view(), name='communication-detail'),
    path('<int:pk>/communication/spoken-language/edit', PracticeCommunicationEdit.as_view(), name='communication-spoken-language-edit'),
    path('communication/spoken-language/<int:pk>/delete', CommunicationSpokenLanguageDelete.as_view(), name='communication-spoken-language-delete'),
    path('communication/spoken-language/<int:pk>/percentage', CommunicationSpokenLanguagePercentage.as_view(), name='communication-spoken-language-percentage'),
    path('communication/interpreter/<int:pk>/delete', CommunicationInterpreterDelete.as_view(), name='communication-interpreter-delete'),
    path('communication/interpreter/<int:pk>/amount', CommunicationInterpreterAmount.as_view(), name='communication-interpreter-amount'),

    path('<int:pk>/communication/colleagues', PracticeDigitalCommunicationWithColleaguesEdit.as_view(), name='communication-digital-colleagues-edit'),
    path('communication/colleagues/<int:pk>/delete', CommunicationWithColleaguesDelete.as_view(), name='communication-digital-colleagues-delete'),
    
    path('<int:pk>/communication/patients', PracticeDigitalCommunicationWithPatientsEdit.as_view(), name='communication-digital-patients-edit'),
    path('communication/patients/<int:pk>/delete', CommunicationWithPatientsDelete.as_view(), name='communication-digital-patients-delete'),
]

contract_urls = [
    path('<int:pk>/contract/download', download_contract_file, name='contract-download'),
    path('<int:pk>/contract/delete', delete_contract_file, name='contract-delete'),
    path('<int:pk>/contract/edit', ContractEdit.as_view(), name='contract-edit'),
]

persons_urls = [
    path('<int:pk>/persons/', PersonsInPractice.as_view(), name='persons'),
    path('<int:pk>/persons/new', PersonAdd.as_view(), name='person-add'),
]

research_tandem_urls = [
    path('<int:pk>/research-tandem/doctor/', TandemDoctorDetail.as_view(), name='tandem-doctor-detail'),
    path('<int:pk>/research-tandem/assistant/', TandemAssistantDetail.as_view(), name='tandem-assistant-detail'),
]

allowed_institutes_urls = [
    path('<int:pk>/allowed-institutes/', AllowedInstitutes.as_view(), name="allowed-institutes"),
    path('<int:pk>/allowed-institutes/add', AllowedInstitutesEdit.as_view(), name="allowed-institutes-edit")
]

urlpatterns = []
urlpatterns.extend(practice_urls)
urlpatterns.extend(persons_urls)
urlpatterns.extend(contract_urls)
urlpatterns.extend(it_urls)
urlpatterns.extend(communication_urls)
urlpatterns.extend(structure_urls)
urlpatterns.extend(contract_urls)
urlpatterns.extend(contact_notes_urls)
urlpatterns.extend(documents_urls)
urlpatterns.extend(event_urls)
urlpatterns.extend(process_urls)
urlpatterns.extend(study_urls)
urlpatterns.extend(todo_urls)
urlpatterns.extend(accreditation_urls)
urlpatterns.extend(research_tandem_urls)
urlpatterns.extend(allowed_institutes_urls)
