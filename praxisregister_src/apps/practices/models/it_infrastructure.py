from django.db import models
from django.utils.translation import gettext_lazy as _
from practices.types import YesNoUnknowType

class ITInfrastructureMixin(models.Model):
    """
    Represents all aspects for IT infrastructure information in a practice.
    """

    class YesNoReluctant(models.TextChoices):
        YES = 'YES', _('Yes')
        NO = 'NO', _('No')
        PLANNED = 'RELUCTANT', _('Reluctant')

    class InternetSpeedOptions(models.TextChoices):
        UNKNOWN = 'UNKNOWN', _('Unknown')
        FAST = 'FAST', _('Fast')
        NORMAL = 'NORMAL', _('Normal')
        SLOW = 'SLOW', _('Slow')

    class HowOftenType(models.TextChoices):
        UNKNOWN = 'UNKNOWN', _('Unknown')
        RARELY = 'RARELY', _('Rarely')
        NOW_AND_THEN = 'NOW AND THEN', _('Now and then')
        FREQUENTLY = 'FREQUENTLY', _('Frequently')

    it_pdms_software = models.CharField(
        verbose_name=_('Software (PVS)'),
        max_length=255,
        null=True,
        blank=True,
        default=""
    )
    it_system_house_name = models.CharField(
        verbose_name=_('System house company name'),
        max_length=255,
        null=True,
        blank=True,
        default=""
    )
    it_system_house_address_street = models.CharField(
        verbose_name=_('System house street name'),
        max_length=255,
        null=True,
        blank=True,
        default=""
    )
    it_system_house_address_house_number = models.CharField(
        verbose_name=_('System house House number'),
        max_length=255,
        null=True,
        blank=True,
        default=""
    )
    it_system_house_address_city = models.CharField(
        verbose_name=_('System house City'),
        max_length=255,
        null=True,
        blank=True, default="")
    
    it_system_house_address_zip_code = models.CharField(
        verbose_name=_('System house ZIP code'),
        max_length=255, null=True,
        blank=True, default="")
    
    it_system_house_contact_person_name = models.CharField(
        verbose_name=_('System house contact person'),
        max_length=255,
        null=True,
        blank=True,
        default=""
    )
    it_system_house_contact_email = models.EmailField(
        verbose_name=_('System house E-Mail'),
        max_length=255,
        null=True,
        blank=True,
        default=None
    )
    it_system_house_contact_phone = models.CharField(
        verbose_name=_('System house Phone'),
        max_length=255,
        null=True,
        blank=True,
        default=None
    )

    it_epa_used_for_medical_documentation = models.CharField(verbose_name=_('EPA is used for medical documentation'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)
    it_automatic_backups_established = models.CharField(verbose_name=_('Backup processes established'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)
    it_openness_data_export = models.CharField(verbose_name=_("Openness concerning automated data export"), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)
    it_fopranet_ready_workstation_available = models.CharField(verbose_name=_('Workstation with access to PDMS and internet'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)

    it_security_permissions = models.CharField(
        verbose_name=_("Security permissions"),
        max_length=1024,
        default="",
        null=False,
        blank=True
    )
    it_practice_networks = models.CharField(
        verbose_name=_("Practice Networks"),
        max_length=1024,
        default="",
        null=False,
        blank=True
    )
    it_remote_maintenance = models.CharField(verbose_name=_('Remote maintenance in use? For example Teamviewer.'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)

    it_notepad = models.TextField(
        verbose_name=_("IT Notepad"),
        default="",
        null=False,
        blank=True
    )

    it_operating_system = models.ManyToManyField(
        'practices.OperatingSystemOption',
        verbose_name=_('Practice operating System'),
        blank=True,
    )

    it_internet_speed = models.CharField(
        verbose_name=_("Practice internet speed"),
        max_length=255,
        choices=InternetSpeedOptions.choices,
        default=InternetSpeedOptions.UNKNOWN,
    )

    it_pdms_connected_to_medical_devices = models.CharField(verbose_name=_('Medical devices are connected with the PDMS (e.g. via GDT)'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)
    it_checked_for_interoperability_interfaces = models.CharField(verbose_name=_('Performed check for interoperability interfaces'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)
    it_is_email_checked_frequently = models.CharField(verbose_name=_('Is the contact email adress checked frequently?'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)

    it_how_often_is_fax_used_for_communication = models.CharField(
        verbose_name=_("How often is fax used for communication?"),
        max_length=255,
        choices=HowOftenType.choices,
        default=HowOftenType.UNKNOWN,
    )

    class Meta:
        abstract = True
