from django.db import models
from django.utils.translation import gettext_lazy as _
from practices.types import YesNoUnknowType, BillsPerQuarterType, TreatmentsByAgeGroupType


@staticmethod
def convert_to_int(value):
    number = 0
    try:
        number = int(value)
    except TypeError:
        pass
    return number

class PracticeStructureMixin(models.Model):

    structure_practice_type = models.CharField(verbose_name=_("Practice Type"), max_length=512, default="", blank=True, null=False)
    structure_urbanisation_level = models.CharField(verbose_name=_("Urbanisation"), max_length=128, default="", blank=True, null=False)

    structure_full_care_mandate = models.CharField(verbose_name=_('Full care mandate'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)
    structure_bills_per_quarter = models.CharField(verbose_name=_('Treatment certificates per quarter'), max_length=128, default=BillsPerQuarterType.UNKNOWN, choices=BillsPerQuarterType.choices)
    
    
    structure_at_least_35_hours_openend = models.CharField(verbose_name=_('At least 35 hours opened per week'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)
    structure_gp_range_of_services = models.CharField(verbose_name=_('GP range of services more than 50%'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)

    structure_practice_operating_until = models.DateField(verbose_name=_('Operation is guaranteed until'), null=True, blank=True)

    structure_number_of_doctors_that_are_owners = models.PositiveSmallIntegerField(verbose_name=_('Number of owners working as doctors'), null=True, blank=True)
    structure_number_of_doctors_that_are_employees = models.PositiveSmallIntegerField(verbose_name=_('Number of employed doctors'), null=True, blank=True)
    structure_number_of_doctors_that_are_in_training = models.PositiveSmallIntegerField(verbose_name=_('Number of doctors in training'), null=True, blank=True)
    structure_full_time_equivalent_for_doctors = models.PositiveSmallIntegerField(verbose_name=_('Full-time equivalent (exclusive Doctors in further training)'), null=True, blank=True)
    structure_full_time_equivalent_for_doctors_in_education = models.PositiveSmallIntegerField(verbose_name=_('Full-time equivalent doctors in training'), null=True, blank=True)
    structure_number_of_non_gp_doctors_that_are_in_the_practice = models.PositiveSmallIntegerField(verbose_name=_('Number of non-GP doctors in the practice'), null=True, blank=True)
    structure_number_of_employed_medical_assistants = models.PositiveSmallIntegerField(verbose_name=_('Number of employed MFAs'), null=True, blank=True)
    structure_mfa_research_project_interest = models.CharField(verbose_name=_('Are MFAs interested in participating in research projects'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)
    structure_number_of_employed_administrative_assistants = models.PositiveSmallIntegerField(verbose_name=_('Number of administrative employees'), null=True, blank=True)
    structure_number_of_medical_assistants_in_training = models.PositiveSmallIntegerField(verbose_name=_('Number of MFAs in training'), null=True, blank=True)
    structure_full_time_equivalent_for_assistants = models.PositiveSmallIntegerField(verbose_name=_('Full-time equivalent of assistants'), null=True, blank=True)

    structure_non_gp_doctors_are_working_in_the_practice = models.CharField(verbose_name=_('Non-GP doctors in the practice'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)
    services_home_visits = models.CharField(verbose_name=_('Regular home visits'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)
    services_nursing_home_visits = models.CharField(verbose_name=_('Regular nursing home visits'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)

    @property
    def total_number_of_doctors(self):
        return convert_to_int(self.structure_number_of_doctors_that_are_owners) \
               + convert_to_int(self.structure_number_of_doctors_that_are_employees) \
               + convert_to_int(self.structure_number_of_doctors_that_are_in_training) \
               + convert_to_int(self.structure_number_of_non_gp_doctors_that_are_in_the_practice)
    
    @property
    def total_number_of_staff(self):
        return convert_to_int(self.structure_number_of_employed_medical_assistants) \
               + convert_to_int(self.structure_number_of_employed_administrative_assistants) \
               + convert_to_int(self.structure_number_of_medical_assistants_in_training)
    
    class Meta:
        abstract = True
        verbose_name = _('Practice structure')

class PracticeTreatmentByAgeGroupPerQuarter(models.Model):

    age_group = models.CharField(verbose_name=_('Age group'), max_length=128, choices=TreatmentsByAgeGroupType.choices)
    amount_of_threatments = models.IntegerField(verbose_name=_('Amount of treatments'))
    practice = models.ForeignKey('practices.Practice', verbose_name=_('Practice'), on_delete=models.CASCADE)

    class Meta:
        unique_together = ('age_group', 'practice')

    def __str__(self) -> str:
        return f"{self.age_group}: {self.amount_of_threatments}"