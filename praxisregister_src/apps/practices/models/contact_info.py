from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import URLValidator


class ContactInfoMixin(models.Model):
    class PreferredContactMediumType(models.TextChoices):
        PHONE = 'PHONE', _('Phone')
        EMAIL = 'EMAIL', _('E-Mail')
        FAX = 'FAX', _('Fax')
        UNKNOWN = 'UNKNOWN', _('Unknown')

    contact_phone = models.CharField(verbose_name=_('Phone'), max_length=255, null=True, blank=True, default="")
    contact_phone_alt = models.CharField(verbose_name=_('Phone (alternative)'), max_length=255, null=True, blank=True,
                                         default="")
    contact_fax = models.CharField(verbose_name=_('Fax'), max_length=255, null=True, blank=True, default=None)
    contact_email = models.EmailField(verbose_name=_('E-Mail'), max_length=255, null=True, blank=True, default=None)
    contact_email_alt = models.EmailField(verbose_name=_('E-Mail (alternative)'), max_length=255, null=True, blank=True)
    contact_website = models.CharField(verbose_name=_('Website'), validators=[URLValidator], max_length=255, null=True, blank=True, default=None)
    contact_preferred_medium = models.CharField(
        verbose_name=_("Preferred medium"),
        max_length=128,
        choices=PreferredContactMediumType.choices,
        default=PreferredContactMediumType.UNKNOWN,
        blank=False,
        null=False
    )

    class Meta:
        abstract = True
