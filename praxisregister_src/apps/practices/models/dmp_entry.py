from django.db import models
from django.utils.translation import gettext_lazy as _


class DiseaseManagementProgramEntry(models.Model):
    """
    Represents the relation of a practice to specific DMP.
    All possible entries for a practice are created automatically via signals.
    Keeping DMPs as relations allows the admin to add DMP types via admin panel.
    """

    class YesNoPlannedType(models.TextChoices):
        YES = 'YES', _('Yes')
        NO = 'NO', _('No')
        PLANNED = 'Planned', _('Planned')
        UNKNOWN = 'UNKNOWN', _('Unknown')

    practice = models.ForeignKey(
        'practices.Practice',
        related_name="disease_management_programs",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    type = models.ForeignKey(
        'practices.DiseaseManagementProgramOption',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    status = models.CharField(
        verbose_name=_('Status'),
        max_length=128,
        choices=YesNoPlannedType.choices,
        default=YesNoPlannedType.UNKNOWN,
        blank=False
    )

    @property
    def is_available(self):
        return self.status == self.YesNoPlannedType.YES

    class Meta:
        verbose_name = _("Disease management program")
        unique_together = ('practice', 'type')
        ordering = ['type']
