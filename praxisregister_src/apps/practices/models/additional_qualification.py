from django.db import models
from django.utils.translation import gettext_lazy as _


class PracticeAssistantsAdditionalQualification(models.Model):
    """
    Represents an AdditionalQualification, associated with a practice.
    Type field is CharField - neither ForeignKey nor limited to text choices in order to allow users to enter free text.
    Associated forms will still give the user pre-defined options based on AssistantAdditionalQualificationOption model.

    TODO:
     AssistantAdditionalQualificationOption is defined in persons app currently - worth considering moving all "option"-
     models in utils app or a separate app, which can be referenced by others.
     Hint: This dependency is just implicit, because type is actually a CharField - but options are fetched in the Form.
    """
    type = models.CharField(verbose_name="Type", default="", blank=True, max_length=512, )
    practice = models.ForeignKey(
        'practices.Practice',
        verbose_name=_('Practice'),
        related_name="additional_qualifications",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self) -> str:
        additional_qualification = _("Additional qualifications (staff)")
        return f"{additional_qualification}"
    
    class Meta:
        verbose_name = _("Additional qualifications (staff)")
        verbose_name_plural = _("Additional qualifications (staff)")
