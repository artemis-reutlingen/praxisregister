from addresses.models import AddressMixin
from config.settings import UPLOAD_DIRECTORY
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils.translation import gettext_lazy as _
from guardian.shortcuts import (assign_perm, get_objects_for_user, get_objects_for_group, get_perms,
                                remove_perm)
from guardian.models import UserObjectPermissionBase, GroupObjectPermissionBase
from institutes.models import Institute
from practices.models.contact_info import ContactInfoMixin
from practices.models.it_infrastructure import ITInfrastructureMixin
from practices.models.practice_structure import PracticeStructureMixin
from practices.models.practice_communication import PracticeCommunicationMixin
from practices.types import SiteType, BillsPerQuarterType, YesNoUnknowType
from practices.validators import validate_file_size
from track_changes.models import TrackChangesMixin
from importer.models import ImportableMixin
from taggit_selectize.managers import TaggableManager
from kbv_importer.models import KbvImportMixin
from datetime import date


def generate_contract_document_upload_path(instance, filename):
    """
    Generates upload path for storing user-uplaoded contract documents.
    TODO: Find better placement for this code.
    """
    return f"{UPLOAD_DIRECTORY}/contract_documents/practice_{instance.id}/{filename}"


class PracticeManager(models.Manager):
    @staticmethod
    def get_practice_objects_for_user(user):
        """
        Fetches all practice objects the requesting user has access to.
        """
        return get_objects_for_user(user, "practices.manage_practice").order_by('-updated_at')

    @staticmethod
    def get_practice_ids_for_user(user):
        """
        Fetches all practice IDs the requesting user has access to.
        """
        return get_objects_for_user(user, "practices.manage_practice") \
            .values_list('id', flat=True) \
            .order_by('-updated_at')

    @staticmethod
    def get_practice_objects_for_group(group):
        return get_objects_for_group(group, "practices.manage_practice")
    
    def create_practice_for_institute(self, institute: Institute, practice_name: str):
        """
        Creates practice and assigns the necessary permissions for the given institute.
        """
        practice = self.create(name=practice_name, assigned_institute=institute)
        practice.assign_perms(institute=institute)
        return practice

class Practice(TrackChangesMixin, AddressMixin, ContactInfoMixin, PracticeStructureMixin, ITInfrastructureMixin, PracticeCommunicationMixin, ImportableMixin, KbvImportMixin, models.Model):
    """
    Main model for Praxisregister.
    Using multiple Mixins to separate contexts to different classes while still only having a single table.
    """
    objects = PracticeManager()

    tags = TaggableManager(blank=True)

    name = models.CharField(verbose_name=_('Name'), max_length=256, null=True)
    
    identification_number = models.CharField(verbose_name=_('BSNR'), max_length=255, null=True, blank=True, default="")
    type = models.CharField(verbose_name=_("Site type"), max_length=256, choices=SiteType.choices, default=SiteType.PRIMARY, blank=False,)

    questionnaire_date = models.DateField(verbose_name=_('Questionnaire date'), blank=True, null=True)

    is_active = models.BooleanField(default=True, verbose_name=_("Is active"))
    inactive_note = models.CharField(max_length=256, verbose_name=_("Reason for inactivity"), null=True, blank=True)

    number_of_sites =  models.IntegerField(verbose_name=_('Number of sites'), default=1)

    doctor_has_education_authorization = models.CharField(verbose_name=_('Doctor has education authorization'), max_length=128, default=YesNoUnknowType.UNKNOWN, choices=YesNoUnknowType.choices)

    notepad = models.TextField(verbose_name=_("Notepad"), default="", null=False, blank=True)

    assigned_institute = models.ForeignKey('institutes.Institute', verbose_name=_('Institute'),
                                           on_delete=models.SET_NULL,
                                           null=True)
    allowed_institutes = models.ManyToManyField(
        Institute,
        related_name="accessable_practices",
        blank=True
    )

    # Contract
    is_contract_signed = models.BooleanField(verbose_name=_('Contract is signed'), default=False)
    contract_signed_date = models.DateField(verbose_name=_('Contract signed date'), blank=True, null=True)
    contract_document = models.FileField(
        verbose_name=_('Contract document'),
        upload_to=generate_contract_document_upload_path,
        validators=[validate_file_size],
        blank=True,
        null=True
        )
    
    # Research Tandem
    research_doctor = models.OneToOneField('persons.Person',
                                           verbose_name=_('Research Doctor'),
                                           related_name=_('current_research_doctor'),
                                           on_delete=models.SET_NULL,
                                           null=True, blank=True)

    research_assistant = models.OneToOneField('persons.Person',
                                              verbose_name=_('Research MFA'),
                                              related_name=_('current_research_assistant'),
                                              on_delete=models.SET_NULL,
                                              null=True, blank=True)
    
    @property
    def owners_names_formatted(self):
        return ", ".join([f"{o.full_name_with_title}" for o in self.owners])

    contact_times = GenericRelation(
        "contacttimes.DayContactTimeSlot",
        verbose_name = _("Contact times"),
        related_query_name = "practice"
    )

    @property
    def structure_more_than_500_bills_per_quarter(self):
        return self.structure_bills_per_quarter not in [BillsPerQuarterType.UNDER_500, BillsPerQuarterType.UNKNOWN]

    @property
    def all_accreditations(self):
        """
        Provides string representation of related accreditation objects separated by linebreak.
        """
        return '\n'.join([accreditation.get_type_display() for accreditation in self.accreditations.all()])

    @property
    def contact_times_ordered(self):
        """
        Helper to have contact times in the right order in template.
        Using ID to order works because of initial create order of contact time objects.
        """
        return self.contact_times.all().order_by('id')

    def assign_perms(self, institute):
        """
        Granting permissions for assigned_institute. Called when creating/initializing a practice.
        """
        assign_perm('practices.manage_practice', institute.group, self)
        assign_perm('practices.allow_other_institutes', institute.group, self)

    def update_practices_access(self):
        """
        Updates permissions by checking self.allowed_institutes and all institutes.
        Cleaning up perms if necessary.
        """
        permission = 'manage_practice'
        for institute in Institute.objects.all().exclude(id=self.assigned_institute.id):
            if institute in self.allowed_institutes.all() and permission not in get_perms(institute.group, self):
                assign_perm(permission, institute.group, self)
            else:
                if permission in get_perms(institute.group, self):
                    remove_perm(permission, institute.group, self)

    @property
    def additional_sites(self):
        return self.site_set.all()

    @property
    def contact_notes(self):
        return self.contactnote_set.all()

    @property
    def todos(self):
        from todos.models import ToDo
        return ToDo.objects.filter(practice=self.id).order_by('-status', '-created_at')

    @property
    def processes(self):
        from processes.models import Process
        return Process.objects.filter(practice=self.id).order_by('-created_at')
    
    @property
    def overdue_processes(self):
        result = self.processes.filter(step__is_done = False, deadline__lt = date.today())
        return result

    @property
    def owners(self):
        return self.person_set.all().exclude(role_owner__isnull=True)

    @property
    def persons(self):
        return self.person_set.all().order_by('last_name')

    @property
    def event_participations(self):
        from events.models import PracticeEventParticipation
        return PracticeEventParticipation.objects.filter(practice=self.id)

    @property
    def get_name(self):
        return f"{self.name}" if self.name else _("new practice")

    def __str__(self):
        return f"{self.get_name}"

    class Meta:
        verbose_name = _("Practice")
        verbose_name_plural = _("Practices")
        permissions = (
            ('manage_practice', 'Manage practice'),
            ('allow_other_institutes', 'Allow other institutes to manage practice')
        )


# Explicit definition of practice object permissions. This ensures permission objects are also deleted when a practice is deleted instead of being orphaned.
class PracticeUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(Practice, on_delete = models.CASCADE)

class PracticeGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(Practice, on_delete = models.CASCADE)
