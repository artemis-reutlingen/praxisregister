from django.db import models
from django.utils.translation import gettext_lazy as _
from importer.models import ImportableMixin


class PracticeEngagement(ImportableMixin):
    """
    Represents a PracticeEngagement, associated with a practice.
    Type field is CharField - neither ForeignKey nor limited to text choices in order to allow users to enter free text.
    Associated forms will still give the user pre-defined options based on PracticeEngagementOption model.
    """
    type = models.CharField(verbose_name="Type", default="", blank=True, max_length=512, )
    practice = models.ForeignKey('practices.Practice', verbose_name=_('Practice'), related_name="engagements", on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self) -> str:
        return f"{_('Other function/role of the practice')}"
    
    class Meta:
        verbose_name = _("Other function/role of the practice")
        verbose_name_plural = _("Other functions/roles of the practice")
    

class ServicesPracticeOffers(models.Model):

    service_name = models.CharField(verbose_name="Service name", default="", blank=True, max_length=512)
    practice = models.ForeignKey('practices.Practice', verbose_name=_('Practice'), on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.service_name
    
    class Meta:
        verbose_name = _("Service offered by practice")
        verbose_name_plural = _("Services offered by practice")