from django.apps import AppConfig


class PracticesConfig(AppConfig):
    name = 'practices'

    def ready(self):
        """
        Registers signals when app is loaded.
        """
        from . import signals
        from . import signals_logger