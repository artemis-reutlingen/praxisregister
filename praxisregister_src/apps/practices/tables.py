from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django_tables2 import Column, Table, CheckBoxColumn
from practices.models import Practice


class PracticeTable(Table):

    identification_number = Column(orderable=True, attrs={"td": {"class": "text-nowrap"}})
    owners_names_formatted = Column(verbose_name=_('Owner(s)'), orderable=False)
    zip_code = Column(orderable=True, attrs={"td": {"class": "text-nowrap"}})
    accreditations = Column(verbose_name=_('Accreditations'))  # , orderable=False)

    @staticmethod
    def render_name(value, record):
        practice_url = reverse("practices:detail", kwargs={"pk": record.id})
        return mark_safe(f'<a href="{practice_url}">{value}</a>')

    @staticmethod
    def render_accreditations(value, record):
        if record.accreditations.all():
            accreditation_list_string = "<br>".join(
                [f"{a.accreditation_type.get_name_display()}" if a.accreditation_type else "" for a in record.accreditations.all()]
            )
        else:
            accreditation_list_string = "---"
        return mark_safe(accreditation_list_string)

    class Meta:
        model = Practice
        template_name = "django_tables2/bootstrap5.html"
        fields = (
            'identification_number',
            'name',
            'owners_names_formatted',
            'zip_code',
            'city',
            'county',
            'accreditations'
        )


class SelectPracticesTable(Table):
    selection = CheckBoxColumn(verbose_name="", accessor='pk')
    owners_names_formatted = Column(verbose_name=_('Owner(s)'), orderable=True)

    def render_name(self, value, record):
        practice_url = reverse("practices:detail", kwargs={"pk": record.id})
        return mark_safe(f'<a href="{practice_url}" id="id_practice_{record.id}_link">{record}</a>')

    class Meta:
        model = Practice
        template_name = "django_tables2/bootstrap5.html"
        fields = (
            'selection',
            'identification_number',
            'name',
            'owners_names_formatted',
            'zip_code',
            'city',
            'county'
        )