# Generated by Django 3.2.7 on 2022-01-22 09:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('practices', '0012_alter_practice_contract_document'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='diseasemanagementprogramoption',
            options={'ordering': ['sort_index']},
        ),
        migrations.AddField(
            model_name='diseasemanagementprogramoption',
            name='sort_index',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
    ]
