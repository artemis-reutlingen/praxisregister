# Generated by Django 4.2.7 on 2024-02-20 07:25

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('addresses', '0007_alter_county_unique_together_alter_county_full_name_and_more'),
        ('practices', '0063_remove_practice_owners_names_formatted'),
    ]

    operations = [
        migrations.AlterField(
            model_name='practice',
            name='contact_website',
            field=models.CharField(blank=True, default=None, max_length=255, null=True, validators=[django.core.validators.URLValidator], verbose_name='Website'),
        ),
        migrations.AlterField(
            model_name='practice',
            name='county',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='addresses.county', verbose_name='County'),
        ),
        migrations.AlterField(
            model_name='practice',
            name='federal_state',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='addresses.federalstate', verbose_name='Federal state'),
        ),
        migrations.AlterField(
            model_name='practice',
            name='structure_practice_operating_until',
            field=models.DateField(blank=True, null=True, verbose_name='Operation is guaranteed until'),
        ),
        migrations.AlterField(
            model_name='site',
            name='county',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='addresses.county', verbose_name='County'),
        ),
        migrations.AlterField(
            model_name='site',
            name='federal_state',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='addresses.federalstate', verbose_name='Federal state'),
        ),
    ]
