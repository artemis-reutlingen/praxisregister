# Generated by Django 4.1.7 on 2023-08-01 12:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('practices', '0050_practiceengagement_import_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='practice',
            name='structure_non_gp_doctors_that_are_in_the_practice',
        ),
        migrations.AlterField(
            model_name='practice',
            name='structure_non_gp_doctors_are_working_in_the_practice',
            field=models.CharField(choices=[('YES', 'Yes'), ('NO', 'No'), ('UNKNOWN', 'Unknown')], default='UNKNOWN', max_length=128, verbose_name='Non-GP doctors in the practice'),
        ),
    ]
