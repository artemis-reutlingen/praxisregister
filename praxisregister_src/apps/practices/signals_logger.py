from django.dispatch import receiver
import logging
from practices.views.practice import practice_create_log
from practices.views.practice_base_data import practice_base_data_log
from practices.views.contract import practice_contract_log
from practices.views.practice_structure import practice_structure_data_edit_log, practice_structure_data_add_delete_log
from practices.views.sites import practice_additional_site_log
from practices.views.it_infrastructure import practice_it_infrastructure_log
from practices.views.notepad import practice_notepad_log
from practices.views.allowed_institutes import allowed_institutes_added_log, allowed_institutes_removed_log
from accreditation.views import accreditation_log
from contactnotes.views import contactnotes_log
from documents.views import documents_log
from persons.views import persons_crud_log, persons_change_role_log, persons_additional_info_log
from processes.views import processes_log
from django.contrib.auth.signals import user_logged_in, user_logged_out, user_login_failed
from events.views.participation_views import events_edit_log, events_participation_edit_log,events_bulk_creation_log
from studies.views import studies_log, study_participation_log, study_bulk_practice_log, study_bulk_people_log
from todos.views import todo_log
from users.views import users_profil_log

user_logger=logging.getLogger('user_activity')

#Practice logging
@receiver(practice_create_log)
def create_practice(sender, username, id, **kwargs):
    user_logger.info(f"{username} created practice with id {id}")

@receiver(practice_contract_log)
def practice_contract_log(sender, username, id, **kwargs):
    user_logger.info(f"{username} changed contract file of practice id {id}")

@receiver([practice_base_data_log, practice_structure_data_edit_log, practice_it_infrastructure_log, practice_notepad_log])
def structure_data_edit(sender, username, id, **kwargs):
    user_logger.info(f"{username} edited {sender.__name__} data of practice id {id}")

@receiver(practice_structure_data_add_delete_log)
def practice_structure_data_add_delete_log(sender, username, type, id, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} with {type} data on practice id {id}")

@receiver(practice_additional_site_log)
def practice_additional_site_log(sender, username, site_id, practice_id, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on site with id {site_id} of practice id {practice_id}")

#Accreditation logging
@receiver(accreditation_log)
def accreditation_log(sender, username, accreditation_id, practice_id, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on accreditation with type {accreditation_id} of practice id {practice_id}")

#Contact notes logging
@receiver(contactnotes_log)
def contactnotes_log(sender, username, contact_note_id, practice_id, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on contact_note {contact_note_id} of practice id {practice_id}")

#Documents logging
@receiver(documents_log)
def documents_log(sender, username, document_id, practice_id, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on document {document_id} of practice id {practice_id}")

#Persons logging
@receiver(persons_crud_log)
def persons_crud_log(sender, username, person_id, practice_id, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on person with id {person_id} of practice id {practice_id}")

@receiver(persons_change_role_log)
def persons_change_role_log(sender, username, person_id, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on person with id {person_id}")

@receiver(persons_additional_info_log)
def persons_additional_info_log(sender, username, person_id, model_id, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} with model id {model_id} on person {person_id}")

#Processes logging
@receiver(processes_log)
def processes_log(sender, username, process, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on process defintion with id {process.id} and name {process.name} ")
    
#User logging
#Login/Logout logging using djangos contrib.auth.signals 
@receiver(user_logged_in)
def log_user_login(sender, request, user, **kwargs):
    user_logger.info(f"{user.username} successful login")

@receiver(user_login_failed)
def log_user_login(sender, credentials, request, **kwargs):
    user_logger.info(f"{credentials.get('username')} failed to log in")

@receiver(user_logged_out)
def log_user_login(sender, request, user, **kwargs):
    user_logger.info(f"{user.username} logged out")

@receiver(users_profil_log)
def users_profil_log(sender, username, **kwargs):
    user_logger.info(f"{username} used  {sender.__name__}")

#Event logging
@receiver(events_edit_log)
def events_edit_log(sender, username, event, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on event with id {event.id} with title {event.title}")

@receiver(events_bulk_creation_log)
def events_bulk_creation_log(sender, username, event_id, participations, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on event {event_id} with ids {participations}")

@receiver(events_participation_edit_log)
def events_participation_edit_log(sender, username, participation_id, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on event_participation with id {participation_id}")

#Study logging
@receiver(studies_log)
def studies_log(sender, username, study, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on study with id {study.id} with title {study.title}")

@receiver(study_participation_log)
def study_participation_log(sender, username, study_id, practice_id, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on study with id {study_id} with practice {practice_id}")

@receiver(study_bulk_practice_log)
def study_bulk_practice_log(sender, username, study_id, participations, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on study {study_id} with ids {participations}")

@receiver(study_bulk_people_log)
def study_bulk_people_log(sender, username, study_id, participations, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on study {study_id} with ids {participations}")

@receiver(todo_log)
def todo_log(sender, username, todo, **kwargs):
    user_logger.info(f"{username} used {sender.__name__} on todo with id {todo.id}")

#Allowed institutes
@receiver(allowed_institutes_removed_log)
def allowed_institutes_removed_log(sender, username, removed_institutes, **kwargs):
    user_logger.info(f"{username} removed institutes with id {[i.id for i in removed_institutes]}")

@receiver(allowed_institutes_added_log)
def allowed_institutes_added_log(sender, username, added_institutes, **kwargs):
    user_logger.info(f"{username} added institutes with id {[i.id for i in added_institutes]}")
    