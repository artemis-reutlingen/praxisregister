from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator


def validate_file_size(file):
    max_size_kb = 5000
    error_msg = _('Files cannot be larger than')

    if file.size > max_size_kb * 1024:
        raise ValidationError(f"{error_msg} {max_size_kb/1000}MB!")