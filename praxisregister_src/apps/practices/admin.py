from django.contrib import admin
from guardian.admin import GuardedModelAdmin

from django.contrib import admin
from practices.models import Practice, Site, OperatingSystemOption, PDMSSoftwareOption, DiseaseManagementProgramOption
from practices.models.additional_qualification import PracticeAssistantsAdditionalQualification
from django.utils.translation import gettext_lazy as _


class OperatingSystemOptionAdmin(admin.ModelAdmin):
    model = OperatingSystemOption

class PDMSSoftwareOptionAdmin(admin.ModelAdmin):
    model = PDMSSoftwareOption

class PracticeAdmin(GuardedModelAdmin):
    model = Practice
    list_display = ('id', 'updated_at', 'name', 'address', 'assigned_institute',)
    list_filter = ['assigned_institute', 'is_active']
    search_fields = ['name__icontains', 'zip_code__startswith', 'city__startswith', 'assigned_institute__name__icontains', 'assigned_institute__organisation__icontains']


    @admin.display(description='Address')
    def address(self, obj):
        return f"""
        {obj.street} {obj.house_number}, {obj.zip_code} \
        {obj.city}
        """

class SiteAdmin(admin.ModelAdmin):
    model = Site
    list_display = (
        'id',
        'practice',
        'address_repr',
        'identification_number',
    )

    @admin.display(description='Address')
    def address_repr(self, obj):
        return f"""
        {obj.street} {obj.house_number}, {obj.zip_code} {obj.city}
        """
    
class AdditionalQualificationAdmin(admin.ModelAdmin):
    model = PracticeAssistantsAdditionalQualification
    list_display = (
        'id',
        'practice',
        'type',
    )  

class DiseaseManagementProgramOptionAdmin(admin.ModelAdmin):
    model = DiseaseManagementProgramOption
    list_display = (
        'id',
        'name',
        'sort_index',
    )


PracticeAdmin.model._meta.verbose_name_plural = "1. " + _("Practices")
SiteAdmin.model._meta.verbose_name_plural = "2. " + _("Sites")
OperatingSystemOptionAdmin.model._meta.verbose_name_plural = "3. Operating system options"
PDMSSoftwareOptionAdmin.model._meta.verbose_name_plural = "4. PDMS software options"
DiseaseManagementProgramOptionAdmin.model._meta.verbose_name_plural = "5. Disease management program options"
AdditionalQualificationAdmin.model._meta.verbose_name_plural = "6. Additional qualifications (staff)"

admin.site.register(Practice, PracticeAdmin)
admin.site.register(Site, SiteAdmin)
admin.site.register(OperatingSystemOption, OperatingSystemOptionAdmin)
admin.site.register(PDMSSoftwareOption, PDMSSoftwareOptionAdmin)
admin.site.register(DiseaseManagementProgramOption, DiseaseManagementProgramOptionAdmin)
admin.site.register(PracticeAssistantsAdditionalQualification, AdditionalQualificationAdmin)

