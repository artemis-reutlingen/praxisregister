from django import template

register = template.Library()


@register.filter
def verbose(instance, arg):
    """
    helper filter to access "verbose_name" attribute in template generically
    """
    return instance._meta.get_field(arg).verbose_name
