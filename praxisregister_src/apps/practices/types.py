from django.utils.translation import gettext_lazy as _
from django.db import models

class YesNoUnknowType(models.TextChoices):
    YES = "YES", _("Yes")
    NO = "NO", _("No")
    UNKNOWN = "UNKNOWN", _("Unknown")
    
class BillsPerQuarterType(models.TextChoices):
    UNKNOWN = 'UNKNOWN', _('Unknown')
    UNDER_500 = 'UNDER_500', _('Under 500')
    FROM_500_TO_999 = 'FROM_500_TO_999', '500-999'
    FROM_1000_TO_1499 = 'FROM_1000_TO_1499', '1000-1499'
    FROM_1500_TO_1999 = 'FROM_1500_TO_1999', '1500-1999'
    FROM_2000_TO_2499 = 'FROM_2000_TO_2499', '2000-2499'
    FROM_2500_TO_2999 = 'FROM_2500_TO_2999', '2500-2999'
    FROM_3000_TO_3499 = 'FROM_3000_TO_3499', '3000-3499'
    FROM_3500_TO_4000 = 'FROM_3500_TO_4000', '3500-4000'
    MORE_THAN_4000 = 'MORE_THAN_4000', _('More than 4000')

class ServicesPracticeOffersType(models.TextChoices):
    ULTRASOUND_ABDOMEN = "ULTRASOUND_ABDOMEN", _("Ultrasound (diagnostic) abdomen")
    ULTRASOUND_THYROID_GLAND = "ULTRASOUND_THYROID_GLAND", _("Ultrasound (diagnostic) thyroid gland")
    ULTRASOUND_DOPPLER_DIAGNOSTICS = "ULTRASOUND_DOPPLER_DIAGNOSTICS", _("Ultrasound (diagnostic) Doppler diagnostics")
    ERGOMETRY = "ERGOMETRY", _("Ergometry")	
    LONG_TERM_ECG = "LONG_TERM_ECG", _("Long term ECG")
    ACTICE_IN_DMP = "ACTICE_IN_DMP", _("Active participation in the DMP (>10 patients per quarter)")
    SKIN_CANCER_SCREENING = "SKIN_CANCER_SCREENING", _("Skin cancer screening")
    PSYCHOSOMATIC_PRIMARY_CARE = "PSYCHOSOMATIC_PRIMARY_CARE", _("Psychosomatic primary care")
    LUNG_FUNCTION = "LUNG_FUNCTION", _("Lung function")
    MINOR_SURGERY = "MINOR_SURGERY", _("Minor surgery")
    PRESENCE_LABORATORY = "PRESENCE_LABORATORY", _("Presence laboratory (for troponin, D-dimer, etc.)")

class SiteType(models.TextChoices):
    PRIMARY = 'MAIN', _('Primary site')
    SECONDARY = 'SIDE', _('Secondary site')

class UrbanisationLevel(models.TextChoices):
    UNKNOWN = 'UNKNOWN', _('Unknown')
    UNDER_5000_INHABITANTS = 'UNDER_5000_INHABITANTS', _('Under 5000 inhabitants')
    FROM_5000_TO_19999_INHABITANTS = 'FROM_5000_TO_19999_INHABITANTS', _('5000-19999 inhabitants')
    FROM_20000_TO_99999_INHABITANTS = 'FROM_20000_TO_99999_INHABITANTS', _('20000-99999 inhabitants')
    FROM_100000_TO_499999_INHABITANTS = 'FROM_100000_TO_499999_INHABITANTS', _('100000-499999 inhabitants')
    MIN_500000_INHABITANTS = 'MIN_500000_INHABITANTS', _('Minimum 500000 inhabitants')

class CommunicationSpokenLanguageType(models.TextChoices):
    GERMAN = 'GERMAN', _('German')
    ENGLISH = 'ENGLISH', _('English')
    FRENCH = 'FRENCH', _('French')
    ITALIAN = 'ITALIAN', _('Italian')
    SPANISH = 'SPANISH', _('Spanish')
    ARABIC = 'ARABIC', _('Arabic')
    GREEK = 'GREEK', _('Greek')
    DUTCH = 'DUTCH', _('Dutch')
    POLISH = 'POLISH', _('Polish')
    RUSSIAN = 'RUSSIAN', _('Russian')
    TURKISH = 'TURKISH', _('Turkish')

class CommunicationDigitalWithColleaguesType(models.TextChoices):
    UNKNOWN = 'UNKNOWN', _('Unknown')
    EMAIL_PERSONAL = 'EMAIL_PERSONAL', _('Personal email')
    EMAIL_FORUM = 'EMAIL_FORUM', _('Email forum')
    KV_CONNECT = 'KV_CONNECT', _('KV-Connect')
    VIDEO_CONFERENCE = 'VIDEO_CONFERENCE', _('Video conference')
    MESSENGER = 'MESSENGER', _('Messenger')
    DATA_CARRIER = 'DATA_CARRIER', _('Data carrier')

class CommunicationWrittenWithColleaguesType(models.TextChoices):
    FULL_DIGITAL = 'FULL_DIGITAL', _('Completely digitized')
    MOSTLY_DIGITAL = 'MOSTLY_DIGITAL', _('Mostly digitized')
    HALF_DIGITAL = 'HALF_DIGITAL', _('Half digitized, half analog')
    MOSTLY_ANALOG = 'MOSTLY_ANALOG', _('Mostly analog (paper form)')
    FULL_ANALOG = 'FULL_ANALOG', _('Full analog')
    UNKNOWN = 'UNKNOWN', _('Unknown')

class CommunicationTypeWithPatientsType(models.TextChoices):
    UNKNOWN = 'UNKNOWN', _('Unknown')
    EMAIL = 'EMAIL', _('Email')
    PATIENT_APP = 'PATIENT_APP', _('Patient app')
    ONLINE_PORTAL = 'ONLINE_PORTAL', _('Online portal')
    VIDEO_CONSULTATION = 'VIDEO_CONSULTATION', _('Video consultation')
    MESSENGER = 'MESSENGER', _('Messenger')

class TreatmentsByAgeGroupType(models.TextChoices):
    UNDER_4 = 'UNDER_4', _('Under 4 years')
    FROM_5_TO_18 = 'FROM_5_TO_18', _('5-18 years')
    FROM_19_TO_54 = 'FROM_19_TO_54', _('19-54 years')
    FROM_55_TO_75 = 'FROM_55_TO_75', _('55-75 years')
    OVER_75 = 'OVER_75', _('Over 75 years')
