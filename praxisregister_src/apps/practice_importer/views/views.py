import logging
from typing import Any

from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.http.response import Http404
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.decorators.http import require_POST
from django.views.generic import FormView, TemplateView
from importer.models import Mapper, MapperField, ChunkImport
from importer.views.importer_history_views import create_data_import_object
from importer.resources import CustomResource
from importer.views.importer_utils import *
from importer.views.chunk_views import check_for_existing_import
from persons.models import Person, ResearchExperience, ResearchTraining
from practice_importer.constants import *
from practice_importer.forms import (CustomConfirmForm, CustomImportFileForm)
from practice_importer.resources import PracticeResource, PersonResource
from practice_importer.views.default_importer_utils import *
from practices.models.engagement import PracticeEngagement
from practices.models.practice import Practice
from practices.models.site import Site
from practice_importer.types import ImportType
from persons.models.roles import DoctorRole, AssistantRole, OwnerRole
from kbv_importer.import_builder import ImportObject

logger = logging.getLogger(__name__)


class ImportOverview(TemplateView):
    template_name = "practice_importer/import_overview.html"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        
        actions = []
        if self.request.user.has_perm("institutes.can_clear_data", self.request.user.assigned_institute):
            actions.append("CLEAR_DATA")
        context["actions"] = actions
        return context
    
def config_practice(request):
    institute = request.user.assigned_institute

    #main model
    practice_mapper, created = get_or_create_mapper(Practice, institute)

    #related models
    site_mapper, created = get_or_create_mapper(Site, institute, Practice)
    engagement_mapper, created = get_or_create_mapper(PracticeEngagement, institute, Practice)
    #add new models here and add them to the dictionarys below

    mappers = [practice_mapper, site_mapper, engagement_mapper]
    
    # mapper : foreignkey field name
    foreignkey_dict = {site_mapper : "practice",
                     engagement_mapper : "practice"}
    
    redirect_url = reverse("practice_importer:import-practice")
    return default_mapper_fields_creation(request, practice_mapper, mappers, foreignkey_dict, {}, redirect_url)

def config_person(request):
    institute = request.user.assigned_institute

    #main model
    person_mapper, created = get_or_create_mapper(Person, institute)

    #related models
    research_experience_mapper, created = get_or_create_mapper(ResearchExperience, institute, Person)
    research_training_mapper, created = get_or_create_mapper(ResearchTraining, institute, Person)
    doctor_mapper, created = get_or_create_mapper(DoctorRole, institute, Person)
    assistant_mapper, created = get_or_create_mapper(AssistantRole, institute, Person)
    owner_mapper, created = get_or_create_mapper(OwnerRole, institute, Person)
    #add new models here and add them to the dictionarys below

    mappers = [person_mapper,
                  research_experience_mapper,
                  research_training_mapper,
                  doctor_mapper,
                  assistant_mapper,
                  owner_mapper,
                  ]

    foreignkey_dict = {research_experience_mapper : "person",
                      research_training_mapper : "person",
                      }
    
    one_to_one_dict = {
                      doctor_mapper : "person",
                      assistant_mapper : "person",
                      owner_mapper : "person",
    }
    
    # Only use the import_id field of a practice to match the person to a practice
    practice_mapper, created = get_or_create_mapper(Practice, institute, Person)
    custom_fields = { practice_mapper : Practice._meta.get_field("import_id")}
    
    redirect_url = reverse("practice_importer:import-person")
    return default_mapper_fields_creation(request, person_mapper, mappers, foreignkey_dict, one_to_one_dict, redirect_url, custom_fields)


def clear_institute_data(request):
    """Irreversibly deletes all data from the institute of the user.
    Only intended to be used for testing purposes!"""
    if not request.user.assigned_institute:
        raise Http404

    institute = request.user.assigned_institute
    logger.warning(f"User '{request.user.username}' has requested deletion of all data of institute '{institute.name}'.")

    if not request.user.has_perm("institutes.can_clear_data", institute):
        raise PermissionDenied

    from persons.models import Person
    Person.objects.filter(practice__assigned_institute = institute).all().delete()
    institute.practice_set.all().delete()
    institute.study_set.all().delete()
    institute.eventinfo_set.all().delete()
    request.user.processdefinition_set.all().delete()

    logger.info(f"All data of institute '{institute.name}' has been deleted upon request by user '{request.user.username}'.")
    messages.success(request, _("All institute data has been deleted successfully."))
    return redirect("practice_importer:importer-overview")




def detailed_pratice_import(import_object: ImportObject, dry_run=True):
        institute = import_object.user.assigned_institute

        #First gather the main model mapper and its related models mapper
        practice_mapper, created = get_or_create_mapper(Practice, institute)
        if created:
            raise Exception("No existing mapper for this import was found")
        
        import_object.set_resource(PracticeResource(practice_mapper, import_object.groups)) \
        .set_mappers(Mapper.objects.filter(name__contains=str(institute.id) + str(Practice._meta.model_name).lower() + MAPPER_NAME_ADDITION)) \
        .set_main_content_type(practice_mapper.content_type)

        return custom_import(import_object, dry_run)

def detailed_person_import(import_object: ImportObject, dry_run=True):
        institute = import_object.user.assigned_institute

        person_mapper, created = get_or_create_mapper(Person, institute)
        mappers = Mapper.objects.filter(name__contains=str(institute.id) + str(Person._meta.model_name).lower() + MAPPER_NAME_ADDITION)
        mappers = mappers.exclude(pk=person_mapper.pk)      

        #This is for connecting the person to a practice (Code can be improved)
        practice_person_mapper, created = get_or_create_mapper(Practice, institute, Person)
        if created:
            raise Exception("No existing mapper for this import was found")
        mappers = mappers.exclude(pk=practice_person_mapper.pk)      
        practice_import_id_field = MapperField.objects.filter(mapper=practice_person_mapper, internal_fieldname="import_id").first()
        add_pratice_field_to_dataset(import_object.sub_dataset, practice_person_mapper, person_mapper, practice_import_id_field, import_object.groups)
        import_object.set_resource(PersonResource(person_mapper, import_object.groups)) \
        .set_mappers(mappers) \
        .set_main_content_type(person_mapper.content_type)
        return custom_import(import_object, dry_run)

"""
Temporarily store the uploaded file in the temp storage and create a dataset from it
Show preview of the dataset to the user
"""
class ImportView(FormView):

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # Let the user decide to which of his groups the imported objects should belong
        kwargs["groups"] = self.request.user.groups.all()
        return kwargs
    
    def form_valid(self, form):

        import_file, dataset = create_dataset_from_import_form(form)
        
        import_object = ImportObject().set_import_type(self.import_type) \
        .set_model_import_type(self.import_type) \
        .set_groups(form.cleaned_data["groups"]) \
        .set_input_format(form.cleaned_data["input_format"]) \
        .set_user(self.request.user) \
        .set_storage_dataset(dataset) \
        .set_original_file_name(import_file.tmp_storage_name) \
        .set_file_name(import_file.name) \
        .set_chunk_import(ChunkImport.objects.update_or_create(
            institute=self.request.user.assigned_institute,
            import_type=self.import_type,
            defaults={"last_user": self.request.user,
                      "file_name": import_file.name},
        )[0]) \
        .set_confirm_form_class(self.confirm_form) \
        .set_import_url(self.import_url)

        context = prepare_confirm_form(self.request, import_object, self.import_method)
        
        # We render the same template but pass the confirm form with the context
        return self.render_to_response(context)

class PracticeImportView(ImportView):
    template_name = 'practice_importer/import_file_practice.html'
    form_class = CustomImportFileForm

    def __init__(self, **kwargs: Any) -> None:
        self.import_type = ImportType.PRACTICES
        self.import_url = "practice_importer:import-practice"
        self.import_method = detailed_pratice_import
        self.confirm_form = CustomConfirmForm
        super().__init__(**kwargs)

class PersonImportView(ImportView):
    template_name = 'practice_importer/import_file_person.html'
    form_class = CustomImportFileForm

    def __init__(self, **kwargs: Any) -> None:
        self.import_type = ImportType.PERSONS
        self.import_url = "practice_importer:import-person"
        self.import_method = detailed_person_import
        self.confirm_form = CustomConfirmForm
        super().__init__(**kwargs)


# Import data after the user has confirmed the import
@require_POST
def process_confirm_form(request, *args, **kwargs):
    """
    Perform the actual import action (after the user has confirmed the import)
    """
    kwargs = {"groups": request.user.groups.all()}

    logger.debug("Processing import")
    confirm_form = CustomConfirmForm(data=request.POST, **kwargs)
    if confirm_form.is_valid():
        logger.debug("Confirm form is valid")

        import_type = confirm_form.cleaned_data["import_type"]
        if(import_type == ImportType.PRACTICES):
            import_url = "practice_importer:import-practice"
            template_name = "practice_importer/import_file_practice.html"
            import_method = detailed_pratice_import
        elif(import_type == ImportType.PERSONS):
            import_url = "practice_importer:import-person"
            template_name = "practice_importer/import_file_person.html"
            import_method = detailed_person_import
 
        storage_dataset = load_dataset_from_temp_storage(confirm_form.cleaned_data["input_format"], confirm_form.cleaned_data["original_file_name"], remove_storage=False)
        chunk_import = ChunkImport.objects.get(institute=request.user.assigned_institute, import_type=import_type)
        
        import_object = ImportObject().set_confirm_form_data(confirm_form) \
            .set_import_type(import_type) \
            .set_model_import_type(import_type) \
            .set_user(request.user) \
            .set_storage_dataset(storage_dataset) \
            .set_chunk_import(chunk_import) \
            .set_data_import(create_data_import_object(request.user, confirm_form.cleaned_data["groups"])) \
            .set_import_url(import_url) \
            .set_html_template(template_name)

        import_result = import_method(import_object, dry_run=False)
        
        logger.info(f"Detailed {import_object.model_import_type} import by user {request.user}")

        handle_import_result(request, import_result, import_object.data_import)

        return check_for_ongoing_import(import_object, import_result, request, import_method)

    else:
        messages.error(request, _('Form invalid.'))
        logger.error("Confirm form invalid")
    return redirect(import_url) 

def custom_import(import_object: ImportObject, dry_run=True, import_id_field_name="import_id", import_by_field_name="import_by"):    

        #Second we (temporary) import our main model, in order to get the primary keys
        import_result = import_object.resource.import_data(
                import_object.sub_dataset,
                dry_run=dry_run,
                raise_errors=True,
                file_name=import_object.original_file_name,
                user=import_object.user,
                groups=import_object.groups,
                #Import history only for the main model
                create_import_history=not dry_run,
                content_type=import_object.main_content_type,
                data_import=import_object.data_import,
            )
        if import_object.data_import and not dry_run:
            import_object.data_import.headers = import_result.diff_headers
            import_object.data_import.save()

        logger.debug("Starting to import related models")
        #Then we add the primary keys to the dataset, so that we can import the related models
        primary_keys_main_model = [row.object_id for row in import_result.rows]
        logger.debug(f"Primary keys of main model: {primary_keys_main_model}")
        import_object.sub_dataset.append_col(primary_keys_main_model, header=f"PRM_{FOREIGNKEY_IMPORT_FIELDNAME}_id")

        for mapper in import_object.mappers:
            if mapper.mapperfield_set.all().count() > 0:
                
                process_one_to_one_fields(mapper, import_object, primary_keys_main_model, dry_run)
                
                import_object.resource = CustomResource(mapper, import_object.groups,  dry_run=dry_run)
                foreignkey_model_result = import_object.resource.import_data(
                    import_object.sub_dataset,
                    dry_run=dry_run,
                    raise_errors=True,
                    file_name=import_object.original_file_name,
                    user=import_object.user,
                )

                import_result.base_errors.extend(foreignkey_model_result.base_errors)
                logger.debug(f"Import result diff headers: {foreignkey_model_result.diff_headers}")
                
                logger.debug(f"Removing duplicate header fields")
                remove_from_foreignkey_diff = [count for count, field in enumerate(import_object.resource.get_fields()) if field.attribute == import_by_field_name]
                for index in sorted(remove_from_foreignkey_diff, reverse=True):
                    foreignkey_model_result.diff_headers.pop(index)
                                    
                import_result.diff_headers.extend(foreignkey_model_result.diff_headers)
                import_result.invalid_rows.extend(foreignkey_model_result.invalid_rows)

                for count, row in enumerate(import_result.rows):
                    try:
                        for index in sorted(remove_from_foreignkey_diff, reverse=True):
                            foreignkey_model_result.rows[count].diff.pop(index)
                        row.diff.extend(foreignkey_model_result.rows[count].diff)
                    except Exception as e:
                        logger.error(f"Error while adding diff to row {e}")
        return import_result

def process_one_to_one_fields(mapper, import_object, primary_keys_main_model, dry_run):
    """
    Process the OneToOne field in the mapper field.
    """
    for mapper_field in mapper.mapperfield_set.all():
        if mapper_field.is_one_to_one_field():
            logger.debug(f"Found OneToOne field {mapper_field.import_fieldname}")

            import_id_field = MapperField.objects.get(mapper=mapper, internal_fieldname='import_id')
            import_id_column = import_object.sub_dataset[import_id_field.import_fieldname]

            new_col = []
            for index, import_id_row in enumerate(import_id_column):
                if import_id_row:
                    new_col.append(primary_keys_main_model[index])
                else:
                    new_col.append(None)

            new_col_name = f"PRM_{mapper.content_type.model_class()._meta.model_name}_id"
            import_object.sub_dataset.append_col(new_col, header=new_col_name)
            mapper_field.import_fieldname = new_col_name
            mapper_field.save()

            if not dry_run:
                logger.debug("deleting oneToOne relations")
                main_model = import_object.main_content_type.model_class()
                filter = main_model._meta.model_name + "__id__in"
                to_delete = mapper_field.mapper.content_type.model_class().objects.filter(**{ filter: primary_keys_main_model })
                logger.debug(f"deleting {list(to_delete)} rows")
                to_delete.delete()

"""
Uses the practice import_id in order to get the primary keys of the practices and add them to the dataset
The primary keys in the dataset can then be used to connect the person to a practice
"""
def add_pratice_field_to_dataset(dataset, practice_person_mapper, person_mapper, practice_import_id_field, groups):
    field_name = f"PRM_practice_id" #naming comes from get_foreignkey_mapperField_import_fieldname method
    if practice_import_id_field:
        if MapperField.objects.filter(mapper=person_mapper, internal_fieldname="practice").count() == 0:
            mapper_field = add_related_model_as_foreignkey_to_mapper(practice_person_mapper, person_mapper, "practice")
            mapper_field.import_fieldname = field_name
            mapper_field.save()

        #We need to check if the this specific groups already have practices with this import_id
        practice_import_ids = [str(field) for field in dataset[practice_import_id_field.import_fieldname]]
        practices = []
        for group in groups:
            # Get all practices for the group (permission based)
            practices.extend(list(Practice.objects.get_practice_objects_for_group(group).values_list('id', flat=True)))
        #get a dictionary with import_id as key and the primary key as value
        existing_practices = Practice.objects.filter(import_id__in=practice_import_ids, id__in=practices).values('import_id', 'id')
        existing_practices = {practice['import_id']: practice['id'] for practice in existing_practices}
        practice_ids = []
        for import_id in practice_import_ids:
            if import_id in existing_practices.keys():
                practice_ids.append(existing_practices[import_id])
            else:
                practice_ids.append(None)
        dataset.append_col(practice_ids, header=field_name)


def check_for_existing_practice_import(request):
    return check_for_existing_import(request, ImportType.PRACTICES, "practice_importer:import-practice")

def check_for_existing_person_import(request):
    return check_for_existing_import(request, ImportType.PERSONS, "practice_importer:import-person")