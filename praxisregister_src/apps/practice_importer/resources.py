import logging
from django.utils.translation import gettext_lazy as _
from importer.resources import CustomResource
from practices.models.practice import Practice
from institutes.models import Institute
from persons.models import Person


logger = logging.getLogger(__name__)

class PracticeResource(CustomResource):

    def __init__(self, mapper, groups, import_id="import_id", import_by="import_by"):
        super().__init__(mapper, groups, import_id=import_id, import_by=import_by)
        self.institutes = list(Institute.objects.filter(group__in = self.groups))

    def get_queryset(self):
        practices = []
        for group in self.groups:
            practices.extend(list(Practice.objects.get_practice_objects_for_group(group).values_list('id', flat=True)))
        return Practice.objects.filter(id__in=practices)
    
    # overrides custom resource method
    def before_save_instance(self, instance, using_transactions, dry_run):
        if not dry_run and isinstance(instance, Practice) and self.user:
            instance.assigned_institute = self.user.assigned_institute

    # overrides custom resource method
    def after_save_instance(self, instance, using_transactions, dry_run):
        if not dry_run and self.institutes:
            add_permissions(instance, Practice, self.institutes)

class PersonResource(CustomResource):

    def __init__(self, mapper, groups):
        super().__init__(mapper, groups)
        self.institutes = list(Institute.objects.filter(group__in = self.groups))

    def get_queryset(self):
        persons = []
        for group in self.groups:
            persons.extend(list(Person.objects.get_person_objects_for_group(group).values_list('id', flat=True)))
        return Person.objects.filter(id__in=persons)

    # overrides custom resource method
    def after_save_instance(self, instance, using_transactions, dry_run):
        if not dry_run and self.institutes:
            add_permissions(instance, Person, self.institutes)

def add_permissions(instance, model, institutes):
    for institute in institutes:
        instance.assign_perms(institute)
