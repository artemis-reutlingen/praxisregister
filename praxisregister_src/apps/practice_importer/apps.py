from django.apps import AppConfig


class PracticeImporterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'practice_importer'
