from django.db import models
from django.utils.translation import gettext_lazy as _

class ImportType(models.TextChoices):
    PRACTICES = "PRACTICE", _("Practice")
    PERSONS = "PERSONS", _("Persons")
