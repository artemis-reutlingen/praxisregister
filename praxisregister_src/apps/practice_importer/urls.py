from django.urls import path
from practice_importer.views import views

app_name = 'practice_importer'

urlpatterns = [

    path('', views.ImportOverview.as_view(), name="importer-overview"),
    path('practice-config', views.config_practice, name="practice-config"),
    path('person-config', views.config_person, name="person-config"),
    path('check-practice-import', views.check_for_existing_practice_import, name="check_for_existing_practice_import"),
    path('check-person-import', views.check_for_existing_person_import, name="check_for_existing_person_import"),
    path('import-practice', views.PracticeImportView.as_view(), name="import-practice"),
    path('import-person', views.PersonImportView.as_view(), name="import-person"),
    path('import', views.process_confirm_form, name="import"),
    path('clear-data', views.clear_institute_data, name="clear-data"),

]
