import logging

from django import forms
from django.utils.translation import gettext_lazy as _
from importer.forms import ImportFileForm
from practice_importer.types import ImportType
from .constants import *
from import_export.forms import ConfirmImportForm
from practices.models.practice import PracticeStructureMixin

logger = logging.getLogger(__name__)


class MapperFieldsImportForm(forms.Form):

    def __init__(self, *args, **kwargs):
        mappers = kwargs.pop("mappers", {})
        custom_fields = kwargs.pop("custom_fields", {})
        super().__init__(*args, **kwargs)

        # Possibility to add custom fields
        for mapper, field in custom_fields.items():
            self.fields[mapper.name + field.name] = forms.CharField(label=f"{self.get_model_name(mapper)}: {field.verbose_name}", max_length=255, required=False)

        # Iterate over all mappers and add a field for each field in the model, we also add an identifier to the field name
        for mapper in mappers:
            fields = {}
            for field in mapper.content_type.model_class()._meta.get_fields():
                # if not field.related_model and not field.primary_key and not field.name in ["import_by", "updated_at", "created_at"]:
                if not field.related_model and not field.primary_key and not field.name in ["import_by", "created_at"]:
                    fields[mapper.name + field.name] = forms.CharField(label=f"{self.get_model_name(mapper)}: {field.verbose_name}", max_length=255, required=False)
                if field.name == "import_id":
                    fields[mapper.name + field.name].help_text = _("If you want to use fields of this type, please add the field name of your import ID here (Practice or Person ID).")
                    mapper_model = mapper.content_type.model_class()._meta.model_name
                    if mapper_model in ["practice", "person"]:
                        fields[mapper.name + field.name].required = True

            # Sort fields by label
            fields = dict(sorted(fields.items(), key=lambda item: item[1].label))
            form_field = mapper.name + "import_id"
            # Move import_id field to the top
            fields = {form_field: fields.pop(form_field), **fields}
            self.fields.update(fields)

    def get_model_name(self, mapper):
        model_class = mapper.content_type.model_class()
        return model_class._meta.verbose_name if model_class else mapper.content_type.model 


class CustomImportFileForm(ImportFileForm):

    def __init__(self, *args, **kwargs):
        groups = kwargs.pop("groups")
        super().__init__(*args, **kwargs)
        self.fields['groups'].queryset = groups

    groups = forms.ModelMultipleChoiceField(
        label=_('Groups'),
        queryset=None, # set in __init__
        required=True,
    )

class CustomConfirmForm(ConfirmImportForm):

    def __init__(self, *args, **kwargs):
        groups = kwargs.pop("groups")
        super().__init__(*args, **kwargs)
        self.fields['groups'].queryset = groups
        
    import_type = forms.ChoiceField(choices=ImportType.choices, required=True, widget=forms.HiddenInput())
    groups = forms.ModelMultipleChoiceField(widget=forms.MultipleHiddenInput, queryset=None)

