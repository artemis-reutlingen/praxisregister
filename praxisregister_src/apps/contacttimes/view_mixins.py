import abc

from django.forms import modelformset_factory
from django.http import HttpResponseRedirect

from contacttimes.forms import TimeSlotForm
from contacttimes.models import DayContactTimeSlot


class ContactTimesEditMixin(abc.ABC):
    """
    Mixin for views that edit all contact time slot objects for a given related object at the same time.
    Probably room for improvement for idiomatic clean code / readability.
    TODO: ValidationErrors are not displayed, user just gets redirected to base data view.
    """

    formset_class = modelformset_factory(
        model=DayContactTimeSlot,
        form=TimeSlotForm,
        # Redundancy for defined fields (compare Form definition!) seems to be necessary
        fields=('early_start', 'early_end', 'late_start', 'late_end',)
    )

    @abc.abstractmethod
    def get_timeslots(self):
        raise NotImplementedError

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        timeslots = self.get_timeslots()
        formset = self.formset_class(queryset=timeslots)

        timeslot_entries_and_formset = zip(timeslots, formset)

        context['timeslot_entries_and_formset'] = timeslot_entries_and_formset
        context['management_form'] = formset.management_form
        if context.get('practice'):
            context['practice_id'] = context['practice'].pk
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset = self.formset_class(self.request.POST, queryset=self.get_timeslots())

        if form.is_valid():
            related_instance = self.get_object()
            for f in formset:
                if f.is_valid():
                    # Before committing the DayContactTimeSlot instance to the database, the related instance (practice/person) must be set.
                    instance = f.save(commit = False)
                    instance.content_object = related_instance
                    instance.save()
            return self.form_valid(form)
        return self.form_invalid(form)
    
    def form_valid(self, form):
        #do not save the form, as the formset is already saved
        self.object = form.instance
        return HttpResponseRedirect(self.get_success_url())