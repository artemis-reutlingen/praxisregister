from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

class DayContactTimeSlot(models.Model):
    # Generic foreign key so that any model can reference this model using a GenericRelation
    content_type = models.ForeignKey(ContentType, on_delete = models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")

    class Weekday(models.TextChoices):
        MONDAY = "MONDAY", _("Monday")
        TUESDAY = "TUESDAY", _("Tuesday")
        WEDNESDAY = "WEDNESDAY", _("Wednesday")
        THURSDAY = "THURSDAY", _("Thursday")
        FRIDAY = "FRIDAY", _("Friday")
        SATURDAY = "SATURDAY", _("Saturday")
        SUNDAY = "SUNDAY", _("Sunday")

    weekday = models.CharField(
        max_length=128,
        choices=Weekday.choices,
        blank=False, null=False,
        verbose_name=_("Weekday")
    )

    early_start = models.TimeField(verbose_name=_("Start (early)"), blank=True, null=True)
    early_end = models.TimeField(verbose_name=_("End (early)"), blank=True, null=True)

    late_start = models.TimeField(verbose_name=_("Start (late)"), blank=True, null=True)
    late_end = models.TimeField(verbose_name=_("End (late)"), blank=True, null=True)

    @property
    def is_set(self):
        """
        Helper property indicating whether any value was set.
        Used in template to only show days where a timeslot is defined.
        """
        return any(
            [
                self.early_start,
                self.early_end,
                self.late_start,
                self.late_end,
            ]
        )
    
    class Meta:
        indexes = [
            models.Index(fields = ["content_type", "object_id"])
        ]
        verbose_name = _("Contact time slot")
        verbose_name_plural = _("Contact time slots")
