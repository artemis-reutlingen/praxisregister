from enum import Enum

from django.utils.translation import gettext_lazy as _

EVENT_TYPES = [
    "Infoveranstaltung",
    "Studienspezifisch (z.B. Initiierung)",
    "Fort-/Weiterbildung (GCP, Basis, Aufbau)",
    "Freie Fortbildung",
    "Innovations-Workshop (evtl. inkl Patient)",
    "Praxenbeirat/Steuerungsgremien/Partizipationstreffen/runder Tisch (wiss. Personen)",
    "Visitation",
    "Weiterbildung innerhalb des Netzes (mehrere Praxen)",
    "Netzwerktreffen",
]

OPERATING_SYSTEM_OPTIONS = [
    "Windows 7",
    "Windows 10",
    "Windows 11",
    "Apple MacOS",
    "Linux",
]
PDMS_OPTIONS = [
    "TURBOMED",
    "MEDISTAR",
    "x.isynet",
    "x.concept",
    "ALBIS",
    "x.comfort",
    "QUINCY WIN",
    "Medical Office",
    "CGM M1 PRO",
    "medatixx",
    "DURIA",
    "EL - Elaphe Longissima",
    "T2med",
    "Data-AL",
    "S3-Win",
    "PegaMed",
    "tomedo",
    "Praxis-Programm",
    "EVA",
    "DATA VITAL",
    "QUINCY WIN",
    "Pro_Medico",
    "QMED.PRAXIS",
    "MEDICUS",
]
ASSISTANT_VOCATIONAL_TRAINING_OPTIONS = [
    "Kein Berufsabschluss",
    "Medizinische/-r Fachangestellte/-r, Arzthelfer/-in (MFA)",
    "Medizinisch-technische/-r Assistent/-in (MTA/ MTLA/ MTRA)",
    "Gesundheits- und Krankenpfleger/-in",
]

ASSISTANT_ADDITIONAL_QUALIFICATION_OPTIONS = [
    "VERAH",
    "NäPa/EVA",
    "Fachwirt*in für amb. med. Versorgung",
    "Physician Assistant",
]

DOCTOR_ZUSATZWEITERBILDUNGEN_CHOICES = [
    "Ärztliches Qualitätsmanagement",
    "Akupunktur",
    "Allergologie",
    "Andrologie",
    "Balneologie und Med. Klimatologie",
    "Betriebsmedizin",
    "Dermatohistologie",
    "Diabetologie",
    "Flugmedizin",
    "Geriatrie",
    "Gynäkologische Exfoliativ-Zytologie",
    "Hämostaseologie",
    "Handchirurgie",
    "Homöopathie",
    "Infektiologie",
    "Intensivmedizin",
    "Kinder-Endokrinologie und-Diabetologie",
    "Kinder-Gastroenterologie",
    "Kinder-Nephrologie",
    "Kinder-Orthopädie",
    "Kinder-Pneumologie",
    "Kinder-Rheumatologie",
    "Labordiagnostik - fachgebunden",
    "Magnetresonanztomographie - fachgebunden",
    "Kardio-MRT",
    "Manuelle Medizin/Chirotherapie",
    "Medikamentöse Tumortherapie",
    "Medizinhygiene",
    "Medizinische Informatik",
    "Naturheilverfahren",
    "Notfallmedizin",
    "Orthopädische Rheumatologie",
    "Palliativmedizin",
    "Phlebologie",
    "Physikalische Therapie und Balneologie",
    "Plastische Operationen",
    "Proktologie",
    "Psychoanalyse",
    "Psychotherapie - fachgebunden",
    "Rehabilitationswesen",
    "Röntgendiagnostik - fachgebunden",
    "Schlafmedizin",
    "Sozialmedizin",
    "Spezielle Orthopädische Chirurgie",
    "Spezielle Schmerztherapie",
    "Spezielle Unfallchirurgie",
    "Spezielle Viszeralchirurgie",
    "Sportmedizin",
    "Suchtmedizin",
    "Tropenmedizin",
]

DOCTOR_SPECIALTY_OPTIONS = [
    "Facharzt für Allgemeinchirurgie",
    "Facharzt für Allgemeinmedizin (Hausarzt)",
    "Facharzt für Anästhesiologie",
    "Facharzt für Anatomie",
    "Facharzt für Arbeitsmedizin",
    "Facharzt für Augenheilkunde",
    "Facharzt für Biochemie",
    "Facharzt für Chirurgie",
    "Facharzt für Frauenheilkunde und Geburtshilfe (Gynäkologie)",
    "Facharzt für Gefäßchirurgie",
    "Facharzt für Hals-Nasen-Ohrenheilkunde (HNO)",
    "Facharzt für Haut- und Geschlechtskrankheiten",
    "Facharzt für Herzchirurgie",
    "Facharzt für Humangenetik",
    "Facharzt für Hygiene und Umweltmedizin",
    "Facharzt für Innere Medizin",
    "Facharzt für Innere Medizin und Angiologie",
    "Facharzt für Innere Medizin und Endokrinologie und Diabetologie",
    "Facharzt für Innere Medizin und Gastroenterologie",
    "Facharzt für Innere Medizin und Hämatologie und Onkologie",
    "Facharzt für Innere Medizin und Kardiologie",
    "Facharzt für Innere Medizin und Nephrologie",
    "Facharzt für Innere Medizin und Pneumologie",
    "Facharzt für Innere Medizin und Rheumatologie",
    "Facharzt für Kinder- und Jugendchirurgie",
    "Facharzt für Kinder- und Jugendmedizin",
    "Facharzt für Kinder- und Jugendpsychiatrie und -psychotherapie",
    "Facharzt für Klinische Pharmakologie",
    "Facharzt für Laboratoriumsmedizin",
    "Facharzt für Mikrobiologie, Virologie und Infektionsepidemiologie",
    "Facharzt für Mund-Kiefer-Gesichtschirurgie",
    "Facharzt für Neurochirurgie",
    "Facharzt für Neurologie",
    "Facharzt für Nuklearmedizin",
    "Facharzt für Öffentliches Gesundheitswesen",
    "Facharzt für Orthopädie und Unfallchirurgie",
    "Facharzt für Pathologie",
    "Facharzt für Phoniatrie und Pädaudiologie",
    "Facharzt für Physikalische und Rehabilitative Medizin",
    "Facharzt für Physiologie",
    "Facharzt für Plastische, Rekonstruktive und Ästhetische Chirurgie",
    "Facharzt für Psychiatrie und Psychotherapie",
    "Facharzt für Psychosomatische Medizin und Psychotherapie",
    "Facharzt für Radiologie",
    "Facharzt für Rechtsmedizin",
    "Facharzt für Strahlentherapie",
    "Facharzt für Thoraxchirurgie",
    "Facharzt für Transfusionsmedizin",
    "Facharzt für Urologie",
    "Facharzt für Viszeralchirurgie",
]

RESEARCH_EXPERIENCE_OPTIONS = [
    _("University research"),
    _("Clinical research"),
    _("Insurance companies"),
    _("Pharmaceutical industry"),
]

DMP_OPTIONS = [
    "Asthma Bronchiale",
    "Brustkrebs",
    "DM Typ 1",
    "DM Typ 2",
    "KHK",
    "COPD",
    "Herzinsuffizienz",
    "Osteoporose",
    "Depression",
    "Chronische Rückenschmerzen",
]

RESEARCH_TRAINING_OPTIONS = [
    "Grundlagenkurs AMG",
    "Aufbaukurs AMG",
    "Auffrischungskurs AMG",
    "GCP-Grundlagenkurs",
    "Grundlagenkurs Medizinprodukterecht",
    "Aufbauergänzungskurs Medizinprodukterecht",
    "Qualifizierung zur Studienassistenz/Study Nurse",
    "Qualifizierung zur Studienkoordinatorin",
]


# Referenced from accreditation
class FoPraNetTrainingType(Enum):
    FOPRANET_BASIC_TRAINING = "FoPraNet Fortbildungsprogramm"
    FOPRANET_ADVANCED_TRAINING = "FoPraNet-PLUS Fortbildungsprogramm"


SUDY_SPECIFIC_TRAINING_OPTIONS = [
    "Interventionsstudie Intermittierendes Fasten",
    "Nicht interventionelle Studie Polymyalgia Rheumatica",
    "Nicht interventionelle Studie Herzinsuffizienz",
    "Nicht interventionelle Studie Depression",
]

FEDERAL_STATE_BADEN_WUERTTEMBERG = {
    "name": "Baden-Württemberg",
    "abbreviation": "BW",
    "counties": [
        "Alb-Donau-Kreis",
        "Baden-Baden",
        "Biberach",
        "Böblingen",
        "Bodenseekreis",
        "Breisgau-Hochschwarzwald",
        "Calw",
        "Emmendingen",
        "Esslingen",
        "Freiburg im Breisgau",
        "Freudenstadt",
        "Göppingen",
        "Heidelberg",
        "Heidenheim",
        "Heilbronn",
        "Hohenlohekreis",
        "Karlsruhe",
        "Konstanz",
        "Lörrach",
        "Ludwigsburg",
        "Main-Tauber-Kreis",
        "Mannheim",
        "Neckar-Odenwald-Kreis",
        "Ortenaukreis",
        "Ostalbkreis",
        "Pforzheim",
        "Rastatt",
        "Ravensburg",
        "Rems-Murr-Kreis",
        "Reutlingen",
        "Rhein-Neckar-Kreis",
        "Rottweil",
        "Schwäbisch Hall",
        "Schwarzwald-Baar-Kreis",
        "Sigmaringen",
        "Stuttgart",
        "Tübingen",
        "Tuttlingen",
        "Ulm",
        "Waldshut",
        "Zollernalbkreis",
    ],
}

FEDERAL_STATE_Schleswig_Holstein = {
    "name": "Schleswig-Holstein",
    "abbreviation": "SH",
    "counties": [
        "Dithmarschen",
        "Herzogtum Lauenburg",
        "Nordfriesland",
        "Ostholstein",
        "Pinneberg",
        "Plön",
        "Rendsburg-Eckernförde",
        "Schleswig-Flensburg",
        "Segeberg",
        "Steinburg",
        "Stormarn",
    ],
}

FEDERAL_STATE_Hamburg = {
    "name": "Hamburg",
    "abbreviation": "HH",
    "counties": ["Hamburg"],
}

FEDERAL_STATE_Niedersachsen = {
    "name": "Niedersachsen",
    "abbreviation": "NDS",
    "counties": [
        "Gifhorn",
        "Goslar",
        "Helmstedt",
        "Northeim",
        "Peine",
        "Wolfenbüttel",
        "Göttingen",
        "Region Hannover",
        "Diepholz",
        "Hameln-Pyrmont",
        "Hildesheim",
        "Holzminden",
        "Nienburg (Weser)",
        "Schaumburg",
        "Celle",
        "Cuxhaven",
        "Harburg",
        "Lüchow-Dannenberg",
        "Lüneburg",
        "Osterholz",
        "Rotenburg (Wümme)",
        "Heidekreis",
        "Stade",
        "Uelzen",
        "Verden",
        "Ammerland",
        "Aurich",
        "Cloppenburg",
        "Emsland",
        "Friesland",
        "Grafschaft Bentheim",
        "Leer",
        "Oldenburg",
        "Osnabrück",
        "Vechta",
        "Wesermarsch",
        "Wittmund",
    ],
}

FEDERAL_STATE_Bremen = {
    "name": "Bremen",
    "abbreviation": "Bremen",
    "counties": ["Bremen"],
}

FEDERAL_STATE_Nordrhein_Westfalen = {
    "name": "Nordrhein-Westfalen",
    "abbreviation": "NRW",
    "counties": [
        "Kleve",
        "Mettmann",
        "Rhein-Kreis Neuss",
        "Viersen",
        "Wesel",
        "Städteregion Aachen",
        "Düren",
        "Rhein-Erft-Kreis",
        "Euskirchen",
        "Heinsberg",
        "Oberbergischer Kreis",
        "Rheinisch-Bergischer Kreis",
        "Rhein-Sieg-Kreis",
        "Borken",
        "Coesfeld",
        "Recklinghausen",
        "Steinfurt",
        "Warendorf",
        "Gütersloh",
        "Herford",
        "Höxter",
        "Lippe",
        "Minden-Lübbecke",
        "Paderborn",
        "Ennepe-Ruhr-Kreis",
        "Hochsauerlandkreis",
        "Märkischer Kreis",
        "Olpe",
        "Siegen-Wittgenstein",
        "Soest",
        "Unna",
    ],
}

FEDERAL_STATE_Hessen = {
    "name": "Hessen",
    "abbreviation": "HE",
    "counties": [
        "Bergstraße",
        "Darmstadt-Dieburg",
        "Groß-Gerau",
        "Hochtaunuskreis",
        "Main-Kinzig-Kreis",
        "Main-Taunus-Kreis",
        "Odenwaldkreis",
        "Offenbach",
        "Rheingau-Taunus-Kreis",
        "Wetteraukreis",
        "Gießen",
        "Lahn-Dill-Kreis",
        "Limburg-Weilburg",
        "Marburg-Biedenkopf",
        "Vogelsbergkreis",
        "Fulda",
        "Hersfeld-Rotenburg",
        "Kassel",
        "Schwalm-Eder-Kreis",
        "Waldeck-Frankenberg",
        "Werra-Meißner-Kreis",
    ],
}

FEDERAL_STATE_Rheinland_Pfalz = {
    "name": "Rheinland-Pfalz",
    "abbreviation": "RP",
    "counties": [
        "Ahrweiler",
        "Altenkirchen (Westerwald)",
        "Bad Kreuznach",
        "Birkenfeld",
        "Cochem-Zell",
        "Mayen-Koblenz",
        "Neuwied",
        "Rhein-Hunsrück-Kreis",
        "Rhein-Lahn-Kreis",
        "Westerwaldkreis",
        "Bernkastel-Wittlich",
        "Eifelkreis Bitburg-Prüm",
        "Vulkaneifel",
        "Trier-Saarburg",
        "Alzey-Worms",
        "Bad Dürkheim",
        "Donnersbergkreis",
        "Germersheim",
        "Kaiserslautern",
        "Kusel",
        "Südliche Weinstraße",
        "Rhein-Pfalz-Kreis",
        "Mainz-Bingen",
        "Südwestpfalz",
    ],
}

FEDERAL_STATE_Bayern = {
    "name": "Bayern",
    "abbreviation": "BY",
    "counties": [
        "Altötting",
        "Berchtesgadener Land",
        "Bad Tölz-Wolfratshausen",
        "Dachau",
        "Ebersberg",
        "Eichstätt",
        "Erding",
        "Freising",
        "Fürstenfeldbruck",
        "Garmisch-Partenkirchen",
        "Landsberg am Lech",
        "Miesbach",
        "Mühldorf a.Inn",
        "München",
        "Neuburg-Schrobenhausen",
        "Pfaffenhofen a.d.Ilm",
        "Rosenheim",
        "Starnberg",
        "Traunstein",
        "Weilheim-Schongau",
        "Deggendorf",
        "Freyung-Grafenau",
        "Kelheim",
        "Landshut",
        "Passau",
        "Regen",
        "Rottal-Inn",
        "Straubing-Bogen",
        "Dingolfing-Landau",
        "Amberg-Sulzbach",
        "Cham",
        "Neumarkt i.d.OPf.",
        "Neustadt a.d.Waldnaab",
        "Regensburg",
        "Schwandorf",
        "Tirschenreuth",
        "Bamberg",
        "Bayreuth",
        "Coburg",
        "Forchheim",
        "Hof",
        "Kronach",
        "Kulmbach",
        "Lichtenfels",
        "Wunsiedel i.Fichtelgebirge",
        "Ansbach",
        "Erlangen-Höchstadt",
        "Fürth",
        "Nürnberger Land",
        "Neustadt a.d.Aisch-Bad Windsheim",
        "Roth",
        "Weißenburg-Gunzenhausen",
        "Aschaffenburg",
        "Bad Kissingen",
        "Rhön-Grabfeld",
        "Haßberge",
        "Kitzingen",
        "Miltenberg",
        "Main-Spessart",
        "Schweinfurt",
        "Würzburg",
        "Aichach-Friedberg",
        "Augsburg",
        "Dillingen a.d.Donau",
        "Günzburg",
        "Neu-Ulm",
        "Lindau (Bodensee)",
        "Ostallgäu",
        "Unterallgäu",
        "Donau-Ries",
        "Oberallgäu",
    ],
}

FEDERAL_STATE_Saarland = {
    "name": "Saarland",
    "abbreviation": "SL",
    "counties": [
        "Merzig-Wadern",
        "Neunkirchen",
        "Saarlouis",
        "Saarpfalz-Kreis",
        "St. Wendel",
    ],
}

FEDERAL_STATE_Berlin = {"name": "Berlin", "abbreviation": "BER", "counties": ["Berlin"]}

FEDERAL_STATE_Brandenburg = {
    "name": "Brandenburg",
    "abbreviation": "BB",
    "counties": [
        "Barnim",
        "Dahme-Spreewald",
        "Elbe-Elster",
        "Havelland",
        "Märkisch-Oderland",
        "Oberhavel",
        "Oberspreewald-Lausitz",
        "Oder-Spree",
        "Ostprignitz-Ruppin",
        "Potsdam-Mittelmark",
        "Prignitz",
        "Spree-Neiße",
        "Teltow-Fläming",
        "Uckermark",
    ],
}

FEDERAL_STATE_Mecklenburg_Vorpommern = {
    "name": "Mecklenburg-Vorpommern",
    "abbreviation": "MV",
    "counties": [
        "Mecklenburgische Seenplatte",
        "Landkreis Rostock",
        "Vorpommern-Rügen",
        "Nordwestmecklenburg",
        "Vorpommern-Greifswald",
        "Ludwigslust-Parchim",
    ],
}

FEDERAL_STATE_Sachsen = {
    "name": "Sachsen",
    "abbreviation": "SN",
    "counties": [
        "Erzgebirgskreis",
        "Mittelsachsen",
        "Vogtlandkreis",
        "Zwickau",
        "Bautzen",
        "Görlitz",
        "Meißen",
        "Sächsische Schweiz-Osterzgebirge",
        "Leipzig",
        "Nordsachsen",
    ],
}

FEDERAL_STATE_Sachsen_Anhalt = {
    "name": "Sachsen-Anhalt",
    "abbreviation": "LSA",
    "counties": [
        "Altmarkkreis Salzwedel",
        "Anhalt-Bitterfeld",
        "Börde",
        "Burgenlandkreis",
        "Harz",
        "Jerichower Land",
        "Mansfeld-Südharz",
        "Saalekreis",
        "Salzlandkreis",
        "Stendal",
        "Wittenberg",
    ],
}

FEDERAL_STATE_Thüringen = {
    "name": "Thüringen",
    "abbreviation": "TH",
    "counties": [
        "Eichsfeld",
        "Nordhausen",
        "Wartburgkreis",
        "Unstrut-Hainich-Kreis",
        "Kyffhäuserkreis",
        "Schmalkalden-Meiningen",
        "Gotha",
        "Sömmerda",
        "Hildburghausen",
        "Ilm-Kreis",
        "Weimarer Land",
        "Sonneberg",
        "Saalfeld-Rudolstadt",
        "Saale-Holzland-Kreis",
        "Saale-Orla-Kreis",
        "Greiz",
        "Altenburger Land",
    ],
}


class InstituteGroupNames:
    TUEBINGEN = "TUEBINGEN"
    FREIBURG = "FREIBURG"
    ULM = "ULM"
    HEIDELBERG = "HEIDELBERG"
    REUTLINGEN = "REUTLINGEN"


class PracticeType(Enum):
    EINZELPRAXIS_OHNE_ANGESTELLTE_AERZTE = _(
        "Einzelpraxis (ohne angestellte Ärzt*innen)"
    )
    EINZELPRAXIS_MIT_ANGESTELLTE_AERZTE = _(
        "Einzelpraxis (mit angestellten Ärzt*innen)"
    )
    PRAXISGEMEINSCHAFT = _("Praxisgemeinschaft")
    OERTLICHE_BAG = _("örtliche BAG")
    UEBEROERTLICHE_BAG = _("überörtliche BAG")
    MVZ = _("MVZ")
    OTHER = _("Andere (Freitext)")


PRACTICE_ENGAGEMENT_OPTIONS = [
    "Lehrpraxis",
    "KWBW-Praxis",
    "QP Praxen",
    "Sonstiges",
    # Old fields are kept for now:
    # "Akademische Lehrpraxis",
    # "Weiterbildungsermächtigung",
    # "Mitglied im Ärztenetz",
    # "Zertifiziertes QM",
]

WORKFLOW_DIC= {
        "Praxis anlegen": [
            "Stammdaten",
            "Strukturdaten",
            "IT-Infrastruktur",
            "Kommunikation",
        ],
        "Praxis akkreditieren": [
            "Akkreditierungsart für eine Praxis auswählen",
            "Eine Akkreditierungscheckliste erstellen",
            "Praxis akkreditieren",
        ],
        "Personen anlegen": [
            "Personendaten ausfüllen/ergänzen",
            "Rolle Arzt hinzufügen",
            "Rollenspezifische Felder ausfüllen",
            "Rolle Arzt entfernen",
            "Rolle MFA hinzufügen",
            "Rollenspezifische Felder ausfüllen",
        ],
        "Veranstaltung anlegen": [
            "Teilnehmer zu der Veranstaltung hinzufügen",
            "Teilnahmestatus ändern",
        ],
        "Email anlegen": [
            "Neue Vorlage anlegen",
            "Vorlage für E-Mail verwenden ",
            "Eine Veranstaltung mit der E-Mail verknüpfen",
            "Teilnehmer hinzufügen/bearbeiten",
        ],
        "Prozess anlegen": [
            "Prozessschritte hinzufügen",
            "Prozess einer Praxis zuordnen ",
            "Prozessschritte innerhalb der Praxis als erledigt markieren",
        ],
        "Import": [
            "Standard Importer auswählen",
            "Praxisimport für eine Beispieldatei konfigurieren",
            "Daten importieren",
        ]
    }

DATALIST_INPUT_HELP_TEXT = _(
    "Type in a custom value or show available choices by clicking into the field (clear current input first)."
)

MULTIPLE_ENTRIES_HELP_TEXT = _(
    "To add multiple entries, save the form and enter a value again."
)

RULES = {
    "hourly": {
        "name": _("Hourly"),
        "description": _("Recurent rule for hourly events"),
        "frequency": "HOURLY",
    },
    "daily": {
        "name": _("Daily"),
        "description": _("Recurent rule for daily events"),
        "frequency": "DAILY",
    },
    "weekly": {
        "name": _("Weekly"),
        "description": _("Recurent rule for weekly events"),
        "frequency": "WEEKLY",
    },
    "monthly": {
        "name": _("Monthly"),
        "description": _("Recurent rule for monthly events"),
        "frequency": "MONTHLY",
    },
    "yearly": {
        "name": _("Yearly"),
        "description": _("Recurent rule for yearly events"),
        "frequency": "YEARLY",
    },
}
