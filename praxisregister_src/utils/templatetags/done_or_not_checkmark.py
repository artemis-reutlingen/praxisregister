from django import template
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

register = template.Library()


@register.filter("doneornot", is_safe=True)
def doneornot(value):
    if bool(value):
        done_string = '<div style="display:none;">' + _('Done') + '</div>'
        result = f'{done_string}<i class="fa fa-check-circle text-success"></i>'
    else:
        done_string = '<div style="display:none;">' + _('Open') + '</div>'
        result = f'{done_string}<i class="fa fa-times-circle text-danger"></i>'
    return mark_safe(result)
