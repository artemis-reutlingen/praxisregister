from django import template
from django.utils.html import escape, urlize
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter("safe_urlize", is_safe = True)
def safe_urlize(text: str):
    if not isinstance(text, str):
        text = str(text)
    return mark_safe(urlize(escape(text)))
