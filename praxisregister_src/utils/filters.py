from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Submit
from django.forms import TextInput
from django.utils.translation import gettext_lazy as _
from django_property_filter import PropertyFilterSet, PropertyCharFilter


class AbstractPracticeFilter(PropertyFilterSet):
    """
    Abstract filterset for practice selection forms, used in exporter, events, processes apps.
    To be overridden in specific apps and setting class Meta with model and fields.
    """
    practice = PropertyCharFilter(
        lookup_expr='icontains',
        label="",
        field_name="practice__name",
        widget=TextInput(attrs={'placeholder': 'Practice'})
    )

    class Meta:
        model = None
        fields = ('practice',)


class PracticeFilterFormHelper(FormHelper):
    """ Form helper for simple practice name filtering, used in many apps. """
    form_method = 'GET'
    layout = Layout(
        Row(
            Column('practice'),
            Column(Submit('submit', value=_('Apply filter')))
        )
    )
