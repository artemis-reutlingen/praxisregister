from django.forms import TextInput
from typing import Iterable
from django.core.exceptions import FieldDoesNotExist
from django.template.loader import render_to_string

class ModelRenderer:
    fields: Iterable[str]
    enable_deleting = False
    template = "global/snippets/model_renderer.html"
    css_class = None

    def __init__(self, instance, fields: Iterable[str]):
        self.instance = instance
        self.fields = fields
        self.context = self.__generate_context()
        if self.enable_deleting:
            self.delete_url = self.__class__.get_delete_url(instance)

        if not self.css_class:
            self.css_class = self.__class__.__name__

    def __generate_context(self):
        context = {}
        for field_name in self.fields:
            try:
                display_name = self.instance._meta.get_field(field_name).verbose_name
            except FieldDoesNotExist:
                display_name = field_name

            context[display_name] = getattr(self.instance, field_name)

        return context
            
    def __str__(self):
        return render_to_string(self.template, {"details": self})

    def get_delete_url(instance):
        raise NotImplementedError()



class ListTextWidget(TextInput):
    """
    Widget that allows the user to select values from a dropdown, but also to type in a custom value.
    Taken and adapted from: https://stackoverflow.com/a/32791625
    """

    def __init__(self, data_list, name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._name = name
        self._list = data_list
        self.attrs.update({'list': f'list__{self._name}'})

    def render(self, name, value, attrs=None, renderer=None):
        text_html = super().render(name, value, attrs=attrs)
        data_list = f'<datalist id="list__{self._name}">'
        for item in self._list:
            data_list += f'<option value="{item}">'
        data_list += '</datalist>'

        return text_html + data_list
