from django.urls import reverse_lazy
from django.views.generic import DetailView, UpdateView, TemplateView
from guardian.mixins import PermissionRequiredMixin
from utils.fields import ModelRenderer
from practices.models import Practice
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin


class ManagePracticePermissionRequired(PermissionRequiredMixin):
    permission_required = "manage_practice"
    return_403 = True

class ManagePersonPermissionRequired(PermissionRequiredMixin):
    permission_required = "manage_person"
    return_403 = True

class AllowOtherInstitutesPermissionRequired(PermissionRequiredMixin):
    permission_required = "allow_other_institutes"
    return_403 = True


class AbstractPracticeDetailView(ManagePracticePermissionRequired, DetailView):
    """
    TODO: That's kinda bad practice to have multiple inheritance layers. Might get confusing.
    """
    model = Practice
    context_object_name = 'practice'


class PracticeUpdateView(ManagePracticePermissionRequired, UpdateView):
    model = Practice
    template_name = 'practices/base_data/forms/practice_base_data_generic.html'

class AdminStaffRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff

class ModelRendererMixin(DetailView):
    template_name = 'global/generic_detail_view.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["modelRenderer"] = ModelRenderer(instance=self.get_object(), fields=self.fields)
        return context