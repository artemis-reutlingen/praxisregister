from django.utils.deconstruct import deconstructible
from django.utils.translation import ngettext_lazy
from django.core.validators import BaseValidator

@deconstructible
class LengthValidator(BaseValidator):
    message = ngettext_lazy(
        "Ensure this value has %(limit_value)d character (it has %(show_value)d).",
        "Ensure this value has %(limit_value)d characters (it has %(show_value)d).",
        "limit_value"
    )
    code = "exact_length"

    def compare(self, value, limit):
        return value != limit
    
    def clean(self, value):
        return len(value)
    