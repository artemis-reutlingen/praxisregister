from django.contrib.auth.models import Group

from institutes.models import Institute
from practices.models import Practice
from users.models import User


def create_test_users_with_institute(n_users: int = 2, institute_name="SomeInstitute"):
    group = Group.objects.create(name=f"Group {institute_name}")
    institute = Institute.objects.create(
        organisation="Organization X",
        name=institute_name,
        group=group
    )

    users = []
    for i in range(n_users):
        user = User.objects.create(username=f"tester{i}_{institute_name}")
        user.set_password("password")
        user.first_name = "Thomas"
        user.last_name = "Testmann"
        user.assigned_institute = institute
        user.groups.add(Group.objects.get(name=group))
        user.medical_title = "Dr."
        user.save()
        users.append(user)

    return users, institute


def setup_test_basics(diff_id: str = ""):
    """
    Creates an institute, a group and a user ("tester"//"password")
    Creates 2 practices for which the user has management rights
    TODO: return a dictionary
    """
    group = Group.objects.create(name=f"Group {diff_id}")
    institute = Institute.objects.create(
        organisation=f"University {diff_id}",
        name=f"Department {diff_id}",
        group=group
    )

    user = User.objects.create(username=f"tester{diff_id}")
    user.set_password("password")
    user.first_name = "Thomas"
    user.last_name = "Testmann"
    user.assigned_institute = institute

    user.groups.add(Group.objects.get(name=group))
    user.medical_title = "Dr."
    user.save()

    practice_1 = Practice.objects.create(name="Practice_A")
    practice_2 = Practice.objects.create(name="Practice_B")
    practice_1.identification_number = "ABC-1"
    practice_2.identification_number = "ABC-2"
    practice_1.save()
    practice_2.save()

    practice_1.assign_perms(institute)
    practice_1.assigned_institute = institute
    practice_2.assign_perms(institute)
    practice_2.assigned_institute = institute

    return institute, group, user, [practice_1, practice_2]
