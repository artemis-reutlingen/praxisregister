from django.conf import settings


def last_software_update(request):
    """
    Adds setting value LAST_SOFTWARE_UPDATE to all request contexts to enable displaying this in every view.
    """
    return {
        'LAST_SOFTWARE_UPDATE': settings.LAST_SOFTWARE_UPDATE,
        'CURRENT_SOFTWARE_VERSION': settings.CURRENT_SOFTWARE_VERSION,
    }
