import argparse
import csv

from django.core.management.base import BaseCommand

from utils.mock import initialize_mockup_data


class Command(BaseCommand):
    help = 'Creates inititial mockup data. Best to follow this order: 1) flush 2) initialize_db 3) mockup'

    def add_arguments(self, parser):
        parser.add_argument(
            '--npractices',
            nargs="?",
            type=int,
            default=100,
            help="specify number of generated practices"
        )

        parser.add_argument(
            '--ninstitutes',
            nargs="?",
            type=int,
            default=4,
            help="specify number of generated institutes"
        )

        parser.add_argument(
            '--nusers',
            nargs="?",
            type=int,
            default=20,
            help="specify number of generated users"
        )

        parser.add_argument(
            '--usersfile',
            nargs="?",
            type=argparse.FileType('r'),
            help="optional username/password csv file"
        )

    def handle(self, *args, **options):
        self.stdout.write("Initializing mockup data ...")

        self.stdout.write("Institutes/groups, practices, users ...")

        users_dict = None
        if options['usersfile']:
            with options['usersfile'] as infile:
                reader = csv.reader(infile)
                users_dict = {rows[0]: rows[1] for rows in reader}

        initialize_mockup_data(
            number_of_institutes=options['ninstitutes'],
            number_of_practices=options['npractices'],
            number_of_test_users=options['nusers'],
            verbose=True
        )

        self.stdout.write("Done!")
