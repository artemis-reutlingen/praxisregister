from django.core.management.base import BaseCommand

from addresses.models import FederalState, County
from persons.models import AssistantVocationalTrainingOption, AssistantAdditionalQualificationOption, \
    DoctorAdditionalTrainingOption, ResearchExperienceOption, ResearchTrainingOption, FoPraNetTrainingOption
from persons.models.options import DoctorSpecialtyOption, StudySpecificTrainingOption
from practices.models import OperatingSystemOption, PDMSSoftwareOption, DiseaseManagementProgramOption, \
    PracticeEngagementOption
from events.models.options import EventTypeOption
from feedback.models import Workflow, WorkflowStep
from schedule.models.rules import Rule
from utils.constants import *

class Command(BaseCommand):
    help = 'Initializes models with standard values (e.g. options for OperatingSystem model).'

    def fill_db_with_constants(self, model, constant):
        self.stdout.write(f"... {model.__name__} options")
        for con in constant:
            model.objects.get_or_create(name=con)
        self.delete_outdated_constants(model, constant)

    def delete_outdated_constants(self, model, constant):
        self.stdout.write(f"... and cleaning {model.__name__} options")
        model.objects.exclude(name__in=constant).delete()

    def fill_db_with_federal_states_and_counties(self, federal_state_constant):
        federal_state, created = FederalState.objects.get_or_create(
            full_name=federal_state_constant['name'],
            abbreviation=federal_state_constant['abbreviation']
        )
        for county_full_name in federal_state_constant['counties']:
            County.objects.get_or_create(
                full_name=county_full_name
                )

    def handle(self, *args, **options):
        self.stdout.write("Initializing ...")

        #Event type options
        self.fill_db_with_constants(EventTypeOption, EVENT_TYPES)

        # IT Infrastructure options
        self.fill_db_with_constants(OperatingSystemOption, OPERATING_SYSTEM_OPTIONS)
        self.fill_db_with_constants(PDMSSoftwareOption, PDMS_OPTIONS)

        # ASSISTANT
        self.fill_db_with_constants(AssistantVocationalTrainingOption, ASSISTANT_VOCATIONAL_TRAINING_OPTIONS)
        self.fill_db_with_constants(AssistantAdditionalQualificationOption, ASSISTANT_ADDITIONAL_QUALIFICATION_OPTIONS)

        # DOCTOR
        self.fill_db_with_constants(DoctorAdditionalTrainingOption, DOCTOR_ZUSATZWEITERBILDUNGEN_CHOICES)
        self.fill_db_with_constants(DoctorSpecialtyOption, DOCTOR_SPECIALTY_OPTIONS)

        # Research Experience options
        self.fill_db_with_constants(ResearchExperienceOption, RESEARCH_EXPERIENCE_OPTIONS)

        # Disease Management Program options
        self.fill_db_with_constants(DiseaseManagementProgramOption, DMP_OPTIONS)

        # Research Training Options
        self.fill_db_with_constants(ResearchTrainingOption, RESEARCH_TRAINING_OPTIONS)

        # FoPraNet Training Options
        self.stdout.write("... FoPraNet training options")
        for option in FoPraNetTrainingType:
            FoPraNetTrainingOption.objects.get_or_create(name=option.value)

        # Study specific Training Options
        self.fill_db_with_constants(StudySpecificTrainingOption, SUDY_SPECIFIC_TRAINING_OPTIONS)

        # Federal states, counties  
        self.stdout.write("... federal state BADEN_WUERTTEMBERG with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_BADEN_WUERTTEMBERG)
        self.stdout.write("... federal state Schleswig-Holstein with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Schleswig_Holstein)
        self.stdout.write("... federal state Hamburg with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Hamburg)
        self.stdout.write("... federal state Niedersachsen with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Niedersachsen)
        self.stdout.write("... federal state Bremen with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Bremen)
        self.stdout.write("... federal state Nordrhein-Westfalen with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Nordrhein_Westfalen)
        self.stdout.write("... federal state Hessen with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Hessen)
        self.stdout.write("... federal state Rheinland-Pfalz with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Rheinland_Pfalz)
        self.stdout.write("... federal state Bayern with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Bayern)
        self.stdout.write("... federal state Saarland with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Saarland)
        self.stdout.write("... federal state Berlin with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Berlin)
        self.stdout.write("... federal state Brandenburg with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Brandenburg)
        self.stdout.write("... federal state Mecklenburg-Vorpommern with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Mecklenburg_Vorpommern)
        self.stdout.write("... federal state Sachsen with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Sachsen)
        self.stdout.write("... federal state Sachsen-Anhalt with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Sachsen_Anhalt)
        self.stdout.write("... federal state Thüringen with counties")
        self.fill_db_with_federal_states_and_counties(FEDERAL_STATE_Thüringen)
        
        # Practice Engagement Options
        self.fill_db_with_constants(PracticeEngagementOption, PRACTICE_ENGAGEMENT_OPTIONS)

        self.stdout.write("... Feedback workflow options")
        self.create_workflow_steps()

        self.stdout.write("... Schedule rules")
        for rule in RULES.values():
            Rule.objects.get_or_create(name=rule["name"], description=rule["description"], frequency=rule['frequency'])
            
        self.stdout.write("Done!")

    def create_workflow_steps(self):
        workflow_number = 1
        for workflow, steps in WORKFLOW_DIC.items():
            wf, created = Workflow.objects.get_or_create(topic=workflow)
            wf.number = workflow_number
            wf.save()
            workflow_number += 1
            for count, step in enumerate(steps):
                wf_step, created = WorkflowStep.objects.get_or_create(name=step, workflow=wf)
                wf_step.number = count + 1
                wf_step.save()
