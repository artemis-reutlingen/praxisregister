from django.urls import path, include
from rest_framework import routers

from practices.views.practice import Practices, PracticeViewSet
from persons.views import PersonViewSet
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from utils.ckeditor5 import upload_file

api_router = routers.DefaultRouter()
api_router.register(r'practices', PracticeViewSet, basename='practices')
api_router.register(r'persons', PersonViewSet, basename='persons')

# Admin stuff
admin.site.site_header = "Praxisregister Admin"
admin.site.site_title = "Praxisregister Admin Portal"

urlpatterns = [
    path('', Practices.as_view(), name='landing-page'),  # TODO: Maybe use a redirect to Practice list view here.
    path('taggit/', include('taggit_selectize.urls')),
    path('headquarters/', admin.site.urls),
    path('users/', include('users.urls')),
    path('institutes/', include('institutes.urls')),
    path('practices/', include('practices.urls')),
    path('accreditations/', include('accreditation.urls')),
    path('studies/', include('studies.urls')),
    path('events/', include('events.urls')),
    path('todos/', include('todos.urls')),
    path('contactnotes/', include('contactnotes.urls')),
    path('documents/', include('documents.urls')),
    path('processes/', include('processes.urls')),
    path('persons/', include('persons.urls')),
    path('exporter/', include('exporter.urls')),
    path('changelogs/', include('changelogs.urls')),
    path('mail/', include('mail.urls')),
    path('importer/', include('importer.urls')),
    path('practice_importer/', include('practice_importer.urls')),
    path('kbv_importer/', include('kbv_importer.urls')),
    path('feedback/', include('feedback.urls')),
    path('schedule/', include('schedule.urls')),
    path("ckeditor5/image_upload/", upload_file, name="ck_editor_5_upload_file"), #Using own implementation of image upload
    path('api/', include(api_router.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]