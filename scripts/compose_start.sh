#!/bin/bash
echo "Waiting few seconds for Postgres to come up, so that applying migrations does not fail."
sleep 2.5

echo "Collecting static files for Django ..."
python manage.py migrate

echo "Collecting static files for Django ..."
python manage.py collectstatic --no-input

echo "Compiling translations file (Django compilemessages)"
python manage.py compilemessages --ignore=venv* --ignore=static-files*

if [ "$DEBUG" = True ]
then
    echo "Starting Django development server..."
    python manage.py runserver 0.0.0.0:8080
else
    echo "Starting gunicorn..."
    gunicorn config.wsgi -b 0.0.0.0:8080
fi

